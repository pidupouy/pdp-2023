//Languages files
var fr = require('./src/assets/json-data/strings/fr.json');
var en = require('./src/assets/json-data/strings/en.json');

var express = require('express');
var cli = require('./cli')

let app = express()
const bodyParser = require('body-parser');
const fs = require('fs');

//Multer is used to load files from the client
const multer = require('multer');
const upload = multer({ dest: './' })

/**
 * Functions who listen on the 8080 port
 */

app.use(express.static('dist/'))
app.use(bodyParser.urlencoded({ extended: false }))

app.get('/accessibility-mode', (req, res) => {
    sendPage(res, "seq32bits", "en");
});

app.post('/accessibility-mode', (req, res) => {
    sendPage(res, req.body.kernel, req.body.language);
});

app.post('/accessibility-mode-execution', upload.single('uploaded_file'), (req, res) => {
    let y86 = Buffer.from(req.body.y86, 'utf8').toString('base64');
    let hcl = Buffer.from(req.body.hcl, 'utf8').toString('base64');
    let instructionSet = Buffer.from(req.body.instructionSet, 'utf8').toString('base64');
    let kernel = req.body.currentKernel;
    let currentStep = Number(req.body.currentStep);
    let result;

    if (req.body.assemble !== undefined) {
        console.log("Post : Assemble");
        result = cli.assemble({ ys: y86, hcl: hcl, kernelName: kernel, instructionSet: instructionSet });
    }
    if (req.body.reset !== undefined) {
        console.log("Post : Reset");
        result = cli.assemble({ ys: y86, hcl: hcl, kernelName: kernel, instructionSet: instructionSet });
        currentStep = 0;
    }
    if (req.body.undo !== undefined) {
        console.log("Post : Undo");
        if (currentStep > 0) {
            currentStep--;
        }
        result = cli.runSimulationStep({ ys: y86, hcl: hcl, kernelName: kernel, nbSteps: currentStep, instructionSet: instructionSet })
    }
    if (req.body.step !== undefined) {
        console.log("Post : Step");
        currentStep++;
        result = cli.runSimulationStep({ ys: y86, hcl: hcl, kernelName: kernel, nbSteps: currentStep, instructionSet: instructionSet })
    }
    if (req.body.continue !== undefined) {
        console.log("Post : Continue");
        result = cli.runSimulationContinue({ ys: y86, hcl: hcl, kernelName: kernel, instructionSet: instructionSet });
    }
    if (req.body.load !== undefined) {
        console.log("Post : Load");
        try {
            readDiskFile(__dirname + '/' + req.file.filename, function (data) {
                let filenameExtension = req.file.originalname.split('.')[1]
                if (filenameExtension === "ys") {
                    req.body.y86 = data.toString();
                    result = cli.assemble({ ys: y86, hcl: hcl, kernelName: kernel, instructionSet: instructionSet });
                } else if (filenameExtension === "hcl") {
                    req.body.hcl = data.toString()
                    result = cli.assemble({ ys: y86, hcl: hcl, kernelName: kernel, instructionSet: instructionSet });
                } else {
                    let fileData = JSON.parse(data.toString());
                    currentStep = 0;

                    if (fileData.hcl !== undefined) {
                        req.body.hcl = fileData.hcl;
                    }
                    if (fileData.instructionSet !== undefined) {
                        let instructionSetString = data.toString().split("\"instructionSet\" : ");
                        instructionSetString = instructionSetString[1].split(",\n\"hcl\"");

                        req.body.instructionSet = instructionSetString[0];
                    }
                    if (fileData.editor !== undefined) {
                        req.body.y86 = fileData.editor;
                    }

                    y86 = Buffer.from(req.body.y86, 'utf8').toString('base64');
                    hcl = Buffer.from(req.body.hcl, 'utf8').toString('base64');
                    instructionSet = Buffer.from(req.body.instructionSet, 'utf8').toString('base64');

                    if (fileData.machineState !== undefined) {
                        if (fileData.hcl === undefined || fileData.instructionSet === undefined || fileData.editor === undefined) {
                            result.errors.push("Unbale to load machineState because hcl, instructionSet or y86 code were missing")
                        }

                        currentStep = fileData.machineState.stepNum;
                        kernel = fileData.machineState.kernel;
                        result = cli.runSimulationStep({ ys: y86, hcl: hcl, kernelName: kernel, nbSteps: currentStep, instructionSet: instructionSet })
                    } else {
                        result = cli.assemble({ ys: y86, hcl: hcl, kernelName: kernel, instructionSet: instructionSet });
                    }
                }

                sendResults(result, res, currentStep, req);
            });
        } finally {
            //Remove the uploaded file from the server even if an error occurs
            fs.unlinkSync(__dirname + '/' + req.file.filename);
        }
        return
    }
    if (req.body.save !== undefined) {
        console.log("Post : Save");

        let saveItems = []
        if (req.body.editorCheckbox) saveItems.push("editor")
        if (req.body.machineStateCheckbox) saveItems.push("execution")
        if (req.body.instructionSetCheckbox) saveItems.push("instructionSet")
        if (req.body.hclCheckbox) saveItems.push("hcl")

        let data = cli.save({ saveItems: saveItems, ys: y86, hcl: hcl, kernelName: kernel, nbSteps: currentStep, instructionSet: instructionSet })[1];

        res.type('.json');
        res.send(data);
        return;
    }
    if (req.body.step === undefined && req.body.continue === undefined && req.body.assemble === undefined && req.body.reset === undefined && req.body.load === undefined && req.body.save === undefined) {
        console.log("Post : No button seems to pressed");
    }

    sendResults(result, res, currentStep, req);
})

app.get('/cli', (req, res) => {
    res.setHeader('content-type', 'text/javascript')
    try {
        let command = req.query.command
        let result;        

        switch (command) {
            case "assemble":
                result = cli.assemble(req.query);
                break;
            case "step":
                result = cli.runSimulationStep(req.query)
                break;
            case "continue":
                result = cli.runSimulationContinue(req.query);
                break;
            default:
                throw new Error("Unknown command in request : " + command + ". Should have been \"assemble\", \"step\" or \"continue\"");
        }

        let data = JSON.stringify(result.dump, (key, value) =>
            typeof value === 'bigint'
                ? value.toString()
                : value // return everything else unchanged
        );

        if (result.errors.length == 0) {
            res.status(200)
            res.type('.json')
            res.send(data)
        } else {
            res.status(202)
            result.errors.forEach(error => {
                res.write(error.toString())
                res.write('\n')
            });
            res.end()
        }
    } catch (e) {
        console.error("An unexpected error happened while running simulation in CLI mode : ")
        console.error(e)
        res.status(500)
        res.send(e.message)
    }
})

app.get('/public/css/fontawesome/webfonts/Inconsolata-Regular.ttf', (req, res) => {
    res.send(fs.readFileSync('public/css/fontawesome/webfonts/Inconsolata-Regular.ttf'));
});

app.use(function (req, res) {
    res.status(404);
    res.send('404: File Not Found');
});

app.listen(8080)

/**
 * Other functions
 */

function readDiskFile(filePath, cb) {
    fs.readFile(filePath, 'utf8', function (err, data) {
        if (err) throw err;
        cb(data);
    });
}

function getLanguageFile(language) {
    switch (language) {
        case "fr":
            return fr;
        case "en":
            return en;
        default:
            return null;
    }
}

function sendPage(res, kernel, language) {
    let languageFile = getLanguageFile(language);
    let type = kernel === "seq32bits" || kernel === "seq64bits" ? "seq" : "pipe"
    let size = kernel === "seq32bits" || kernel === "pipe32bits" ? "hcl32bits" : "hcl64bits"
    fs.readFile(__dirname + '/accessibility.html', (err, data) => {
        fs.readFile(__dirname + '/ts/model/y86.txt', (err, defaultY86) => {
            fs.readFile(__dirname + '/ts/model/' + type + '/' + size + '.txt', (err, defaultHCl) => {
                fs.readFile(__dirname + '/ts/model/instructionSet.json', (err, defaultInstructionSet) => {
                    res.send(htmlReplace(data.toString(), languageFile)
                        .replace('{{ cpuState }}', languageFile.Accessibility.notCompiled)
                        .replace('{{ objectCode }}', languageFile.Accessibility.notCompiled)
                        .replace('{{ y86 }}', defaultY86.toString())
                        .replace('{{ hcl }}', defaultHCl.toString())
                        .replace('{{ instructionSet }}', defaultInstructionSet.toString())
                        .replace('{{ currentStep }}', "0")
                        .replace('{{ currentKernel }}', kernel)
                        .replace('{{ currentLanguage }}', language)
                        .replace('{{ currentKernel }}', kernel)
                        .replace('{{ currentLanguage }}', language))
                });
            });
        });
    });
}

function sendResults(result, res, currentStep, req) {
    let languageFile = getLanguageFile(req.body.currentLanguage);
    if (result.errors.length === 0) {
        res.status(200)
        fs.readFile(__dirname + '/accessibility.html', (err, data) => {
            let registersHTML = "<h4>" + languageFile.Registers.title + "</h4>" +
                "<table><thead><tr>" +
                "<th>" + languageFile.Accessibility.name + "</th>" +
                "<th>" + languageFile.Accessibility.hexaValue + "</th>" +
                "<th>" + languageFile.Accessibility.decimalValue + "</th>" +
                "<th>" + languageFile.Accessibility.nextHexaValue + "</th>" +
                "<th>" + languageFile.Accessibility.nextDecimalValue + "</th>" +
                "</tr></thead><tbody>";

            let flagsHTML = "<h4>" + languageFile.Flags.title + "</h4>" +
                "<table><thead><tr>" +
                "<th>" + languageFile.Accessibility.name + "</th>" +
                "<th>" + languageFile.Accessibility.value + "</th>" +
                "<th>" + languageFile.Accessibility.nextValue + "</th>" +
                "</tr></thead><tbody>";

            let statusHTML = "<h4>" + languageFile.Status.title + "</h4>" +
                "<table><thead><tr>" +
                "</tr></thead><tbody>";

            let memoryHTML = "<h4>" + languageFile.Memory.title + "</h4>" +
                "<table><thead><tr>" +
                "<th>" + languageFile.Accessibility.address + "</th>" +
                "<th>" + languageFile.Accessibility.value + "</th>" +
                "<th>" + languageFile.Accessibility.nextValue + "</th>" +
                "</tr></thead><tbody>";

            let performancesHTML = "<h4>" + languageFile.Performance.title + "</h4>" +
                "<table><thead><tr>" +
                "</tr></thead><tbody>";

            let objectCodeHTML = "<div class='lines'>";

            let cpuStateHTML = '<div>';
            result.dump.CpuState.forEach((value, key) => {
                cpuStateHTML += '<h4>' + value.name + '</h4>';
                cpuStateHTML += "<table><thead><tr>";

                value.content["columns"].forEach((value, key) => {
                    cpuStateHTML += "<th scope=\"col\">" + value + "</th>";
                });

                cpuStateHTML += "</tr></thead><tbody>";

                value.content["lines"].forEach((value, key) => {
                    cpuStateHTML += "<tr>";
                    if (value.name !== "") {
                        cpuStateHTML += "<th scope=\"row\">" + value.name + "</th>";
                    }

                    value.labels.forEach((label_value, key) => {
                        if (value.labels[key] !== "") {
                            cpuStateHTML += "<th>" + value.labels[key] + "</th>";
                        } else {
                            cpuStateHTML += "<th>" + value.values[key] + "</th>";
                        }
                    });

                    if (req.body.currentKernel === "seq32bits" || req.body.currentKernel === "seq64bits"){
                        cpuStateHTML += "</tr>";
                        cpuStateHTML += "<tr>";

                        value.values.forEach((value, key) => {
                            cpuStateHTML += "<th>" + value + "</th>";
                        })
                    }

                    cpuStateHTML += "</tr>";
                });

                cpuStateHTML += "</tbody></table>";
            });

            result.compile_code.forEach(object => {
                objectCodeHTML += "<div class='object-code-line'><div class='" + object.class + "'><div>" + object.line + "</div></div></div>";
            });

            result.dump.CurrentRegisters.forEach((value, key) => {
                registersHTML += "<tr>" +
                    "<td>" + result.dump.CurrentRegisters[key].name + "</td>" +
                    "<td>" + result.dump.CurrentRegisters[key].value_hex + "</td>" +
                    "<td>" + result.dump.CurrentRegisters[key].value_dec + "</td>" +
                    "<td>" + result.dump.NextRegisters[key].value_hex + "</td>" +
                    "<td>" + result.dump.NextRegisters[key].value_dec + "</td>" +
                    "</tr>"
            });

            result.dump.CurrentFlags.forEach((value, key) => {
                flagsHTML += "<tr>" +
                    "<td>" + result.dump.CurrentFlags[key].name + "</td>" +
                    "<td>" + result.dump.CurrentFlags[key].value + "</td>" +
                    "<td>" + result.dump.NextFlags[key].value + "</td>" +
                    "</tr>";
            });

            result.dump.Status.forEach((value, key) => {
                statusHTML += "<tr>" +
                    "<td>" + result.dump.Status[key].name + "</td>" +
                    "<td>" + result.dump.Status[key].value + "</td>" +
                    "</tr>";
            });

            if(result.dump.Performances !== undefined){
                result.dump.Performances.forEach((value, key) => {
                    performancesHTML += "<tr>" +
                        "<td>" + result.dump.Performances[key].name + "</td>" +
                        "<td>" + result.dump.Performances[key].value + "</td>" +
                        "</tr>";
                });
            }

            result.dump.CurrentMemory.forEach((value, key) => {
                memoryHTML += "<tr>" +
                    "<td>" + value.address + "</td>" +
                    "<td>" + value.value + "</td>";
                    
                if(result.dump.NextMemory.address === value.address && result.dump.NextMemory.isWrite){
                    memoryHTML += "<td>" + result.dump.NextMemory.value + "</td>";
                } else {
                    memoryHTML += "<td>" + value.value + "</td>";
                }
                memoryHTML += "</tr>";
            });

            registersHTML += "</tbody></table>";
            flagsHTML += "</tbody></table>";
            statusHTML += "</tbody></table>";
            performancesHTML += "</tbody></table>";
            cpuStateHTML += "</div>";
            objectCodeHTML += "</div>";

            if (result.dump.Performances === undefined) {
                performancesHTML = "";
            }

            let resultHTML =
                '<p id="cpuState">' + cpuStateHTML + '</p>' +
                '<p id="registers">' + registersHTML + '</p>' +
                '<p id="flags">' + flagsHTML + '</p>' +
                '<p id="status">' + statusHTML + '</p>' +
                '<p id="performances">' + performancesHTML + '</p>' +
                '<p id="memory">' + memoryHTML + '</p>';

            res.send(htmlReplace(data.toString(), languageFile)
                .replace('{{ cpuState }}', resultHTML)
                .replace('{{ objectCode }}', objectCodeHTML)
                .replace('{{ y86 }}', req.body.y86)
                .replace('{{ hcl }}', req.body.hcl)
                .replace('{{ instructionSet }}', req.body.instructionSet)
                .replace('{{ currentStep }}', String(currentStep))
                .replace('{{ currentKernel }}', req.body.currentKernel)
                .replace('{{ currentLanguage }}', req.body.currentLanguage)
                .replace('{{ currentKernel }}', req.body.currentKernel)
                .replace('{{ currentLanguage }}', req.body.currentLanguage))
        });
    } else {
        res.status(202)
        result.errors.forEach(error => {
            res.write(error.toString())
            res.write('\n')
        });
        res.end()
    }
}

function htmlReplace(stringValue, languageFile) {
    return stringValue
        .replace('{{ assemble }}', languageFile.Header.HeaderButton.assemble)
        .replace('{{ reset }}', languageFile.Header.HeaderButton.reset)
        .replace('{{ step }}', languageFile.Header.HeaderButton.step)
        .replace('{{ undo }}', languageFile.Header.HeaderButton.undo)
        .replace('{{ continue }}', languageFile.Header.HeaderButton.continue)
        .replace('{{ load }}', languageFile.Header.HeaderButton.load)
        .replace('{{ save }}', languageFile.Header.HeaderButton.save)
        .replace('{{ sourceCodeTitle }}', languageFile.Editor.title)
        .replace('{{ hclTitle }}', languageFile.Tabs.hcl)
        .replace('{{ instructionSetTitle }}', languageFile.Tabs.instructionSet)
        .replace('{{ objectCodeTitle }}', languageFile.ObjectCode.title)
        .replace('{{ cpuStateTitle }}', languageFile.CpuState.title)
        .replace('{{ kernelLanguageSelection }}', languageFile.Accessibility.kernelLanguageSelection)
        .replace('{{ kernelLanguageSelectionButton }}', languageFile.Accessibility.kernelLanguageSelectionButton)
        .replace('{{ kernelLanguageCurrentSelection }}', languageFile.Accessibility.kernelLanguageCurrentSelection)
        .replace('{{ editorCheckbox }}', languageFile.SaveDialog.editor)
        .replace('{{ machineStateCheckbox }}', languageFile.SaveDialog.machineState)
        .replace('{{ instructionSetCheckbox }}', languageFile.SaveDialog.instructionSet)
        .replace('{{ hclCheckbox }}', languageFile.SaveDialog.hcl);
}