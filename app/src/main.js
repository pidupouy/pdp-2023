import { createApp } from 'vue'


import App from './App.vue'
// Allows calling app's methods in a convenient way: app.getY86Code()
window.app = createApp(App);

window.app.config.productionTip = false

window.app.mount('#app');