var KernelController = require('./src/controllers/kernelController')
var HclController = require('./src/controllers/hclController')
var YasController = require('./src/controllers/yasController')
var SimulatorController = require('./src/controllers/simulatorController')
var en = require('./src/assets/json-data/strings/en.json');

let fs = require('fs')
const { query } = require('express')

const DEFAULT_FILES_PATH = './ts/model/'
const DEFAULT_YAS_NAME = "y86.txt"
const DEFAULT_HCL_NAME = 'hcl.txt'
const DEFAULT_INSTRUCTION_SET_NAME = 'instructionSet.json'

///
/// Example of query :
/// Starts at label 'Init' and set a breakpoint at the line 5
/// http://localhost:8080/cli?start=Init&breakpoints=5&ys=Ci5wb3MgMApJbml0OgogICAgaXJtb3ZsIFN0YWNrLCAlZWJwCiAgICBpcm1vdmwgU3RhY2ssICVlc3AKICAgIAoucG9zIDB4MTAwClN0YWNrOgogICAg
///

exports.assemble = (query) => {
    let result = {
        errors: [],
        dump: "",
    }

    let args = extractArgs(query, result.errors)
    let compilationResult = simulatorCompile(args.kernelName, args.ys, args.instructionSet, args.hcl)
    let simulatorController = compilationResult.simulatorController;
    let yasCompilationResult = compilationResult.yasCompilationResult;

    if (compilationResult.errors.length !== 0) {
        return result
    }

    result.dump = simulatorController.getDump()
    result.compile_code = computeHighlightAndTags(yasCompilationResult.output.split('\n'), simulatorController.getCurrentKernelHighlightRules())
    return result
}

exports.save = (query) => {
    let result = {
        errors: [],
        dump: "",
    }

    let args = extractArgs(query, result.errors)
    let compilationResult = simulatorCompile(args.kernelName, args.ys, args.instructionSet, args.hcl)
    let simulatorController = compilationResult.simulatorController;
    let yasCompilationResult = compilationResult.yasCompilationResult;

    if (compilationResult.errors.length !== 0) {
        return result
    }

    for (let i = 0; i < args.nbSteps; i++) {
        simulatorController.step();
    }

    return simulatorController.save(args.saveItems, args.ys, args.hcl, args.instructionSet, yasCompilationResult);
}

exports.runSimulationContinue = (query) => {
    let result = {
        errors: [],
        dump: "",
    }

    let args = extractArgs(query, result.errors)
    let compilationResult = simulatorCompile(args.kernelName, args.ys, args.instructionSet, args.hcl)
    let simulatorController = compilationResult.simulatorController;
    let yasCompilationResult = compilationResult.yasCompilationResult;

    if (compilationResult.errors.length !== 0) {
        return result
    }

    simulatorController.continue();

    result.dump = simulatorController.getDump()
    result.compile_code = computeHighlightAndTags(yasCompilationResult.output.split('\n'), simulatorController.getCurrentKernelHighlightRules())
    return result
}

exports.runSimulationStep = (query) => {
    let result = {
        errors: [],
        dump: "",
    }

    let args = extractArgs(query, result.errors)
    let compilationResult = simulatorCompile(args.kernelName, args.ys, args.instructionSet, args.hcl)
    let simulatorController = compilationResult.simulatorController;
    let yasCompilationResult = compilationResult.yasCompilationResult;

    if (compilationResult.errors.length !== 0) {
        return result
    }

    for (let i = 0; i < args.nbSteps; i++) {
        simulatorController.step();
    }

    result.dump = simulatorController.getDump()
    result.compile_code = computeHighlightAndTags(yasCompilationResult.output.split('\n'), simulatorController.getCurrentKernelHighlightRules())
    return result
}

/**
 * Parses the query arguments used as parameter to run a simulation.
 * The default used kernel is "seq32bits".
 * If no HCL or instruction set is provided, the current kernel defaults will be used.
 */
function extractArgs(query, errors) {
    let args = {
        kernelName: "seq32bits",
        ys: undefined,
        instructionSet: undefined,
        hcl: undefined,
        nbSteps: 0,
        saveItems: []
    }

    if (query.kernelName !== undefined) {
        args.kernelName = query.kernelName
    }

    if (query.ys !== undefined) {
        args.ys = base64ToString(query.ys)
    } else {
        args.ys = loadDefaultFile(args.kernelName, DEFAULT_YAS_NAME)
    }

    if (query.hcl !== undefined) {
        args.hcl = base64ToString(query.hcl)
    } else {
        args.hcl = loadDefaultFile(args.kernelName, DEFAULT_HCL_NAME);
    }

    if (query.instructionSet !== undefined) {
        try {
            args.instructionSet = JSON.parse(base64ToString(query.instructionSet))
            checkFormat(args.instructionSet, errors)
        } catch (error) {
            errors.push(error.message)
        }
    } else {
        try {
            args.instructionSet = JSON.parse(loadDefaultFile(args.kernelName, DEFAULT_INSTRUCTION_SET_NAME))
            checkFormat(args.instructionSet, errors)
        } catch (error) {
            errors.push(error.message)
        }
    }

    if (query.nbSteps !== undefined) {
        args.nbSteps = query.nbSteps;
    }

    if (query.saveItems !== undefined) {
        args.saveItems = query.saveItems
    }
    return args
}

function simulatorCompile(kernelName, ys, instructionSet, hcl) {
    let kernelController = new KernelController.KernelController()
    let hclController = new HclController.HclController(kernelController)
    let yasController = new YasController.YasController(kernelController)
    let simulatorController = new SimulatorController.SimulatorController(kernelController)
    let errors = [];

    //Kernel selection
    kernelController.useKernel(kernelName, instructionSet, hcl)

    //HCL compilation
    const hclCompilationResult = hclController.assemble(hcl)
    errors = errors.concat(compilationErrorsToCLIErrors('hcl', hclCompilationResult.errors))

    //Yas compilation
    ys += '\n'; //Dernière ligne vide car la grammaire
    const yasCompilationResult = yasController.assemble(ys)
    errors = errors.concat(compilationErrorsToCLIErrors('yas', yasCompilationResult.errors))

    return {
        errors: errors,
        simulatorController: simulatorController,
        yasCompilationResult: yasCompilationResult
    }
}

function compilationErrorsToCLIErrors(header, compilationErrors) {
    return compilationErrors.map(compilationError => {
        return header + ' | Line ' + compilationError.line + ' : ' + compilationError.message
    })
}

function base64ToString(input) {
    return Buffer.from(input, 'base64').toString('ascii')
}

function checkFormat(instructionSet, errors) {
    let details = en.InstructionSet.messages.detail;
    instructionSet.forEach((element, index) => {
        if (Object.keys(element).length != 4){
            throw new Error(this.scopedStrings.messages.detail.loadJsonNotARawInstructionObject + "WOP" + "</b>.");
        }
        if (element.name === undefined || typeof (element.name) !== "string") {
            throw new Error(`${details.instructionIndex} <b>${index}</b> ${details.invalidName}`);
        }
        if (element.icode === undefined || element.ifun === undefined || typeof (element.icode) !== "number" || typeof (element.ifun) !== "number") {
            throw new Error(`${element.name} : ${details.field} <b>icode</b> ${details.badFieldType} <b>${details.typeNumber}</b>.`);
        }
        if (element.args === undefined || typeof (element.args) !== "string") {
            throw new Error(`${element.name} : ${details.field} <b>args</b> ${details.badFieldType} <b>${details.typeString}</b>.`);
        }
    });
}

function loadDefaultFile(kernelName, fileName) {
    let path = "";
    let oldFileName = fileName
    fileName = oldFileName !== DEFAULT_HCL_NAME ? oldFileName : kernelName === "seq32bits" || kernelName === "pipe32bits" ? "hcl32bits.txt" : "hcl64bits.txt"
    kernelName = oldFileName !== DEFAULT_HCL_NAME ? "" : kernelName === "seq32bits" || kernelName === "seq64bits" ? "seq" : "pipe"

    switch(kernelName){
        case "":
            path = DEFAULT_FILES_PATH + fileName;
            break;
        case "seq" :
        case "pipe":
            path = DEFAULT_FILES_PATH + kernelName + '/' + fileName;
            break;
        default :
            throw new Error('Failed to read default file "' + fileName + '". Reason : Invalid path "' + DEFAULT_FILES_PATH + kernelName + '"')
    }
    
    try {
        return fs.readFileSync(path, 'utf8')
    } catch (err) {
        throw new Error('Failed to read default file "' + fileName + '". Reason : ' + err.message)
    }
}

function applyTag(line, tag) {
    if (tag.length > this.gutterSize)
        tag = "*".repeat(this.gutterSize);

    var taggedLine = line.split(/(\|)/);
    var regexp = new RegExp(" {" + tag.length + "}");  // expression used to replace as many spaces as the tag length
    taggedLine[2] = taggedLine[2].replace(regexp, tag);
    return taggedLine.join("");
}

function computeHighlightAndTags(code, highlightRules) {
    var computedCodeLines = [];
    const highlightClasses = [
        "highlightedV1", "highlightedV2", "highlightedV3", "highlightedV4",
        "highlightedV5", "highlightedV6", "highlightedV7",
        "highlightedV8", "highlightedV9", "highlightedV10"
    ]
    for (var lineIndex = 0; lineIndex < code.length; lineIndex++) {
        var line = code[lineIndex];
        // extract the instruction's address from the line
        var addr = line.substring(2, 8).trim();
        // extract the instruction's binary code
        var binary = line.substring(9, 22).trim();

        var highlightClass = null;
        if (addr && binary.trim().length) {
            var addr_dec = parseInt(addr, 16);
            var tagToAdd = "";

            // ensure that we're not using more rules than available highlight classes
            for (var index = 0; index < highlightRules.length && index < highlightClasses.length; index++) {
                var rule = highlightRules[index];
                if (addr_dec === Number(rule.address)) {
                    tagToAdd += rule.tag;
                    if (highlightClass === null) {
                        highlightClass = highlightClasses[index];
                    }
                }
            }

            if (tagToAdd !== "") {
                line = applyTag(line, tagToAdd);
            }
        }
        computedCodeLines.push({ "line": line, "class": highlightClass });
    }
    return computedCodeLines;
}