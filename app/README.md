# The U-Bordeaux y86 simulator

This projects aims at improving over the earlier implementations of the y86 web simulator used at *Université de Bordeaux*. The main features ov this 'version 2' JavaScript implementation recast are:

* Display of relevant internal features of the processor architecture(s): CPU stages, registers, memory, etc.;
* Easy, interactive edition of y86 source code, HCL code and the instruction set;
* Step-by-step execution of y86 code;
* Availability of various server execution modes (e.g. CLI mode, through GET requests);
* Dynamic selection among several processor architectures (at the time being, only the seqequential 32-bit kernel is implemented).

## Preparation

To create the hcl2js and yas parsers used within the simulator, you will need the [TypeScript](https://www.typescriptlang.org/index.html) and [Jison](https://zaa.ch/jison/docs/) packages. However, if you do not edit the hcl2js module or the yas grammar, the parser will not have to be generated again, and **jison** will not be necessary.

As a system administrator, run:

```bash
npm install typescript -g
npm install jison -g
```

Then, move to the **app/** subdirectory.

To install the remaining dependencies, use:

```bash
npm install
```

### Build parsers

The parsers are described in **grammars/**. To build the hcl2js and yas parsers, use:

```bash
jison grammars/hcl2js.jison -o ts/model/hcl2js/hcl2jsParser.js
jison grammars/yas.jison -o ts/model/yas/yasParser.js
```

## Tarball

To prepare a tarball for deployment on a third-party server, use:

```bash
npm run build
tar cvzf /tmp/y86js_v2.tgz dist
```

## Tests

To run unit tests, launch :

```bash
npm test
```

## Run the application

There are two modes: development and release.

### Dev mode

The CLI mode does not work when running dev server. To launch it, use:

```bash
npm run serve
```

### Release mode

To launch the application in CLI mode on the server, use:

```bash
npm run release
```

The server is launched with an extended request header max size, in order to handle large GET requests.

## Run with docker

```bash
docker build . -t webSimY86         # Build the image
docker run -d -p 80:8080 webSimY86  # Start the container and listen on port 80
```

## Contributing

If you want to contribute to the project, you will find some relevant ressources in **doc/**. Many are student reports in French, but they provide important architectural design sketches.

## Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
