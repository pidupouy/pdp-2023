import { ICompiler, CompilationResult, CompilationError } from "../interfaces/ICompiler";
import { yasLexer } from "./yasLexer"
import { yasParser } from "./yasParser"
import { yasVisitor } from "./yasVisitor"

import { IInstructionSet } from "../interfaces/IInstructionSet";
import { Document } from './nodes/document';
import { ProgramData } from './programData';
import { padStringBigInt } from '../numberUtils';

export class Yas implements ICompiler {
    registersEnum : any;
    instructionSet : IInstructionSet;
    wordSize : number;
    
    constructor(registersEnum : any, instructionSet : IInstructionSet, wordSize : number) {
        this.registersEnum = registersEnum;
        this.instructionSet = instructionSet;
        this.wordSize = wordSize;
    }
    
    /**
    * Compiles the given source.
    * It also set few data (see ProgramData)
    * in the CompilationResult data field.
    * @param src
    */
    assemble(src: string) : CompilationResult {
        let result = new CompilationResult();

        //Add the needed last empty line if not present
        if(src.charAt(src.length-1) !== `\n`){
            src += "\n";
        }
        
        try {
            const lexerResult = yasLexer(src)
            if (lexerResult.errors.length > 0){
                result.errors = lexerResult.errors
                return result
            }
            
            const parserResult = yasParser(lexerResult.output)
            if (parserResult.errors.length > 0){
                result.errors = parserResult.errors
                return result
            }
            
            const visitorResult = yasVisitor(parserResult.output)
            if (visitorResult.errors.length > 0){
                result.errors = visitorResult.errors
                return result
            }
            
            let document = visitorResult.data;
            result = this._compile(document);
            result.data = new ProgramData(document);
        } catch (error) {
            if(error instanceof CompilationError) {
                result.errors.push(error);
            } else {
                throw new Error("An unknown error happened when parsing in yas : " + error);
            }
        }
        
        return result;
    }
    
    private _compile(document : Document) : CompilationResult {
        let result = new CompilationResult();
        
        let ctx = {
            vaddr : BigInt(0),
            labels: new Map(),
            registersEnum: this.registersEnum,
            instructionSet: this.instructionSet,
            wordSize: this.wordSize,
            errors: result.errors,
        }
        
        document.evaluate(ctx);
        document.postEvaluate(ctx);
        
        result.output = document.render();
        
        return result;
    }
    
    updateInstructionSet(instructionSet : IInstructionSet){
        this.instructionSet = instructionSet;
    }
}

const ADDRESS_PADDING_SIZE = 2;
const MIDDLE_PADDING_SIZE_32BITS = 25;
const MIDDLE_PADDING_SIZE_64BITS = 32;
const GUTTER_SIZE = 2;
const YS_PADDING_SIZE = 2;

export function createObjectLine(address : bigint, bytes : number[], ys : string) : string {
    let output = '';
    
    for(let i = 0; i < ADDRESS_PADDING_SIZE; i++) {
        output += ' ';
    }
    
    output += '0x' + padStringBigInt(address.toString(16), 4) + ': ';
    
    bytes.forEach((byte) => {
        output += padStringBigInt(byte.toString(16), 2)
    });

    while(output.length < MIDDLE_PADDING_SIZE_64BITS) {
        output += ' ';
    }
    
    output += '|';
    
    for(let i = 0; i < GUTTER_SIZE; i++) {
        output += ' ';
    }
    
    output += '|';
    
    for(let i = 0; i < YS_PADDING_SIZE; i++) {
        output += ' ';
    }
    
    output += ys;
    
    return output;
}

export function createEmptyObjectLine(ys = '') : string {
    let output = '';

    for(let i = 0; i < MIDDLE_PADDING_SIZE_64BITS; i++) {
        output += ' ';
    }
    
    output += '|';
    
    for(let i = 0; i < GUTTER_SIZE; i++) {
        output += ' ';
    }
    
    output += '|';
    
    for(let i = 0; i < YS_PADDING_SIZE; i++) {
        output += ' ';
    }
    
    output += ys;
    
    return output;
}
