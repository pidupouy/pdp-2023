import { CompilationError } from '../../interfaces/ICompiler'
import { Instruction } from '../../instruction'
import { YasNode } from './yasNode'
import { Arguments } from './arguments'

export class InstructionLine extends YasNode {
    name : string;
    args : Arguments;

    constructor(name : string, args : Arguments, line : number) {
        super(line);
        this.name = name;
        this.args = args;
    }

    evaluate(ctx : any) : void {
        try {
            ctx.instructionSet.getInstructions(this.name)[this.args.instrNum] as Instruction;
        } catch {
            throw new CompilationError(this.line, "The instruction '" + this.name + "' does not exist");
        }
        
        this.args.evaluate(ctx);

        const instruction = ctx.instructionSet.getInstructions(this.name)[this.args.instrNum] as Instruction;
        this.statementAsText = this._getRepresentation();
        this.vaddr = ctx.vaddr;
        ctx.vaddr += BigInt(instruction.length);
    }

    postEvaluate(ctx : any) {
        const instruction = ctx.instructionSet.getInstructions(this.name)[this.args.instrNum] as Instruction;
        /* this representation is correct most of times, for instructions with several representation (as mrmovl or rmmovl) the size might be wrong
        here we should found what is the good instruction used in the list.
        */
        let sizeInBytes = 1;
        sizeInBytes += instruction.useRegisters ? 1 : 0;
        sizeInBytes += instruction.useValC ? ctx.wordSize : 0;

        this.instructionBytes = new Array<number>(0);

        for(let i = 0; i < sizeInBytes; i++) {
            this.instructionBytes.push(0);
        }

        this.instructionBytes[0] |= instruction.icode << 4;
        this.instructionBytes[0] |= instruction.ifun;
        this.args.instructionBytes = this.instructionBytes;
        this.args.postEvaluate(ctx);
        this.instructionBytes = this.args.instructionBytes;
    }

    private _getRepresentation() {
        let output = '    ' + this.name + ' ';
        output += this.args.getArgs();

        return output;
    }
}
