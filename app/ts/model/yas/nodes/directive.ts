import { CompilationError } from '../../interfaces/ICompiler'
import { stringToNumber, bigIntToByteArray, stringToBigInt, isNaN, is64Bits, is32Bits } from '../../numberUtils'
import { YasNode } from './yasNode'

export class Directive extends YasNode {
    directiveName : string
    value : string

    constructor(directiveName : string, value : string, line : number) {
        super(line)
        this.directiveName = directiveName
        this.value = value
    }

    evaluate(ctx : any) : void {
        this.vaddr = ctx.vaddr;
        
        switch(this.directiveName) {
            case 'align': {
                let value = stringToBigInt(this.value);
                if(!((ctx.wordSize === 4 && is32Bits(this.value)) || (ctx.wordSize === 8 && is64Bits(this.value)))){
                    throw new CompilationError(this.line, "The Alignement value '" + value + "' overflows in " + (ctx.wordSize === 4 ? "32bits" : "64bits"));
                }
                if(value < BigInt(1)) {
                    throw new CompilationError(this.line, "Alignement value must be higher than 1")
                }
                while(ctx.vaddr % value != BigInt(0)) {
                    ctx.vaddr++
                }
                this.instructionBytes = []
                break;
            }
            case 'long': {
                let value;
                if(isNaN(this.value)) {
                    ctx.vaddr += BigInt(ctx.wordSize * 2)
                    return
                } else {
                    value = stringToBigInt(this.value);
                }

                if(!((ctx.wordSize === 4 && is32Bits(this.value)) || (ctx.wordSize === 8 && is64Bits(this.value)))){
                    throw new CompilationError(this.line, "The long value '" + value + "' overflows in " + (ctx.wordSize === 4 ? "32bits" : "64bits"));
                }

                let value2 = stringToBigInt(this.value);
                const bytes = bigIntToByteArray(value2, ctx.wordSize, true)
                ctx.vaddr += BigInt(bytes.length)

                this.instructionBytes = bytes
                break;
            }
            case 'pos': {
                let value = stringToBigInt(this.value);
                if(!((ctx.wordSize === 4 && is32Bits(this.value)) || (ctx.wordSize === 8 && is64Bits(this.value)))){
                    throw new CompilationError(this.line, "The address value '" + value + "' overflows in " + (ctx.wordSize === 4 ? "32bits" : "64bits"));
                }
                if(value < BigInt(0)) {
                    throw new CompilationError(this.line, "An address is expected to be positive")
                }
                ctx.vaddr = value
                this.vaddr = ctx.vaddr

                this.instructionBytes = []
                break;
            }
            default:
                throw new CompilationError(this.line, 'The directive ".' + this.directiveName + '" does not exist')
        }

        this.statementAsText = '.' + this.directiveName + ' ' + this.value
    }

    postEvaluate(ctx : any) : void {
        switch(this.directiveName) {
            case 'align': {
                break;
            }
            case 'long': {
                let value;
                try {
                    value = stringToBigInt(this.value)
                } catch (e) {
                    value = NaN;
                }
                if(isNaN(value)) {
                    if(!ctx.labels.has(this.value)) {
                        throw new CompilationError(this.line, "The label '" + this.value + "' does not exist");
                    }
                    value = ctx.labels.get(this.value);

                    const bytes = bigIntToByteArray(value, ctx.wordSize, true)

                    this.instructionBytes = bytes
                }
                
                break;
            }
            case 'pos': {
                break;
            }
            default:
                throw new CompilationError(this.line, 'The directive ".' + this.directiveName + '" does not exist')
        }

        this.statementAsText = '.' + this.directiveName + ' ' + this.value
    }
}