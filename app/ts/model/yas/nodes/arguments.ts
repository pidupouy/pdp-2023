import { YasNode } from './yasNode';
import { Instruction } from '../../instruction';
import { CompilationError } from '../../interfaces/ICompiler';
import { isNaN, bigIntToByteArray, stringToBigInt, is32Bits, is64Bits } from '../../numberUtils';

const REG_POSITION = 1;
let valcPosition = 0;

export class Arguments extends YasNode {
    private args: string[];
    private instructionName: string;
    private name: string;
    private rawArgs: string;
    instrNum: number;

    constructor(args : string = "", name : string, line : number) {
        super(line);
        this.instructionName = name;
        args = args.replace(/\s+/g, '');
        this.rawArgs = args;
        this.args = [];
        this.name = name;
        this.instrNum = 0;
    }

    getArgs():string {
        return this.rawArgs;
    }

    evaluate(ctx : any) : void {
        const instruction = ctx.instructionSet.getInstructions(this.instructionName) as Instruction[];

        //check validity of arguments
        let regEx  = / /g;
        let values = null;
        let instructionNum = 0;
        do{ // find which Instruction verify the regExp
            regEx = instruction[instructionNum].argsRegex;
            values = this.rawArgs.match(regEx);
            if(values != null){
                // check that regExp matches exactly rawArgs
                if(this.rawArgs.localeCompare(values[0]) != 0){
                    values = null;
                }
            }
            instructionNum++;
        }while(instructionNum < instruction.length && values == null)
        if (values === null) {
            throw new CompilationError(this.line, "Pre: Invalid arguments for instruction '" + this.name + "'" + ",rawArgs = '"+this.rawArgs+"'");
        }
        this.instrNum = instructionNum - 1;
    }

    postEvaluate(ctx : any) : void {
        const instruction = ctx.instructionSet.getInstructions(this.instructionName)[this.instrNum] as Instruction;

        if(instruction.useRegisters) {
            this.instructionBytes[REG_POSITION] = 0xff;
            valcPosition = REG_POSITION + 1;
        } else {
            valcPosition = REG_POSITION;
        }
        // When we match like this, the regex used to match valC produce 2 groups with the value of valC (if an immediate value is in rawArgs)
        //                                                               and some undefined groups.
        // We need to put them away. The tricky part is with "valC?" because we are not sure there is an immediate value in rawArgs.
        let values_ = this.rawArgs.match(instruction.argsRegex);
        if (values_ === null) {
            throw new CompilationError(this.line, "Post: Invalid arguments for instruction '" + this.name + "'" + ",rawArgs = '"+this.rawArgs+"'");
        }
        const values = this.filterValues(values_);
        let argNum = 0;
        instruction.lexeme.forEach((argType, index) => {
            if (values === null) {  // just here to stop typescript from complaining
                throw new CompilationError(this.line, "Invalid arguments for instruction '" + this.name + "', (should never be triggered)");
            }
            let shouldCount = true; // represents if valC has been provided or not
            switch (argType) {
                case "valC?":
                    if (values[argNum] !== null || values[argNum] !== undefined) {
                        try {
                            this._processValC(ctx, this.instructionBytes, values[argNum]);
                        } catch (error) {
                            if (error instanceof CompilationError) {
                                shouldCount = false;
                                this._processValC(ctx, this.instructionBytes, "0");
                            }
                        }
                    }
                    break;
                case "valC":
                    this._processValC(ctx, this.instructionBytes, values[argNum]);
                    break;

                case "rA":
                    this._processRegister(ctx, this.instructionBytes, values[argNum], 0);
                    break;

                case "rB":
                    this._processRegister(ctx, this.instructionBytes, values[argNum], 1);
                    break;

                default:
                    throw new Error("Unknown value " + instruction.lexeme[argNum] + " in lexeme for instruction '" + this.name + "'"); //if this is triggered there is an issue with the instruction set
            }
            if(shouldCount)
                argNum += 2; // all values are duplicated in tab
        })
    }

    /**
     * When a regular expression is matched, JavaScript generate an array whose length equals the number of captures of the expression.
     * A capture (or group of capture) is either : a pair of parenthesis, the expression itself or a part of the expression.
     * This function removes the capture at index 0 (which corresponds to the expression itself) and the undefined captures (especially a capture group
     * that wasn't matched but was a part of an OR expression) in order to keep the matched values only.
     * @param values an array containing the capture groups
     * @returns the filtered array which contains the values associated to the matched keywords
     */
    private filterValues(values : RegExpMatchArray){
        let filteredValues: string[] = [];
        values.forEach((arg, index) => {
            if(index > 0 && arg != undefined) { // eliminate complete match at index 0 and potential undefined captures
                filteredValues.push(arg);
            }
        });
        return filteredValues;
    }

    private _processValC(ctx : any, instructionBytes : Array<number>, userArg : string) {
        let value = BigInt(0);

        if(isNaN(userArg)) {
            if(!ctx.labels.has(userArg)) {
                throw new CompilationError(this.line, "The label '" + userArg + "' does not exist");
            }
            value = ctx.labels.get(userArg) as bigint;
        } else {
            if(!((ctx.wordSize === 4 && is32Bits(userArg)) || (ctx.wordSize === 8 && is64Bits(userArg)))){
                throw new CompilationError(this.line, "The number '" + userArg + "' overflows in " + (ctx.wordSize === 4 ? "32bits" : "64bits"));
            } else {
                value = stringToBigInt(userArg)
            }
        }

        const bytes = bigIntToByteArray(value, ctx.wordSize, true);
        bytes.forEach((byte, index) => {
            instructionBytes[valcPosition + index] = byte;
        })
    }

    private _processRegister(ctx : any, instructionBytes : Array<number>, userArg : string, position : number) {
        if(!ctx.registersEnum.hasOwnProperty(userArg)) {
            throw new CompilationError(this.line, "Register '" + userArg + "' does not exist");
        }
        if(position < 0 || position > 1) {
            throw new CompilationError(this.line, 'A register must have a position in [0;1]. Received : ' + position.toString());
        }
        let registerID = ctx.registersEnum[userArg] as number;

        if(position == 0) {
            registerID <<= 4;
            registerID |= 0x0f;
        } else {
            registerID |= 0xf0;
        }

        instructionBytes[REG_POSITION] &= registerID;
    }
}