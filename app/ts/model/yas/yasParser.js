"use strict"
// Written Docs for this tutorial step can be found here:
// https://chevrotain.io/docs/tutorial/step2_parsing.html

import { CompilationError, CompilationResult } from "../interfaces/ICompiler";
import { tokenVocabulary } from "./yasLexer"
const CstParser = require("chevrotain").CstParser

// We gather all the needed TokenType from the lexer vocabulary
const NewLine = tokenVocabulary.NEW_LINE
const Comment = tokenVocabulary.COMMENT
const Colon = tokenVocabulary.COLON
const Number = tokenVocabulary.NUMBER
const Eof = tokenVocabulary.EOF
const Identifier = tokenVocabulary.IDENTIFIER
const DirectiveIdentifier = tokenVocabulary.DIRECTIVE_IDENTIFIER
const Args = tokenVocabulary.ARGS
const Invalid = tokenVocabulary.INVALID

const WhiteSpace = tokenVocabulary.WHITE_SPACE
const OtherSpace = tokenVocabulary.OTHER_SPACE

/* ------------------ Parser ------------------ */
/*
Parser documentation :
- RULE : define a new rule
- SUBRULE : expect to match the indicated rule
- CONSUME : expect to match a TokenType
- OR : expect to match one alternative
- MANY : expect to match zero or more 
- { label : ... } : help recognize when there is 
same subrule/TokenType in a rule during the visitor phase
*/
class YasParser extends CstParser {
  constructor() {
    super(tokenVocabulary)
    
    const $ = this
    
    $.RULE("document", () => {
      $.SUBRULE($.line_list)
      //$.CONSUME(Eof)
    })
    
    $.RULE("line_list", () => {
      $.AT_LEAST_ONE(() => {
        $.SUBRULE($.line)
      });
    })
    
    $.RULE("line", () => {
      $.OR([
        { ALT: () => $.SUBRULE($.line_label_statement_linecomment) },
        { ALT: () => $.SUBRULE($.line_label_linecomment) },
        { ALT: () => $.SUBRULE($.line_statement_linecomment) },
        { ALT: () => $.SUBRULE($.line_linecomment) }
      ])
    })
    
    $.RULE("line_label_statement_linecomment", () => {
      $.SUBRULE($.label)
      $.SUBRULE($.statement)
      $.SUBRULE($.line_comment)
      $.CONSUME(NewLine)
    })
    
    $.RULE("line_label_linecomment", () => {
      $.SUBRULE($.label)
      $.SUBRULE($.line_comment)
      $.CONSUME(NewLine)
    })
    
    $.RULE("line_statement_linecomment", () => {
      $.SUBRULE($.statement)
      $.SUBRULE($.line_comment)
      $.CONSUME(NewLine)
    })
    
    $.RULE("line_linecomment", () => {
      $.SUBRULE($.line_comment)
      $.CONSUME(NewLine)
    })
    
    $.RULE("label", () => {
      $.CONSUME(Identifier)
      $.CONSUME(Colon)
    })
    
    $.RULE("statement", () => {
      $.OR([
        { ALT: () => $.SUBRULE($.directive) },
        { ALT: () => $.SUBRULE($.instruction) },
      ])
    })
    
    $.RULE("directive", () => {
      $.CONSUME(DirectiveIdentifier)
      $.OR([
        { ALT: () => $.CONSUME(Number) },
        { ALT: () => $.CONSUME(Identifier) },
      ])
    })
    
    $.RULE("instruction", () => {
      $.CONSUME(Identifier)
      $.OPTION(() => {
        $.SUBRULE($.arg_list)
      })
    })
    
    $.RULE("arg_list", () => {
      $.OR([
        { ALT: () => $.SUBRULE($.identifier_arg_list) },
        { ALT: () => $.CONSUME(Identifier) },
        { ALT: () => $.SUBRULE($.number_arg_list) },
        { ALT: () => $.CONSUME(Number) },
        { ALT: () => $.CONSUME(Args) }
      ])
    })
    
    $.RULE("identifier_arg_list", () => {
      $.CONSUME(Identifier)
      $.SUBRULE($.arg_list)
    })
    
    $.RULE("number_arg_list", () => {
      $.CONSUME(Number)
      $.SUBRULE($.arg_list)
    })
    
    $.RULE("line_comment", () => {
      $.OPTION(() => {
        $.CONSUME(Comment)
      }) 
    })  
    
    this.performSelfAnalysis()
  }
}

function yasParser (inputTokens) {
  // A new parser instance with CST output (enabled by default).
  const parserInstance = new YasParser()
  
  // ".input" is a setter which will reset the parser's internal's state.
  parserInstance.input = inputTokens
  
  // Automatic CST created when parsing
  const cst = parserInstance.document()
  const errors_list = []
  
  if (parserInstance.errors.length > 0) {
    for (const error in parserInstance.errors){
      errors_list.push(new CompilationError(parserInstance.errors[error].token.startLine, parserInstance.errors[error].message))
    }
  }
  return new CompilationResult(cst, errors_list, {})
}

export { YasParser, yasParser }
