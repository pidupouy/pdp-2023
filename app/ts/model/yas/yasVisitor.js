"use strict"
// Written Docs for chevrotain visitor can be found here
// https://chevrotain.io/docs/tutorial/step3a_adding_actions_visitor.html

import { CompilationError, CompilationResult } from "../interfaces/ICompiler";
import { YasParser } from "./yasParser"
import { Directive } from "./nodes/directive"
import { Document } from "./nodes/document"
import { InstructionLine } from "./nodes/instruction"
import { Label } from "./nodes/label"
import { Line } from "./nodes/line"
import { Arguments } from "./nodes/arguments"

// A new parser instance with CST output (enabled by default).
const parserInstance = new YasParser()
// The base visitor class can be accessed via the a parser instance.
const BaseCstVisitor = parserInstance.getBaseCstVisitorConstructor()

class YasVisitor extends BaseCstVisitor {
  constructor() {
    super()
    this.validateVisitor()
  }
  
  document(ctx){
    const line_list_value = this.visit(ctx.line_list).value
    return {
      type: "document",
      value: new Document(line_list_value)
    }
  }
  
  line_list(ctx){
    const line_list_value = []
    for (const line of ctx.line) {
      line_list_value.push(this.visit(line).value)
    }
    return {
      type: "line_list",
      value: line_list_value
    }
  }
  
  line(ctx){
    let line_value;
    if (ctx.line_label_statement_linecomment) {
      line_value = this.visit(ctx.line_label_statement_linecomment).value
    } else if (ctx.line_label_linecomment) {
      line_value = this.visit(ctx.line_label_linecomment).value
    } else if (ctx.line_statement_linecomment) {
      line_value = this.visit(ctx.line_statement_linecomment).value
    } else if (ctx.line_linecomment) {
      line_value = this.visit(ctx.line_linecomment).value
    }
    return {
      type: "line",
      value: line_value
    }
  }
  
  line_label_statement_linecomment(ctx){
    const first_line = ctx.NEW_LINE[0].startLine
    return {
      type: "line_label_statement_linecomment",
      value: new Line(first_line, [this.visit(ctx.label).value,this.visit(ctx.statement).value], this.visit(ctx.line_comment).value)
    }
  }
  
  line_label_linecomment(ctx){
    const first_line = ctx.NEW_LINE[0].startLine
    return {
      type: "line_label_linecomment",
      value: new Line(first_line, [this.visit(ctx.label).value], this.visit(ctx.line_comment).value)
    }
  }
  
  line_statement_linecomment(ctx){
    const first_line = ctx.NEW_LINE[0].startLine
    return {
      type: "line_statement_linecomment",
      value: new Line(first_line, [this.visit(ctx.statement).value], this.visit(ctx.line_comment).value)
    }
  }
  
  line_linecomment(ctx){
    const first_line = ctx.NEW_LINE[0].startLine
    return {
      type: "line_linecomment",
      value: new Line(first_line, [], this.visit(ctx.line_comment).value)
    }
  }
  
  statement(ctx){
    let statement_value
    if (ctx.directive) {
      statement_value = this.visit(ctx.directive).value
    } else if(ctx.instruction){
      statement_value = this.visit(ctx.instruction).value
    }
    return {
      type: "statement",
      value: statement_value
    }
  }
  
  label(ctx){
    return {
      type: "label",
      value: new Label(ctx.IDENTIFIER[0].image,ctx.IDENTIFIER[0].startLine)
    }
  }
  
  directive(ctx){
    let args;
    if (ctx.NUMBER){
      args = ctx.NUMBER[0].image
    } else {
      args = ctx.IDENTIFIER[0].image
    }
    return {
      type: "directive",
      value: new Directive(ctx.DIRECTIVE_IDENTIFIER[0].image.substr(1),args,ctx.DIRECTIVE_IDENTIFIER[0].startLine)
    }
  }
  
  instruction(ctx){
    if (ctx.arg_list) {
      const arg_list_value = this.visit(ctx.arg_list).value
      return {
        type: "instruction_arg_list",
        value: new InstructionLine(ctx.IDENTIFIER[0].image, new Arguments(arg_list_value,ctx.IDENTIFIER[0].image,ctx.IDENTIFIER[0].startLine),ctx.IDENTIFIER[0].startLine)
      }
    } else if (ctx.IDENTIFIER) {
      return {
        type: "instruction",
        value: new InstructionLine(ctx.IDENTIFIER[0].image, new Arguments("",ctx.IDENTIFIER[0].image,ctx.IDENTIFIER[0].startLine),ctx.IDENTIFIER[0].startLine)
      }
    }
  }
  
  arg_list(ctx){
    if (ctx.identifier_arg_list) {
      return {
        type: "arg_list_identifier_list",
        value: this.visit(ctx.identifier_arg_list).value
      }
    }
    if (ctx.IDENTIFIER){
      return {
        type: "arg_list_identifier",
        value: ctx.IDENTIFIER[0].image
      }
    }
    if (ctx.number_arg_list) {
      return {
        type: "arg_list_number_list",
        value: this.visit(ctx.number_arg_list).value
      }
    }
    if (ctx.NUMBER){
      return {
        type: "arg_list_number",
        value: ctx.NUMBER[0].image
      }
    }
    if (ctx.ARGS) {
      return {
        type: "arg_list_args",
        value: ctx.ARGS[0].image
      }
    }
  }
  
  identifier_arg_list(ctx){
    let arg_list_value = this.visit(ctx.arg_list).value
    return {
      type: "identifier_arg_list",
      value: ctx.IDENTIFIER[0].image + " " + arg_list_value
    }
  }
  
  number_arg_list(ctx){
    let arg_list_value = this.visit(ctx.arg_list).value
    return {
      type: "number_arg_list",
      value: ctx.NUMBER[0].image + " " + arg_list_value
    }
  }
  
  line_comment(ctx){
    if (ctx.COMMENT){
      return {
        type: "line_comment",
        value: ctx.COMMENT[0].image
      }
    } else {
      return {
        type: "line_comment",
        value: ''
      }
    }
  }
}

function yasVisitor  (inputCst) {
  // Our visitor has no state, so a single instance is sufficient.
  const yasVisitorInstance = new YasVisitor()
  
  const result = yasVisitorInstance.visit(inputCst)
  
  return new CompilationResult(result.jsOutput, result.errors, result.value)
}

export { yasVisitor }