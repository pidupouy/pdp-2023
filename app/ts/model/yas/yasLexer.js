"use strict"
// Written Docs for this tutorial step can be found here:
// https://chevrotain.io/docs/tutorial/step1_lexing.html

import { CompilationError, CompilationResult } from "../interfaces/ICompiler"
const chevrotain = require("chevrotain")
const Lexer = chevrotain.Lexer
const createToken = chevrotain.createToken

/* ------------------ Lexical grammar ------------------ */
/*
CreateToken is used to create a TokenType
The Lexer's output will contain an array of token Objects created by metadata
*/
const NewLine = createToken({name: "NEW_LINE", pattern: /\n/})
const Comment = createToken({name: "COMMENT", pattern: /\#[^\n]*/})
const Colon = createToken({name: "COLON", pattern: /:/})
const Number = createToken({name: "NUMBER", pattern: /(0x[0-9a-fA-F]+)|(\-?\b[0-9]+\b)/})
//const Eof = createToken({name: "EOF", pattern: /})
const Identifier = createToken({name: "IDENTIFIER", pattern: /[a-zA-Z][0-9a-zA-Z_]*/})
const DirectiveIdentifier = createToken({name: "DIRECTIVE_IDENTIFIER", pattern: /\.[a-zA-Z][0-9a-zA-Z_]*/})
const Args = createToken({name: "ARGS", pattern: /[^\n#]+/})
const Invalid = createToken({name: "INVALID", pattern: /./})

// Tokens that are ignored
const WhiteSpace = createToken({name: "WHITE_SPACE", pattern: /[^\S\n]+/, group: Lexer.SKIPPED})
const OtherSpace = createToken({name: "OTHER_SPACE", pattern: /[ \r\t\f]/, group: Lexer.SKIPPED})


// The order of tokens is important because it determines which token will be compared to the word by the lexer
const allTokens = [
  WhiteSpace,
  OtherSpace,
  NewLine,
  Comment,
  Colon,
  //Eof,
  Identifier,
  DirectiveIdentifier,
  Number,
  Args,
  Invalid,
]

// the vocabulary will be exported and used in the Parser definition.
const tokenVocabulary = {}

allTokens.forEach((tokenType) => {
  tokenVocabulary[tokenType.name] = tokenType
})

function yasLexer(inputText) {
  const SelectLexer = new Lexer(allTokens)
  const lexingResult = SelectLexer.tokenize(inputText)
  const errors_list = []
  
  if (lexingResult.errors.length > 0) {
    for (const error in lexingResult.errors){
      errors_list.push(new CompilationError(lexingResult.errors[error].line, lexingResult.errors[error].message))
    }
  }
  
  return new CompilationResult(lexingResult.tokens, errors_list, {})
}

export { tokenVocabulary, yasLexer}
