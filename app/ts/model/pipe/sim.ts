import { Alu } from "../alu";
import { IAlu, alufct } from "../interfaces/IAlu"
import { HCL } from "../hcl";
import { IInstructionSet } from "../interfaces/IInstructionSet";
import { ISimulator, convert_dict_status } from "../interfaces/ISimulator";
import { Memory } from "../memory";
import { Registers } from "../registers";
import { IRegisters, registers_enum } from "../interfaces/IRegisters";
import { IMemory } from "../interfaces/IMemory";
import { simStatus } from "../interfaces/ISimulator";
import { PContext } from "./pcontext"
import * as stages from "./stages"
import { pipestatus } from "./pipeline";

export class Sim implements ISimulator {
    public context: PContext;
    public registers: IRegisters;
    public memory: IMemory;
    public alu: IAlu;
    public hcl: HCL;
    public instructionSet: IInstructionSet;
    
    public status: simStatus;
    public errorMessage: string;
    private yo: string;
    private stepNum: number;
    private wordSize: number;
    private registersNamesList: string[];
    private instructions: number;
    private cycles: number;
    private starting_up: boolean;

    constructor(instructionSet: IInstructionSet, wordSize: number, registersNamesList: string[]) {
        this.context = new PContext();
        this.registers = new Registers(wordSize, registersNamesList)
        this.memory = new Memory(wordSize);
        this.alu = new Alu(wordSize);
        this.hcl = new HCL(instructionSet, registers_enum, alufct);
        this.hcl.setCtx(this.context);
        this.instructionSet = instructionSet;

        this.status = simStatus.AOK;
        this.errorMessage = "";
        this.yo = "";
        this.stepNum = 0;
        this.wordSize = wordSize;
        this.registersNamesList = registersNamesList;
        this.instructions = 0;
        this.cycles = 0;
        this.starting_up = true;
    }

    step(): void {
        try {
            this.alu.updateFlags();
            this.memory.updateMemory();
            this.registers.updateRegisters();

            this.context.updatePipes();

            if (this.context.getFetch().getOp() == pipestatus.ERROR)
                this.context.getCurrentFetch().simStatus = simStatus.PIPE;
            if (this.context.getDecode().getOp() == pipestatus.ERROR)
                this.context.getCurrentDecode().simStatus = simStatus.PIPE;
            if (this.context.getExecute().getOp() == pipestatus.ERROR)
                this.context.getCurrentExecute().simStatus = simStatus.PIPE;
            if (this.context.getMemory().getOp() == pipestatus.ERROR)
                this.context.getCurrentMemory().simStatus = simStatus.PIPE;
            if (this.context.getWriteBack().getOp() == pipestatus.ERROR)
                this.context.getCurrentWriteBack().simStatus = simStatus.PIPE;

            stages.fetch(this);
            stages.writeBack(this);
            stages.memory(this);
            stages.execute(this);
            stages.decode(this);


            stages.do_stall_check(this);

            if (this.context.getCurrentExecute().simStatus != simStatus.AOK
                && this.context.getCurrentExecute().simStatus != simStatus.BUBBLE) {
                this.context.getDecode().setOp(pipestatus.BUBBLE);
                this.context.getExecute().setOp(pipestatus.BUBBLE);
            }

            /* Performance monitoring */
            if (this.context.getCurrentWriteBack().simStatus != simStatus.BUBBLE) {
                this.starting_up = false;
                this.instructions++;
                this.cycles++;
            } else {
                if (!this.starting_up)
                    this.cycles++;
            }

            this.stepNum++;
        } catch (error) {
            this.status = simStatus.HALT;
            this.errorMessage = error;
        }

        if (stages.currentInstructionIsHalt(this)) {
            this.status = simStatus.HALT;
        }
    }

    continue(breakpoints: Array<bigint> = [], maxSteps: number = 10000): void {
        let stepCount = 0;
        
        while (this.status == simStatus.AOK && (stepCount < maxSteps || maxSteps < 0)) {
            this.step();
            for (let brk of breakpoints) {
                if (this.context.getNextFetch().pc == brk) {
                    return;
                };
            };
            stepCount++;
        }

        if (this.status == simStatus.AOK && stepCount == maxSteps) {
            this.errorMessage = "Sim : Max number of steps reached (" + maxSteps + ")";
        }
    }


    reset(): void {
        this.context = new PContext();
        this.registers = new Registers(this.wordSize, this.registersNamesList)
        this.memory = new Memory(this.wordSize);
        this.alu = new Alu(this.wordSize);
        this.hcl.setCtx(this.context);
        this.loadProgram(this.yo);

        this.status = simStatus.AOK;
        this.errorMessage = "";
        this.stepNum = 0;
        this.instructions = 0;
        this.cycles = 0;
        this.starting_up = true;
    }

    undo(): void {
        let previousStep = this.stepNum - 1;
        this.reset();
        this.loadProgram(this.yo);

        while (previousStep > 0) {
            this.step();
            previousStep--;
        }
    }

    load(jsonContent: any): void {
        this.loadProgram(jsonContent.compilationResult.output)
        this.context.load(jsonContent.cpuState);
        this.registers.load(jsonContent.registers);
        this.alu.load(jsonContent.flags);
        this.cycles = jsonContent.cycles;
        this.instructions = jsonContent.instructions;
        this.status = convert_dict_status[jsonContent.status];
        this.errorMessage = jsonContent.error;
        this.stepNum = jsonContent.stepNum
        this.memory.load(jsonContent.memory)
    }

    save(): string {
        let content = ""

        content += "\"cpuState\" : " + this.context.save() + ",\n"
        content += "\"registers\" : " + this.registers.save() + ",\n"
        content += "\"flags\" : " + this.alu.save() + ",\n"
        content += "\"cycles\" : " + this.cycles + ",\n"
        content += "\"instructions\" : " + this.instructions + ",\n"
        content += "\"status\" : " + JSON.stringify(simStatus[this.status]) + ",\n"
        content += "\"error\" : " + JSON.stringify(this.errorMessage) + ",\n"
        content += "\"stepNum\" : " + this.stepNum + ",\n"
        content += "\"memory\" : " + this.memory.save() + ",\n"

        return content
    }

    loadProgram(yo: string): void {
        this.memory.loadProgram(yo);
        this.yo = yo;
    }
    
    insertHclCode(js: string): void {
        this.hcl.setHclCode(js);
    }
    
    getStageView() {
        return {
            "WriteBack": {
                "columns": ["", "stat", "instr", "valE", "valM", "dstE", "dstM"],
                "lines": [
                    {
                        "name": "State",
                        "labels": ["", "", "", "", "", ""],
                        "values": [
                            simStatus[this.context.getCurrentWriteBack().simStatus],
                            this.instructionSet.getInstructionName(this.context.getCurrentWriteBack().icode, this.context.getCurrentWriteBack().ifun),
                            this.context.getCurrentWriteBack().valE,
                            this.context.getCurrentWriteBack().valM,
                            this.registers.getRegisterName(this.context.getCurrentWriteBack().dstE),
                            this.registers.getRegisterName(this.context.getCurrentWriteBack().dstM)
                        ]
                    },
                    {
                        "name": "Input",
                        "labels": ["", "", "", "", "", ""],
                        "values": [
                            simStatus[this.context.getNextWriteBack().simStatus],
                            this.instructionSet.getInstructionName(this.context.getNextWriteBack().icode, this.context.getNextWriteBack().ifun),
                            this.context.getNextWriteBack().valE,
                            this.context.getNextWriteBack().valM,
                            this.registers.getRegisterName(this.context.getNextWriteBack().dstE),
                            this.registers.getRegisterName(this.context.getNextWriteBack().dstM)
                        ]
                    }
                ]
            },
            "Memory": {
                "columns": ["", "stat", "instr", "Bch", "valE", "valA", "dstE", "dstM"],
                "lines": [
                    {
                        "name": "State",
                        "labels": ["", "", "", "", "", "", ""],
                        "values": [
                            simStatus[this.context.getCurrentMemory().simStatus],
                            this.instructionSet.getInstructionName(this.context.getCurrentMemory().icode, this.context.getCurrentMemory().ifun),
                            this.context.getCurrentMemory().bcond,
                            this.context.getCurrentMemory().valE,
                            this.context.getCurrentMemory().valA,
                            this.registers.getRegisterName(this.context.getCurrentMemory().dstE),
                            this.registers.getRegisterName(this.context.getCurrentMemory().dstM)
                        ]
                    },
                    {
                        "name": "Input",
                        "labels": ["", "", "", "", "", "", ""],
                        "values": [
                            simStatus[this.context.getNextMemory().simStatus],
                            this.instructionSet.getInstructionName(this.context.getNextMemory().icode, this.context.getNextMemory().ifun),
                            this.context.getNextMemory().bcond,
                            this.context.getNextMemory().valE,
                            this.context.getNextMemory().valA,
                            this.registers.getRegisterName(this.context.getNextMemory().dstE),
                            this.registers.getRegisterName(this.context.getNextMemory().dstM)
                        ]
                    }
                ]
            },
            "Execute": {
                "columns": ["", "stat", "instr", "srcA", "srcB", "valA", "valB", "valC", "dstE", "dstM"],
                "lines": [
                    {
                        "name": "State",
                        "labels": ["", "", "", "", "", "", "", "", ""],
                        "values": [
                            simStatus[this.context.getCurrentExecute().simStatus],
                            this.instructionSet.getInstructionName(this.context.getCurrentExecute().icode, this.context.getCurrentExecute().ifun),
                            this.registers.getRegisterName(this.context.getCurrentExecute().srcA),
                            this.registers.getRegisterName(this.context.getCurrentExecute().srcB),
                            this.context.getCurrentExecute().valA,
                            this.context.getCurrentExecute().valB,
                            this.context.getCurrentExecute().valC,
                            this.registers.getRegisterName(this.context.getCurrentExecute().dstE),
                            this.registers.getRegisterName(this.context.getCurrentExecute().dstM)
                        ]
                    },
                    {
                        "name": "Input",
                        "labels": ["", "", "", "", "", "", "", "", ""],
                        "values": [
                            simStatus[this.context.getNextExecute().simStatus],
                            this.instructionSet.getInstructionName(this.context.getNextExecute().icode, this.context.getNextExecute().ifun),
                            this.registers.getRegisterName(this.context.getNextExecute().srcA),
                            this.registers.getRegisterName(this.context.getNextExecute().srcB),
                            this.context.getNextExecute().valA,
                            this.context.getNextExecute().valB,
                            this.context.getNextExecute().valC,
                            this.registers.getRegisterName(this.context.getNextExecute().dstE),
                            this.registers.getRegisterName(this.context.getNextExecute().dstM)
                        ]
                    }
                ]
            },
            "Decode": {
                "columns": ["", "stat", "instr", "ra", "rb", "valC", "valP"],
                "lines": [
                    {
                        "name": "State",
                        "labels": ["", "", "", "", "", ""],
                        "values": [
                            simStatus[this.context.getCurrentDecode().simStatus],
                            this.instructionSet.getInstructionName(this.context.getCurrentDecode().icode, this.context.getCurrentDecode().ifun),
                            this.registers.getRegisterName(this.context.getCurrentDecode().ra),
                            this.registers.getRegisterName(this.context.getCurrentDecode().rb),
                            this.context.getCurrentDecode().valC,
                            this.context.getCurrentDecode().valP
                        ]
                    },
                    {
                        "name": "Input",
                        "labels": ["", "", "", "", "", ""],
                        "values": [
                            simStatus[this.context.getNextDecode().simStatus],
                            this.instructionSet.getInstructionName(this.context.getNextDecode().icode, this.context.getNextDecode().ifun),
                            this.registers.getRegisterName(this.context.getNextDecode().ra),
                            this.registers.getRegisterName(this.context.getNextDecode().rb),
                            this.context.getNextDecode().valC,
                            this.context.getNextDecode().valP
                        ]
                    }
                ]
            },
            "Fetch": {
                "columns": ["", "stat", "newPC"],
                "lines": [
                    {
                        "name": "State",
                        "labels": ["", ""],
                        "values": [
                            simStatus[this.context.getCurrentFetch().simStatus],
                            this.context.getCurrentFetch().newPC
                        ]
                    },
                    {
                        "name": "Input",
                        "labels": ["", ""],
                        "values": [
                            simStatus[this.context.getNextFetch().simStatus],
                            this.context.getNextFetch().newPC
                        ]
                    }
                ]
            }
        };
    }

    getRegistersView() {
        return {
            "eax": this.registers.read(registers_enum.eax),
            "ebx": this.registers.read(registers_enum.ebx),
            "ecx": this.registers.read(registers_enum.ecx),
            "edx": this.registers.read(registers_enum.edx),
            "esi": this.registers.read(registers_enum.esi),
            "edi": this.registers.read(registers_enum.edi),
            "ebp": this.registers.read(registers_enum.ebp),
            "esp": this.registers.read(registers_enum.esp)
        }
    }

    getNextRegistersView(): any {
        return {
            "eax": this.registers.readNext(registers_enum.eax),
            "ebx": this.registers.readNext(registers_enum.ebx),
            "ecx": this.registers.readNext(registers_enum.ecx),
            "edx": this.registers.readNext(registers_enum.edx),
            "esi": this.registers.readNext(registers_enum.esi),
            "edi": this.registers.readNext(registers_enum.edi),
            "ebp": this.registers.readNext(registers_enum.ebp),
            "esp": this.registers.readNext(registers_enum.esp)
        };
    }

    getMemoryView() {
        return this.memory.getMemoryView();
    }

    getNextMemoryView() {
        return this.memory.getNextMemoryView();
    }

    getFlagsView() {
        return {
            "ZF": this.alu.getZF(),
            "OF": this.alu.getOF(),
            "SF": this.alu.getSF()
        };
    }

    getNextFlagsView() {
        return {
            "ZF": this.alu.getNextZF(),
            "OF": this.alu.getNextOF(),
            "SF": this.alu.getNextSF()
        };
    }

    getStatusView() {
        return {
            "STAT": this.status,
            "ERR": this.errorMessage,
            "PC": this.context.getCurrentFetch().pc < BigInt(0) ? BigInt(0) : this.context.getCurrentFetch().pc
        };
    }

    getPerformancesView() : { [key : string] : number } | undefined {
        return {
            "Cycles" : this.cycles,
            "Instructions" : this.instructions,
            "CPI" : this.instructions > 0 ? this.cycles / this.instructions : 1.0
        }
    }

    getHighlightRules(): any[] {
        return [
            {
                "address": this.context.getCurrentFetch().pc,
                "tag": "F"
            },
            {
                "address": this.context.getCurrentDecode().pc,
                "tag": "D"
            },
            {
                "address": this.context.getCurrentExecute().pc,
                "tag": "E"
            },
            {
                "address": this.context.getCurrentMemory().pc,
                "tag": "M"
            },
            {
                "address": this.context.getCurrentWriteBack().pc,
                "tag": "W"
            }
        ];
    }

    getStepNum(): number {
        return this.stepNum;
    }

    isInterfaceTightened(): boolean {
        return false;
    }

    updateSim(): void {
        stages.updateSim(this);
    }
}