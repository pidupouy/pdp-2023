import { Pipeline } from "./pipeline"
import { Context } from "../context";
import { pipestatus } from "./pipeline";

class PContext {
    private _pipes : Pipeline[];
    private _nb_pipes : number = 5;

    constructor(){
        this._pipes = new Array<Pipeline>();
        for (let i=0; i<this._nb_pipes; i++){
            this._pipes[i] = new Pipeline();
        }
    }

    getFetch() : Pipeline { return this._pipes[0]; }
    getDecode() : Pipeline { return this._pipes[1]; }
    getExecute() : Pipeline { return this._pipes[2]; }
    getMemory() : Pipeline { return this._pipes[3]; }
    getWriteBack() : Pipeline { return this._pipes[4]; }

    getNextFetch() : Context { return this._pipes[0].getNext(); }
    getNextDecode() : Context { return this._pipes[1].getNext(); }
    getNextExecute() : Context { return this._pipes[2].getNext(); }
    getNextMemory() : Context { return this._pipes[3].getNext(); }
    getNextWriteBack() : Context { return this._pipes[4].getNext(); }

    getCurrentFetch() : Context { return this._pipes[0].getCurrent(); }
    getCurrentDecode() : Context { return this._pipes[1].getCurrent(); }
    getCurrentExecute() : Context { return this._pipes[2].getCurrent(); }
    getCurrentMemory() : Context { return this._pipes[3].getCurrent(); }
    getCurrentWriteBack() : Context { return this._pipes[4].getCurrent(); }

    updatePipes() : void {
        for(let i=0; i<this._nb_pipes; i++){
            let current_pipe = this._pipes[i];
            switch (current_pipe.getOp()){
                case pipestatus.LOAD:
                    current_pipe.getCurrent().load(current_pipe.getNext());
                    break;
                case pipestatus.BUBBLE:
                case pipestatus.ERROR:
                    current_pipe.getCurrent().load(current_pipe.getBubble());
                    break;
                case pipestatus.STALL:
                default:
            }
            if (current_pipe.getOp() != pipestatus.ERROR) current_pipe.setOp(pipestatus.LOAD);
        }
    }

    load(newContext: any): void{
        this.getFetch().load(newContext.fetch);
        this.getDecode().load(newContext.decode);
        this.getExecute().load(newContext.execute);
        this.getMemory().load(newContext.memory);
        this.getWriteBack().load(newContext.writeBack);
    }

    save(): string {
        let content = ""
        content += "{\n"
        content += "\"fetch\" : " + this.getFetch().save() + ",\n"
        content += "\"decode\" : " + this.getDecode().save() + ",\n"
        content += "\"execute\" : " + this.getExecute().save() + ",\n"
        content += "\"memory\" : " + this.getMemory().save() + ",\n"
        content += "\"writeBack\" : " + this.getWriteBack().save() + "\n}"

        return content;
    }
}

export { PContext };