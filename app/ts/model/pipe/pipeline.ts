import { Context } from "../context";

export enum pipestatus {
    LOAD=0,
    STALL=1,
    BUBBLE=2,
    ERROR=3
}

export const convert_dict_status: { [index: string]: pipestatus } = {
    "LOAD": pipestatus.LOAD, "STALL": pipestatus.STALL, "BUBBLE": pipestatus.BUBBLE,
    "ERROR": pipestatus.ERROR
};

export class Pipeline{
    private _current: Context; // Current register state
    private _next: Context; // Next Register state
    private _bubble_val: Context = new Context(); // When bubbles appear, show content of registers
    private _op: pipestatus // udapted state

    constructor(){
        this._current = new Context();
        this._next = new Context();
        this._bubble_val = new Context();
        this._op = pipestatus.LOAD;
    }

    getNext() : Context {
        return this._next;
    }

    getCurrent() : Context {
        return this._current;
    }

    getOp() : pipestatus {
        return this._op;
    }

    setOp(new_op : pipestatus) : void {
        this._op = new_op;
    }

    getBubble() : Context {
        let tmp = this._bubble_val;
        this._bubble_val = new Context();
        return tmp
    }

    load(newContext: any): void {
        this._current.load(newContext.current);
        this._next.load(newContext.next);
        this._op = convert_dict_status[newContext.op];
    }

    save(): string{
        let content = ""
        content += "{\n"
        content += "\"current\" : " + this._current.save() + ",\n"
        content += "\"next\" : " + this._next.save() + ",\n"
        content += "\"op\" : \"" + pipestatus[this._op] + "\"\n}"

        return content;
    }
}