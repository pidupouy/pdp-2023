import { IRegisters, registers_enum, convert_dict_register } from "./interfaces/IRegisters"
import { stringToBigInt, toUint } from "./numberUtils";

/**
 * Class who contains all the registers of the CPU.
 * Based on the registers enumerate, it create automatically all the registers and init them with an empty word.
 * Registers works with Word type. They have no treatment on data. They just store words.
 */
class Registers implements IRegisters {
    /**
     * Array for all registers.
     */
    private content: Uint32Array | BigUint64Array
    private contentNext: Uint32Array | BigUint64Array
    private registersNames: Map<registers_enum, string>;
    private wordSize: number;

    constructor(wordSize: number, registersNamesList: string[]) {
        this.wordSize = wordSize;
        if(wordSize === 4){
            this.content = new Uint32Array(8);
            this.contentNext = new Uint32Array(8);
            this.content.fill(0,0,15);
            this.contentNext.fill(0,0,15);
        } else {
            this.content = new BigUint64Array(15);
            this.contentNext = new BigUint64Array(15);
            this.content.fill(BigInt(0),0,15);
            this.contentNext.fill(BigInt(0),0,15);
        }

        this.registersNames = new Map<registers_enum, string>();
        this.setRegistersNames(registersNamesList);
    }

    write(register: registers_enum, value: bigint): void {
        this.contentNext[register] = this.wordSize === 4 ? Number(toUint(value, this.wordSize)) : toUint(value, this.wordSize)
    }

    read(register: registers_enum): bigint {
        return BigInt(this.content[register]);
    }

    readNext(register: registers_enum): bigint {
        return BigInt(this.contentNext[register]);
    }

    updateRegisters(): void {
        for (let key in this.content) {
            this.content[key] = this.contentNext[key];
        }
    }

    getRegisterName(register: registers_enum): string {
        if (this.registersNames.has(register)){
            return this.registersNames.get(register) as string;
        } else {
            return "";
        }
    }

    load(newRegisters: { [key: string]: { [key: string]: string } }): void {
        for (let register in newRegisters.current) {
            this.content[convert_dict_register[register]] = this.wordSize === 4 ? Number(toUint(stringToBigInt(newRegisters.current[register]), this.wordSize)) : toUint(stringToBigInt(newRegisters.current[register]), this.wordSize);;
            this.contentNext[convert_dict_register[register]] = this.wordSize === 4 ? Number(toUint(stringToBigInt(newRegisters.next[register]), this.wordSize)) : toUint(stringToBigInt(newRegisters.next[register]), this.wordSize);;
        }
    }

    save(): string {
        let content : { [key : string] : bigint } = {};
        let contentNext : { [key : string] : bigint } = {};
        
        for (let registerName of this.registersNames){
            if(registerName[1] !== "none") {
                content[registerName[1]] = this.read(registerName[0]);
                contentNext[registerName[1]] = this.readNext(registerName[0]);
            }
        }

        return "{ \"current\" : " + (JSON.stringify(content, (key, value) =>
            typeof value === 'bigint' || typeof value === 'number'
                ? value.toString()
                : value // return everything else unchanged
        )) + ", \"next\" : " + (JSON.stringify(contentNext, (key, value) =>
            typeof value === 'bigint' || typeof value === 'number'
                ? value.toString()
                : value // return everything else unchanged
        )) + "}";
    }

    private setRegistersNames(registersNamesList: string[]): void {
        let index = 0;
        for (let registerName of registersNamesList) {
            this.registersNames.set(index, registerName);
            index++;
        }
        this.registersNames.set(15, "none");
    }
}

export { Registers };