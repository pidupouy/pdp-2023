import { registers_enum } from "./interfaces/IRegisters"
import { CC } from "./interfaces/IAlu";
import { simStatus } from "./interfaces/ISimulator";

/**
 * Context is an abstraction of buses and all value used by the CPU
 * during the execution.
 */
class Context {
    // Inner state
    pc        : bigint = BigInt(-1);
    valP      : bigint = BigInt(0);
    simStatus : simStatus = simStatus.BUBBLE;

    // Instruction
    icode     : number = 0;
    ifun      : number = 0;
    ra        : number = registers_enum.none;
    rb        : number = registers_enum.none;
    valC      : bigint = BigInt(0);

    // Registers output
    valA      : bigint = BigInt(0);
    valB      : bigint = BigInt(0);
    srcA      : number = registers_enum.none;
    srcB      : number = registers_enum.none;

    // Registers input
    dstE      : number = registers_enum.none;
    dstM      : number = registers_enum.none;

    // ALU input
    aluA      : bigint = BigInt(0);
    aluB      : bigint = BigInt(0);

    // ALU output
    valE      : bigint = BigInt(0);

    // Condition flag
    cc        : CC = CC.NONE;
    bcond     : boolean = false;

    // Memory input
    mem_addr  : bigint = BigInt(0);
    mem_data  : bigint = BigInt(0);
    mem_read  : boolean = false;
    mem_write : boolean = false;

    // Memory output
    valM      : bigint = BigInt(0);

    // update pc output
    newPC     : bigint = BigInt(0);

    load(newContext: Context): void {
        this.pc = BigInt(newContext.pc);
        this.valP = BigInt(newContext.valP);
        this.simStatus = newContext.simStatus;

        this.icode = newContext.icode;
        this.ifun = newContext.ifun;
        this.ra = newContext.ra;
        this.rb = newContext.rb;
        this.valC = BigInt(newContext.valC);

        this.valA = BigInt(newContext.valA);
        this.valB = BigInt(newContext.valB);
        this.srcA = newContext.srcA;
        this.srcB = newContext.srcB;

        this.dstE = newContext.dstE;
        this.dstM = newContext.dstM;

        this.aluA = BigInt(newContext.aluA);
        this.aluB = BigInt(newContext.aluB);

        this.valE = BigInt(newContext.valE);

        this.cc = newContext.cc;
        this.bcond = newContext.bcond;

        this.mem_addr = BigInt(newContext.mem_addr);
        this.mem_data = BigInt(newContext.mem_data);
        this.mem_read = newContext.mem_read;
        this.mem_write = newContext.mem_write;

        this.valM = BigInt(newContext.valM);

        this.newPC = BigInt(newContext.newPC);
    }

    save(): string {
        return JSON.stringify(this, (key, value) =>
            typeof value === 'bigint'
                ? value.toString()
                : value // return everything else unchanged
        );
    }
}

export {Context};