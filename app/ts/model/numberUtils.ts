export const INT32_MAX = (1 << 30) * 2 - 1 // '* 2'  is a little hack to counter js' floating points
export const INT32_MIN = 1 << 31

export const UINT32_MAX = (1 << 30) * 4 - 1 // Same as above, '* 4' is here to fool fp
//export const UINT64_MAX = BigInt(BigInt(1) << BigInt(62))
export const UINT64_MAX = BigInt(0xffffffffffffffff) - BigInt(1)

export function isByte(value: number): boolean {
    return value >= 0 && value <= 255
}

export function isNaN(value: any) {
    return Number.isNaN(Number(value))
}

export function is32Bits(strValue: string) {
    if (strValue.match("-?[0-9]+")?.at(0)?.length === strValue.length) {
        return BigInt(strValue) <= BigInt('2147483647') && BigInt(strValue) >= BigInt('-2147483648');
    } else if (strValue.match("0x[0-9a-fA-F]+")?.at(0)?.length === strValue.length) {
        return BigInt(strValue) <= BigInt('0xffffffff') && BigInt(strValue) >= BigInt('0x0');
    } else if (strValue.match("0b[0-1]+")?.at(0)?.length === strValue.length) {
        return BigInt(strValue) <= BigInt('0xffffffff') && BigInt(strValue) >= BigInt('0x0');
    } else {
        throw new Error("The format of the number '" + strValue + "' is not supported")
    }
}

export function is64Bits(strValue: string) {
    if (strValue.match("-?[0-9]+")?.at(0)?.length === strValue.length) {
        return BigInt(strValue) <= BigInt('9223372036854775807') && BigInt(strValue) >= BigInt('-9223372036854775808');
    } else if (strValue.match("0x[0-9a-fA-F]+")?.at(0)?.length === strValue.length) {
        return BigInt(strValue) <= BigInt('0xffffffffffffffff') && BigInt(strValue) >= BigInt('0x0');
    } else if (strValue.match("0b[0-1]+")?.at(0)?.length === strValue.length) {
        return BigInt(strValue) <= BigInt('0xffffffffffffffff') && BigInt(strValue) >= BigInt('0x0');
    } else {
        throw new Error("The format of the number '" + strValue + "' is not supported")
    }
}

export function toInt(value: bigint, wordSize: number): bigint {
    switch(wordSize){
        case 4 :
            return toInt32(value);
        case 8 :
            return toInt64(value);
        default:
            return BigInt(0);
    }
}

export function toInt32(value: bigint) {
    const maxValue = BigInt('0x100000000')
    const maxPositiveValue = BigInt('0x7fffffff');
    if (value >= maxValue){
        value &= BigInt('0xffffffff')
    }
    return value > maxPositiveValue ? value - maxValue : value
}

export function toInt64(value: bigint) {
    const maxValue = BigInt('0x10000000000000000')
    const maxPositiveValue = BigInt('0x7fffffffffffffff');
    if (value >= maxValue){
        value &= BigInt('0xffffffffffffffff')
    }
    return value > maxPositiveValue ? value - maxValue : value
}

export function toUint(value: bigint, wordSize: number): bigint {
    switch(wordSize){
        case 4 :
            return toUint32(value);
        case 8 :
            return toUint64(value);
        default:
            return BigInt(0);
    }
}

export function toUint32(value: bigint) {
    const maxValue = BigInt('0x100000000')
    if (value >= maxValue){
        value &= BigInt('0xffffffff')
    }
    return value < 0 ? maxValue + value : value
}

export function toUint64(value: bigint) {
    const maxValue = BigInt('0x10000000000000000')
    if (value >= maxValue){
        value &= BigInt('0xffffffffffffffff')
    }
    return value < 0 ? maxValue + value : value
}

export function byteArrayToBigInt(bytes: number[]): bigint {
    if(bytes.length != 8 && bytes.length != 4){
        throw new Error('ByteArray with wrong length : ' + bytes.length + " instead of 8.")
    }

    let result = BigInt(0)
    bytes.reverse().forEach((byte) => {
        if (isByte(byte)) {
            result *= BigInt(256)
            result += BigInt(byte)
        } else {
            throw new Error('Byte out of range, received ' + byte)
        }
    })

    return result
}

export function stringToNumber(strValue: string): number {
    if (strValue.match("-?[0-9]+")?.at(0)?.length === strValue.length) {
        return parseInt(strValue) & 0xffffffff
    } else if (strValue.match("0x[0-9a-fA-F]+")?.at(0)?.length === strValue.length) {
        return parseInt(strValue, 16) & 0xffffffff
    } else if (strValue.match("0b[0-1]+")?.at(0)?.length === strValue.length) {
        return parseInt(strValue.slice(2, strValue.length), 2) & 0xffffffff
    } else {
        throw new Error("The format of the number '" + strValue + "' is not supported")
    }
}

export function stringToBigInt(strValue: string): bigint {
    if (strValue.match("0x[0-9a-fA-F]+")?.at(0)?.length === strValue.length) {
        return BigInt(strValue) & BigInt('0xffffffffffffffff')
    } else if (strValue.match("[0-9]+")?.at(0)?.length === strValue.length) {
        return BigInt(strValue) & BigInt('0xffffffffffffffff')
    } else if (strValue.match("-[0-9]+")?.at(0)?.length === strValue.length) {
        return UINT64_MAX + BigInt(strValue) + BigInt(1)
    } else if (strValue.match("0b[0-1]+")?.at(0)?.length === strValue.length) {
        return BigInt(strValue) & BigInt('0xffffffffffffffff')
    } else {
        throw new Error("The format of the number '" + strValue + "' is not supported")
    }
}

export function padStringBigInt(value: string, digits: number, pad: string = '0') {
    if (pad.length === 0) {
        throw new Error("Can not pad with an empty character")
    }

    let newValue = ""

    while (newValue.length + value.length < digits) {
        newValue += pad
    }

    return newValue + value
}

export function bigIntToByteArray(value: bigint, maxBytes: number | bigint = UINT64_MAX, padToMax: boolean = false): number[] {
    if (maxBytes === 4){
        value = toUint32(value);
    }
    if (maxBytes === 8){
        value = toUint64(value);
    }

    
    let bytes = new Array<number>()

    let signBit = BigInt('0x8000000000000000');
    const sign64 = !!(BigInt(value) & signBit);

    let stringValue = value.toString(16)
    if (stringValue.length > 16) {
        stringValue = stringValue.slice(stringValue.length - 16, stringValue.length)
    }

    if (stringValue.length % 2 != 0) {
        stringValue = '0' + stringValue
    }

    for (let i = 0; i < stringValue.length; i += 2) {
        const byte = Number('0x' + stringValue.slice(i, i + 2))
        bytes.push(byte)
    }
    bytes = bytes.reverse()

    const padding = sign64 ? 0xff : 0
    while (padToMax && bytes.length < maxBytes) {
        bytes.push(padding)
    }

    return bytes
}

export function stringHexaToByteArray(value: string): number[] {
    if (!value.match('([0-9a-zA-Z][0-9a-zA-Z])+')) {
        throw new Error('The string "' + value + '" is not an hexadecimalbigint or has an oddbigint of digits')
    }

    let bytes = new Array<number>()

    for (let i = 0; i < value.length; i += 2) {
        bytes.push(stringToNumber('0x' + value.slice(i, i + 2)))
    }

    return bytes
}