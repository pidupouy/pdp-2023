import { Instruction } from '../instruction'


/**
 * Represents a consistent set of instructions.
 */
export interface IInstructionSet {
    /**
     * Helper function to call addInstruction method multiple times.
     * The raw instructions are converted into standard instructions during the process.
     * @param instructions All the instructions to add.
     */
    addInstructions(instructions : Instruction[]) : void;

    /**
     * Adds the instruction if and only if the instruction set is still
     * in a consistent state after the insertion.
     * If the instruction can not be added, an exception is thrown.
     * @param instruction
     */
    addInstruction(instruction : Instruction) : void;

    get(name : string) : Instruction;

    getInstructions(name : string) : Instruction[]

    getInstructionName(icode : number, ifun : number) : string;

    /**
     * Clears all ths instructions from the instruction set.
     */
    clear() : void;
}