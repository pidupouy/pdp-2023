export interface IMemory {
    /**
     * Return the number of bytes in a word
     * 32 bits : 4
     * 64 bits : 8
     */
    getWordSize(): number;

    /**
     * Loads in memory a .yo program.
     * The given string is supposed to be correct.
     * Therefore, the format is not checked and incorrect
     * lines are not used.
     * @param yo
     */
    loadProgram(yo: string): void;

    /**
     * Inserts a word starting at the given address.
     * If the register is 0xddccbbaa and we write it at 0x6,
     * the memory will be :
     * ...
     * 0x4 : 00 00 aa bb
     * 0x8 : cc dd 00 00
     * ...
     * @param address
     * @param word
     */
    writeWord(address: bigint, word: bigint): void;

    /**
     * Inserts a byte at the given address.
     * @param address
     * @param word
     */
    writeByte(address: bigint, byte: number): void;

    /**
     * Returns a word starting at the given address.
     * If the memory is :
     * ...
     * 0x4 : aa bb cc dd
     * 0x8 : ee ff 00 11
     * ...
     * and we read at 0x4, we will get the 0xddccbbaa word.
     * @param address
     */
    readWord(address: bigint): bigint;

    /**
     * Returns the byte at the given address.
     * @param address
     */
    readByte(address: bigint): number;

    /**
     * Update current memory with next values if necessary
     */
    updateMemory(): void;

    /**
     * Return the current state of the memory
     * - wordSize : number of bytes in a word
     * - startAddress : the first address in memory
     * - maxAddress : the last address in memory
     * - modifiedMem : Set with all the address that have been modified and their word. This doesn't count the original program loaded in memory.
     * - bytes : Map with all the bytes in memory.
     */
    getMemoryView(): {
        "wordSize": number,
        "startAddress": number,
        "maxAddress": number,
        "modifiedMem": Map<bigint, bigint>,
        "bytes": Map<bigint, number>,
    };

    /**
     * Return the next state of the memory
     * - wordSize : number of bytes in a word
     * - address : the address of the next word to be written
     * - isWrite : state if the next word will be written. It is put to true after a writeWord and put to false after a updateMemory.
     * This allow the memory to be updated at the next cycle
     * - bytes : the byte array of the next word to be written
     */
    getNextMemoryView(): {
        "wordSize": number,
        "address": bigint,
        "isWrite": boolean,
        "bytes": number[],
    };

    /**
     * Update values with new ones
     * @param newFlags JSON that contains new values
     * @format { "current" : [{"address" : "value", "value" : "value"}], "next" : { "address": "value", "isWrite": value, "bytes": "value"}}
     */
    load(newMemory: { "current" : {"address" : string, "value" : string}[], "next" : { "address": string, "isWrite": boolean, "bytes": string}}): void;

    /**
     * Save values in JSON string
     * @format "{ "current" : [{"address" : "value", "value" : "value"}], "next" : { "address": "value", "isWrite": value, "bytes": "value"}"
     */
    save(): string;
}