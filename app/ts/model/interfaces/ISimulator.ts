/**
 * Enum with the different states the processor can be in.
 */
export enum simStatus {
    NONE,
    AOK,
    HALT,
    ADDR,
    INSTR,
    BUBBLE,
    PIPE,
}

/**
 * Dictionary used to read load files and convert STAT into simStatus enum index
 */
export const convert_dict_status: { [index: string]: simStatus } = {
    "NONE": simStatus.NONE, "AOK": simStatus.AOK, "HALT": simStatus.HALT,
    "ADDR": simStatus.ADDR, "INSTR": simStatus.INSTR, "BUBBLE": simStatus.BUBBLE,
    "PIPE": simStatus.PIPE
};

export interface ISimulator {
    /**
     * Makes a step and returns the status after the execution.
     */
    step() : void

    /**
     * Executes steps until the program finish or reach a breakpoint.
     * maxSteps < 0 means no limits.
     * @param breakpoints
     * @param maxSteps
     */
    continue(breakpoints : Array<bigint>, maxSteps : number) : void

    /**
     * Reset all the components of the simulator.
     */
    reset() : void

    /**
     * Return to the previous step.
     */
    undo() : void

    /**
     * Load json data which contains the machineState
     * @param jsonContent
     */
    load(jsonContent: any) : void

    /**
     * Return the machineState as a string in json data format
     */
    save() : string

    /**
     * Takes a compiled ys program and injects it into its memory.
     * Throws an exception if an error occurs.
     * @param yo
     */
    loadProgram(yo : string) : void

    /**
     * Injects HCL code.
     * @param js
     */
    insertHclCode(js : string) : void

    /**
     * Returns a json holding CPU stages content.
     * It is of the form : { "stageName1" : {"columns" : ["col1", "col2", ...], "lines" : [lineObject1, lineObject2, ...]},
     * "stageName2" : {"columns" : ["col1", "col2", ...], "lines": [lineObject1, lineObject2, ...]}, ...}
     * Each "lineObject" is of the form : {"name" : "line1", "labels" : ["label1", "label2", ...], "values" : [val1, val2, ...]}.
     * For more information, read the "Tweak UI component's content layout" > "CPU State" section in the how_to.md file.
     * You can find examples in each sim.ts file for each kernel.
     */
    getStageView() : any

    /**
     * Returns a json holding current registers names and their values.
     * @format { "registerName" : bigint, ... }
     */
    getRegistersView() : { [key : string] : bigint };

    /**
     * Returns a json holding next registers names and their values.
     * @format { "registerName" : bigint, ... }
     */
    getNextRegistersView() : { [key : string] : bigint };

    /**
     * Return the current state of the memory
     * - wordSize : number of bytes in a word
     * - startAddress : the first address in memory
     * - maxAddress : the last address in memory
     * - modifiedMem : Set with all the address that have been modified and their word. This doesn't count the original program loaded in memory.
     * - bytes : Map with all the bytes in memory.
     */
    getMemoryView() : {
        "wordSize": number,
        "startAddress": number,
        "maxAddress": number,
        "modifiedMem": Map<bigint, bigint>,
        "bytes": Map<bigint, number>
    };

    /**
     * Return the next state of the memory
     * - wordSize : number of bytes in a word
     * - address : the address of the next word to be written
     * - isWrite : state if the next word will be written. It is put to true after a writeWord and put to false after a updateMemory.
     * This allow the memory to be updated at the next cycle
     * - bytes : the byte array of the next word to be written
     */
    getNextMemoryView() : {
        "wordSize": number,
        "address": bigint,
        "isWrite": boolean,
        "bytes": number[]
    };

    /**
     * Returns a json holding current flags names and their values.
     * @format { "flagName" : boolean, ... }
     */
    getFlagsView() : { [key : string] : boolean };

    /**
     * Returns a json holding next flags names and their values.
     * @format { "flagName" : boolean, ... }
     */
    getNextFlagsView() : { [key : string] : boolean };

    /**
     * Returns a json holding current status of the simulator, possible error messages and pc.
     */
    getStatusView() : {
        "STAT": simStatus,
        "ERR": string,
        "PC": bigint
    };

    /**
     * Returns a json holding 3 values :
     * - Cycles : number of steps since the end of the first instruction
     * - Instructions : number of instructions treated since the end of the first instruction
     * - CPI : ratio of instructions done per cycles.
     * @format { "valueName" : number, ... }
     * Non-pipelined kernels return undefined.
     */
    getPerformancesView() : { [key : string] : number } | undefined ;

    /**
     * Returns an array of objects describing the lines in the object code to be highlighted.
     * It is of the form : [{"address" :  val1, "tag" : tag1}, ... {"address" : valN, "tag" : tagN}]
     * Every instructions whose address matches the address property of an object in the array will be highlighted and the rule's tag
     * will be applied on this instruction's object code.
     * NOTE :
     * The color used corresponds to the first matched rule. For example, if an instruction matches the 2nd and 3rd rule, the 2nd color variant will
     * be applied.
     * You may define up to 10 rules MAX.
     * All the rule's tag can cumulate if an instruction matches several rules.
     * If the total length of the applied tags on a code line exceeds GUTTER_SIZE (yas.ts), the applied tag will be replaced with as many "*" as
     * the gutter size.
     */
    getHighlightRules() : {
        "address": bigint,
        "tag": string
    }[];
    
    /**
     * Returns the current number of steps done
     */
    getStepNum() : number

    /**
     * Returns whether the CPU State interface should be tightened or not.
     */
    isInterfaceTightened() : boolean

    /**
     * Update the simulator with the values of the previous instruction
     */
    updateSim() : void
}
