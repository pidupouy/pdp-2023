/**
 * Enum for jmp flags.
 */
export enum JMP_enum {
    J_YES,
    J_LE,
    J_L,
    J_E,
    J_NE,
    J_GE,
    J_G
}

/**
 * Enumerate for alu operand.
 */
export enum alufct {
    A_ADD,
    A_SUB,
    A_AND,
    A_XOR,
    A_SAL,
    A_SAR,
    A_NONE
};

/**
 * Enum for place flag's places in alu flags array.
 *   - ZF : Zero flag - when the result is zero
 *   - OF : Overflow flag - when computation overflow
 *   - SF : Sign flag - When the sign bit is at one.
 */
export enum CC {
    NONE = 0,
    ZF = 1,
    SF = 2,
    OF = 3,
}

/**
 * Dictionary used to read load files and convert flag name into flag enum index
 */
export const convert_dict_flags: { [index: string]: CC } = {
    "ZF": CC.ZF,
    "OF": CC.OF,
    "SF": CC.SF,
    "none": CC.NONE
};

export interface IAlu {
    /**
     * Make a single computation with aluA and aluB parameters according to OP given by alu_fun.
     * Operation is computed with Uint32Array or BigUint64Array to force the operation to use only 32 or 64 bits
     * @param aluA Left member to compute
     * @param aluB Right member to compute
     * @param alu_fun The operator of computation. It must be an alufct enum type.
     */
    computeAlu(aluA: bigint, aluB: bigint, alu_fun: alufct): bigint;

    /**
     * Set the flag to the correct value according the calculation asked in parameters.
     * Operation is computed with Uint32Array or BigUint64Array to force the operation to use only 32 or 64 bits
     * Necessary function according to HCL because of its "set_cc" function.
     * @param aluA Left operand
     * @param aluB Right operand
     * @param alu_fun Operand Indexed by ::alufct.
     */
    computeCC(aluA: bigint, aluB: bigint, alu_fun: alufct): void;

    /**
     * Return if the execution stage allow the conditional branchement with a jmp.
     * By default, this function is called by execution stage when hcl function is_bch returned
     * a true value, when user defined that the instruction should make a jump.
     * @param ifun The ifun of the jmp function.
     */
    computeBch(ifun: number): boolean;

    /**
     * Update current flags value with next flags value
     */
    updateFlags(): void;

    /**
     * Returns zero flag.
     */
    getZF(): boolean;

    /**
     * Returns zero flag of the next instruction.
     */
    getNextZF(): boolean;

    /**
     * Returns sign flag.
     */
    getSF(): boolean;

    /**
     * Returns sign flag of the next instruction.
     */
    getNextSF(): boolean;

    /**
     * Returns overflow flag.
     */
    getOF(): boolean;

    /**
     * Returns overflow flag of the next instruction.
     */
    getNextOF(): boolean;

    /**
     * Update values with new ones
     * @param newFlags JSON that contains new values
     * @format { "current" : {"flagName" : flagValue}, "next" : {"flagName" : flagValue}}
     */
    load(newFlags: { [key: string]: { [key: string]: boolean } }): void;

    /**
     * Save values in JSON string
     * @format "{ "current" : {"flagName" : flagValue}, "next" : {"flagName" : flagValue}}"
     */
    save(): string;
}
