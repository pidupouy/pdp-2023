/**
 * Enum wich contains all the registers plus the special register "none".
 */
export enum registers_enum {
    eax,
    ecx,
    edx,
    ebx,
    esp,
    ebp,
    esi,
    edi,
    r8,
    r9,
    r10,
    r11,
    r12,
    r13,
    r14,
    none = 0xf,
}

/**
 * Enum used only in the yas Compiler to limit the available registers in 32 bits kernels
 */
export enum registers_enum32bits {
    eax,
    ecx,
    edx,
    ebx,
    esp,
    ebp,
    esi,
    edi,
    none = 0xf,
}

export const registersList32bits = ["eax", "ecx", "edx", "ebx", "esp", "ebp", "esi", "edi"];
export const registersList64bits = ["eax", "ecx", "edx", "ebx", "esp", "ebp", "esi", "edi", "r8", "r9", "r10", "r11", "r12", "r13", "r14"];

/**
 * Dictionary used to read load files and convert register name into register enum index
 */
export const convert_dict_register: { [index: string]: registers_enum } = {
    "eax": registers_enum.eax, "ebx": registers_enum.ebx, "ecx": registers_enum.ecx,
    "edx": registers_enum.edx, "esi": registers_enum.esi, "edi": registers_enum.edi,
    "ebp": registers_enum.ebp, "esp": registers_enum.esp, "r8": registers_enum.r8,
    "r9": registers_enum.r9, "r10": registers_enum.r10, "r11": registers_enum.r11,
    "r12": registers_enum.r12, "r13": registers_enum.r13, "r14": registers_enum.r14,
    "none": registers_enum.none
};

export interface IRegisters {
    /**
     * Write Word in register @param register.
     * Given value will be cast to unsigned, then store in Uint32Array or 
     * BigUint64Array to only keep first 32 or 64 bits
     * @param register
     * @param value
     */
    write(register: registers_enum, value: bigint): void;

    /**
     * Return value contains in one register.
     * Returning value is cast in BigInt from Uint32Array or BigUint64Array
     * @param register
     */
    read(register: registers_enum): bigint

    /**
     * Return next value contains in one register.
     * Returning value is cast in BigInt from Uint32Array or BigUint64Array
     * @param register
     */
    readNext(register: registers_enum): bigint

    /**
     * Update current registers value with next registers value
     */
    updateRegisters(): void

    /**
     * Return the string name of the register given in parameter.
     * @param index Index of the register, according to the registers_enum.
     */
    getRegisterName(register: registers_enum): string

    /**
     * Update values with new ones
     * @param newRegisters JSON that contains new values
     * @format { "current" : {"registerName" : "registerValue"}, "next" : {"registerName" : "registerValue"}}
     */
    load(newRegisters: { [key: string]: { [key: string]: string } }): void

    /**
     * Save values in JSON string
     * @format "{ "current" : {"registerName" : "registerValue"}, "next" : {"registerName" : "registerValue"}}"
     */
    save(): string
}