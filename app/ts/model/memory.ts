import { IMemory } from "./interfaces/IMemory"
import { MemoryException } from "./exceptions/simulatorException"
import { byteArrayToBigInt, bigIntToByteArray, isByte, stringToBigInt, padStringBigInt, stringHexaToByteArray, toUint } from "./numberUtils"

class Memory implements IMemory {
    static LAST_ADDRESS = 0x2000

    private _wordSize: number;

    private _content = new Map<bigint, number>()

    private _changedWordAddress = new Set<bigint>()

    private _nextIsWrite: boolean = false;
    private _nextWord: bigint = BigInt(0);
    private _nextAddress: bigint = BigInt(0);

    constructor(wordSize : number){
        this._wordSize = wordSize;
    }

    getWordSize() {
        return this._wordSize;
    }

    loadProgram(yo: string) {
        const lines = yo.split("\n")

        lines.forEach((line, index) => {
            const splittedLine = line.split("|")

            if (splittedLine.length == 1) {
                return // Line is considered as empty
            }

            const instructionsWithAddress = splittedLine[0]
            const instructionsWithAddressSplitted = instructionsWithAddress.split(":")

            if (instructionsWithAddressSplitted.length == 1) {
                return // There is no instruction on this line
            }
            else if (instructionsWithAddressSplitted.length != 2) {
                throw new Error("Line " + index + " : Invalid format (no or multiple ':' separators) --> '" + instructionsWithAddress + "'")
            }

            if (instructionsWithAddressSplitted.length == 2) {
                const firstAddress = BigInt(instructionsWithAddressSplitted[0])
                const instructionAsString = instructionsWithAddressSplitted[1].trim()

                if (instructionAsString.length == 0) {
                    return
                }

                const instruction = stringHexaToByteArray(instructionAsString)

                Memory._checkAddress(firstAddress, BigInt(instruction.length))

                if (Number.isNaN(firstAddress)) {
                    throw new Error('The given address (' + firstAddress + ') is not abigint')
                }

                instruction.forEach((byte, offset) => {
                    this.writeByte(firstAddress + BigInt(offset), byte)
                })
                // The program code is not supposed to be considered as changed memory
                this._changedWordAddress.clear()
            }
        })
    }

    writeWord(address: bigint, word: bigint) {
        this._nextIsWrite = true;
        this._nextAddress = address;
        this._nextWord = word;
    }

    writeByte(address: bigint, byte: number) {
        Memory._checkAddress(address, BigInt(0))
        this._content.set(address, byte)

        const wordAddress = address - (address % BigInt(this._wordSize))
        this._changedWordAddress.add(wordAddress)
    }

    readWord(address: bigint): bigint {
        let bytes = new Array<number>()

        for (let i = BigInt(0); i < this._wordSize; i++) {
            bytes.push(this.readByte(BigInt(address + i)))
        }

        return BigInt(byteArrayToBigInt(bytes));
    }

    readByte(address: bigint): number {
        Memory._checkAddress(address, BigInt(0));

        const byte = this._content.get(address)
        return byte == undefined ? 0 : byte;
    }

    updateMemory() {
        if (this._nextIsWrite) {
            const bytes = bigIntToByteArray(this._nextWord, this._wordSize, true)
            bytes.forEach((byte, offset) => {
                this.writeByte(this._nextAddress + BigInt(offset), byte)
            })
            this._nextIsWrite = false;
        }
    }

    getMemoryView() {
        let modifiedWords = new Map<bigint, bigint>()

        this._changedWordAddress.forEach((wordAddress) => {
            const word = BigInt(this.readWord(BigInt(wordAddress)));
            if (word != BigInt(0)) {
                modifiedWords.set(BigInt(wordAddress), word)
            }
        })

        return {
            "wordSize": this.getWordSize(),
            "startAddress": 0,
            "maxAddress": Memory.LAST_ADDRESS,
            "modifiedMem": modifiedWords,
            "bytes": this._content,
        }
    }

    getNextMemoryView() {
        return {
            "wordSize": this.getWordSize(),
            "address": this._nextAddress,
            "isWrite": this._nextIsWrite,
            "bytes": bigIntToByteArray(this._nextWord, this._wordSize, true),
        }
    }

    load(newMemory: { "current": { "address": string, "value": string }[], "next": { "address": string, "isWrite": boolean, "bytes": string } }): void {
        for (let memoryLine of newMemory.current) {
            this.writeWord(stringToBigInt("0x" + memoryLine.address), byteArrayToBigInt(stringHexaToByteArray(memoryLine.value)));
            this.updateMemory();
        }
        this._nextAddress = stringToBigInt("0x" +newMemory.next.address);
        this._nextIsWrite = newMemory.next.isWrite;
        this._nextWord = toUint(stringToBigInt(newMemory.next.bytes), this._wordSize);
    }

    save(): string {
        const memoryView = this.getMemoryView().modifiedMem;
        let content: any[] = []

        memoryView.forEach((word: bigint, address: bigint) => {
            const wordAsString = byteArrayToBigInt(bigIntToByteArray(word, this.getWordSize(), true).reverse())
            content.push({
                address: padStringBigInt(address.toString(16), this.getWordSize()),
                value: padStringBigInt(wordAsString.toString(16), this.getWordSize() * 2)
            })
        })

        let contentNext = {
            "address": padStringBigInt(this._nextAddress.toString(16), this.getWordSize()),
            "isWrite": this._nextIsWrite,
            "bytes": this._nextWord.toString(10)
        }

        return "{ \"current\" : " + (JSON.stringify(content, (key, value) =>
            typeof value === 'bigint'
                ? value.toString()
                : value // return everything else unchanged
        )) + ", \"next\" : " + (JSON.stringify(contentNext, (key, value) =>
            typeof value === 'bigint'
                ? value.toString()
                : value // return everything else unchanged
        )) + "}";
    }

    static byteArrayToWord(bytes: number[], wordSize: number) {
        if (bytes.length > wordSize) {
            throw new Error("A word can not hold more than " + wordSize + " bytes (current : " + bytes.length + ")")
        }
        return byteArrayToBigInt(bytes)
    }

    /**
     * Returns the 4 HSB of the current byte.
     */
    static HI4(value: number): number {
        if (!isByte(value)) {
            throw new Error("Memory.HI4() was expecting a byte as argument (current : " + value + ")")
        }
        return (value >> 4) & 0xf
    }

    /**
     * Returns the 4 LSB of the current byte.
     */
    static LO4(value: number): number {
        if (!isByte(value)) {
            throw new Error("Memory.LO4() was expecting a byte as argument (current : " + value + ")")
        }
        return value & 0xf
    }

    private static _checkAddress(address: bigint, size: bigint) {
        if (address < BigInt(0) || address + size > BigInt(Memory.LAST_ADDRESS)) {
            throw new MemoryException(address);
        }
    }
}

export { Memory }