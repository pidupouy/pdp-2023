import { IInstructionSet } from './interfaces/IInstructionSet'
import { Instruction } from './instruction'

/**
 * Represents a set of instructions.
 * This class takes the responsibility to check the coherency
 * of all its instructions.
 */
export class InstructionSet implements IInstructionSet {
    /**
     * Handler of all the instructions of the set.
     * To each instruction name is associated a list of all Instruction possibility.
     */
    private handle = new Map<string, Instruction[]>();

    private handleBackup = new Map<string, Instruction[]>();
    /**
     * The size of a processor word.
     */
    private wordSize : number;

    constructor(defaultInstructions : Instruction[], wordSize : number) {
        this.wordSize = wordSize;
        this.addInstructions(defaultInstructions);
    }

    addInstructions(instructions: Instruction[]) {
        try {
            instructions.forEach((item) => {
                this.addInstruction(new Instruction(item.name, item.icode, item.ifun, item.args, this.wordSize));
            })
        } catch (error) {
            this.handle = this.handleBackup;
            throw error;
        }
    }

    addInstruction(instruction: Instruction) {
        this.handle.forEach((instrList) => {
            let instr0 = instrList[0];
            if (instr0.name === instruction.name && instr0.args === instruction.args) {
                throw new Error("The instruction '"+instr0.name+"' with the same format string already exists");
            }
        })
        // The new instruction is valid, it is added to the instruction array which is associated to its name
        let instruction_list = this.handle.get(instruction.name);
        if(instruction_list != null){
            instruction_list.push(instruction);
            this.handle.set(instruction.name, instruction_list);
        }else{
            this.handle.set(instruction.name, [instruction]);
        }
    }

    getInstructions(name : string) : Instruction[] {
        let instructions = this.handle.get(name);
        if(instructions !== undefined){
            return instructions;
        } else {
            throw new Error("Instruction " + name + " does not exist");
        }
    }

    get(name : string) : Instruction {
        let instructions = this.handle.get(name);
        if(instructions !== undefined){
            return instructions[0];
        } else {
            throw new Error("Instruction " + name + " does not exist");
        }
    }

    getInstructionName(icode : number, ifun : number) : string {
        let name = "";
        this.handle.forEach((value, key) => {
            value.forEach((value, key) => {
                if (value.icode == icode && value.ifun == ifun){
                    name = value.name;
                }
            });
        });
        return name;
    }

    clear() {
        this.handleBackup = this.handle;
        this.handle = new Map<string, Instruction[]>();
    }
}