"use strict"
// Written Docs for chevrotain visitor can be found here
// https://chevrotain.io/docs/tutorial/step3a_adding_actions_visitor.html

import { CompilationError, CompilationResult } from "../interfaces/ICompiler";
import { HclParser } from "./hclParser";

const parserInstance = new HclParser()
// The base visitor class can be accessed via the a parser instance.
const BaseCstVisitor = parserInstance.getBaseCstVisitorConstructor()

class HclVisitor extends BaseCstVisitor {
  line = 1 // To be suppr
  quoteList = [] // No idea -> Store the quote ??
  intsigs = [] // Pairs intsig <-> value
  boolsigs = [] // Pairs boolsig <-> value
  intDefinitions = [] // Pairs int function <-> instruction list 
  boolDefinitions = [] // Pairs bool function <-> instruction
  identifiersList = [] //No idea -> Store the identifiers ??
  errors = [] // List all encountered errors
  
  constructor() {
    super()
    this.validateVisitor()
  }
  
  /* ------------------ Utility function ------------------ */
  
  // Checks if both intsig and boolsig have not any
  // value associated to the given identifier
  checkSigUnicity(identifier) {
    if (this.intsigs[identifier[0].image]) {
      this.errors.push(new CompilationError(identifier[0].startLine, identifier[0].image + " is already declared as an intsig"))
    }
    if (this.boolsigs[identifier[0].image]) {
      this.errors.push(new CompilationError(identifier[0].startLine, identifier[0].image + " is already declared as a boolsig"))
    }
  }
  
  // Checks if both int definitions and bool definitions have not any
  // value associated to the given identifier
  checkDefinitionUnicity(identifier) {
    if (this.intDefinitions[identifier[0].image]) {
      this.errors.push(new CompilationError(identifier[0].startLine, identifier[0].image + " is already defined as an int defintion"))
    }
    if (this.boolDefinitions[identifier[0].image]) {
      this.errors.push(new CompilationError(identifier[0].startLine, identifier[0].image + " is already defined as an bool defintion"))
    }
  }
  
  // HCL strings are parsed as 'text'.
  // This function returns text, without the ' character
  // It assumes the given string is of the form 'text'.
  cleanHclString(str) {
    return str.substring(1, str.length - 1)
  }
  
  sanitizeString(str) {
    return str.replace(/"/g, "'")
  }
  
  // Returns a intsig or boolsig value
  // using the given identifier as key.
  getSigValue(identifier) {
    let jsSigName = "'none'"
    
    if (this.intsigs[identifier[0].image]) {
      jsSigName = this.intsigs[identifier[0].image]
    } else if (this.boolsigs[identifier[0].image]) {
      jsSigName = this.boolsigs[identifier[0].image]
    } else {
      this.errors.push(new CompilationError(identifier[0].startLine, identifier[0].image + " is not declared"))
    }
    
    let finalValue = this.cleanHclString(jsSigName)
    this.identifiersList.push(finalValue)
    
    return finalValue
  }
  
  generateIdentifiersVerificationJs(identifiersList, functionName) {
    if (identifiersList.length === 0) {
      return ""
    }
    
    let jsOutput = "   // Checks if some identifiers are undefined\n"
    
    identifiersList.forEach((identifier) => {
      jsOutput += "   try { if(" + identifier + " === undefined) { throw '' } } catch(e) { throw \"HCL : "
      + this.sanitizeString(identifier) + " is not accessible in function '" + functionName + "'\" }\n"
    })
    jsOutput += "   // End of checks\n\n"
    
    return jsOutput
  }
  
  /* ------------------ Visitor ------------------ */
  hcl(ctx) {
    this.visit(ctx.statements)
    
    let jsOutput = "new function() {\n\n"
    
    //Step 1 : Render user's quotes
    this.quoteList.forEach(function (item) {
      this.jsOutput += item + "\n\n"
    })
    
    //Step 2 : Render int definitions
    for (let name in this.intDefinitions) {
      const instr = this.intDefinitions[name].definition
      const identifiersList = this.intDefinitions[name].identifiersList
      
      jsOutput += "this." + name + " = () => {\n"
      jsOutput += this.generateIdentifiersVerificationJs(identifiersList, name)
      jsOutput += "   return " + instr + ";\n}\n\n"
    }
    
    //Step 3 : Render bool definitions
    for (let name in this.boolDefinitions) {
      const instr = this.boolDefinitions[name].definition
      const identifiersList = this.boolDefinitions[name].identifiersList
      
      jsOutput += "this." + name + " = () => {\n"
      jsOutput += this.generateIdentifiersVerificationJs(identifiersList, name)
      jsOutput += "   return " + instr + ";\n}\n\n"
    }
    
    jsOutput += "}"
    
    return {
      type: "HCL",
      jsOutput: jsOutput,
      errors: this.errors
    }
  }
  
  /* ------------------ Visitor : statements ------------------ */
  
  statements(ctx) {
    if(ctx.statement) {
      for (const statement of ctx.statement) {
        this.visit(statement)
      }
    }
  }
  
  statement(ctx) {
    if (ctx.quote_statement) {
      this.visit(ctx.quote_statement)
    } else if (ctx.boolarg_statement) {
      this.visit(ctx.boolarg_statement)
    } else if (ctx.intarg_statement) {
      this.visit(ctx.intarg_statement)
    } else if (ctx.bool_statement) {
      this.visit(ctx.bool_statement)
    } else if (ctx.int_statement) {
      this.visit(ctx.int_statement)
    }
  }
  
  quote_statement(ctx) {
    this.quoteList.push(this.cleanHclString(ctx.QSTRING[0].image))
  }
  
  boolarg_statement(ctx) {
    this.checkSigUnicity(ctx.VAR)
    this.boolsigs[ctx.VAR[0].image] = ctx.QSTRING[0].image
  }
  
  intarg_statement(ctx) {
    this.checkSigUnicity(ctx.VAR)
    this.intsigs[ctx.VAR[0].image] = ctx.QSTRING[0].image
  }
  
  bool_statement(ctx) {
    this.checkDefinitionUnicity(ctx.VAR)
    const expr_value = this.visit(ctx.expr).value
    const content = {
      definition: expr_value,
      identifiersList: this.identifiersList
    }
    this.boolDefinitions[ctx.VAR[0].image] = content
    this.identifiersList = []
  }
  
  int_statement(ctx) {
    this.checkDefinitionUnicity(ctx.VAR)
    const expr_value = this.visit(ctx.expr).value
    const content = {
      definition: expr_value,
      identifiersList: this.identifiersList
    }
    this.intDefinitions[ctx.VAR[0].image] = content
    this.identifiersList = []
  }
  
  /* ------------------ Visitor : expression ------------------ */
  
  expr(ctx) {
    if (ctx.expr) {
      const expr_value_left = this.visit(ctx.expr_and).value
      const expr_value_right = this.visit(ctx.expr).value
      const operator_value = ctx.OR[0].image
      return {
        type: "EXPR",
        value: expr_value_left + operator_value + expr_value_right
      }
    } else {
      return {
        type: "EXPR",
        value: this.visit(ctx.expr_and).value
      }
    } 
  }
  
  expr_and(ctx) {
    if (ctx.expr) {
      const expr_value_left = this.visit(ctx.expr_bitwise_or).value
      const expr_value_right = this.visit(ctx.expr).value
      const operator_value = ctx.AND[0].image
      return {
        type: "EXPR_AND",
        value: expr_value_left + operator_value + expr_value_right
      }
    } else {
      return {
        type: "EXPR_AND",
        value: this.visit(ctx.expr_bitwise_or).value
      }
    } 
  }
  
  expr_bitwise_or(ctx) {
    if (ctx.expr) {
      const expr_value_left = this.visit(ctx.expr_xor).value
      const expr_value_right = this.visit(ctx.expr).value
      const operator_value = ctx.BITWISE_OR[0].image
      return {
        type: "EXPR_XOR",
        value: expr_value_left + operator_value + expr_value_right
      }
    } else {
      return {
        type: "EXPR_XOR",
        value: this.visit(ctx.expr_xor).value
      }
    } 
  }
  
  expr_xor(ctx) {
    if (ctx.expr) {
      const expr_value_left = this.visit(ctx.expr_bitwise_and).value
      const expr_value_right = this.visit(ctx.expr).value
      const operator_value = ctx.XOR[0].image
      return {
        type: "EXPR_XOR",
        value: expr_value_left + operator_value + expr_value_right
      }
    } else {
      return {
        type: "EXPR_XOR",
        value: this.visit(ctx.expr_bitwise_and).value
      }
    } 
  }
  
  expr_bitwise_and(ctx) {
    if (ctx.expr) {
      const expr_value_left = this.visit(ctx.expr_equal).value
      const expr_value_right = this.visit(ctx.expr).value
      const operator_value = ctx.BITWISE_AND[0].image
      return {
        type: "EXPR_XOR",
        value: expr_value_left + operator_value + expr_value_right
      }
    } else {
      return {
        type: "EXPR_XOR",
        value: this.visit(ctx.expr_equal).value
      }
    } 
  }
  
  expr_equal(ctx) {
    if (ctx.DIFF) {
      const expr_value_left = this.visit(ctx.expr_comp).value
      const expr_value_right = this.visit(ctx.expr).value
      const operator_value = ctx.DIFF[0].image
      return {
        type: "EXPR_EQUAL",
        value: expr_value_left + operator_value + expr_value_right
      }
    } else if (ctx.EQUAL) {
      const expr_value_left = this.visit(ctx.expr_comp).value
      const expr_value_right = this.visit(ctx.expr).value
      const operator_value = ctx.EQUAL[0].image
      return {
        type: "EXPR_EQUAL",
        value: expr_value_left + operator_value + expr_value_right
      }
    } else {
      return {
        type: "EXPR_EQUAL",
        value: this.visit(ctx.expr_comp).value
      }
    } 
  }
  
  expr_comp(ctx) {
    if (ctx.GREATER_EQUAL_THAN) {
      const expr_value_left = this.visit(ctx.term).value
      const expr_value_right = this.visit(ctx.expr).value
      const operator_value = ctx.GREATER_EQUAL_THAN[0].image
      return {
        type: "EXPR_COMP",
        value: expr_value_left + operator_value + expr_value_right
      }
    } else if (ctx.LESS_EQUAL_THAN) {
      const expr_value_left = this.visit(ctx.term).value
      const expr_value_right = this.visit(ctx.expr).value
      const operator_value = ctx.LESS_EQUAL_THAN[0].image
      return {
        type: "EXPR_COMP",
        value: expr_value_left + operator_value + expr_value_right
      }
    } else if (ctx.GREATER_THAN) {
      const expr_value_left = this.visit(ctx.term).value
      const expr_value_right = this.visit(ctx.expr).value
      const operator_value = ctx.GREATER_THAN[0].image
      return {
        type: "EXPR_COMP",
        value: expr_value_left + operator_value + expr_value_right
      }
    } else if (ctx.LESS_THAN) {
      const expr_value_left = this.visit(ctx.term).value
      const expr_value_right = this.visit(ctx.expr).value
      const operator_value = ctx.LESS_THAN[0].image
      return {
        type: "EXPR_COMP",
        value: expr_value_left + operator_value + expr_value_right
      }
    } else {
      return {
        type: "EXPR_COMP",
        value: this.visit(ctx.term).value
      }
    }
  }
  
  term(ctx) {
    if (ctx.paren_expr) {
      return this.visit(ctx.paren_expr)
    } else if (ctx.brace_expr) {
      return this.visit(ctx.brace_expr)
    } else if (ctx.brack_expr) {
      return this.visit(ctx.brack_expr)
    } else if (ctx.not_expr) {
      return this.visit(ctx.not_expr)
    } else if (ctx.factor) {
      return this.visit(ctx.factor)
    }
  }
  
  paren_expr(ctx) {
    const expr_value = this.visit(ctx.expr).value
    const lparen_value = ctx.LPAREN[0].image
    const rparen_value = ctx.RPAREN[0].image
    return {
      type: "PAREN_EXPR",
      value: lparen_value + expr_value + rparen_value
    }
  }
  
  brack_expr(ctx) {
    let expr_value = ""
    const case_list = this.visit(ctx.case_list).list
    
    let i = 0
    for (i = 0; i < case_list.length; i++) {
      expr_value += "(" + case_list[i].condition + ") ? "
      expr_value += "(" + case_list[i].value + ") : "
    }
    expr_value += '0'
    
    return {
      type: "BRACK_EXPR",
      value: expr_value
    }
  }
  
  brace_expr(ctx) {
    const expr_value = "(" + this.visit(ctx.factor).value + ")"
    const expr_list = this.visit(ctx.expr_list).list
    let expr_list_value = ""
    
    for (let i=0; i<expr_list.length; i++) {
      if (i == 0) {
        expr_list_value += "(" + expr_value + " === (" + expr_list[i] + "))"
      } else {
        expr_list_value += " || (" + expr_value + " === (" + expr_list[i] + "))"
      }
    }
    
    return {
      type: "BRACE_EXPR",
      value: "(" + expr_list_value + ")" 
    }
  }
  
  not_expr(ctx) {
    const expr_value = this.visit(ctx.expr).value
    const not_value = ctx.NOT[0].image
    return {
      type: "NOT_EXPR",
      value: not_value + expr_value
    }
  }
  
  not_expr(ctx) {
    const expr_value = this.visit(ctx.expr).value
    const not_value = ctx.NOT[0].image
    return {
      type: "NOT_EXPR",
      value: not_value + expr_value
    }
  }
  
  not_expr(ctx) {
    const expr_value = this.visit(ctx.expr).value
    const not_value = ctx.NOT[0].image
    return {
      type: "NOT_EXPR",
      value: not_value + expr_value
    }
  }
  
  expr_list(ctx) {
    const list_value = []
    for (const expr of ctx.expr) {
      list_value.push(this.visit(expr).value)
    }
    return {
      type: "EXPR_LIST",
      list: list_value
    }
  }
  
  case_list(ctx) {
    const list_value = []
    if (ctx.left.length != ctx.right.length) {
      this.errors.push(new CompilationError(-1, "wrong declaration of case_list"))
      return {
        type: "CASE_LIST",
        list: list_value
      }
    }
    
    let i = 0
    for (i = 0; i < ctx.left.length; i++) {
      const content = {
        condition: this.visit(ctx.left[i]).value,
        value: this.visit(ctx.right[i]).value
      }
      list_value.push(content)
    }
    
    return {
      type: "CASE_LIST",
      list: list_value
    }
  }
  
  /* ------------------ Visitor : term ------------------ */
  
  factor(ctx) {
    let var_value
    if (ctx.VAR) {
      var_value = this.getSigValue(ctx.VAR)
    } else if (ctx.P_NUM) {
      var_value = ctx.P_NUM[0].image
    } else if (ctx.N_NUM) {
      var_value = ctx.N_NUM[0].image
    }
    
    return {
      type: "FACTOR",
      value: var_value
    }
  }
}

function hclVisitor  (inputCst) {
  // Our visitor has no state, so a single instance is sufficient.
  const hclVisitorInstance = new HclVisitor()
  
  const result = hclVisitorInstance.visit(inputCst)
  
  return new CompilationResult(result.jsOutput, result.errors, {})
}

export { hclVisitor }
