"use strict"
// Written Docs for chevrotain parser can be found here
// https://chevrotain.io/docs/tutorial/step2_parsing.html

import { CompilationError, CompilationResult } from "../interfaces/ICompiler";
import { tokenVocabulary } from "./hclLexer"
const CstParser = require("chevrotain").CstParser

// We gather all the needed TokenType from the lexer vocabulary
const Quote = tokenVocabulary.QUOTE
const BoolArg = tokenVocabulary.BOOLARG
const Bool = tokenVocabulary.BOOL
const Intarg = tokenVocabulary.INTARG
const Int = tokenVocabulary.INT
const In = tokenVocabulary.IN

const Qstring = tokenVocabulary.QSTRING
const Var = tokenVocabulary.VAR
const P_num = tokenVocabulary.P_NUM
const N_num = tokenVocabulary.N_NUM

const Semi = tokenVocabulary.SEMI
const Colon = tokenVocabulary.COLON
const Comma = tokenVocabulary.COMMA

const Lparen = tokenVocabulary.LPAREN
const Rparen = tokenVocabulary.RPAREN
const Lbrace = tokenVocabulary.LBRACE
const Rbrace = tokenVocabulary.RBRACE
const Lbrack = tokenVocabulary.LBRACK
const Rbrack = tokenVocabulary.RBRACK

const And = tokenVocabulary.AND
const Or = tokenVocabulary.OR

const BitwiseAnd = tokenVocabulary.BITWISE_AND
const BitwiseOr = tokenVocabulary.BITWISE_OR
const Xor = tokenVocabulary.XOR

const Diff = tokenVocabulary.DIFF
const Equal = tokenVocabulary.EQUAL
const GreaterEqualThan = tokenVocabulary.GREATER_EQUAL_THAN
const LessEqualThan = tokenVocabulary.LESS_EQUAL_THAN
const GreaterThan = tokenVocabulary.GREATER_THAN
const LessThan = tokenVocabulary.LESS_THAN
const Not = tokenVocabulary.NOT
const Assign = tokenVocabulary.ASSIGN

/* ------------------ Parser ------------------ */
/*
Parser documentation :
- RULE : define a new rule
- SUBRULE : expect to match the indicated rule
- CONSUME : expect to match a TokenType
- OR : expect to match one alternative
- MANY : expect to match zero or more 
- { label : ... } : help recognize when there is 
same subrule/TokenType in a rule during the visitor phase
*/
class HclParser extends CstParser {
  constructor() {
    super(tokenVocabulary)
    
    const $ = this
    
    $.RULE("hcl", () => {
      $.SUBRULE($.statements)
    })
    
    $.RULE("statements", () => {
      $.MANY(() => {
        $.SUBRULE($.statement)
      });
    })
    
    $.RULE("statement", () => {
      $.OR([
        { ALT: () => $.SUBRULE($.quote_statement) },
        { ALT: () => $.SUBRULE($.boolarg_statement) },
        { ALT: () => $.SUBRULE($.intarg_statement) },
        { ALT: () => $.SUBRULE($.bool_statement) },
        { ALT: () => $.SUBRULE($.int_statement) }
      ])
    })
    
    $.RULE("quote_statement", () => {
      $.CONSUME(Quote)
      $.CONSUME(Qstring)
    })
    
    $.RULE("boolarg_statement", () => {
      $.CONSUME(BoolArg)
      $.CONSUME(Var)
      $.CONSUME(Qstring)
    })
    
    $.RULE("intarg_statement", () => {
      $.CONSUME(Intarg)
      $.CONSUME(Var)
      $.CONSUME(Qstring)
    })
    
    $.RULE("bool_statement", () => {
      $.CONSUME(Bool)
      $.CONSUME(Var)
      $.CONSUME(Assign)
      $.SUBRULE($.expr)
      $.CONSUME(Semi)
    })
    
    $.RULE("int_statement", () => {
      $.CONSUME(Int)
      $.CONSUME(Var)
      $.CONSUME(Assign)
      $.SUBRULE($.expr)
      $.CONSUME(Semi)
    })
    
    $.RULE("expr", () => {
      $.SUBRULE($.expr_and)
      $.OPTION(() => {
        $.CONSUME(Or)
        $.SUBRULE($.expr) 
      })
    })
    
    $.RULE("expr_and", () => {
      $.SUBRULE($.expr_bitwise_or)
      $.OPTION(() => {
        $.CONSUME(And)
        $.SUBRULE($.expr) 
      })
    })
    
    $.RULE("expr_bitwise_or", () => {
      $.SUBRULE($.expr_xor)
      $.OPTION(() => {
        $.CONSUME(BitwiseOr)
        $.SUBRULE($.expr) 
      })
    })
    
    $.RULE("expr_xor", () => {
      $.SUBRULE($.expr_bitwise_and)
      $.OPTION(() => {
        $.CONSUME(Xor)
        $.SUBRULE($.expr) 
      })
    })
    
    $.RULE("expr_bitwise_and", () => {
      $.SUBRULE($.expr_equal)
      $.OPTION(() => {
        $.CONSUME(BitwiseAnd)
        $.SUBRULE($.expr) 
      })
    })
    
    $.RULE("expr_equal", () => {
      $.SUBRULE($.expr_comp)
      $.OPTION(() => {
        $.OR([
          { ALT: () => $.CONSUME(Diff) },
          { ALT: () => $.CONSUME(Equal) }
        ])
        $.SUBRULE($.expr) 
      })
    })
    
    $.RULE("expr_comp", () => {
      $.SUBRULE($.term)
      $.OPTION(() => {
        $.OR([
          { ALT: () => $.CONSUME(GreaterEqualThan) },
          { ALT: () => $.CONSUME(LessEqualThan) },
          { ALT: () => $.CONSUME(GreaterThan) },
          { ALT: () => $.CONSUME(LessThan) }
        ])
        $.SUBRULE($.expr) 
      })
    })
    
    $.RULE("term", () => {
      $.OR([
        { ALT: () => $.SUBRULE($.paren_expr) },
        { ALT: () => $.SUBRULE($.brack_expr) },
        { ALT: () => $.SUBRULE($.brace_expr) },
        { ALT: () => $.SUBRULE($.not_expr) },
        { ALT: () => $.SUBRULE($.factor) },
      ])
    })
    
    $.RULE("paren_expr", () => {
      $.CONSUME(Lparen)
      $.SUBRULE($.expr)
      $.CONSUME(Rparen)
    })
    
    $.RULE("brack_expr", () => {
      $.CONSUME(Lbrack)
      $.SUBRULE($.case_list)
      $.CONSUME(Rbrack)
    })
    
    $.RULE("brace_expr", () => {
      $.SUBRULE($.factor)
      $.CONSUME(In)
      $.CONSUME(Lbrace)
      $.SUBRULE($.expr_list)
      $.CONSUME(Rbrace)
    })
    
    $.RULE("not_expr", () => {
      $.CONSUME(Not)
      $.SUBRULE($.expr)
    })
    
    $.RULE("expr_list", () => {
      $.SUBRULE($.expr)
      $.MANY(() => {
        $.CONSUME(Comma)
        $.SUBRULE1($.expr)
      });
    })
    
    $.RULE("case_list", () => {
      $.MANY(() => {
        $.SUBRULE($.expr, { LABEL: "left" })
        $.CONSUME(Colon)
        $.SUBRULE1($.expr, { LABEL: "right" })
        $.CONSUME(Semi)
      });
    })
    
    $.RULE("factor", () => {
      $.OR([
        { ALT: () => $.CONSUME(Var) },
        { ALT: () => $.CONSUME(P_num) },
        { ALT: () => $.CONSUME(N_num) }
      ])
    })
    
    this.performSelfAnalysis()
  }
}

function hclParser (inputTokens) {
  // A new parser instance with CST output (enabled by default).
  const parserInstance = new HclParser()
  
  // ".input" is a setter which will reset the parser's internal's state.
  parserInstance.input = inputTokens
  
  // Automatic CST created when parsing
  const cst = parserInstance.hcl()
  const errors_list = []
  
  if (parserInstance.errors.length > 0) {
    for (const error in parserInstance.errors){
      errors_list.push(new CompilationError(parserInstance.errors[error].token.startLine, parserInstance.errors[error].message))
    }
  }
  return new CompilationResult(cst, errors_list, {})
}

export { HclParser, hclParser }
