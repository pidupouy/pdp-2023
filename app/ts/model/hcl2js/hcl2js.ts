import { ICompiler, CompilationResult, CompilationError } from "../interfaces/ICompiler";
import { hclLexer } from "./hclLexer"
import { hclParser } from "./hclParser"
import { hclVisitor } from "./hclVisitor"

export class Hcl2js implements ICompiler {
    assemble(src: string): CompilationResult {
        let result = new CompilationResult()

        try {
            const lexerResult = hclLexer(src)
            if (lexerResult.errors.length > 0){
                result.errors = lexerResult.errors
                return result
            }

            const parserResult = hclParser(lexerResult.output)
            if (parserResult.errors.length > 0){
                result.errors = parserResult.errors
                return result
            }

            const visitorResult = hclVisitor(parserResult.output)
            if (visitorResult.errors.length > 0){
                result.errors = visitorResult.errors
                return result
            }

            result.output = visitorResult.output
        } catch (error) {
            if (error instanceof CompilationError) {
                result.errors.push(error);
            } else {
                throw new Error("An unknown error happened when parsing in hcl2js : " + error)
            }
        }

        return result
    }
}