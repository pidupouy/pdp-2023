"use strict"
// Written Docs for chevrotain lexer can be found here
// https://chevrotain.io/docs/tutorial/step1_lexing.html

import { CompilationError, CompilationResult } from "../interfaces/ICompiler"
const chevrotain = require("chevrotain")
const Lexer = chevrotain.Lexer
const createToken = chevrotain.createToken

/* ------------------ Lexical grammar ------------------ */
/*
CreateToken will create a TokenType
The lexer's output is an array of token Objects with metadata
*/
const Qstring = createToken({ name: "QSTRING", pattern: /'.+?(?=')'/ })
const Var = createToken({ name: "VAR", pattern: /[a-zA-Z][a-zA-Z0-9_]*/ })
const P_num = createToken({ name: "P_NUM", pattern: /[0-9][0-9]*/ })
const N_num = createToken({ name: "N_NUM", pattern: /-[0-9][0-9]*/ })

/*
The "longer_alt" property will verify if the current word is corresponding to a longer token
Exemple : IN et VAR for a variable named "instr"
Instead of findig the token IN, the lexer wil search the longer_alt and found VAR
*/
const Quote = createToken({ name: "QUOTE", pattern: /quote/, longer_alt: Var })
const BoolArg = createToken({ name: "BOOLARG", pattern: /boolsig/, longer_alt: Var })
const Bool = createToken({ name: "BOOL", pattern: /bool/, longer_alt: Var })
const Intarg = createToken({ name: "INTARG", pattern: /intsig/, longer_alt: Var })
const Int = createToken({ name: "INT", pattern: /int/, longer_alt: Var })
const In = createToken({ name: "IN", pattern: /in/, longer_alt: Var })

const Semi = createToken({ name: "SEMI", pattern: /;/ })
const Colon = createToken({ name: "COLON", pattern: /:/ })
const Comma = createToken({ name: "COMMA", pattern: /,/ })

const Lparen = createToken({ name: "LPAREN", pattern: /\(/ })
const Rparen = createToken({ name: "RPAREN", pattern: /\)/ })
const Lbrace = createToken({ name: "LBRACE", pattern: /{/ })
const Rbrace = createToken({ name: "RBRACE", pattern: /}/ })
const Lbrack = createToken({ name: "LBRACK", pattern: /\[/ })
const Rbrack = createToken({ name: "RBRACK", pattern: /\]/ })

const And = createToken({ name: "AND", pattern: /&&/ })
const Or = createToken({ name: "OR", pattern: /\|\|/ })

const BitwiseAnd = createToken({ name: "BITWISE_AND", pattern: /&/ })
const BitwiseOr = createToken({ name: "BITWISE_OR", pattern: /\|/ })
const Xor = createToken({ name: "XOR", pattern: /\^/ })

const Diff = createToken({ name: "DIFF", pattern: /!=/ })
const Equal = createToken({ name: "EQUAL", pattern: /==/ })
const GreaterEqualThan = createToken({ name: "GREATER_EQUAL_THAN", pattern: />=/ })
const LessEqualThan = createToken({ name: "LESS_EQUAL_THAN", pattern: /<=/ })
const GreaterThan = createToken({ name: "GREATER_THAN", pattern: />/ })
const LessThan = createToken({ name: "LESS_THAN", pattern: /</ })
const Not = createToken({ name: "NOT", pattern: /!/ })
const Assign = createToken({ name: "ASSIGN", pattern: /=/ })

// Tokens that are ignored
const WhiteSpace = createToken({ name: "WHITE_SPACE", pattern: /[^\S\n]+/, group: Lexer.SKIPPED })
const NewLine = createToken({ name: "NEW_LINE", pattern: /\n/, group: Lexer.SKIPPED })
const OtherSpace = createToken({ name: "OTHER_SPACE", pattern: /[ \r\t\f]/, group: Lexer.SKIPPED })
const Comments = createToken({ name: "COMMENTS", pattern: /\#[^\n]+/, group: Lexer.SKIPPED })

// The order of tokens is important because it determines which token will be compared to the word by the lexer
const allTokens = [
  WhiteSpace,
  NewLine,
  OtherSpace,
  Comments,
  
  Quote,
  BoolArg,
  Bool,
  Intarg,
  Int,
  In,
  
  Semi,
  Colon,
  Comma,
  
  Lparen,
  Rparen,
  Lbrace,
  Rbrace,
  Lbrack,
  Rbrack,
  
  And,
  Or,
  
  BitwiseAnd,
  BitwiseOr,
  Xor,
  
  Diff,
  Not,
  Equal,
  Assign,
  GreaterEqualThan,
  GreaterThan,
  LessEqualThan,
  LessThan,
  
  Qstring,
  Var,
  N_num,
  P_num,
]

// The vocabulary will be exported and used in the Parser definition.
const tokenVocabulary = {}

allTokens.forEach((tokenType) => {
  tokenVocabulary[tokenType.name] = tokenType
})

function hclLexer (inputText) {
  const SelectLexer = new Lexer(allTokens)
  const lexingResult = SelectLexer.tokenize(inputText)
  const errors_list = []
  
  if (lexingResult.errors.length > 0) {
    for (const error in lexingResult.errors){
      errors_list.push(new CompilationError(lexingResult.errors[error].line, lexingResult.errors[error].message))
    }
  }
  
  return new CompilationResult(lexingResult.tokens, errors_list, {})
}

export { tokenVocabulary, hclLexer }
