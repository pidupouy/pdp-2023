/**
 * Represents an instruction.
 */

export class Instruction {
    private static ICODE_IFUN_BYTES_LENGTH = 1;
    private static REGISTERS_BYTE_LENGTH = 1;

    /**
     * The name of the instruction. It can not be empty.
     */
    name: string

    /**
     * The code representing the instruction. It must be in [0; 15] (encoded on 4 bits).
     */
    icode: number

    /**
     * The code representing the function of the instruction. It must be in [0; 15] (encoded on 4 bits).
     */
    ifun: number

    /**
     * Length in bytes of the encoded instructions.
     */
    length = 0;

    /**
     * Used to store the keywords contained in the format string describing the instruction's arguments.
     */
    lexeme: string[] = [];

    /**
     * Expected arguments for this instruction as indicated in the instruction editor.
     */
    args: string = "";

    /**
     * Regular expression to identify the arguments.
     */
    argsRegex: RegExp = / /g;

    useRegisters = false;

    useValC = false;

    useMemory = false;

    constructor(name: string, icode: number, ifun: number, args: string, wordSize: number = 4) {
        try {
            this._checkName(name);
            this._checkCodes(icode, ifun);
            this._parseArgs(args);
        } catch (e) {
            throw new Error(name + " : " + e.message);
        }

        this.name = name;
        this.icode = icode;
        this.ifun = ifun;

        this._setArgs(args);
        this._computeLength(wordSize);
    }

    private _checkName(name: string) {
        let instructionNameRegex = new RegExp("[a-zA-Z][0-9a-zA-Z_]*");
        let regexResult = instructionNameRegex.exec(name);
        if (name.length === 0 || regexResult === null || regexResult.length != 1 || regexResult[0].length != name.length) {
            throw new Error("Invalid name : " + name);
        }
    }

    private _checkCodes(icode: number, ifun: number) {
        if (icode < 0 || ifun < 0 || icode > 15 || ifun > 15) {
            throw new Error("Invalid icode/ifun : " + icode + "/" + ifun);
        }
    }

    /**
     * Function used to check that the regular expression given by the user will work with the configuration of the simulator.
     * @param args the format string used to describe the instruction's arguments
     */
    private _parseArgs(args: string) {
        if (args != "") {
            let lexeme = args.match(/valC\??|rA|rB/g); //here we get the components of the arguments given by user ("valC(rA) => [valC, rA]")
            if (lexeme == null) {
                throw new Error("Characters present but no keywords in " + args);
            }
            this.lexeme = lexeme; // We save it because it is used when parsing y86 code
            let length = lexeme.length; // a correct entry will have 3 lexemes at most ("rA", "rB", ("valC" or valC?))
            for (let i = 0; i < length; i++) {
                let test = lexeme[0];
                for (let j = i + 1; j < length; j++) {
                    if (test == lexeme[j]) {
                        throw new Error("Duplication of argument. " + test + " is present twice in " + args);
                    }
                    if (test == "valC" && lexeme[j] == "valC?") {
                        throw new Error("Duplication of argument. valC is present twice in " + args);
                    }
                    if (test == "valC?" && lexeme[j] == "valC") {
                        throw new Error("Duplication of argument. valC is present twice in " + args);
                    }
                }
            }


            let prohibed_characters = ['\\'];


            let prohibed_length = prohibed_characters.length;
            for (let i = 0; i < prohibed_length; i++) {
                if (args.includes(prohibed_characters[i])) {
                    throw new Error("Illegal character " + prohibed_characters[i] + " in " + args);
                }
            }

            let args_length = args.length;
            let current_pos = 0;
            let re = /valC\??|rA|rB/g;
            let alphabet = /[a-zA-Z0-9\?]+/;
            let result = re.exec(args);
            let KWRD_REGISTER_LENGTH = 2;
            let KWRD_VALC_LENGTH = 4;
            let KWRD_VALC2_LENGTH = 5; // Keyword valC?
            //@ts-ignore
            let separators = args.slice(current_pos, result.index);
            while (current_pos != args_length) {
                if (alphabet.test(separators)) {
                    throw new Error("alphanumeric characters or '?' detected in separator (" + separators + ") of " + args);
                }
                if (result === null) {
                    throw new Error("oops ... (instruction.ts _setArgs"); // should never be triggered, just here to make ts happy
                }
                if (result[0] === "rA" || result[0] === "rB") {
                    current_pos = result.index + KWRD_REGISTER_LENGTH;
                } else if (result[0] === "valC") {
                    current_pos = result.index + KWRD_VALC_LENGTH;
                } else if (result[0] === "valC?") {
                    current_pos = result.index + KWRD_VALC2_LENGTH;
                }

                result = re.exec(args);
                if (result == null) { //no more keywords to be read
                    separators = args.slice(current_pos, args_length);
                    current_pos = args_length;
                } else {
                    separators = args.slice(current_pos, result.index);
                }
            }
        }
    }

    private _SpecialCharBackslash(str: string) {
        let authorized_special = ['(', ')', '|', ':', '[', ']', '*', '+', '-'];
        for (let i = 0; i < authorized_special.length; i++) {
            str = str.replace(authorized_special[i], '\\' + authorized_special[i]);
        }
        str = str.replace(/\s+/g, '\\s*');
        return str;
    }

    /**
     * Reads the given format string to generate the regular expression used by the compiler to match the instruction's arguments.
     * @param args the format string used to describe the instruction's arguments
     */
    private _setArgs(args: string) {
        this.useRegisters = this.useValC = false;

        let valC = /valC\??/g;              // detection of what is used in arguments
        let register = /(rA|rB)/g;

        if (register.test(args)) {
            this.useRegisters = true;
        }
        if (valC.test(args)) {
            this.useValC = true;
        }

        this.args = args;

        let newregex: string = "";
        if (args != "") {
            let args_length = args.length;
            let current_pos = 0;
            let re = /valC\??|rA|rB/g;
            let result = re.exec(args);
            let KWRD_REGISTER_LENGTH = 2;
            let KWRD_VALC_LENGTH = 4;
            let KWRD_VALC2_LENGTH = 5; // Keyword valC?
            //@ts-ignore
            let copy = args.slice(current_pos, result.index);
            while (current_pos != args_length) {
                copy = this._SpecialCharBackslash(copy);
                newregex = newregex + copy;

                if (result === null) {
                    throw new Error("oops ... (instruction.ts _setArgs"); // should never be triggered, just here to make ts happy
                }
                if (result[0] === "rA" || result[0] === "rB") {
                    newregex = newregex + "\%(([a-z0-9]+))";
                    current_pos = result.index + KWRD_REGISTER_LENGTH;
                } else if (result[0] === "valC") {
                    newregex = newregex + "((0x[0-9a-fA-F]+)|(\-?[0-9]+)|([a-zA-Z][0-9a-zA-Z_]*))";
                    current_pos = result.index + KWRD_VALC_LENGTH;
                } else if (result[0] === "valC?") {
                    newregex = newregex + "((0x[0-9a-fA-F]+)|(\-?[0-9]+)|([a-zA-Z][0-9a-zA-Z_]*))?";
                    current_pos = result.index + KWRD_VALC2_LENGTH;
                }

                result = re.exec(args);
                if (result == null) { //no more keywords to be read
                    copy = args.slice(current_pos, args_length);
                    copy = this._SpecialCharBackslash(copy);
                    newregex = newregex + copy;
                    current_pos = args_length;
                } else {
                    copy = args.slice(current_pos, result.index);
                }
            }
        }
        this.argsRegex = new RegExp(newregex);
    }

    private _computeLength(wordSize: number) {
        this.length = Instruction.ICODE_IFUN_BYTES_LENGTH;

        this.length += this.useRegisters ? Instruction.REGISTERS_BYTE_LENGTH : 0;
        this.length += this.useValC ? wordSize : 0;
    }
}