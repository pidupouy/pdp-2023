import { ISimulator, convert_dict_status, simStatus } from "../interfaces/ISimulator";
import { IInstructionSet } from "../interfaces/IInstructionSet";
import { IAlu, alufct } from "../interfaces/IAlu";
import { IRegisters, registers_enum } from "../interfaces/IRegisters";
import { IMemory } from "../interfaces/IMemory";
import { Context } from "../context";
import { HCL } from "../hcl";
import { Alu } from "../alu";
import { Registers } from "../registers";
import { Memory } from "../memory";
import * as stages from "./stages";

export class Sim implements ISimulator {
    public context: Context;
    public registers: IRegisters;
    public memory: IMemory;
    public alu: IAlu;
    public hcl: HCL;
    public instructionSet: IInstructionSet;
    
    public status: simStatus;
    public errorMessage: string;
    private yo: string;
    private stepNum: number;
    private wordSize: number;
    private registersNamesList: string[];

    constructor(instructionSet: IInstructionSet, wordSize: number, registersNamesList: string[]) {
        this.context = new Context();
        this.registers = new Registers(wordSize, registersNamesList)
        this.memory = new Memory(wordSize);
        this.alu = new Alu(wordSize);
        this.hcl = new HCL(instructionSet, registers_enum, alufct);
        this.hcl.setCtx(this.context);
        this.instructionSet = instructionSet;

        this.status = simStatus.AOK;
        this.errorMessage = "";
        this.yo = "";
        this.stepNum = 0;
        this.wordSize = wordSize;
        this.registersNamesList = registersNamesList;
    }

    step(): void {
        this.hcl.setCtx(this.context);
        try {
            this.updateSim();
            stages.fetch(this);
            stages.decode(this);
            stages.execute(this);
            stages.memory(this);
            stages.writeBack(this);
            stages.updatePC(this);
            this.stepNum++;
        } catch (error) {
            this.status = simStatus.HALT;
            this.errorMessage = error;
        }
    }

    continue(breakpoints: Array<bigint> = [], maxSteps: number = 10000): void {
        let stepCount = 0;
        
        while (this.status == simStatus.AOK && (stepCount < maxSteps || maxSteps < 0)) {
            this.step();
            for (let brk of breakpoints) {
                if (this.context.newPC == brk) {
                    return;
                };
            };
            stepCount++;
        }

        if (this.status == simStatus.AOK && stepCount == maxSteps) {
            this.errorMessage = "Sim : Max number of steps reached (" + maxSteps + ")";
        }
    }

    reset(): void {
        this.context = new Context();
        this.registers = new Registers(this.wordSize, this.registersNamesList)
        this.memory = new Memory(this.wordSize);
        this.alu = new Alu(this.wordSize);
        this.hcl.setCtx(this.context);
        this.loadProgram(this.yo);

        this.status = simStatus.AOK;
        this.errorMessage = "";
        this.stepNum = 0;
    }

    undo(): void {
        let previousStep = this.stepNum - 1;
        this.reset();
        this.loadProgram(this.yo);

        while (previousStep > 0) {
            this.step();
            previousStep--;
        }
    }

    load(jsonContent: any): void {
        this.loadProgram(jsonContent.compilationResult.output)
        this.context.load(jsonContent.cpuState);
        this.registers.load(jsonContent.registers);
        this.alu.load(jsonContent.flags);
        this.status = convert_dict_status[jsonContent.status];
        this.errorMessage = jsonContent.error;
        this.stepNum = jsonContent.stepNum
        this.memory.load(jsonContent.memory)
    }

    save(): string {
        let content = "";

        content += "\"cpuState\" : " + this.context.save() + ",\n";
        content += "\"registers\" : " + this.registers.save() + ",\n";
        content += "\"flags\" : " + this.alu.save() + ",\n";
        content += "\"status\" : " + JSON.stringify(simStatus[this.status]) + ",\n";
        content += "\"error\" : " + JSON.stringify(this.errorMessage) + ",\n";
        content += "\"stepNum\" : " + this.stepNum + ",\n";
        content += "\"memory\" : " + this.memory.save() + ",\n";

        return content;
    }

    loadProgram(yo: string): void {
        this.memory.loadProgram(yo);
        this.yo = yo;
    }

    insertHclCode(js: string): void {
        this.hcl.setHclCode(js);
    }

    getStageView() : any {
        return {
            "Fetch": {
                "columns": [],
                "lines": [
                    {
                        "name": "",
                        "labels": ["icode", "ifun", "ra", "rb", "valC", "valP"],
                        "values": [
                            this.context.icode,
                            this.context.ifun,
                            this.registers.getRegisterName(this.context.ra),
                            this.registers.getRegisterName(this.context.rb),
                            this.context.valC,
                            this.context.valP
                        ]
                    }
                ]
            },
            "Decode": {
                "columns": [],
                "lines": [
                    {
                        "name": "",
                        "labels": ["srcA", "srcB", "valA", "valB"],
                        "values": [
                            this.registers.getRegisterName(this.context.srcA),
                            this.registers.getRegisterName(this.context.srcB),
                            this.context.valA,
                            this.context.valB
                        ]
                    }
                ]
            },
            "Execute": {
                "columns": [],
                "lines": [
                    {
                        "name": "",
                        "labels": ["aluA", "aluB", "valE", "Bch"],
                        "values": [
                            this.context.aluA,
                            this.context.aluB,
                            this.context.valE,
                            this.context.bcond
                        ]
                    }
                ]
            },
            "Memory": {
                "columns": [],
                "lines": [
                    {
                        "name": "",
                        "labels": ["addr", "data", "r", "w", "valM"],
                        "values": [
                            this.context.mem_addr,
                            this.context.mem_data,
                            this.context.mem_read,
                            this.context.mem_write,
                            this.context.valM
                        ]
                    }
                ]
            },
            "WriteBack": {
                "columns": [],
                "lines": [
                    {
                        "name": "",
                        "labels": ["dstE", "dstM"],
                        "values": [
                            this.registers.getRegisterName(this.context.dstE),
                            this.registers.getRegisterName(this.context.dstM)
                        ]
                    }
                ]
            },
            "UpdatePC": {
                "columns": [],
                "lines": [
                    {
                        "name": "",
                        "labels": ["newPC"],
                        "values": [
                            this.context.newPC
                        ]
                    }
                ]
            }
        };
    }

    //FAUT DELEGUER A LA CLASSE REGISTER
    getRegistersView() : { [key : string] : bigint } {
        let output : { [key : string] : bigint } = {};
        let index = 0;
        for (let registerName of this.registersNamesList){
            output[registerName] = this.registers.read(index);
            index++;
        }
        return output;
    }

    getNextRegistersView() : { [key : string] : bigint } {
        let output : { [key : string] : bigint } = {};
        let index = 0;
        for (let registerName of this.registersNamesList){
            output[registerName] = this.registers.readNext(index);
            index++;
        }
        return output;
    }

    getMemoryView() : { "wordSize": number, "startAddress": number, "maxAddress": number, "modifiedMem": Map<bigint, bigint>, "bytes": Map<bigint, number> } {
        return this.memory.getMemoryView();
    }

    getNextMemoryView() : { "wordSize": number, "address": bigint, "isWrite": boolean, "bytes": number[] } {
        return this.memory.getNextMemoryView();
    }

    getFlagsView() : { [key : string] : boolean } {
        return {
            "ZF": this.alu.getZF(),
            "OF": this.alu.getOF(),
            "SF": this.alu.getSF()
        };
    }

    getNextFlagsView() : { [key : string] : boolean } {
        return {
            "ZF": this.alu.getNextZF(),
            "OF": this.alu.getNextOF(),
            "SF": this.alu.getNextSF()
        };
    }

    getStatusView() : { "STAT": simStatus, "ERR": string, "PC": bigint } {
        return {
            "STAT": this.status,
            "ERR": this.errorMessage,
            "PC": this.context.pc < BigInt(0) ? BigInt(0) : this.context.pc
        };
    }

    getPerformancesView() : { [key : string] : number } | undefined {
        return undefined;
    }

    getHighlightRules() : { "address": bigint, "tag": string }[] {
        return [
            {
                "address": this.context.pc,
                "tag": "*"
            }
        ];
    }

    getStepNum(): number {
        return this.stepNum;
    }

    isInterfaceTightened(): boolean {
        return false;
    }

    updateSim(): void {
        this.alu.updateFlags();
        this.memory.updateMemory();
        this.registers.updateRegisters();
    }
}
