import { toUint } from "./numberUtils";
import { IAlu, JMP_enum, CC, convert_dict_flags, alufct } from "./interfaces/IAlu"

class Alu implements IAlu {
    /**
     * Boolean array which contain all the flags.
     */
    private flags: boolean[] = [];
    private nextFlags: boolean[] = [];
    private wordSize: number;

    constructor(wordSize: number) {
        for (let key in CC) {
            this.flags.push(false);
            this.nextFlags.push(false);
        }
        this.wordSize = wordSize;
    }

    computeAlu(aluA: bigint, aluB: bigint, alu_fun: alufct): bigint {
        //TO BE REDONE
        if (aluA > BigInt('0xFFFFFFFFFFFFFFFF') || aluB > BigInt('0xFFFFFFFFFFFFFFFF')) {
            throw new Error("Error in computeAlu : Values are more than 64bits long.")
        }
        let val32bits = new Uint32Array(2);
        val32bits[0] = Number(toUint(aluA, this.wordSize));
        val32bits[1] = Number(toUint(aluB, this.wordSize));

        let val64bits = new BigUint64Array(2);
        val64bits[0] = toUint(aluA, this.wordSize);
        val64bits[1] = toUint(aluB, this.wordSize);

        let result;
        if(this.wordSize === 4){
            result = new Uint32Array(1);
        } else {
            result = new BigUint64Array(1);
        }

        if (alu_fun == alufct.A_ADD) {
            result[0] = this.wordSize === 4 ? val32bits[0] + val32bits[1] : val64bits[0] + val64bits[1];
            return BigInt(result[0]);
        }
        else if (alu_fun == alufct.A_AND) {
            result[0] = this.wordSize === 4 ? val32bits[0] & val32bits[1] : val64bits[0] & val64bits[1];
            return BigInt(result[0]);
        }
        else if (alu_fun == alufct.A_SUB) {
            result[0] = this.wordSize === 4 ? val32bits[1] - val32bits[0] : val64bits[1] - val64bits[0];
            return BigInt(result[0]);
        }
        else if (alu_fun == alufct.A_XOR) {
            result[0] = this.wordSize === 4 ? val32bits[0] ^ val32bits[1] : val64bits[0] ^ val64bits[1];
            return BigInt(result[0]);
        }
        else if (alu_fun == alufct.A_SAL) {
            result[0] = this.wordSize === 4 ? val32bits[1] << val32bits[0] : val64bits[1] << val64bits[0];
            return BigInt(result[0]);
        }
        else if (alu_fun == alufct.A_SAR) {
            result[0] = this.wordSize === 4 ? val32bits[1] >> val32bits[0] : val64bits[1] >> val64bits[0];
            return BigInt(result[0]);
        }
        else if (alu_fun == alufct.A_NONE) {
            throw new Error("A_NONE constant set.")
        }
        throw new Error("Error, alu function (ifun) not found.")
    }

    computeCC(aluA: bigint, aluB: bigint, alu_fun: alufct): void {

        // get value and cast into uint32
        let result;
        if(this.wordSize === 4){
            result = new Uint32Array(1);
        } else {
            result = new BigUint64Array(1);
        }

        result[0] = this.wordSize === 4 ? Number(this.computeAlu(aluA, aluB, alu_fun)) : this.computeAlu(aluA, aluB, alu_fun);
        // sign bits
        let sgnA, sgnB, sgnV, signBit = this.wordSize === 4 ? BigInt('0x80000000') : BigInt('0x8000000000000000');
        sgnA = !!(aluA & signBit);
        sgnB = !!(aluB & signBit);
        sgnV = !!(BigInt(result[0]) & signBit);
        // set Zero flag
        this.nextFlags[CC.ZF] = BigInt(result[0]) == BigInt(0);
        // set Sign flag
        this.nextFlags[CC.SF] = sgnV;
        // set Overflow flag
        if (alu_fun == alufct.A_ADD)
            this.nextFlags[CC.OF] = (sgnA && sgnB && !sgnV) || (!sgnA && !sgnB && sgnV);
        else if (alu_fun == alufct.A_SUB) {
            this.nextFlags[CC.OF] = (sgnA != sgnB) && (sgnB != sgnV);
        }
        else {
            this.nextFlags[CC.OF] = false;
        }
    }

    computeBch(ifun: number): boolean {
        if (ifun == JMP_enum.J_YES) return true;
        if (ifun == JMP_enum.J_LE) return this._boolean_xor(this.nextFlags[CC.SF], this.nextFlags[CC.OF]) || this.nextFlags[CC.ZF];
        if (ifun == JMP_enum.J_L) return this._boolean_xor(this.nextFlags[CC.SF], this.nextFlags[CC.OF]);
        if (ifun == JMP_enum.J_E) return this.nextFlags[CC.ZF];
        if (ifun == JMP_enum.J_NE) return !this.nextFlags[CC.ZF];
        if (ifun == JMP_enum.J_GE) return this._boolean_xor(this._boolean_xor(this.nextFlags[CC.SF], this.nextFlags[CC.OF]), true);
        if (ifun == JMP_enum.J_G) return this._boolean_xor(this._boolean_xor(this.nextFlags[CC.SF], this.nextFlags[CC.OF]), true) && this._boolean_xor(this.nextFlags[CC.ZF], true);
        return false;
    }

    updateFlags(): void {
        for (let key in CC) {
            this.flags[key] = this.nextFlags[key];
        }
    }

    getZF(): boolean {
        return this.flags[CC.ZF];
    }

    getNextZF(): boolean {
        return this.nextFlags[CC.ZF];
    }

    getSF(): boolean {
        return this.flags[CC.SF];
    }

    getNextSF(): boolean {
        return this.nextFlags[CC.SF];
    }

    getOF(): boolean {
        return this.flags[CC.OF];
    }

    getNextOF(): boolean {
        return this.nextFlags[CC.OF];
    }

    load(newFlags: { [key: string]: { [key: string]: boolean } }): void {
        for (let flag in newFlags.current) {
            this.flags[convert_dict_flags[flag]] = newFlags.current[flag];
            this.nextFlags[convert_dict_flags[flag]] = newFlags.next[flag];
        }
    }

    save(): string {
        let content = {
            "ZF": this.getZF(),
            "OF": this.getOF(),
            "SF": this.getSF()
        };
        let contentNext = {
            "ZF": this.getNextZF(),
            "OF": this.getNextOF(),
            "SF": this.getNextSF()
        };
        return "{ \"current\" : " + JSON.stringify(content) + ", \"next\" : " + JSON.stringify(contentNext) + "}";
    }

    private _boolean_xor(boolA: boolean, boolB: boolean): boolean {
        return (boolA && !boolB) || (!boolA && boolB);
    }
}

export { Alu };