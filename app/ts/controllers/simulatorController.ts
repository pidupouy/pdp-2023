import { BasicController } from "./basicController";
import { IKernelController } from "./interfaces/IKernelController";
import { padStringBigInt, toInt } from '../model/numberUtils'
import { simStatus } from "../model/interfaces/ISimulator";

export const MAX_STEPS = 10000

export class SimulatorController extends BasicController {
    constructor(kernelController: IKernelController) {
        super(kernelController);
    };

    getRegistersView(): any {
        let registers = this.kernelController.getSim().getRegistersView();
        let registersView: any[] = [];

        for (let register in registers) {
            let value = registers[register];
            registersView.push({
                name: '%' + register,
                value_hex: padStringBigInt(value.toString(16), this.kernelController.getWordSize() * 2),
                value_dec: toInt(value, this.kernelController.getWordSize()),
            });
        };
        return registersView;
    };

    getNextRegistersView(): any {
        let nextRegisters = this.kernelController.getSim().getNextRegistersView();
        let registers = this.kernelController.getSim().getRegistersView();
        let registersView: any[] = [];

        for (let nextRegister in nextRegisters) {
            let valueNext = nextRegisters[nextRegister];
            let value = registers[nextRegister];

            registersView.push({
                name: '%' + nextRegister,
                value_hex: padStringBigInt(valueNext.toString(16), this.kernelController.getWordSize() * 2),
                value_dec: toInt(valueNext, this.kernelController.getWordSize()),
                isModified: valueNext !== value
            });
        };

        return registersView;
    };

    getStatusView(): any {
        let statusView = this.kernelController.getSim().getStatusView();
        let output: any[] = []

        output.push({
            name: "STAT",
            value: simStatus[statusView.STAT]
        });
        output.push({
            name: "ERR",
            value: statusView.ERR
        });
        output.push({
            name: "PC",
            value:  padStringBigInt(statusView.PC.toString(16), this.kernelController.getWordSize() * 2) 
        });

        return output;
    };

    getFlagsView(): any {
        let flags = this.kernelController.getSim().getFlagsView()
        let output: any[] = []

        for (let flag in flags) {
            output.push({
                name: flag,
                value: flags[flag] ? '1' : '0'
            });
        };

        return output;
    };

    getNextFlagsView(): any {
        let nextFlags = this.kernelController.getSim().getNextFlagsView();
        let flags = this.kernelController.getSim().getFlagsView();
        let output: any[] = [];

        for (let flag in nextFlags) {
            let valueNext = nextFlags[flag];
            let value = flags[flag];

            output.push({
                name: flag,
                value: valueNext ? '1' : '0',
                isModified: valueNext !== value
            });
        };

        return output;
    };

    getPerformancesView() : any {
        let performancesView = this.kernelController.getSim().getPerformancesView();
        if (performancesView === undefined) return performancesView
        let output: any[] = []

        for (const performanceName in performancesView){
            output.push({
                name: performanceName,
                value: performancesView[performanceName]
            });
        };

        return output;
    };

    getCPUStateView(): any {
        const stageView = this.kernelController.getSim().getStageView()

        let stages: any[] = []
        for (const stageName in stageView) {
            for (const lineIndex in stageView[stageName].lines) {
                const valuesArray = stageView[stageName].lines[lineIndex].values
                for (const valueIndex in valuesArray) {
                    let value = valuesArray[valueIndex]
                    switch (typeof(value)) {
                        case 'bigint':
                            valuesArray[valueIndex] = padStringBigInt(value.toString(16), this.kernelController.getWordSize() * 2)
                            break;
                        case 'number':
                            valuesArray[valueIndex] = value.toString(16);
                            break;
                        case 'boolean':
                            valuesArray[valueIndex] = value ? '1' : '0';
                            break;
                        default:
                            valuesArray[valueIndex] = value;
                    }
                }
            }
            stages.push({ name: stageName, content: stageView[stageName] });
        };

        return stages;
    };

    getMemoryView(): any {
        const memoryView = this.kernelController.getSim().getMemoryView();
        const registers = this.kernelController.getSim().getRegistersView();

        let words = new Array<any>((memoryView.maxAddress - memoryView.startAddress) / memoryView.wordSize);

        for (let address = memoryView.startAddress; address < memoryView.maxAddress; address += memoryView.wordSize) {
            let value = '';

            for (let offset = 0; offset < memoryView.wordSize; offset++) {
                let byte = memoryView.bytes.get(BigInt(address + offset));
                byte = byte == undefined ? 0 : byte;
                value += padStringBigInt(byte.toString(16), 2);
            };

            words[address / memoryView.wordSize] = {
                address: padStringBigInt(address.toString(16), 4),
                value: value
            };
        };

        return {
            ebp: registers.ebp,
            esp: registers.esp,
            nullWord: padStringBigInt("0", memoryView.wordSize * 2),
            words: words,
            maxAddress: memoryView.maxAddress
        };
    };

    getNextMemoryView(): any {
        const nextMemoryView = this.kernelController.getSim().getNextMemoryView();
        let value = '';

        for (let offset = 0; offset < nextMemoryView.wordSize; offset++) {
            let byte = nextMemoryView.bytes[offset];
            byte = byte == undefined ? 0 : byte;
            value += padStringBigInt(byte.toString(16), 2);
        };

        return {
            wordSize : nextMemoryView.wordSize,
            address : padStringBigInt(nextMemoryView.address.toString(16), 4),
            value : value,
            isWrite : nextMemoryView.isWrite
        };
    };

    getStepNum(): number {
        return this.kernelController.getSim().getStepNum();
    };

    getCurrentKernelHighlightRules(): Array<Object> {
        return this.kernelController.getSim().getHighlightRules();
    };

    isInterfaceTightened(): boolean {
        return this.kernelController.getSim().isInterfaceTightened();
    };

    step() {
        return this.kernelController.getSim().step();
    };

    continue(breakpoints: bigint[] = [], maxSteps: number = MAX_STEPS) {
        return this.kernelController.getSim().continue(breakpoints, maxSteps);
    };

    reset() {
        this.kernelController.getSim().reset();
    };

    undo() {
        this.kernelController.getSim().undo();
    };

    loadProgram(yo: string): void {
        this.kernelController.getSim().loadProgram(yo);
    };

    is64Bits(): boolean {
        return this.kernelController.getWordSize() === 8;
    };

    saveY86InJson(index: number, itemsLength: number, y86Code: string) {
        let data = "";
        data += "\"editor\" : " + JSON.stringify(y86Code);
        data += index !== itemsLength - 1 ? ",\n" : "\n";
        return data;
    };

    saveMachineState(index: number, itemsLength: number, compilationResult: any) {
        let data = "";
        data += "\"machineState\" : {\n";
        data += "\"kernel\" : \"" + this.kernelController.getCurrentKernelName() + "\",\n";
        data += this.kernelController.getSim().save();
        data += "\"compilationResult\" : " + JSON.stringify(compilationResult) + "\n}";
        data += index !== itemsLength - 1 ? ",\n" : "\n";
        return data;
    };

    saveInstructionSet(index: number, itemsLength: number, instructionSet: any) {
        let data = "";
        data += "\"instructionSet\" : " + JSON.stringify(instructionSet, null, 2);
        data += index !== itemsLength - 1 ? ",\n" : "\n";
        return data;
    };

    saveHclInJson(index: number, itemsLength: number, hclCode: string) {
        let data = "";
        data += "\"hcl\" : " + JSON.stringify(hclCode);
        data += index !== itemsLength - 1 ? ",\n" : "\n";
        return data;
    };

    addTimestamp() { // Gives a timestamp with the format --year-month-day--hours-minutes
        let now = new Date();
        let dayString = "--" + now.getFullYear() + "-" + ("0" + (now.getMonth() + 1)).slice(-2) + "-" + ("0" + now.getDate()).slice(-2);
        let hourString = "--" + ("0" + now.getHours()).slice(-2) + "-" + ("0" + now.getMinutes()).slice(-2);
        return dayString + hourString;
    };

    save(saveItems : any, y86Code: string, hclCode: string, instructionSet: any, compilationResult: any): string[] { // Saves all tabs specified by the parameter saveItems
        if (saveItems.length === 0)
            return ["",""];

        let filename = "";
        let filenameExtension = "";
        let data = "";

        if (saveItems.length === 1 && saveItems[0] === "editor") { // For backward compatibility reasons
            filename = "y86Code";
            filenameExtension = ".ys";
            data = y86Code;
        }
        else if (saveItems.length === 1 && saveItems[0] === "hcl") { // For backward compatibility reasons
            filename = "hclCode";
            filenameExtension = ".hcl";
            data = hclCode;
        }
        else {
            filenameExtension = '.json';
            data += "{\n";

            for (let index = 0; index < saveItems.length; ++index) {
                switch (saveItems[index]) {
                    case "editor":
                        data += this.saveY86InJson(index, saveItems.length, y86Code);
                        break;
                    case "execution":
                        filename = "machineState"
                        data += this.saveMachineState(index, saveItems.length, compilationResult);
                        break;
                    case "instructionSet":
                        filename = "instructionSet"
                        data += this.saveInstructionSet(index, saveItems.length, instructionSet);
                        break;
                    case "hcl":
                        data += this.saveHclInJson(index, saveItems.length, hclCode);
                        break;
                    default:
                        return ["",""];
                };
            };
            data += "}";

            if (saveItems.length > 1) filename = "y86simulator";
        };

        filename += this.addTimestamp() + filenameExtension;
        return [filename, data];
    };

    load(jsonContent: any) {
        this.kernelController.getSim().load(jsonContent);
    };

    getDump() {
        let output = {
            CpuState: [] as any,
            CurrentRegisters: [] as any,
            NextRegisters: [] as any,
            CurrentFlags: [] as any,
            NextFlags: [] as any,
            Status: [] as any,
            Performances: [] as any,
            CurrentMemory: [] as any,
            NextMemory: [] as any,
        };

        const cpuState = this.getCPUStateView();
        const currentRegisters = this.getRegistersView();
        const nextRegisters = this.getNextRegistersView();
        const currentFlags = this.getFlagsView();
        const nextFlags = this.getNextFlagsView();
        const status = this.getStatusView();
        const performances = this.getPerformancesView();
        const currentMemory = this.getMemoryView();
        const nextMemory = this.getNextMemoryView();

        currentMemory.words.forEach((word: any, key: any) => {
            if (word.value !== "00000000" && word.value !== "0000000000000000") {
                output.CurrentMemory.push(word);
            };
        });

        output.CpuState = cpuState;
        output.CurrentRegisters = currentRegisters;
        output.NextRegisters = nextRegisters;
        output.CurrentFlags = currentFlags;
        output.NextFlags = nextFlags;
        output.Status = status;
        output.Performances = performances;
        output.NextMemory = nextMemory;

        return output;
    };
}