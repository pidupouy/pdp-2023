import { ISimulator } from "../../model/interfaces/ISimulator";
import { ICompiler } from "../../model/interfaces/ICompiler";
import { IInstructionSet } from "../../model/interfaces/IInstructionSet";
import { Instruction } from "../../model/instruction";

export interface IKernelController {
    /**
     * Uses the specified kernel.
     * Its default instruction set and HCL code must be provided.
     * If the kernel does not exist, an exception is thrown.
     * The kernels list can be retrieved from 'getAvailableKernelNames' method.
     */
    useKernel(name : string, defaultInstructions : Instruction[], defaultHcl : string) : void;

    /**
     * Returns the name of all the available kernels
     */
    getAvailableKernelNames() : string[];

    /**
     * Returns the name of the kernel currently in use.
     */
    getCurrentKernelName() : string;

    /**
     * Returns the simulator used by the current toolchain.
     */
    getSim()            : ISimulator;

    /**
     * Returns the yas compiler used by the current toolchain.
     */
    getYas()            : ICompiler;

    /**
     * Returns the hcl2js compiler used by the current toolchain.
     */
    getHcl2js()         : ICompiler;

    /**
     * Returns the instruction set used by the current toolchain.
     */
    getInstructionSet() : IInstructionSet;

    /**
     * Returns the word size used by the current toolchain.
     */
    getWordSize()       : number;

    /**
     * Clears the current Toolchain instruction set before adding the new instructions into it.
     * The same operations are done on the instruction set used by the compiler (Yas) of the current Toolchain.
     * @param newInstructionSet An array of raw instructions which will be converted into standard instructions
     */
    updateInstructionSet(newInstructionSet : Instruction[]) : void;
}