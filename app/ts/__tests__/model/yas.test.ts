import { Memory as Memory32bits } from '../../model/memory'
import { Memory as Memory64bits } from '../../model/memory'
import { loadDefaultFile } from '../testUtils'
import { InstructionSet } from "../../model/instructionSet"
import { Yas } from '../../model/yas/yas'
import { describe, expect, test } from '@jest/globals';
import { registers_enum, registers_enum32bits } from "../../model/interfaces/IRegisters"

let defaultInstructions = JSON.parse(loadDefaultFile("", "instructionSet.json"))

const validProgram = 
`.pos 0
Init:
    irmovl Stack, %ebp
    irmovl Stack, %esp
    mrmovl 0x100(%esp), %ebp
    ret

.pos 0x1abc # Comment
.align 12
.long 0x1122

Stack: irmovl Stack, %ebp # Comment
    irmovl Stack, %esp

.long -47
    jmp Stack
    iaddl -259, %eax

jmp Stack

# Another comment
.long 1000000000

    irmovl Stack, %ebp
    irmovl Stack, %esp
    rrmovl %eax, %ecx
    rmmovl %ebp, 0x1c
    mrmovl 0x1c, %ebp
    rmmovl %ebp, 4(%eax)
    mrmovl 4(%eax), %ebp

# for (i=0;i<n;i--)
#    t[i] = t[i+1];

    mrmovl n,%eax      # n-i
    irmovl t,%ebx     # p

    isubl  1,%eax   # déjà fini ?
    jl     end

loop:
    rrmovl %ebx,%edx   #)
    iaddl  4,%edx      #) p+1
    mrmovl (%edx),%ecx # lire *(p+1)
    rmmovl %ecx,(%ebx) # écrire dans *p
    rrmovl %edx,%ebx   # p = p+1
    isubl  1,%eax   # décrémenter le compteur
    jge    loop

end:
    halt

n:
    .long 5
t:
    .long 1
    .long 4
    .long 6
    .long 4
    .long 4
    .long 3
    .long n
`

const overflow32bitsProgram = 
`
irmovl 0xfffffffff, %eax
irmovl 68719476735, %eax
`

const overflow64bitsProgram = 
`
irmovl 0xfffffffffffffffff, %eax
irmovl 295147905179352825855, %eax
`

const registerProgram = 
`
iaddl 1, %r8
iaddl 1, %r9
iaddl 1, %r10
iaddl 1, %r11
iaddl 1, %r12
iaddl 1, %r13
iaddl 1, %r14
`

const reference32bits =
`  0x0000:                       |  |  .pos 0 
  0x0000:                       |  |  Init:  
  0x0000: 30f5c81a0000          |  |      irmovl Stack,%ebp 
  0x0006: 30f4c81a0000          |  |      irmovl Stack,%esp 
  0x000c: 505400010000          |  |      mrmovl 0x100(%esp),%ebp 
  0x0012: 90                    |  |      ret  
                                |  |  
  0x1abc:                       |  |  .pos 0x1abc # Comment
  0x1abc:                       |  |  .align 12 
  0x1ac4: 22110000              |  |  .long 0x1122 
                                |  |  
  0x1ac8: 30f5c81a0000          |  |  Stack:      irmovl Stack,%ebp # Comment
  0x1ace: 30f4c81a0000          |  |      irmovl Stack,%esp 
                                |  |  
  0x1ad4: d1ffffff              |  |  .long -47 
  0x1ad8: 70c81a0000            |  |      jmp Stack 
  0x1add: c0f0fdfeffff          |  |      iaddl -259,%eax 
                                |  |  
  0x1ae3: 70c81a0000            |  |      jmp Stack 
                                |  |  
                                |  |  # Another comment
  0x1ae8: 00ca9a3b              |  |  .long 1000000000 
                                |  |  
  0x1aec: 30f5c81a0000          |  |      irmovl Stack,%ebp 
  0x1af2: 30f4c81a0000          |  |      irmovl Stack,%esp 
  0x1af8: 2001                  |  |      rrmovl %eax,%ecx 
  0x1afa: 405f1c000000          |  |      rmmovl %ebp,0x1c 
  0x1b00: 505f1c000000          |  |      mrmovl 0x1c,%ebp 
  0x1b06: 405004000000          |  |      rmmovl %ebp,4(%eax) 
  0x1b0c: 505004000000          |  |      mrmovl 4(%eax),%ebp 
                                |  |  
                                |  |  # for (i=0;i<n;i--)
                                |  |  #    t[i] = t[i+1];
                                |  |  
  0x1b12: 500f4b1b0000          |  |      mrmovl n,%eax # n-i
  0x1b18: 30f34f1b0000          |  |      irmovl t,%ebx # p
                                |  |  
  0x1b1e: c1f001000000          |  |      isubl 1,%eax # déjà fini ?
  0x1b24: 724a1b0000            |  |      jl end 
                                |  |  
  0x1b29:                       |  |  loop:  
  0x1b29: 2032                  |  |      rrmovl %ebx,%edx #)
  0x1b2b: c0f204000000          |  |      iaddl 4,%edx #) p+1
  0x1b31: 501200000000          |  |      mrmovl (%edx),%ecx # lire *(p+1)
  0x1b37: 401300000000          |  |      rmmovl %ecx,(%ebx) # écrire dans *p
  0x1b3d: 2023                  |  |      rrmovl %edx,%ebx # p = p+1
  0x1b3f: c1f001000000          |  |      isubl 1,%eax # décrémenter le compteur
  0x1b45: 75291b0000            |  |      jge loop 
                                |  |  
  0x1b4a:                       |  |  end:  
  0x1b4a: 10                    |  |      halt  
                                |  |  
  0x1b4b:                       |  |  n:  
  0x1b4b: 05000000              |  |  .long 5 
  0x1b4f:                       |  |  t:  
  0x1b4f: 01000000              |  |  .long 1 
  0x1b53: 04000000              |  |  .long 4 
  0x1b57: 06000000              |  |  .long 6 
  0x1b5b: 04000000              |  |  .long 4 
  0x1b5f: 04000000              |  |  .long 4 
  0x1b63: 03000000              |  |  .long 3 
  0x1b67: 4b1b0000              |  |  .long n 
`

const reference64bits = 
`  0x0000:                       |  |  .pos 0 
  0x0000:                       |  |  Init:  
  0x0000: 30f5cc1a000000000000  |  |      irmovl Stack,%ebp 
  0x000a: 30f4cc1a000000000000  |  |      irmovl Stack,%esp 
  0x0014: 50540001000000000000  |  |      mrmovl 0x100(%esp),%ebp 
  0x001e: 90                    |  |      ret  
                                |  |  
  0x1abc:                       |  |  .pos 0x1abc # Comment
  0x1abc:                       |  |  .align 12 
  0x1ac4: 2211000000000000      |  |  .long 0x1122 
                                |  |  
  0x1acc: 30f5cc1a000000000000  |  |  Stack:      irmovl Stack,%ebp # Comment
  0x1ad6: 30f4cc1a000000000000  |  |      irmovl Stack,%esp 
                                |  |  
  0x1ae0: d1ffffffffffffff      |  |  .long -47 
  0x1ae8: 70cc1a000000000000    |  |      jmp Stack 
  0x1af1: c0f0fdfeffffffffffff  |  |      iaddl -259,%eax 
                                |  |  
  0x1afb: 70cc1a000000000000    |  |      jmp Stack 
                                |  |  
                                |  |  # Another comment
  0x1b04: 00ca9a3b00000000      |  |  .long 1000000000 
                                |  |  
  0x1b0c: 30f5cc1a000000000000  |  |      irmovl Stack,%ebp 
  0x1b16: 30f4cc1a000000000000  |  |      irmovl Stack,%esp 
  0x1b20: 2001                  |  |      rrmovl %eax,%ecx 
  0x1b22: 405f1c00000000000000  |  |      rmmovl %ebp,0x1c 
  0x1b2c: 505f1c00000000000000  |  |      mrmovl 0x1c,%ebp 
  0x1b36: 40500400000000000000  |  |      rmmovl %ebp,4(%eax) 
  0x1b40: 50500400000000000000  |  |      mrmovl 4(%eax),%ebp 
                                |  |  
                                |  |  # for (i=0;i<n;i--)
                                |  |  #    t[i] = t[i+1];
                                |  |  
  0x1b4a: 500fa71b000000000000  |  |      mrmovl n,%eax # n-i
  0x1b54: 30f3af1b000000000000  |  |      irmovl t,%ebx # p
                                |  |  
  0x1b5e: c1f00100000000000000  |  |      isubl 1,%eax # déjà fini ?
  0x1b68: 72a61b000000000000    |  |      jl end 
                                |  |  
  0x1b71:                       |  |  loop:  
  0x1b71: 2032                  |  |      rrmovl %ebx,%edx #)
  0x1b73: c0f20400000000000000  |  |      iaddl 4,%edx #) p+1
  0x1b7d: 50120000000000000000  |  |      mrmovl (%edx),%ecx # lire *(p+1)
  0x1b87: 40130000000000000000  |  |      rmmovl %ecx,(%ebx) # écrire dans *p
  0x1b91: 2023                  |  |      rrmovl %edx,%ebx # p = p+1
  0x1b93: c1f00100000000000000  |  |      isubl 1,%eax # décrémenter le compteur
  0x1b9d: 75711b000000000000    |  |      jge loop 
                                |  |  
  0x1ba6:                       |  |  end:  
  0x1ba6: 10                    |  |      halt  
                                |  |  
  0x1ba7:                       |  |  n:  
  0x1ba7: 0500000000000000      |  |  .long 5 
  0x1baf:                       |  |  t:  
  0x1baf: 0100000000000000      |  |  .long 1 
  0x1bb7: 0400000000000000      |  |  .long 4 
  0x1bbf: 0600000000000000      |  |  .long 6 
  0x1bc7: 0400000000000000      |  |  .long 4 
  0x1bcf: 0400000000000000      |  |  .long 4 
  0x1bd7: 0300000000000000      |  |  .long 3 
  0x1bdf: a71b000000000000      |  |  .long n 
`

describe("Yas - tests", () => {
    test('Yas - 32bits - valid program', () => {
        const wordSize = 4;
        let instructionSet = new InstructionSet(defaultInstructions, wordSize);
        let yas = new Yas(registers_enum32bits, instructionSet, wordSize);

        let result = yas.assemble(validProgram);
        expect(result.errors.length).toBe(0);

        let memory = new Memory32bits(4)
        memory.loadProgram(result.output)

        let referenceMemory = new Memory32bits(4)
        referenceMemory.loadProgram(reference32bits)

        for (let i = 0; i < Memory32bits.LAST_ADDRESS; i++) {
            if (memory.readByte(BigInt(i)) != referenceMemory.readByte(BigInt(i))) {
                "Wrong byte at 0x" + console.log(i.toString(16))
            }
            expect(memory.readByte(BigInt(i))).toBe(referenceMemory.readByte(BigInt(i)))
        }

        expect(result.output).toBe(reference32bits)
    })

    test('Yas - 64bits - valid program', () => {
        const wordSize = 8;
        let instructionSet = new InstructionSet(defaultInstructions, wordSize);
        let yas = new Yas(registers_enum, instructionSet, wordSize);

        let result = yas.assemble(validProgram);
        expect(result.errors.length).toBe(0)

        let memory = new Memory64bits(8)
        memory.loadProgram(result.output)

        let referenceMemory = new Memory64bits(8)
        referenceMemory.loadProgram(reference64bits)

        for (let i = 0; i < Memory64bits.LAST_ADDRESS; i++) {
            if (memory.readByte(BigInt(i)) != referenceMemory.readByte(BigInt(i))) {
                "Wrong byte at 0x" + console.log(i.toString(16))
            }
            expect(memory.readByte(BigInt(i))).toBe(referenceMemory.readByte(BigInt(i)))
        }

        expect(result.output).toBe(reference64bits)
    });

    test('Yas - 32bits - invalid program', () => {
        const wordSize = 4;
        let instructionSet = new InstructionSet(defaultInstructions, wordSize);
        let yas = new Yas(registers_enum32bits, instructionSet, wordSize);

        //error in label
        let result = yas.assemble(`label`);
        expect(result.errors.length).not.toBe(0);

        result = yas.assemble(`1aebf:`);
        expect(result.errors.length).not.toBe(0);

        result = yas.assemble(`label: label:`);
        expect(result.errors.length).not.toBe(0);

        //error in directive
        result = yas.assemble(`pos 0x200`);
        expect(result.errors.length).not.toBe(0);

        result = yas.assemble(`.test 0x200`);
        expect(result.errors.length).not.toBe(0);

        result = yas.assemble(`.long label`);
        expect(result.errors.length).not.toBe(0);

        result = yas.assemble(`.long .pos 0x200`);
        expect(result.errors.length).not.toBe(0);

        //error on lines
        result = yas.assemble(`irmovl #comment 1, %eax`);
        expect(result.errors.length).not.toBe(0);

        result = yas.assemble(`irmovl 1, %eax iaddl 1, %ebx`);
        expect(result.errors.length).not.toBe(0);
    });

    test('Yas - 64bits - invalid program', () => {
        const wordSize = 8;
        let instructionSet = new InstructionSet(defaultInstructions, wordSize);
        let yas = new Yas(registers_enum, instructionSet, wordSize);

        //error in label
        let result = yas.assemble(`label`);
        expect(result.errors.length).not.toBe(0);

        result = yas.assemble(`1aebf:`);
        expect(result.errors.length).not.toBe(0);

        result = yas.assemble(`label: label:`);
        expect(result.errors.length).not.toBe(0);

        //error in directive
        result = yas.assemble(`pos 0x200`);
        expect(result.errors.length).not.toBe(0);

        result = yas.assemble(`.test 0x200`);
        expect(result.errors.length).not.toBe(0);

        result = yas.assemble(`.long label`);
        expect(result.errors.length).not.toBe(0);

        result = yas.assemble(`.long .pos 0x200`);
        expect(result.errors.length).not.toBe(0);

        //error on lines
        result = yas.assemble(`irmovl #comment 1, %eax`);
        expect(result.errors.length).not.toBe(0);

        result = yas.assemble(`irmovl 1, %eax iaddl 1, %ebx`);
        expect(result.errors.length).not.toBe(0);
    });

    test('Yas - 32bits - overflow error', () => {
        const wordSize = 4;
        let instructionSet = new InstructionSet(defaultInstructions, wordSize);
        let yas = new Yas(registers_enum32bits, instructionSet, wordSize);

        //expect to overflow
        let result = yas.assemble(overflow32bitsProgram);
        expect(result.errors.length).not.toBe(0);

        //expect to overflow
        result = yas.assemble(overflow64bitsProgram);
        expect(result.errors.length).not.toBe(0);
    });

    test('Yas - 64bits - overflow error', () => {
        const wordSize = 8;
        let instructionSet = new InstructionSet(defaultInstructions, wordSize);
        let yas = new Yas(registers_enum, instructionSet, wordSize);

        //expect to not overflow
        let result = yas.assemble(overflow32bitsProgram);
        expect(result.errors.length).toBe(0);

        //expect to overflow
        result = yas.assemble(overflow64bitsProgram);
        expect(result.errors.length).not.toBe(0);
    });

    test('Yas - 32bits - register error', () => {
        const wordSize = 4;
        let instructionSet = new InstructionSet(defaultInstructions, wordSize);
        let yas = new Yas(registers_enum32bits, instructionSet, wordSize);

        //expect to not recognize registers
        let result = yas.assemble(registerProgram);
        expect(result.errors.length).not.toBe(0);
    });

    test('Yas - 64bits - register error', () => {
        const wordSize = 8;
        let instructionSet = new InstructionSet(defaultInstructions, wordSize);
        let yas = new Yas(registers_enum, instructionSet, wordSize);

        //expect to recognize registers
        let result = yas.assemble(registerProgram);
        expect(result.errors.length).toBe(0);
    });
})