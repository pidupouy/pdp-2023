import { describe, expect, test } from '@jest/globals';
import { bigIntToByteArray, isByte, isNaN, stringToBigInt, toInt32, toInt64, byteArrayToBigInt, stringToNumber, is32Bits, is64Bits, toUint32, toUint64, padStringBigInt, stringHexaToByteArray} from "../../model/numberUtils"

describe("numberUtils - tests", () => {
    test("numberUtils - isByte", () => {
        //When byte is 0
        expect(isByte(0)).toBeTruthy();

        //When byte is max byte
        expect(isByte(255)).toBeTruthy();

        //When byte is min byte
        expect(isByte(-128)).toBeFalsy();

        //When byte is positive
        expect(isByte(75)).toBeTruthy();

        //When byte is negative
        expect(isByte(-75)).toBeFalsy();

        //When byte is superior to max byte
        expect(isByte(256)).toBeFalsy();

        //When byte is inferior to negative
        expect(isByte(-129)).toBeFalsy();
    })

    test("numberUtils - isNaN", () => {
        //When byte is 0
        expect(isNaN(0)).toBeFalsy();

        //When byte is positive
        expect(isNaN(255)).toBeFalsy();

        //When byte is netagive
        expect(isNaN(-128)).toBeFalsy();

        //When byte is bigint
        expect(isNaN(BigInt(75))).toBeFalsy();

        //When byte is negative
        expect(isNaN(NaN)).toBeTruthy();
    })

    test("numberUtils - is32Bits", () => {
        //When number is 0
        expect(is32Bits('0x0')).toBeTruthy();
        expect(is32Bits('0')).toBeTruthy();

        //When number is positive
        expect(is32Bits('0x200')).toBeTruthy();
        expect(is32Bits('512')).toBeTruthy();

        //When number is negative
        expect(is32Bits('0x8fffff00')).toBeTruthy();
        expect(is32Bits('-1879048448')).toBeTruthy();

        //When number is max positive
        expect(is32Bits('0x7fffffff')).toBeTruthy();
        expect(is32Bits('2147483647')).toBeTruthy();

        //When number is min negative
        expect(is32Bits('0x80000000')).toBeTruthy();
        expect(is32Bits('-2147483648')).toBeTruthy();

        //When number is full
        expect(is32Bits('0xffffffff')).toBeTruthy();

        //When number exceed 32bits
        expect(is32Bits('0x7fffffffff')).toBeFalsy();
        expect(is32Bits('549755813887')).toBeFalsy();
        expect(is32Bits('0x8084521357')).toBeFalsy();
        expect(is32Bits('0x7ff84521357')).toBeFalsy();
    })

    test("numberUtils - is64Bits", () => {
        //When number is 0
        expect(is64Bits('0x0')).toBeTruthy();
        expect(is64Bits('0')).toBeTruthy();

        //When number is positive
        expect(is64Bits('0x2000000000')).toBeTruthy();
        expect(is64Bits('137438953472')).toBeTruthy();

        //When number is negative
        expect(is64Bits('0x8fffff0000000000')).toBeTruthy();
        expect(is64Bits('-8070451631759556608')).toBeTruthy();

        //When number is max positive
        expect(is64Bits('0x7fffffffffffffff')).toBeTruthy();
        expect(is64Bits('9223372036854775807')).toBeTruthy();

        //When number is min negative
        expect(is64Bits('0x8000000000000000')).toBeTruthy();
        expect(is64Bits('-9223372036854775808')).toBeTruthy();

        //When number is full
        expect(is64Bits('0xffffffffffffffff')).toBeTruthy();

        //When number exceed 32bits
        expect(is64Bits('0x7fffffffffffffffff')).toBeFalsy();
        expect(is64Bits('549755813887745421328')).toBeFalsy();
        expect(is64Bits('0x80845213574845423128')).toBeFalsy();
        expect(is64Bits('0x7ff84521357fff846842')).toBeFalsy();
    })

    test("numberUtils - toInt32", () => {
        //When number is 0
        expect(toInt32(BigInt(0x0)) === BigInt(0)).toBeTruthy();
        expect(toInt32(BigInt(0)) === BigInt(0)).toBeTruthy();

        //When number is max positive
        expect(toInt32(BigInt(0x7fffffff)) === BigInt(2147483647)).toBeTruthy();
        expect(toInt32(BigInt(2147483647)) === BigInt(2147483647)).toBeTruthy();

        //When number is min negative
        expect(toInt32(BigInt(0x80000000)) === BigInt(-2147483648)).toBeTruthy();
        expect(toInt32(BigInt(-2147483648)) === BigInt(-2147483648)).toBeTruthy();

        //When number exceed 32bits
        expect(toInt32(BigInt(0x7fffffffff)) === BigInt(-1)).toBeTruthy();
        expect(toInt32(BigInt(549755813887)) === BigInt(-1)).toBeTruthy();
        expect(toInt32(BigInt(0x8084521357)) === toInt32(BigInt(0x84521357))).toBeTruthy();
        expect(toInt32(BigInt(0x7ff84521357)) === toInt32(BigInt(0x84521357))).toBeTruthy();
    })

    test("numberUtils - toInt64", () => {
        //When number is 0
        expect(toInt64(BigInt(0x0)) === BigInt(0)).toBeTruthy();
        expect(toInt64(BigInt(0)) === BigInt(0)).toBeTruthy();

        //When number is max positive
        expect(toInt64(BigInt('0x7fffffffffffffff')) === BigInt('9223372036854775807')).toBeTruthy();
        expect(toInt64(BigInt('9223372036854775807')) === BigInt('9223372036854775807')).toBeTruthy();

        //When number is min negative
        expect(toInt64(BigInt('0x8000000000000000')) === BigInt('-9223372036854775808')).toBeTruthy();
        expect(toInt64(BigInt(-'9223372036854775808')) === BigInt('-9223372036854775808')).toBeTruthy();

        //When number exceed 64bits
        expect(toInt64(BigInt('0x7fffffffffffffffff')) === BigInt(-1)).toBeTruthy();
        expect(toInt64(BigInt('2361183241434822606847')) === BigInt(-1)).toBeTruthy();
        expect(toInt64(BigInt('0x808465214875648513')) === toInt64(BigInt('0x8465214875648513'))).toBeTruthy();
        expect(toInt64(BigInt('0x7f6754218864435894')) === toInt64(BigInt('0x6754218864435894'))).toBeTruthy();
    })

    test("numberUtils - toUint32", () => {
        //When number is 0
        expect(toUint32(BigInt(0x0)) === BigInt(0)).toBeTruthy();
        expect(toUint32(BigInt(0)) === BigInt(0)).toBeTruthy();

        //When number is max positive
        expect(toUint32(BigInt(0x7fffffff)) === BigInt(2147483647)).toBeTruthy();
        expect(toUint32(BigInt(2147483647)) === BigInt(2147483647)).toBeTruthy();

        //When number is min negative
        expect(toUint32(BigInt(0x80000000)) === BigInt(2147483648)).toBeTruthy();
        expect(toUint32(BigInt(-2147483648)) === BigInt(2147483648)).toBeTruthy();

        //When number is full
        expect(toUint32(BigInt(0xffffffff)) === BigInt(4294967295)).toBeTruthy();
        expect(toUint32(BigInt(-1)) === BigInt(4294967295)).toBeTruthy();

        //When number exceed 32bits
        expect(toUint32(BigInt(0x7fffffffff)) === BigInt(0xffffffff)).toBeTruthy();
        expect(toUint32(BigInt(549755813887)) === BigInt(0xffffffff)).toBeTruthy();
        expect(toUint32(BigInt(0x8084521357)) === toUint32(BigInt(0x84521357))).toBeTruthy();
        expect(toUint32(BigInt(0x7ff84521357)) === toUint32(BigInt(0x84521357))).toBeTruthy();
    })

    test("numberUtils - toUint64", () => {
        //When number is 0
        expect(toUint64(BigInt(0x0)) === BigInt(0)).toBeTruthy();
        expect(toUint64(BigInt(0)) === BigInt(0)).toBeTruthy();

        //When number is max positive
        expect(toUint64(BigInt('0x7fffffffffffffff')) === BigInt('9223372036854775807')).toBeTruthy();
        expect(toUint64(BigInt('9223372036854775807')) === BigInt('9223372036854775807')).toBeTruthy();

        //When number is min negative
        expect(toUint64(BigInt('0x8000000000000000')) === BigInt('9223372036854775808')).toBeTruthy();
        expect(toUint64(BigInt(-'9223372036854775808')) === BigInt('9223372036854775808')).toBeTruthy();

        //When number is full
        expect(toUint64(BigInt('0xffffffffffffffff')) === BigInt('18446744073709551615')).toBeTruthy();
        expect(toUint64(BigInt('-1')) === BigInt('18446744073709551615')).toBeTruthy();

        //When number exceed 64bits
        expect(toUint64(BigInt('0x7fffffffffffffffff')) === BigInt('0xffffffffffffffff')).toBeTruthy();
        expect(toUint64(BigInt('2361183241434822606847')) === BigInt('0xffffffffffffffff')).toBeTruthy();
        expect(toUint64(BigInt('0x808465214875648513')) === toUint64(BigInt('0x8465214875648513'))).toBeTruthy();
        expect(toUint64(BigInt('0x7f6754218864435894')) === toUint64(BigInt('0x6754218864435894'))).toBeTruthy();
    })

    test("numberUtils - byteArrayToBigInt", () => {
        //When byteArray is 0
        expect(byteArrayToBigInt([0,0,0,0,0,0,0,0]) === BigInt(0)).toBeTruthy();

        //When byteArray is max positive
        expect(byteArrayToBigInt([255,255,255,255,255,255,255,127]) === BigInt('9223372036854775807')).toBeTruthy();

        //When byteArray is min negative
        expect(byteArrayToBigInt([0,0,0,0,0,0,0,128]) === BigInt('9223372036854775808')).toBeTruthy();

        //When byteArray is full
        expect(byteArrayToBigInt([255,255,255,255,255,255,255,255]) === BigInt('18446744073709551615')).toBeTruthy();

        //When byteArray is positive
        expect(byteArrayToBigInt([47,98,125,127,220,48,52,30]) === BigInt('2176418243516654127')).toBeTruthy();

        //When byteArray is negative
        expect(byteArrayToBigInt([156,251,67,58,196,57,246,128]) === BigInt('9292678396079504284')).toBeTruthy();

        //When byteArray is invalid
        expect(() => { byteArrayToBigInt([156,251,67,57,25,68,87]) }).toThrow();
        expect(() => { byteArrayToBigInt([156,251,67,47,88,97,45, 256]) }).toThrow();
        expect(() => { byteArrayToBigInt([156,251,67,78,94,137,98, -129]) }).toThrow();
        expect(() => { byteArrayToBigInt([156,251,67,78,55,48,123,89,44]) }).toThrow();
    })

    test("numberUtils - stringToNumber", () => {
        //When string is decimal
        expect(stringToNumber('184542') === 184542).toBeTruthy();
        expect(stringToNumber('-184542') === -184542).toBeTruthy();

        //When string is hexadecimal
        expect(stringToNumber('0xadb78') === 711544).toBeTruthy();

        //When string is binary
        expect(stringToNumber('0b01001110') === 78).toBeTruthy();

        //When string is max positive
        expect(stringToNumber('0x7fffffff') === 2147483647).toBeTruthy();

        //When string is min negative
        expect(stringToNumber('0x80000000') === -2147483648).toBeTruthy();
        expect(stringToNumber('-2147483648') === -2147483648).toBeTruthy();

        //When string there is 32-bits overflow
        expect(stringToNumber('0x8080000000') === -2147483648).toBeTruthy();
        expect(stringToNumber('551903297536') === -2147483648).toBeTruthy();
        expect(stringToNumber('0b1000000010000000000000000000000000000000') === -2147483648).toBeTruthy();

        //When string is invalid
        expect(() => { stringToNumber('0z12387') }).toThrow();
        expect(() => { stringToNumber('') }).toThrow();
        expect(() => { stringToNumber('afde') }).toThrow();
    })

    test("numberUtils - stringToBigInt", () => {
        //When string is decimal
        expect(stringToBigInt('18454257842487546') === BigInt('18454257842487546')).toBeTruthy();
        expect(stringToBigInt('-184542741238761') === BigInt('18446559530968312855')).toBeTruthy();

        //When string is hexadecimal
        expect(stringToBigInt('0xadb7878139') === BigInt('746108453177')).toBeTruthy();

        //When string is binary
        expect(stringToBigInt('0b01001110') === BigInt(78)).toBeTruthy();

        //When string is max positive
        expect(stringToBigInt('0x7fffffffffffffff') === BigInt('9223372036854775807')).toBeTruthy();
        
        //When string is min negative
        expect(stringToBigInt('0x8000000000000000') === BigInt('9223372036854775808')).toBeTruthy();
        expect(stringToBigInt('-9223372036854775808') === BigInt('9223372036854775808')).toBeTruthy();

        //When string there is 32-bits overflow
        expect(stringToBigInt('0x808000000000000000') === BigInt('9223372036854775808')).toBeTruthy();

        //When string is invalid
        expect(() => { stringToBigInt('0z12387') }).toThrow();
        expect(() => { stringToBigInt('') }).toThrow();
        expect(() => { stringToBigInt('afde') }).toThrow();
    })

    test("numberUtils - padStringBigInt", () => {
        //When string is empty
        expect(padStringBigInt("", 8) === "00000000").toBeTruthy();
        expect(padStringBigInt("", 16) === "0000000000000000").toBeTruthy();

        //When string is hexadecimal
        expect(padStringBigInt("aabb", 8) === "0000aabb").toBeTruthy();
        expect(padStringBigInt("aabb", 16) === "000000000000aabb").toBeTruthy();

        //When string is max positive
        expect(padStringBigInt("ffffffff", 8) === "ffffffff").toBeTruthy();
        expect(padStringBigInt("ffffffffffffffff", 16) === "ffffffffffffffff").toBeTruthy();
        
        //When string is min negative
        expect(padStringBigInt("80000000", 8) === "80000000").toBeTruthy();
        expect(padStringBigInt("8000000000000000", 16) === "8000000000000000").toBeTruthy();

        //When pad is different
        expect(padStringBigInt("aabb", 8, 'x') === "xxxxaabb").toBeTruthy();
        expect(padStringBigInt("aabb", 8, 'xa') === "xaxaaabb").toBeTruthy();

        //When pad is invalid
        expect(() => { padStringBigInt("", 4*2, '') }).toThrow();
    })

    test("bigIntToByteArray", () => {
        let word64bits = 8;

        //When bigint is 0
        expect(bigIntToByteArray(BigInt(0x0), word64bits, true)).toStrictEqual([0, 0, 0, 0, 0, 0, 0, 0])
        expect(bigIntToByteArray(BigInt(0), word64bits, true)).toStrictEqual([0, 0, 0, 0, 0, 0, 0, 0])

        //When bigint is max positive
        expect(bigIntToByteArray(BigInt('0x7fffffffffffffff'), word64bits, true)).toStrictEqual([0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x7f])
        expect(bigIntToByteArray(BigInt('9223372036854775807'), word64bits, true)).toStrictEqual([0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x7f])

        //When bigint is max negative
        expect(bigIntToByteArray(BigInt('0x8000000000000000'), word64bits, true)).toStrictEqual([0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80])
        expect(bigIntToByteArray(BigInt('-9223372036854775808'), word64bits, true)).toStrictEqual([0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80])

        //When bigint is max positive - 1
        expect(bigIntToByteArray(BigInt('0x7ffffffffffffffe'), word64bits, true)).toStrictEqual([0xfe, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x7f])
        expect(bigIntToByteArray(BigInt('9223372036854775806'), word64bits, true)).toStrictEqual([0xfe, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x7f])

        //When bigint is max negative + 1
        expect(bigIntToByteArray(BigInt('0x8000000000000001'), word64bits, true)).toStrictEqual([0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80])
        expect(bigIntToByteArray(BigInt('-9223372036854775807'), word64bits, true)).toStrictEqual([0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80])
        expect(bigIntToByteArray(stringToBigInt('-9223372036854775807'), word64bits, true)).toStrictEqual([0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80])

        //When bigint is superior to max positive
        expect(bigIntToByteArray(BigInt('0x7777666655554444333322221111'), word64bits, true)).toStrictEqual([0x11, 0x11, 0x22, 0x22, 0x33, 0x33, 0x44, 0x44])
        expect(bigIntToByteArray(BigInt('991111111111111111111111111111'), word64bits, true)).toStrictEqual([0xc7, 0x71, 0x1c, 0x87, 0x77, 0x96, 0x37, 0x0f])

        //When bigint is inferior to max negative
        expect(bigIntToByteArray(BigInt('0x88889999aaaabbbbccccddddeeeeffff'), word64bits, true)).toStrictEqual([0xff, 0xff, 0xee, 0xee, 0xdd, 0xdd, 0xcc, 0xcc])
        expect(bigIntToByteArray(BigInt('181484275182906374266210424515078324223'), word64bits, true)).toStrictEqual([0xff, 0xff, 0xee, 0xee, 0xdd, 0xdd, 0xcc, 0xcc])

        //When bigint is positive
        expect(bigIntToByteArray(BigInt('0x1122334455667788'), word64bits, true)).toStrictEqual([0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11])
        expect(bigIntToByteArray(BigInt('1234605616436508552'), word64bits, true)).toStrictEqual([0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11])

        //When bigint is negative
        expect(bigIntToByteArray(BigInt('0xffeeddccbbaa9988'), word64bits, true)).toStrictEqual([0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff])
        expect(bigIntToByteArray(stringToBigInt('-4822678189205112'), word64bits, true)).toStrictEqual([0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff])
    })

    test("numberUtils - stringHexaToByteArray", () => {
        //When string is invalid
        expect(() => { stringHexaToByteArray("") }).toThrow();
        expect(() => { stringHexaToByteArray("abcdefg") }).toThrow();
        expect(() => { stringHexaToByteArray("_aabb") }).toThrow();

        //When sting is valid
        expect(stringHexaToByteArray("00000000")).toStrictEqual([0, 0, 0, 0])
        expect(stringHexaToByteArray("aabbccddeeff")).toStrictEqual([0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff])
        expect(stringHexaToByteArray("123456789")).toStrictEqual([0x12, 0x34, 0x56, 0x78, 0x9])
        expect(stringHexaToByteArray("AABB1122")).toStrictEqual([0xaa, 0xbb, 0x11, 0x22])
    })
});