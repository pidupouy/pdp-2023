import { describe, expect, test } from '@jest/globals';
import { registerNextVerification64Bits, registerVerification64Bits } from '../../testUtils';
import { registers_enum, registersList64bits } from "../../../model/interfaces/IRegisters";
import { Registers } from "../../../model/registers";

const loadData = { "current" : {"eax":"42","ebx":"80","ecx":"50","edx":"198","esi":"-53","edi":"-47","ebp":"-9223372036854775808","esp":"9223372036854775807","r8":"0","r9":"0","r10":"0","r11":"0","r12":"0","r13":"0","r14":"0"}, "next" : {"eax":"875","ebx":"64","ecx":"1894","edx":"3864","esi":"-54452","edi":"-8754","ebp":"-9223372036854775808","esp":"9223372036854775807","r8":"0","r9":"0","r10":"0","r11":"0","r12":"0","r13":"0","r14":"0"}}

const saveData = `{ "current" : {"eax":"42","ecx":"50","edx":"198","ebx":"80","esp":"9223372036854775807","ebp":"9223372036854775808","esi":"18446744073709551563","edi":"18446744073709551569","r8":"0","r9":"0","r10":"0","r11":"0","r12":"0","r13":"0","r14":"0"}, "next" : {"eax":"875","ecx":"1894","edx":"3864","ebx":"64","esp":"9223372036854775807","ebp":"9223372036854775808","esi":"18446744073709497164","edi":"18446744073709542862","r8":"0","r9":"0","r10":"0","r11":"0","r12":"0","r13":"0","r14":"0"}}`

const wordSize = 8;

describe("Registers - 64bits - tests", () => {
    /**
     * Test the registers constructor.
     * Check if all the registers are set to 0 during initialization.
     */
    test("Registers - 64bits - constructor", () => {
        let registers = new Registers(wordSize, registersList64bits);
        registerVerification64Bits(registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification64Bits(registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
    });

    /**
     * Test the write function
     * Check if the value is correctly set in the contentNext
     */
    test("Registers - 64bits - write", () => {
        let registers = new Registers(wordSize, registersList64bits);

        //positive number
        registers.write(registers_enum.eax, BigInt(10));
        registerVerification64Bits(registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification64Bits(registers, BigInt(10), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));

        //negative number
        registers.write(registers_enum.ebx, BigInt(-10));
        registerVerification64Bits(registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification64Bits(registers, BigInt(10), BigInt('0xfffffffffffffff6'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));

        //zero
        registers.write(registers_enum.ebx, BigInt(0));
        registerVerification64Bits(registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification64Bits(registers, BigInt(10), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        
        //max number
        registers.write(registers_enum.ecx, BigInt('0x7fffffffffffffff'));
        registerVerification64Bits(registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification64Bits(registers, BigInt(10), BigInt(0), BigInt('0x7fffffffffffffff'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));

        //min number
        registers.write(registers_enum.edx, BigInt('0x8000000000000000'));
        registerVerification64Bits(registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification64Bits(registers, BigInt(10), BigInt(0), BigInt('0x7fffffffffffffff'), BigInt('0x8000000000000000'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
    });

    /**
     * Test the read and readNext function
     * Check if the value is correctly read from content and contentNext
     */
    test("Registers - 64bits - read/readNext", () => {
        let registers = new Registers(wordSize, registersList64bits);

        //positive number
        registers.write(registers_enum.eax, BigInt(10));
        registers.updateRegisters();
        registers.write(registers_enum.eax, BigInt(20));
        registerVerification64Bits(registers, BigInt(10), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification64Bits(registers, BigInt(20), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        expect(registers.read(registers_enum.eax) === BigInt(10)).toBeTruthy();
        expect(registers.readNext(registers_enum.eax) === BigInt(20)).toBeTruthy();

        //negative number
        registers.write(registers_enum.ebx, BigInt(-10));
        registers.updateRegisters();
        registers.write(registers_enum.ebx, BigInt(-20));
        registerVerification64Bits(registers, BigInt(20), BigInt('0xfffffffffffffff6'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification64Bits(registers, BigInt(20), BigInt('0xffffffffffffffec'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        expect(registers.read(registers_enum.ebx) === BigInt('0xfffffffffffffff6')).toBeTruthy();
        expect(registers.readNext(registers_enum.ebx) === BigInt('0xffffffffffffffec')).toBeTruthy();

        //zero
        expect(registers.read(registers_enum.ecx) === BigInt(0)).toBeTruthy();
        expect(registers.readNext(registers_enum.ecx) === BigInt(0)).toBeTruthy();
        
        //max number
        registers.write(registers_enum.ecx, BigInt('0x7fffffffffffffff'));
        registers.updateRegisters();
        registerVerification64Bits(registers, BigInt(20), BigInt('0xffffffffffffffec'), BigInt('0x7fffffffffffffff'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification64Bits(registers, BigInt(20), BigInt('0xffffffffffffffec'), BigInt('0x7fffffffffffffff'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        expect(registers.read(registers_enum.ecx) === BigInt('0x7fffffffffffffff')).toBeTruthy();
        expect(registers.readNext(registers_enum.ecx) === BigInt('0x7fffffffffffffff')).toBeTruthy();

        //min number
        registers.write(registers_enum.edx, BigInt('0x8000000000000000'));
        registers.updateRegisters();
        registerVerification64Bits(registers, BigInt(20), BigInt('0xffffffffffffffec'), BigInt('0x7fffffffffffffff'), BigInt('0x8000000000000000'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification64Bits(registers, BigInt(20), BigInt('0xffffffffffffffec'), BigInt('0x7fffffffffffffff'), BigInt('0x8000000000000000'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        expect(registers.read(registers_enum.edx) === BigInt('0x8000000000000000')).toBeTruthy();
        expect(registers.readNext(registers_enum.edx) === BigInt('0x8000000000000000')).toBeTruthy();
    });

    /**
     * Test the updateRegister function
     * Check if the value is correctly updated from contentNext to content
     */
    test("Registers - 64bits - updateRegisters", () => {
        let registers = new Registers(wordSize, registersList64bits);

        registers.write(registers_enum.eax, BigInt(10));
        registers.write(registers_enum.ebx, BigInt(-10));
        registers.write(registers_enum.ecx, BigInt('0x7fffffffffffffff'));
        registers.write(registers_enum.edx, BigInt('0x8000000000000000'));

        registers.updateRegisters();
        registerVerification64Bits(registers, BigInt(10), BigInt('0xfffffffffffffff6'), BigInt('0x7fffffffffffffff'), BigInt('0x8000000000000000'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification64Bits(registers, BigInt(10), BigInt('0xfffffffffffffff6'), BigInt('0x7fffffffffffffff'), BigInt('0x8000000000000000'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
    });

    /**
     * Test the getRegisterName function
     * Check if the correct name is returned
     */
    test("Registers - 64bits - getRegisterName", () => {
        let registers = new Registers(wordSize, registersList64bits);

        expect(registers.getRegisterName(registers_enum.eax)).toBe("eax");
        expect(registers.getRegisterName(registers_enum.ebx)).toBe("ebx");
        expect(registers.getRegisterName(registers_enum.ecx)).toBe("ecx");
        expect(registers.getRegisterName(registers_enum.edx)).toBe("edx");
        expect(registers.getRegisterName(registers_enum.esi)).toBe("esi");
        expect(registers.getRegisterName(registers_enum.edi)).toBe("edi");
        expect(registers.getRegisterName(registers_enum.ebp)).toBe("ebp");
        expect(registers.getRegisterName(registers_enum.esp)).toBe("esp");
        expect(registers.getRegisterName(registers_enum.r8)).toBe("r8");
        expect(registers.getRegisterName(registers_enum.r9)).toBe("r9");
        expect(registers.getRegisterName(registers_enum.r10)).toBe("r10");
        expect(registers.getRegisterName(registers_enum.r11)).toBe("r11");
        expect(registers.getRegisterName(registers_enum.r12)).toBe("r12");
        expect(registers.getRegisterName(registers_enum.r13)).toBe("r13");
        expect(registers.getRegisterName(registers_enum.r14)).toBe("r14");
        expect(registers.getRegisterName(registers_enum.none)).toBe("none");
    });

    /**
     * Test the load function
     * Check if the correct numbers are put in the registers
     */
    test("Registers - 64bits - load", () => {
        let registers = new Registers(wordSize, registersList64bits);
        registerVerification64Bits(registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        
        registers.load(loadData);
        registerVerification64Bits(registers, BigInt('42'), BigInt('80'), BigInt('50'), BigInt('198'), BigInt('0xffffffffffffffcb'), BigInt('0xffffffffffffffd1'), BigInt('0x8000000000000000'), BigInt('9223372036854775807'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification64Bits(registers, BigInt('875'), BigInt('64'), BigInt('1894'), BigInt('3864'), BigInt('0xffffffffffff2b4c'), BigInt('0xffffffffffffddce'), BigInt('0x8000000000000000'), BigInt('9223372036854775807'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
    });

    /**
     * Test the save function
     * Check if the returning string is correct
     */
    test("Registers - 64bits - save", () => {
        let registers = new Registers(wordSize, registersList64bits);
        registerVerification64Bits(registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        
        registers.load(loadData);
        expect(registers.save()).toBe(saveData);
    });
});