import { describe, expect, test } from '@jest/globals';
import { computeAluTest, computeCCTest, updateFlagsTest } from '../../testUtils';
import { JMP_enum, alufct } from "../../../model/interfaces/IAlu";
import { Alu } from "../../../model/alu";

const loadData = { "current": { "ZF": false, "OF": true, "SF": false }, "next": { "ZF": true, "OF": false, "SF": true } }

const saveData = `{ "current" : {"ZF":false,"OF":true,"SF":false}, "next" : {"ZF":true,"OF":false,"SF":true}}`

const wordSize = 8;

describe("Alu - 64bits - tests", () => {
    /**
     * Test the alu constructor.
     * For the moment it just check all the flags are set to false (0) during initialization.
     */
    test("Alu - 64bits - constructor", () => {
        // test constructor
        let alu = new Alu(wordSize);

        expect(alu.getZF()).toBe(false);
        expect(alu.getSF()).toBe(false);
        expect(alu.getOF()).toBe(false);

        expect(alu.getNextZF()).toBe(false);
        expect(alu.getNextSF()).toBe(false);
        expect(alu.getNextOF()).toBe(false);
    });

    /**
     * Test the computeAlu function with the ADD operation.
     */
    test("Alu - 64bits - computeAlu - ADD", () => {
        let alu = new Alu(wordSize);

        // Two zero
        computeAluTest(alu, BigInt(0), BigInt(0), alufct.A_ADD, BigInt(0));
        // One positive
        computeAluTest(alu, BigInt(10), BigInt(0), alufct.A_ADD, BigInt(10));
        computeAluTest(alu, BigInt(0), BigInt(10), alufct.A_ADD, BigInt(10));
        computeAluTest(alu, BigInt(-100), BigInt(10), alufct.A_ADD, BigInt('0xFFFFFFFFFFFFFFA6'));
        computeAluTest(alu, BigInt(5), BigInt(-10), alufct.A_ADD, BigInt('0xFFFFFFFFFFFFFFFB'));
        computeAluTest(alu, BigInt('0x80000000000000AC'), BigInt('0xFFFFFFFFFF9C'), alufct.A_ADD, BigInt('0x8001000000000048'));
        // Two positive
        computeAluTest(alu, BigInt(14), BigInt(18), alufct.A_ADD, BigInt(32));
        computeAluTest(alu, BigInt('0xAC'), BigInt('0xFFFFFFFFFF9C'), alufct.A_ADD, BigInt('0x1000000000048'));
        // One negative
        computeAluTest(alu, BigInt(-10), BigInt(0), alufct.A_ADD, BigInt('0xFFFFFFFFFFFFFFF6'));
        computeAluTest(alu, BigInt(0), BigInt(-10), alufct.A_ADD, BigInt('0xFFFFFFFFFFFFFFF6'));
        computeAluTest(alu, BigInt('0x8001000000000048'), BigInt(0), alufct.A_ADD, BigInt('0x8001000000000048'));
        // Two negative
        computeAluTest(alu, BigInt(-15), BigInt(-12), alufct.A_ADD, BigInt('0xFFFFFFFFFFFFFFE5'));
        computeAluTest(alu, BigInt('0x8001000000000048'), BigInt('0x80000000000000AC'), alufct.A_ADD, BigInt('0x10000000000f4'));
        // result positive
        computeAluTest(alu, BigInt(-100), BigInt(1100), alufct.A_ADD, BigInt(1000));
        computeAluTest(alu, BigInt(100), BigInt(-10), alufct.A_ADD, BigInt(90));
        computeAluTest(alu, BigInt(100), BigInt(10), alufct.A_ADD, BigInt(110));
        // result negative
        computeAluTest(alu, BigInt(100), BigInt(-1100), alufct.A_ADD, BigInt('0xFFFFFFFFFFFFFC18'));
        computeAluTest(alu, BigInt(-100), BigInt(10), alufct.A_ADD, BigInt('0xFFFFFFFFFFFFFFA6'));
        computeAluTest(alu, BigInt(-100), BigInt(-10), alufct.A_ADD, BigInt('0xFFFFFFFFFFFFFF92'));
        // result = 0
        computeAluTest(alu, BigInt(100), BigInt(-100), alufct.A_ADD, BigInt(0));
        computeAluTest(alu, BigInt('0xffffffffff'), BigInt('0xffffff0000000001'), alufct.A_ADD, BigInt(0));
        // Overflow
        computeAluTest(alu, BigInt('0x8000000000000000'), BigInt(-1), alufct.A_ADD, BigInt('0x7FFFFFFFFFFFFFFF'));
        computeAluTest(alu, BigInt('0x7FFFFFFFFFFFFFFF'), BigInt(1), alufct.A_ADD, BigInt('0x8000000000000000'));
    });

    /**
     * Test the computeAlu function with the SUB operation.
     */
    test("Alu - 64bits - computeAlu - SUB", () => {
        let alu = new Alu(wordSize);

        // Two zero
        computeAluTest(alu, BigInt(0), BigInt(0), alufct.A_SUB, BigInt(0));
        // One positive
        computeAluTest(alu, BigInt(0), BigInt(10), alufct.A_SUB, BigInt(10));
        computeAluTest(alu, BigInt(10), BigInt(0), alufct.A_SUB, BigInt('0xFFFFFFFFFFFFFFF6'));
        computeAluTest(alu, BigInt(10), BigInt(-100), alufct.A_SUB, BigInt('0xFFFFFFFFFFFFFF92'));
        computeAluTest(alu, BigInt(-10), BigInt(5), alufct.A_SUB, BigInt(15));
        computeAluTest(alu, BigInt('0x80000000000000AC'), BigInt('0xFFFFFFFFFF9C'), alufct.A_SUB, BigInt('0x8000fffffffffef0'));
        // Two positive
        computeAluTest(alu, BigInt(18), BigInt(14), alufct.A_SUB, BigInt('0xFFFFFFFFFFFFFFFC'));
        computeAluTest(alu, BigInt('0xFFFFFFFFFF9C'), BigInt('0xAC'), alufct.A_SUB, BigInt('0xffff000000000110'));
        // One negative
        computeAluTest(alu, BigInt(0), BigInt(-10), alufct.A_SUB, BigInt('0xFFFFFFFFFFFFFFF6'));
        computeAluTest(alu, BigInt(-10), BigInt(0), alufct.A_SUB, BigInt(10));
        computeAluTest(alu, BigInt('0x8001000000000048'), BigInt(0), alufct.A_SUB, BigInt('0x7ffeffffffffffb8'));
        // Two negative
        computeAluTest(alu, BigInt(-12), BigInt(-15), alufct.A_SUB, BigInt('0xFFFFFFFFFFFFFFFD'));
        computeAluTest(alu, BigInt('0x8001000000000048'), BigInt('0x80000000000000AC'), alufct.A_SUB, BigInt('0xffff000000000064'));
        // result negative
        computeAluTest(alu, BigInt(1100), BigInt(-100), alufct.A_SUB, BigInt('0xFFFFFFFFFFFFFB50'));
        // result positive
        computeAluTest(alu, BigInt(10), BigInt(100), alufct.A_SUB, BigInt(90));
        computeAluTest(alu, BigInt(-1100), BigInt(100), alufct.A_SUB, BigInt(1200));
        computeAluTest(alu, BigInt(-10), BigInt(-100), alufct.A_SUB, BigInt('0xFFFFFFFFFFFFFFA6'));
        // result = 0
        computeAluTest(alu, BigInt(-100), BigInt(-100), alufct.A_SUB, BigInt(0));
        computeAluTest(alu, BigInt(100), BigInt(100), alufct.A_SUB, BigInt(0));
        computeAluTest(alu, BigInt('0xffffffffff'), BigInt('0xffffffffff'), alufct.A_SUB, BigInt(0));
        // Overflow
        computeAluTest(alu, BigInt(1), BigInt('0x8000000000000000'), alufct.A_SUB, BigInt('0x7FFFFFFFFFFFFFFF'));
        computeAluTest(alu, BigInt(-1), BigInt('0x7FFFFFFFFFFFFFFF'), alufct.A_SUB, BigInt('0x8000000000000000'));
    });

    /**
     * Test the computeAlu function with the AND operation.
     */
    test("Alu - 64bits - computeAlu - AND", () => {
        let alu = new Alu(wordSize);

        // Two zero
        computeAluTest(alu, BigInt(0), BigInt(0), alufct.A_AND, BigInt(0));
        // One positive
        computeAluTest(alu, BigInt(10), BigInt(0), alufct.A_AND, BigInt(0));
        computeAluTest(alu, BigInt(0), BigInt(10), alufct.A_AND, BigInt(0));
        computeAluTest(alu, BigInt('0xFFFFFF9C'), BigInt('0xa'), alufct.A_AND, BigInt('0x8'));
        computeAluTest(alu, BigInt('0x5'), BigInt('0xFFFFFFF6'), alufct.A_AND, BigInt('0x4'));
        computeAluTest(alu, BigInt('0xFFFFFFFFFFFFFFFF'), BigInt('0xAC'), alufct.A_AND, BigInt('0xAC'));
        // Two positive
        computeAluTest(alu, BigInt('0xE'), BigInt('0x12'), alufct.A_AND, BigInt('0x2'));
        computeAluTest(alu, BigInt('0xFFFFFFFFFF9C'), BigInt('0xFFFFFFFFFFa'), alufct.A_AND, BigInt('0xfffffffff98'));
        // One negative
        computeAluTest(alu, BigInt(-10), BigInt(0), alufct.A_AND, BigInt(0));
        computeAluTest(alu, BigInt(0), BigInt(-10), alufct.A_AND, BigInt(0));
        // Two negative
        computeAluTest(alu, BigInt('0xFFFFFF15'), BigInt('0xFFFFFF12'), alufct.A_AND, BigInt('0xFFFFFF10'));
        computeAluTest(alu, BigInt('0xFFFFFFFFFFFFFFFF'), BigInt('0xFFFFFFFFFFFFFFAC'), alufct.A_AND, BigInt('0xFFFFFFFFFFFFFFAC'));
        // result negative
        computeAluTest(alu, BigInt('0xFFFFFF78'), BigInt('0xF0000000'), alufct.A_AND, BigInt('0xF0000000'));
        // result positive
        computeAluTest(alu, BigInt('0x100'), BigInt('0x110'), alufct.A_AND, BigInt('0x100'));
        computeAluTest(alu, BigInt('0x156400'), BigInt('0xFFFF1100'), alufct.A_AND, BigInt('0x150000'));
        //result zero
        computeAluTest(alu, BigInt('0xffffffffffffffff'), BigInt(0), alufct.A_AND, BigInt(0));
    });

    /**
     * Test the computeAlu function with the XOR operation.
     */
    test("Alu - 64bits - computeAlu - XOR", () => {
        let alu = new Alu(wordSize);

        // Two zero
        computeAluTest(alu, BigInt(0), BigInt(0), alufct.A_XOR, BigInt(0));
        // One positive
        computeAluTest(alu, BigInt('0x10'), BigInt(0), alufct.A_XOR, BigInt('0x10'));
        computeAluTest(alu, BigInt(0), BigInt('0x10'), alufct.A_XOR, BigInt('0x10'));
        computeAluTest(alu, BigInt('0xFFFFFF9C'), BigInt('0xa'), alufct.A_XOR, BigInt('0xFFFFFF96'));
        computeAluTest(alu, BigInt('0x5'), BigInt('0xFFFFFFF6'), alufct.A_XOR, BigInt('0xFFFFFFF3'));
        computeAluTest(alu, BigInt('0xFFFFFFFFFFFFFFFF'), BigInt('0xAC'), alufct.A_XOR, BigInt('0xffffffffffffff53'));
        // Two positive
        computeAluTest(alu, BigInt('0xE'), BigInt('0x12'), alufct.A_XOR, BigInt('0x1C'));
        computeAluTest(alu, BigInt('0xFFFFFFFFFF9C'), BigInt('0xFFFFFFFFFFa'), alufct.A_XOR, BigInt('0xf00000000066'));
        // One negative
        computeAluTest(alu, BigInt('0xFFFFFFFA'), BigInt(0), alufct.A_XOR, BigInt('0xFFFFFFFA'));
        computeAluTest(alu, BigInt(0), BigInt('0xFFFFFFFA'), alufct.A_XOR, BigInt('0xFFFFFFFA'));
        // Two negative
        computeAluTest(alu, BigInt('0xFFFFFF15'), BigInt('0xFFFFFF12'), alufct.A_XOR, BigInt('0x7'));
        computeAluTest(alu, BigInt('0xFFFFFFFFFFFFFFFF'), BigInt('0xFFFFFFFFFFFFFFAC'), alufct.A_XOR, BigInt('0x53'));
        // result negative
        computeAluTest(alu, BigInt('0xFFFFFF78'), BigInt('0xF0000000'), alufct.A_XOR, BigInt('0xFFFFF78'));
        // result positive
        computeAluTest(alu, BigInt('0x100'), BigInt('0x110'), alufct.A_XOR, BigInt('0x010'));
        computeAluTest(alu, BigInt('0x156400'), BigInt('0xFFFF1100'), alufct.A_XOR, BigInt('0xFFEA7500'));
        //result zero
        computeAluTest(alu, BigInt('0xffffffffffffffff'), BigInt('0xffffffffffffffff'), alufct.A_XOR, BigInt(0));
    });

    /**
     * Test the computeAlu function with the SAL operation.
     */
    test("Alu - 64bits - computeAlu - SAL", () => {
        let alu = new Alu(wordSize);

        // Two zero
        computeAluTest(alu, BigInt(0), BigInt(0), alufct.A_SAL, BigInt(0));
        computeAluTest(alu, BigInt(1), BigInt(10), alufct.A_SAL, BigInt(10) << BigInt(1));
        computeAluTest(alu, BigInt(0), BigInt(22), alufct.A_SAL, BigInt(22));
        computeAluTest(alu, BigInt(2), BigInt('0xFFFFFFF6'), alufct.A_SAL, BigInt('0x3ffffffd8'));
        computeAluTest(alu, BigInt(2), BigInt('0xFFFFFFFFABD'), alufct.A_SAL, BigInt('0x3fffffffeaf4'));
        computeAluTest(alu, BigInt(3), BigInt('0x8000000000000000'), alufct.A_SAL, BigInt(0));
        computeAluTest(alu, BigInt(1), BigInt(-3), alufct.A_SAL, BigInt('0xfffffffffffffffa'));
        computeAluTest(alu, BigInt(3), BigInt(1), alufct.A_SAL, BigInt(1) << BigInt(3));
        computeAluTest(alu, BigInt(1), BigInt(1), alufct.A_SAL, BigInt(1) << BigInt(1));
    });

    /**
     * Test the computeAlu function with the SAR operation.
     */
    test("Alu - 64bits - computeAlu - SAR", () => {
        let alu = new Alu(wordSize);

        // Two zero
        computeAluTest(alu, BigInt(0), BigInt(0), alufct.A_SAR, BigInt(0));
        computeAluTest(alu, BigInt(1), BigInt(10), alufct.A_SAR, BigInt(10) >> BigInt(1));
        computeAluTest(alu, BigInt(0), BigInt(22), alufct.A_SAR, BigInt(22));
        computeAluTest(alu, BigInt(2), BigInt('0xFFFFFFF6'), alufct.A_SAR, BigInt('0x3ffffffd')); // until ts does not understand 32 bits
        computeAluTest(alu, BigInt(3), BigInt('0x80000000'), alufct.A_SAR, BigInt('0x10000000'));
        computeAluTest(alu, BigInt(3), BigInt(1), alufct.A_SAR, BigInt(1) >> BigInt(3));
        computeAluTest(alu, BigInt(1), BigInt(1), alufct.A_SAR, BigInt(1) >> BigInt(1));
    });

    /**
     * Test the computeCC function with the ADD operation.
     */
    test("Alu - 64bits - computeCC - ADD", () => {
        let alu = new Alu(wordSize);

        // double zero
        computeCCTest(alu, BigInt(0), BigInt(0), alufct.A_ADD, true, false, false);
        // one positive
        computeCCTest(alu, BigInt(10), BigInt(0), alufct.A_ADD, false, false, false);
        computeCCTest(alu, BigInt(0), BigInt('0x7FFFFFFFFFFFFFFF'), alufct.A_ADD, false, false, false);
        // one negative
        computeCCTest(alu, BigInt(-10), BigInt(0), alufct.A_ADD, false, true, false);
        computeCCTest(alu, BigInt(0), BigInt(-12), alufct.A_ADD, false, true, false);
        // result = 0
        computeCCTest(alu, BigInt(12), BigInt(-12), alufct.A_ADD, true, false, false);
        computeCCTest(alu, BigInt(-12), BigInt(12), alufct.A_ADD, true, false, false);
        // result positive
        computeCCTest(alu, BigInt(50), BigInt(30), alufct.A_ADD, false, false, false);
        computeCCTest(alu, BigInt(12), BigInt(-11), alufct.A_ADD, false, false, false);
        computeCCTest(alu, BigInt(-11), BigInt(12), alufct.A_ADD, false, false, false);
        // result negative
        computeCCTest(alu, BigInt(-50), BigInt(30), alufct.A_ADD, false, true, false);
        computeCCTest(alu, BigInt(10), BigInt(-11), alufct.A_ADD, false, true, false);
        computeCCTest(alu, BigInt(-10), BigInt(-11), alufct.A_ADD, false, true, false);
        // overflow test
        computeCCTest(alu, BigInt('0x7FFFFFFFFFFFFFFF'), BigInt(1), alufct.A_ADD, false, true, true);
        computeCCTest(alu, BigInt('0x8000000000000000'), BigInt(-1), alufct.A_ADD, false, false, true);
    });

    /**
     * Test the computeCC function with the SUB operation.
     */
    test("Alu - 64bits - computeCC - SUB", () => {
        let alu = new Alu(wordSize);

        // double zero
        computeCCTest(alu, BigInt(0), BigInt(0), alufct.A_SUB, true, false, false);
        // one positive
        computeCCTest(alu, BigInt(0), BigInt(10), alufct.A_SUB, false, false, false);
        computeCCTest(alu, BigInt('0x7FFFFFFFFFFFFFFF'), BigInt(0), alufct.A_SUB, false, true, false);
        // one negative
        computeCCTest(alu, BigInt(0), BigInt(-10), alufct.A_SUB, false, true, false);
        computeCCTest(alu, BigInt(-12), BigInt(0), alufct.A_SUB, false, false, false);
        // result = 0
        computeCCTest(alu, BigInt(12), BigInt(12), alufct.A_SUB, true, false, false);
        computeCCTest(alu, BigInt(-12), BigInt(-12), alufct.A_SUB, true, false, false);
        // result positive
        computeCCTest(alu, BigInt(30), BigInt(50), alufct.A_SUB, false, false, false);
        computeCCTest(alu, BigInt(-11), BigInt(12), alufct.A_SUB, false, false, false);
        computeCCTest(alu, BigInt(-12), BigInt(-11), alufct.A_SUB, false, false, false);
        // result negative
        computeCCTest(alu, BigInt(30), BigInt(-50), alufct.A_SUB, false, true, false);
        computeCCTest(alu, BigInt(-11), BigInt(10), alufct.A_SUB, false, false, false);
        // overflow test
        computeCCTest(alu, BigInt(-1), BigInt('0x7FFFFFFFFFFFFFFF'), alufct.A_SUB, false, true, true);
        computeCCTest(alu, BigInt(1), BigInt('0x8000000000000000'), alufct.A_SUB, false, false, true);
    });

    /**
     * Test the computeCC function with the AND operation.
     */
    test("Alu - 64bits - computeCC - AND", () => {
        let alu = new Alu(wordSize);

        // double zero
        computeCCTest(alu, BigInt(0), BigInt(0), alufct.A_AND, true, false, false);
        // one positive
        computeCCTest(alu, BigInt(10), BigInt(0), alufct.A_AND, true, false, false);
        computeCCTest(alu, BigInt(0), BigInt('0x7FFFFFFFFFFFFFFF'), alufct.A_AND, true, false, false);
        // one negative
        computeCCTest(alu, BigInt(-10), BigInt(0), alufct.A_AND, true, false, false);
        computeCCTest(alu, BigInt(0), BigInt(-12), alufct.A_AND, true, false, false);
        // result = 0
        computeCCTest(alu, BigInt(0x8), BigInt(0x7), alufct.A_AND, true, false, false);
        computeCCTest(alu, BigInt(-0x8), BigInt(0x7), alufct.A_AND, true, false, false);
        // result positive
        computeCCTest(alu, BigInt(50), BigInt(30), alufct.A_AND, false, false, false);
        computeCCTest(alu, BigInt(12), BigInt(-11), alufct.A_AND, false, false, false);
        computeCCTest(alu, BigInt(12), BigInt(12), alufct.A_AND, false, false, false);
        // result negative
        computeCCTest(alu, BigInt(-10), BigInt(-11), alufct.A_AND, false, true, false);
        computeCCTest(alu, BigInt(-11), BigInt(-11), alufct.A_AND, false, true, false);
        // overflow test
        // no overflow are possible while using '&' operator.
    });

    /**
     * Test the computeCC function with the XOR operation.
     */
    test("Alu - 64bits - computeCC - XOR", () => {
        let alu = new Alu(wordSize);

        // double zero
        computeCCTest(alu, BigInt(0), BigInt(0), alufct.A_XOR, true, false, false);
        // one positive
        computeCCTest(alu, BigInt(10), BigInt(0), alufct.A_XOR, false, false, false);
        computeCCTest(alu, BigInt(0), BigInt('0x7FFFFFFFFFFFFFFF'), alufct.A_XOR, false, false, false);
        // one negative
        computeCCTest(alu, BigInt(-10), BigInt(0), alufct.A_XOR, false, true, false);
        computeCCTest(alu, BigInt(0), BigInt(-12), alufct.A_XOR, false, true, false);
        // result = 0
        computeCCTest(alu, BigInt(0x8), BigInt(0x8), alufct.A_XOR, true, false, false);
        computeCCTest(alu, BigInt(-0x8), BigInt(-0x8), alufct.A_XOR, true, false, false);
        // result positive
        computeCCTest(alu, BigInt(50), BigInt(30), alufct.A_XOR, false, false, false);
        computeCCTest(alu, BigInt(-10), BigInt(-11), alufct.A_XOR, false, false, false);
        // result negative
        computeCCTest(alu, BigInt(12), BigInt(-11), alufct.A_XOR, false, true, false);
        // overflow test
        // no overflow are possible while using '&' operator.
    });

    /**
     * Test alu's function with the NONE operation.
     */
    test("Alu - 64bits - computeCC - NONE", () => {
        let alu = new Alu(wordSize);

        try {
            computeCCTest(alu, BigInt(0), BigInt(0), alufct.A_NONE, true, false, false);
            expect(true).toBe(false);
        }
        catch (e) {
        }

        try {
            computeCCTest(alu, BigInt(30), BigInt(354), alufct.A_NONE, true, false, false);
            expect(true).toBe(false);
        }
        catch (e) {
        }

        try {
            computeCCTest(alu, BigInt(-10), BigInt(30), alufct.A_NONE, true, false, false);
            expect(true).toBe(false);
        }
        catch (e) {
        }


        expect(true).toBe(true);
    });

    /**
     * Test the computeBch function on positive, negative and zero result
     */
    test("Alu - 64bits - computeBch", () => {
        let alu = new Alu(wordSize);

        // test on positive result
        alu.computeCC(BigInt(10), BigInt(10), alufct.A_ADD);
        expect(alu.computeBch(JMP_enum.J_YES)).toBeTruthy();
        expect(alu.computeBch(JMP_enum.J_LE)).toBeFalsy();
        expect(alu.computeBch(JMP_enum.J_L)).toBeFalsy();
        expect(alu.computeBch(JMP_enum.J_E)).toBeFalsy();
        expect(alu.computeBch(JMP_enum.J_NE)).toBeTruthy();
        expect(alu.computeBch(JMP_enum.J_GE)).toBeTruthy();
        expect(alu.computeBch(JMP_enum.J_G)).toBeTruthy();

        // test on negative result
        alu.computeCC(BigInt(0), BigInt(-10), alufct.A_ADD);
        expect(alu.computeBch(JMP_enum.J_YES)).toBeTruthy();
        expect(alu.computeBch(JMP_enum.J_LE)).toBeTruthy();
        expect(alu.computeBch(JMP_enum.J_L)).toBeTruthy();
        expect(alu.computeBch(JMP_enum.J_E)).toBeFalsy();
        expect(alu.computeBch(JMP_enum.J_NE)).toBeTruthy();
        expect(alu.computeBch(JMP_enum.J_GE)).toBeFalsy();
        expect(alu.computeBch(JMP_enum.J_G)).toBeFalsy();

        // test on zero result
        alu.computeCC(BigInt(0), BigInt(0), alufct.A_ADD);
        expect(alu.computeBch(JMP_enum.J_YES)).toBeTruthy();
        expect(alu.computeBch(JMP_enum.J_LE)).toBeTruthy();
        expect(alu.computeBch(JMP_enum.J_L)).toBeFalsy();
        expect(alu.computeBch(JMP_enum.J_E)).toBeTruthy();
        expect(alu.computeBch(JMP_enum.J_NE)).toBeFalsy();
        expect(alu.computeBch(JMP_enum.J_GE)).toBeTruthy();
        expect(alu.computeBch(JMP_enum.J_G)).toBeFalsy();
    });

    /**
     * Test the updateFlags function
     */
    test("Alu - 64bits - updateFlags", () => {
        let alu = new Alu(wordSize);

        // double zero
        updateFlagsTest(alu, BigInt(0), BigInt(0), alufct.A_ADD, true, false, false);
        // one positive
        updateFlagsTest(alu, BigInt(10), BigInt(0), alufct.A_ADD, false, false, false);
        updateFlagsTest(alu, BigInt(0), BigInt('0x7FFFFFFFFFFFFFFF'), alufct.A_ADD, false, false, false);
        // one negative
        updateFlagsTest(alu, BigInt(-10), BigInt(0), alufct.A_ADD, false, true, false);
        updateFlagsTest(alu, BigInt(0), BigInt(-12), alufct.A_ADD, false, true, false);
        // result = 0
        updateFlagsTest(alu, BigInt(12), BigInt(-12), alufct.A_ADD, true, false, false);
        updateFlagsTest(alu, BigInt(-12), BigInt(12), alufct.A_ADD, true, false, false);
        // result positive
        updateFlagsTest(alu, BigInt(50), BigInt(30), alufct.A_ADD, false, false, false);
        updateFlagsTest(alu, BigInt(12), BigInt(-11), alufct.A_ADD, false, false, false);
        updateFlagsTest(alu, BigInt(-11), BigInt(12), alufct.A_ADD, false, false, false);
        // result negative
        updateFlagsTest(alu, BigInt(-50), BigInt(30), alufct.A_ADD, false, true, false);
        updateFlagsTest(alu, BigInt(10), BigInt(-11), alufct.A_ADD, false, true, false);
        updateFlagsTest(alu, BigInt(-10), BigInt(-11), alufct.A_ADD, false, true, false);
        // overflow test
        updateFlagsTest(alu, BigInt('0x7FFFFFFFFFFFFFFF'), BigInt(1), alufct.A_ADD, false, true, true);
        updateFlagsTest(alu, BigInt('0x8000000000000000'), BigInt(-1), alufct.A_ADD, false, false, true);
    });

    /**
     * Test the load function
     * Check if the correct values are put in the flags
     */
    test("Alu - 64bits - load", () => {
        let alu = new Alu(wordSize);
        expect(alu.getZF()).toBe(false);
        expect(alu.getSF()).toBe(false);
        expect(alu.getOF()).toBe(false);

        alu.load(loadData);
        expect(alu.getZF()).toBe(false);
        expect(alu.getSF()).toBe(false);
        expect(alu.getOF()).toBe(true);

        expect(alu.getNextZF()).toBe(true);
        expect(alu.getNextSF()).toBe(true);
        expect(alu.getNextOF()).toBe(false);
    });

    /**
     * Test the save function
     * Check if the returning string is correct
     */
    test("Alu - 64bits - save", () => {
        let alu = new Alu(wordSize);
        expect(alu.getZF()).toBe(false);
        expect(alu.getSF()).toBe(false);
        expect(alu.getOF()).toBe(false);

        alu.load(loadData);
        expect(alu.save()).toBe(saveData);
    });
})