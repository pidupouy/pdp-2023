import { describe, expect, test } from '@jest/globals';
import { byteTest, getMemoryViewTest, nextMemoryViewTest, wordTest } from "../../testUtils";
import { Memory } from "../../../model/memory";

const loadData = { "current" : [{"address":"00000030","value":"d3ffffffffffffff"}], "next" : {"address":"00000030","isWrite":false,"bytes":"15276209936040722431"}}

const saveData = `{ "current" : [{"address":"00000030","value":"d3ffffffffffffff"}], "next" : {"address":"00000030","isWrite":false,"bytes":"15276209936040722431"}}`

const wordSize = 8;

describe("Memory - 64bits - tests", () => {
    test("Memory - 64bits - Word", () => {
        let word = Memory.byteArrayToWord([0, 0, 0, 0, 0, 0, 0, 0], wordSize);

        expect(word === BigInt(0)).toBeTruthy();

        // More than 8 bytes
        expect(() => {
            Memory.byteArrayToWord([45, 45, 75, 78, 78, 78, 78, 78, 78], wordSize);
        }).toThrow()

        // Byte underflow
        expect(() => {
            Memory.byteArrayToWord([-129, 45, 75, 78, 78, 78, 78, 78], wordSize);
        }).toThrow()

        // Byte overflow
        expect(() => {
            Memory.byteArrayToWord([4, 4, 4, 4, 4, 45, 75, 256], wordSize);
        }).toThrow()

        // Limit of bytes bounds
        expect(() => {
            Memory.byteArrayToWord([0, 45, 75, 75, 75, 75, 75, 255], wordSize);
        }).not.toThrow()

        word = Memory.byteArrayToWord([0x00, 0xef, 0xcd, 0xff, 0xea, 0xda, 0x48, 0x96], wordSize);

        // Checks every byte to be what they're expected to be.
        expect((word & BigInt('0xff')) === BigInt('0x00')).toBeTruthy();
        expect((word >> BigInt(8) & BigInt('0xff')) === BigInt('0xef')).toBeTruthy();
        expect((word >> BigInt(16) & BigInt('0xff')) === BigInt('0xcd')).toBeTruthy();
        expect((word >> BigInt(24) & BigInt('0xff')) === BigInt('0xff')).toBeTruthy();
        expect((word >> BigInt(32) & BigInt('0xff')) === BigInt('0xea')).toBeTruthy();
        expect((word >> BigInt(40) & BigInt('0xff')) === BigInt('0xda')).toBeTruthy();
        expect((word >> BigInt(48) & BigInt('0xff')) === BigInt('0x48')).toBeTruthy();
        expect((word >> BigInt(56) & BigInt('0xff')) === BigInt('0x96')).toBeTruthy();
    });

    test("Memory - 64bits - getWordSize", () => {
        let memory = new Memory(wordSize);

        expect(memory.getWordSize()).toBe(8);
    });

    test("Memory - 64bits - loadProgram", () => {
        let memory = new Memory(wordSize)
        let program = `
        |
        0x0000:              | .pos 0
        0x0000:              | Init:
        0x0000: 30f418000000 |     irmovl Stack, %esp
                            |
        0x0006:              | .pos 0x14
        0x0014: f4ffffff     | .long -12
        0x0018:              | Stack:
        0x0018:              |     .align 12
        0x0018: 7018000000   |     jmp Stack
                            |
        `

        memory.loadProgram(program)
        let result =
            [0x30, 0xf4, 0x18, 0x00,
                0, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, 0, 0,
                0xf4, 0xff, 0xff, 0xff,
                0x70, 0x18, 0, 0]

        // Checks every byte is well-set in memory
        for (let i = 0; i < result.length; i++) {
            expect(memory.readByte(BigInt(i)) === result[i]).toBeTruthy();
        }
    });

    test("Memory - 64bits - readWord/writeWord", () => {
        let memory = new Memory(wordSize)

        let word = Memory.byteArrayToWord([0xff, 0xee, 0xdd, 0xcc, 0xbb, 0xaa, 0x99, 0x88], wordSize);

        // Write at address 0
        wordTest(memory, BigInt(0x0), word);

        // Write at max address 0x1ffc
        wordTest(memory, BigInt(0x1ff8), word);

        // Write at 0x6. It should write on two different words
        wordTest(memory, BigInt(0x14), word);
        expect(memory.readWord(BigInt(0x10)) === BigInt(0xccddeeff00000000)).toBeTruthy();
        expect(memory.readWord(BigInt(0x18)) === BigInt(0x8899aabb)).toBeTruthy();

        // Checks we can not underflow
        expect(() => {
            wordTest(memory, BigInt(-1), word);
        }).toThrow();

        // Checks we can not overflow
        expect(() => {
            wordTest(memory, BigInt(Memory.LAST_ADDRESS + 1), word);
        }).toThrow();

        // We should be able to access the last word
        expect(() => {
            memory.readWord(BigInt(Memory.LAST_ADDRESS - memory.getWordSize() + 1))
        }).not.toThrow();

        // We can not access the last word if their are less than 3 bytes left
        expect(() => {
            memory.readWord(BigInt(Memory.LAST_ADDRESS - memory.getWordSize() + 2))
        }).toThrow();
    });

    test("Memory - 64bits - readByte/writeByte", () => {
        let memory = new Memory(wordSize)

        let word = Memory.byteArrayToWord([0xff, 0xee, 0xdd, 0xcc, 0xbb, 0xaa, 0x99, 0x88], wordSize);

        // Write at address 0
        byteTest(memory, BigInt(0x0), 0xaa);

        // Write at max address 0x1ffc
        byteTest(memory, BigInt(0x1ff8), 0xaa);

        // Write at 0x7. It should write at the end of a word
        byteTest(memory, BigInt(0x17), 0xaa);
        expect(memory.readWord(BigInt(0x10)) === BigInt(0xaa00000000000000)).toBeTruthy();

        // Checks we can not underflow
        expect(() => {
            byteTest(memory, BigInt(-1), 0xaa);
        }).toThrow();

        // Checks we can not overflow
        expect(() => {
            byteTest(memory, BigInt(Memory.LAST_ADDRESS + 1), 0xaa);
        }).toThrow();
    });

    test("Memory - 64bits - updateMemory", () => {
        let memory = new Memory(wordSize)

        let word = Memory.byteArrayToWord([0xff, 0xee, 0xdd, 0xcc, 0xbb, 0xaa, 0x99, 0x88], wordSize);
        
        // Should not write in memory immediately
        memory.writeWord(BigInt(0x0), word);
        expect(memory.readWord(BigInt(0x0)) === BigInt(0x0)).toBeTruthy();

        // Should write in memory after update
        memory.updateMemory();
        expect(memory.readWord(BigInt(0x0)) === BigInt(word)).toBeTruthy();
        
        // Should write in memory immediately
        memory.writeByte(BigInt(0x8), 0xaa);
        expect(memory.readWord(BigInt(0x8)) === BigInt(0xaa)).toBeTruthy();

        // Should not write in memory after update
        memory.updateMemory();
        expect(memory.readWord(BigInt(0x8)) === BigInt(0xaa)).toBeTruthy();
    });

    test("Memory - 64bits - getMemoryView", () => {
        let memory = new Memory(wordSize)

        let word = Memory.byteArrayToWord([0xff, 0xee, 0xdd, 0xcc, 0xbb, 0xaa, 0x99, 0x88], wordSize);

        // Should return this at the start
        getMemoryViewTest(memory, 8, BigInt(0), BigInt(Memory.LAST_ADDRESS), new Map<bigint, bigint>, new Map<bigint, bigint>);

        // Should be modified by writeByte
        memory.writeByte(BigInt(0x0), 0xaa);
        getMemoryViewTest(memory, 8, BigInt(0), BigInt(Memory.LAST_ADDRESS), new Map<bigint, bigint>([[BigInt(0), BigInt(0xaa)]]), new Map<bigint, bigint>([[BigInt(0), BigInt(0xaa)]]));

        // Should not be modified by writeWord immediately
        memory.writeWord(BigInt(0x8), word);
        getMemoryViewTest(memory, 8, BigInt(0), BigInt(Memory.LAST_ADDRESS), new Map<bigint, bigint>([[BigInt(0), BigInt(0xaa)]]), new Map<bigint, bigint>([[BigInt(0), BigInt(0xaa)]]));

        //Should be modified after update
        memory.updateMemory();
        getMemoryViewTest(memory, 8, BigInt(0), BigInt(Memory.LAST_ADDRESS), new Map<bigint, bigint>([[BigInt(0), BigInt(0xaa)], [BigInt(0x8), word]]), new Map<bigint, bigint>([[BigInt(0), BigInt(0xaa)], [BigInt(0x8), BigInt(0xff)], [BigInt(0x9), BigInt(0xee)], [BigInt(0xa), BigInt(0xdd)],
        [BigInt(0xb), BigInt(0xcc)], [BigInt(0xc), BigInt(0xbb)], [BigInt(0xd), BigInt(0xaa)], [BigInt(0xe), BigInt(0x99)], [BigInt(0xf), BigInt(0x88)]]));
    });
    
    test("Memory - 64bits - getNextMemoryView", () => {
        let memory = new Memory(wordSize)

        let word = Memory.byteArrayToWord([0xff, 0xee, 0xdd, 0xcc, 0xbb, 0xaa, 0x99, 0x88], wordSize);

        // Should return this at the start
        nextMemoryViewTest(memory, 8, BigInt(0), false, [0,0,0,0,0,0,0,0]);

        // Should not be modified by writeByte
        memory.writeByte(BigInt(0x0), 0xaa);
        nextMemoryViewTest(memory, 8, BigInt(0), false, [0,0,0,0,0,0,0,0]);

        // Should be modified by writeWord
        memory.writeWord(BigInt(0x8), word);
        nextMemoryViewTest(memory, 8, BigInt(0x8), true, [0xff, 0xee, 0xdd, 0xcc, 0xbb, 0xaa, 0x99, 0x88]);

        // Should keep last value and set isWrite to false if update
        memory.updateMemory();
        nextMemoryViewTest(memory, 8, BigInt(0x8), false, [0xff, 0xee, 0xdd, 0xcc, 0xbb, 0xaa, 0x99, 0x88]);
    });

    /**
     * Test the load function
     * Check if the correct values are put in the memory
     */
    test("Memory - 64bits - load", () => {
        let memory = new Memory(wordSize);
        getMemoryViewTest(memory, memory.getWordSize(), BigInt(0), BigInt(Memory.LAST_ADDRESS), new Map<bigint, bigint>, new Map<bigint, bigint>);
        nextMemoryViewTest(memory, memory.getWordSize(), BigInt(0), false, [0, 0, 0, 0, 0, 0, 0, 0]);

        memory.load(loadData);
        getMemoryViewTest(memory, memory.getWordSize(), BigInt(0), BigInt(Memory.LAST_ADDRESS), new Map<bigint, bigint>([[BigInt('0x30'), BigInt('0xffffffffffffffd3')]]), new Map<bigint, bigint>([[BigInt('0x30'), BigInt('0xd3')], [BigInt('0x31'), BigInt('0xff')], [BigInt('0x32'), BigInt('0xff')], [BigInt('0x33'), BigInt('0xff')], 
        [BigInt('0x34'), BigInt('0xff')], [BigInt('0x35'), BigInt('0xff')], [BigInt('0x36'), BigInt('0xff')], [BigInt('0x37'), BigInt('0xff')]]));
        nextMemoryViewTest(memory, memory.getWordSize(), BigInt('0x30'), false, [255, 255, 255, 255, 255, 255, 255, 211]);
    });

    /**
     * Test the save function
     * Check if the returning string is correct
     */
    test("Memory - 64bits - save", () => {
        let memory = new Memory(wordSize);
        getMemoryViewTest(memory, memory.getWordSize(), BigInt(0), BigInt(Memory.LAST_ADDRESS), new Map<bigint, bigint>, new Map<bigint, bigint>);
        nextMemoryViewTest(memory, memory.getWordSize(), BigInt(0), false, [0, 0, 0, 0, 0, 0, 0, 0]);

        memory.load(loadData);
        expect(memory.save()).toBe(saveData);
    });
})