import { Instruction } from "../../model/instruction";
import { InstructionSet } from "../../model/instructionSet";
import { loadDefaultFile } from "../testUtils";
import { describe, expect, test } from '@jest/globals';

function addInstructionTest(instructionSet: InstructionSet, instruction: Instruction) {
    let isPresent = false;

    instructionSet.addInstruction(instruction);
    for (let instr of instructionSet.getInstructions(instruction.name)) {
        if (instr == instruction) {
            isPresent = true;
        }
    }
    expect(isPresent).toBeTruthy();
}

function addInstructionsTest(instructionSet: InstructionSet, instructions: Instruction[]) {
    instructionSet.addInstructions(instructions);
    for (let expectedInstr of instructions) {
        let isPresent = false;
        for (let instr of instructionSet.getInstructions(expectedInstr.name)) {
            if (instr.name === expectedInstr.name && instr.icode === expectedInstr.icode && instr.ifun === expectedInstr.ifun && instr.args === expectedInstr.args) {
                isPresent = true;
            }
        }
        expect(isPresent).toBeTruthy();
    }
}

describe("InstructionSet - tests", () => {
    test("InstructionSet - new Instruction - name", () => {
        // Correct instruction
        new Instruction("instr_test40", 0, 0, "")

        // Add instruction with empty name
        expect(() => {
            new Instruction("", 0, 0, "")
        }).toThrow()

        // Add instruction with wrong name
        expect(() => {
            new Instruction("9instr", 0, 0, "")
        }).toThrow()

        // Add instruction with wrong name
        expect(() => {
            new Instruction("_instr", 0, 0, "")
        }).toThrow()

        // Add instruction with correct icode/ifun
        new Instruction("instr", 0, 0, "")

        // Add instruction with correct icode/ifun
        new Instruction("instr", 15, 15, "")

        // Add instruction with wrong icode
        expect(() => {
            new Instruction("instr", -1, 0, "")
        }).toThrow()

        // Add instruction with wrong icode
        expect(() => {
            new Instruction("instr", 16, 0, "")
        }).toThrow()

        // Add instruction with wrong ifun
        expect(() => {
            new Instruction("instr", 0, -1, "")
        }).toThrow()

        // Add instruction with wrong ifun
        expect(() => {
            new Instruction("instr", 0, 16, "")
        }).toThrow()
        return
    })

    test("InstructionSet - new Instruction - icode/ifun", () => {
        // Add instruction with correct icode/ifun
        new Instruction("instr", 0, 0, "")

        // Add instruction with correct icode/ifun
        new Instruction("instr", 15, 15, "")

        // Add instruction with wrong icode
        expect(() => {
            new Instruction("instr", -1, 0, "")
        }).toThrow()

        // Add instruction with wrong icode
        expect(() => {
            new Instruction("instr", 16, 0, "")
        }).toThrow()

        // Add instruction with wrong ifun
        expect(() => {
            new Instruction("instr", 0, -1, "")
        }).toThrow()

        // Add instruction with wrong ifun
        expect(() => {
            new Instruction("instr", 0, 16, "")
        }).toThrow()
        return
    })

    test("InstructionSet - new Instruction - format", () => {
        // Add instruction with correct empty format
        new Instruction("instr", 0, 0, "")

        // Add instruction with correct format
        new Instruction("instr", 0, 0, "rA")

        // Add instruction with correct format
        new Instruction("instr", 0, 0, "rB")

        // Add instruction with correct format
        new Instruction("instr", 0, 0, "valC")

        // Add instruction with correct format
        new Instruction("instr", 0, 0, "rA,rB")

        // Add instruction with correct format
        new Instruction("instr", 0, 0, "rB,rA")

        // Add instruction with correct format
        new Instruction("instr", 0, 0, "valC,rA")

        // Add instruction with correct format
        new Instruction("instr", 0, 0, "rA,valC")

        // Add instruction with correct format
        new Instruction("instr", 0, 0, "valC,rB")

        // Add instruction with correct format
        new Instruction("instr", 0, 0, "rA, valC")

        // Add instruction with correct format
        new Instruction("instr", 0, 0, "valC?(rB),rA")

        // Add instruction with correct format
        new Instruction("instr", 0, 0, "valC?(rA),rB")

        // Add instruction with correct format
        new Instruction("instr", 0, 0, "rA,valC?(rB)")

        // Add instruction with correct format
        new Instruction("instr", 0, 0, "rB,valC?(rA)")

        // Add instruction with correct format
        new Instruction("instr", 0, 0, "valC?(rA)")

        // Add instruction with correct format
        new Instruction("instr", 0, 0, "valC?(rB)")

        // There is a maximum of two registers
        expect(() => {
            new Instruction("instr", 0, 0, "rA, rB(rA)")
        }).toThrow()

        // There is a maximum of one valC
        expect(() => {
            new Instruction("instr", 0, 0, "valC, valC?")
        }).toThrow()

        // The given arg did not contain keyword (and is not "")
        expect(() => {
            new Instruction("instr", 0, 0, "[]")
        }).toThrow()

        // No prohibited characters
        expect(() => {
            new Instruction("instr", 0, 0, "rA,rB\\valC\\")
        }).toThrow()

        // No alphanumeric characters in separator
        expect(() => {
            new Instruction("instr", 0, 0, "qbcozqucqiugfceqorA09843540681valCqczqcqz")
        }).toThrow()
    })

    test("InstructionSet - addInstruction", () => {
        let instructionSet = new InstructionSet([], 4)

        // Correct instruction
        addInstructionTest(instructionSet, new Instruction("instr", 0, 0, ""))

        // Add instruction with same name, icode/ifun and format
        expect(() => {
            instructionSet.addInstruction(new Instruction("instr", 0, 0, ""))
        }).toThrow()

        // Add instruction with same name and format
        expect(() => {
            instructionSet.addInstruction(new Instruction("instr", 1, 0, ""))
        }).toThrow()

        // Add instruction with same name
        addInstructionTest(instructionSet, new Instruction("instr", 2, 0, "valC"))

        // Add instruction with same icode/ifun
        addInstructionTest(instructionSet, new Instruction("instr2", 0, 0, "rA,rB"))

        // Add instruction with same format
        addInstructionTest(instructionSet, new Instruction("instr3", 3, 0, "valC"))

        // Add instruction with same name and icode/ifun
        addInstructionTest(instructionSet, new Instruction("instr", 0, 0, "rA,rB"))

        // Add instruction with same icode/ifun and format
        addInstructionTest(instructionSet, new Instruction("instr4", 0, 0, "rA,rB"))

        // Add instruction with different icode/ifun
        addInstructionTest(instructionSet, new Instruction("instr", 5, 0, "valC,rB"))
    })

    test("InstructionSet - addInstructions", () => {
        let instructionSet = new InstructionSet([], 4)

        // Correct instruction list
        addInstructionsTest(instructionSet, [new Instruction("instr", 0, 0, ""), new Instruction("instr2", 1, 2, "rA,rB"), new Instruction("instr3", 3, 4, "valC")])

        // Incorrect instruction list
        expect(() => {
            addInstructionsTest(instructionSet, [new Instruction("instr", 0, 0, ""), new Instruction("instr2", 156, 2, "rA,rB"), new Instruction("instr3", 3, 4, "valC")])
        }).toThrow()
    })

    test("InstructionSet - get", () => {
        let instructionSet = new InstructionSet([], 4)

        // Get correct instruction
        let instruction = new Instruction("instr", 0, 0, "")
        instructionSet.addInstruction(instruction);
        expect(instructionSet.get("instr") === instruction);

        // Get correct instruction when there is multiple instructions with same name
        let instruction2 = new Instruction("instr", 0, 0, "valC")
        instructionSet.addInstruction(instruction2);
        expect(instructionSet.get("instr") === instruction);

        // Get instruction not present
        expect(() => {
            instructionSet.get("instrNotHere");
        }).toThrow()
    })

    test("InstructionSet - getInstructions", () => {
        let instructionSet = new InstructionSet([], 4)

        // Get correct instruction
        let instruction = new Instruction("instr", 0, 0, "")
        instructionSet.addInstruction(instruction);
        expect(instructionSet.getInstructions("instr")[0] === instruction);

        // Get correct instruction when there is multiple instructions with same name
        let instruction2 = new Instruction("instr", 0, 0, "valC")
        instructionSet.addInstruction(instruction2);
        expect(instructionSet.getInstructions("instr")[0] === instruction);
        expect(instructionSet.getInstructions("instr")[1] === instruction2);

        // Get instruction not present
        expect(() => {
            instructionSet.getInstructions("instrNotHere");
        }).toThrow()
    })

    test("InstructionSet - getInstructionName", () => {
        let instructionSet = new InstructionSet([], 4)

        // Get correct instruction
        let instruction = new Instruction("instr", 0, 0, "")
        instructionSet.addInstruction(instruction);
        expect(instructionSet.getInstructionName(0, 0) === instruction.name);

        // Get correct instruction when there is multiple instructions with same icode/ifun
        let instruction2 = new Instruction("instr2", 0, 0, "")
        instructionSet.addInstruction(instruction2);
        expect(instructionSet.getInstructionName(0, 0) === instruction.name);

        // Get correct instruction
        let instruction3 = new Instruction("inst3", 1, 0, "")
        instructionSet.addInstruction(instruction3);
        expect(instructionSet.getInstructionName(1, 0) === instruction3.name);

        // Get correct instruction
        let instruction4 = new Instruction("inst4", 0, 1, "")
        instructionSet.addInstruction(instruction4);
        expect(instructionSet.getInstructionName(0, 1) === instruction4.name);

        // Get instruction not present
        expect(instructionSet.getInstructionName(15, 15) === "");
    })

    test("InstructionSet - clear", () => {
        let instructionSet = new InstructionSet([], 4)

        // Clear backup
        let instructionWrong = {
            "name": "invalid",
            "icode": 45,
            "ifun": -4,
            "args": ""
        }
        let instruction = new Instruction("instr", 0, 0, "")
        instructionSet.addInstruction(instruction);
        expect(instructionSet.get("instr") === instruction);

        instructionSet.clear();
        expect(() => {
            instructionSet.addInstructions([instructionWrong as Instruction]);
        }).toThrow()
        expect(instructionSet.get("instr") === instruction);

        // Clear
        instructionSet.clear();
        expect(() => {
            instructionSet.get("instr");
        }).toThrow()
    })
})