import { describe, expect, test } from '@jest/globals';
import { registerNextVerification32Bits, registerVerification32Bits } from '../../testUtils';
import { registers_enum, registersList32bits } from "../../../model/interfaces/IRegisters";
import { Registers } from "../../../model/registers";

const loadData = { "current" : {"eax":"42","ebx":"80","ecx":"50","edx":"198","esi":"-53","edi":"-47","ebp":"-2147483648","esp":"2147483647"}, "next" : {"eax":"875","ebx":"64","ecx":"1894","edx":"3864","esi":"-54452","edi":"-8754","ebp":"-2147483648","esp":"2147483647"}}

const saveData = `{ "current" : {"eax":"42","ecx":"50","edx":"198","ebx":"80","esp":"2147483647","ebp":"2147483648","esi":"4294967243","edi":"4294967249"}, "next" : {"eax":"875","ecx":"1894","edx":"3864","ebx":"64","esp":"2147483647","ebp":"2147483648","esi":"4294912844","edi":"4294958542"}}`

const wordSize = 4;

describe("Registers - 32bits - tests", () => {
    /**
     * Test the registers constructor.
     * Check if all the registers are set to 0 during initialization.
     */
    test("Registers - 32bits - constructor", () => {
        let registers = new Registers(wordSize, registersList32bits);
        registerVerification32Bits(registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification32Bits(registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
    });

    /**
     * Test the write function
     * Check if the value is correctly set in the contentNext
     */
    test("Registers - 32bits - write", () => {
        let registers = new Registers(wordSize, registersList32bits);

        //positive number
        registers.write(registers_enum.eax, BigInt(10));
        registerVerification32Bits(registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification32Bits(registers, BigInt(10), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));

        //negative number
        registers.write(registers_enum.ebx, BigInt(-10));
        registerVerification32Bits(registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification32Bits(registers, BigInt(10), BigInt('0xfffffff6'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));

        //zero
        registers.write(registers_enum.ebx, BigInt(0));
        registerVerification32Bits(registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification32Bits(registers, BigInt(10), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        
        //max number
        registers.write(registers_enum.ecx, BigInt('0x7fffffff'));
        registerVerification32Bits(registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification32Bits(registers, BigInt(10), BigInt(0), BigInt('0x7fffffff'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));

        //min number
        registers.write(registers_enum.edx, BigInt('0x80000000'));
        registerVerification32Bits(registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification32Bits(registers, BigInt(10), BigInt(0), BigInt('0x7fffffff'), BigInt('0x80000000'), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
    });

    /**
     * Test the read and readNext function
     * Check if the value is correctly read from content and contentNext
     */
    test("Registers - 32bits - read/readNext", () => {
        let registers = new Registers(wordSize, registersList32bits);

        //positive number
        registers.write(registers_enum.eax, BigInt(10));
        registers.updateRegisters();
        registers.write(registers_enum.eax, BigInt(20));
        registerVerification32Bits(registers, BigInt(10), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification32Bits(registers, BigInt(20), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        expect(registers.read(registers_enum.eax) === BigInt(10)).toBeTruthy();
        expect(registers.readNext(registers_enum.eax) === BigInt(20)).toBeTruthy();

        //negative number
        registers.write(registers_enum.ebx, BigInt(-10));
        registers.updateRegisters();
        registers.write(registers_enum.ebx, BigInt(-20));
        registerVerification32Bits(registers, BigInt(20), BigInt('0xfffffff6'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification32Bits(registers, BigInt(20), BigInt('0xffffffec'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        expect(registers.read(registers_enum.ebx) === BigInt('0xfffffff6')).toBeTruthy();
        expect(registers.readNext(registers_enum.ebx) === BigInt('0xffffffec')).toBeTruthy();

        //zero
        expect(registers.read(registers_enum.ecx) === BigInt(0)).toBeTruthy();
        expect(registers.readNext(registers_enum.ecx) === BigInt(0)).toBeTruthy();
        
        //max number
        registers.write(registers_enum.ecx, BigInt('0x7fffffff'));
        registers.updateRegisters();
        registerVerification32Bits(registers, BigInt(20), BigInt('0xffffffec'), BigInt('0x7fffffff'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification32Bits(registers, BigInt(20), BigInt('0xffffffec'), BigInt('0x7fffffff'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        expect(registers.read(registers_enum.ecx) === BigInt('0x7fffffff')).toBeTruthy();
        expect(registers.readNext(registers_enum.ecx) === BigInt('0x7fffffff')).toBeTruthy();

        //min number
        registers.write(registers_enum.edx, BigInt('0x80000000'));
        registers.updateRegisters();
        registerVerification32Bits(registers, BigInt(20), BigInt('0xffffffec'), BigInt('0x7fffffff'), BigInt('0x80000000'), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification32Bits(registers, BigInt(20), BigInt('0xffffffec'), BigInt('0x7fffffff'), BigInt('0x80000000'), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        expect(registers.read(registers_enum.edx) === BigInt('0x80000000')).toBeTruthy();
        expect(registers.readNext(registers_enum.edx) === BigInt('0x80000000')).toBeTruthy();
    });

    /**
     * Test the updateRegister function
     * Check if the value is correctly updated from contentNext to content
     */
    test("Registers - 32bits - updateRegisters", () => {
        let registers = new Registers(wordSize, registersList32bits);

        registers.write(registers_enum.eax, BigInt(10));
        registers.write(registers_enum.ebx, BigInt(-10));
        registers.write(registers_enum.ecx, BigInt('0x7fffffff'));
        registers.write(registers_enum.edx, BigInt('0x80000000'));

        registers.updateRegisters();
        registerVerification32Bits(registers, BigInt(10), BigInt('0xfffffff6'), BigInt('0x7fffffff'), BigInt('0x80000000'), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        registerNextVerification32Bits(registers, BigInt(10), BigInt('0xfffffff6'), BigInt('0x7fffffff'), BigInt('0x80000000'), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
    });

    /**
     * Test the getRegisterName function
     * Check if the correct name is returned
     */
    test("Registers - 32bits - getRegisterName", () => {
        let registers = new Registers(wordSize, registersList32bits);

        expect(registers.getRegisterName(registers_enum.eax)).toBe("eax");
        expect(registers.getRegisterName(registers_enum.ebx)).toBe("ebx");
        expect(registers.getRegisterName(registers_enum.ecx)).toBe("ecx");
        expect(registers.getRegisterName(registers_enum.edx)).toBe("edx");
        expect(registers.getRegisterName(registers_enum.esi)).toBe("esi");
        expect(registers.getRegisterName(registers_enum.edi)).toBe("edi");
        expect(registers.getRegisterName(registers_enum.ebp)).toBe("ebp");
        expect(registers.getRegisterName(registers_enum.esp)).toBe("esp");
        expect(registers.getRegisterName(registers_enum.none)).toBe("none");
    });

    /**
     * Test the load function
     * Check if the correct numbers are put in the registers
     */
    test("Registers - 32bits - load", () => {
        let registers = new Registers(wordSize, registersList32bits);
        registerVerification32Bits(registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        
        registers.load(loadData);
        registerVerification32Bits(registers, BigInt('42'), BigInt('80'), BigInt('50'), BigInt('198'), BigInt('0xffffffcb'), BigInt('0xffffffd1'), BigInt('0x80000000'), BigInt('2147483647'));
        registerNextVerification32Bits(registers, BigInt('875'), BigInt('64'), BigInt('1894'), BigInt('3864'), BigInt('0xffff2b4c'), BigInt('0xffffddce'), BigInt('0x80000000'), BigInt('2147483647'));
    });

    /**
     * Test the save function
     * Check if the returning string is correct
     */
    test("Registers - 32bits - save", () => {
        let registers = new Registers(wordSize, registersList32bits);
        registerVerification32Bits(registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        
        registers.load(loadData);
        expect(registers.save()).toBe(saveData);
    });
});