import { Hcl2js } from '../../model/hcl2js/hcl2js'
import { describe, expect, test } from '@jest/globals';

describe('hcl2js - tests', () => {
    test('hcl2js - simple program', () => {
        let hcl = `
        intsig valb 'VALB'
        intsig icode 'ICODE'

        bool f = icode in {valb, 3};

        int a = 3;
        `

        let hcl2js = new Hcl2js()
        let result = hcl2js.assemble(hcl)

        expect(result.errors.length).toBe(0)

        hcl = `
        intsig valb 'VALB'
        intsig icode 'ICODE'

        bool f = valb | icode;
        bool f2 = valb & icode;
        bool f3 = valb ^ icode;
        `

        result = hcl2js.assemble(hcl)
        expect(result.errors.length).toBe(0)

        hcl = `
        intsig valb 'VALB'
        intsig icode 'ICODE'

        bool f = random in {valb, 3};`

        result = hcl2js.assemble(hcl)
        expect(result.errors.length).not.toBe(0)
    })

    test('hcl2js - complex program', () => {
        let hcl = `
                intsig true     '1'
                intsig false    '0'
                intsig ten      '10'

                bool f1 = ten in {[1: 10; 0: 15;]};
                bool f2 = ten in {[0: 10; 1: 15;]};
                bool f3 = 0 in {[0: 10; 0: 15;]};
            `

        let hcl2js = new Hcl2js()

        let result = hcl2js.assemble(hcl)
        expect(result.errors.length).toBe(0)

        let func = eval(result.output)
        expect(func.f1()).toBeTruthy()
        expect(func.f2()).toBeFalsy()
        expect(func.f3()).toBeTruthy()

        hcl = `
                intsig true     '1'
                intsig false    '0'
                intsig ten      '10'

                bool f1 = true | false;
                bool f2 = false & false;
                bool f3 = true & true;
                bool f4 = false | false | true;
                bool f5 = false & false | true & false;
                bool f6 = false & true | true & true;
                bool f7 = true | !false & false;
            `

        result = hcl2js.assemble(hcl)
        expect(result.errors.length).toBe(0)

        func = eval(result.output)
        expect(func.f1()).toBeTruthy()
        expect(func.f2()).toBeFalsy()
        expect(func.f3()).toBeTruthy()
        expect(func.f4()).toBeTruthy()
        expect(func.f5()).toBeFalsy()
        expect(func.f6()).toBeTruthy()
        expect(func.f7()).toBeTruthy()


        hcl = `
                intsig true     '1'
                intsig false    '0'
                intsig ten      '10'

                bool f1 = true | !true & true;
                bool f2 = true | !true & false;
                bool f3 = true | !false & true;
                bool f4 = true | !false & false;
                bool f5 = false | !true & true;
                bool f6 = false | !true & false;
                bool f7 = false | !false & true;
                bool f8 = false | !false & false;
            `

        result = hcl2js.assemble(hcl)
        expect(result.errors.length).toBe(0)

        func = eval(result.output)
        expect(func.f1()).toBeTruthy()
        expect(func.f2()).toBeTruthy()
        expect(func.f3()).toBeTruthy()
        expect(func.f4()).toBeTruthy()
        expect(func.f5()).toBeFalsy()
        expect(func.f6()).toBeFalsy()
        expect(func.f7()).toBeTruthy()
        expect(func.f8()).toBeFalsy()
    })

    /*------------------ New Tests -------------------*/
    test('hcl2js - var declaration', () => {
        let hcl2js = new Hcl2js()

        /*
        Correct intsig declaration
        */
        let hcl_correct =
            `
        intsig int1 'VAL'
        intsig int2 '1'
        intsig int3 '-1'
        `

        let result_correct = hcl2js.assemble(hcl_correct)
        expect(result_correct.errors.length).toBe(0)

        /*
        Correct boolsig declaration
        */
        hcl_correct =
            `
        boolsig bool1 '1'
        boolsig bool2 '0'
        `

        result_correct = hcl2js.assemble(hcl_correct)
        expect(result_correct.errors.length).toBe(0)


        /*
        Incorrect intsig declaration
        */
        let hcl_incorrect =
            `
        intsig int1 'VAL
        `

        let result_incorrect = hcl2js.assemble(hcl_incorrect)
        expect(result_incorrect.errors.length).not.toBe(0)

        hcl_incorrect =
            `
        intsig int1 1
        `

        result_incorrect = hcl2js.assemble(hcl_incorrect)
        expect(result_incorrect.errors.length).not.toBe(0)

        hcl_incorrect =
            `
        intsig int1 -1
        `

        result_incorrect = hcl2js.assemble(hcl_incorrect)
        expect(result_incorrect.errors.length).not.toBe(0)

        /*
        Incorrect boolsig declaration
        */
        hcl_incorrect =
            `
        boolsig bool1 'VAL
        `

        result_incorrect = hcl2js.assemble(hcl_incorrect)
        expect(result_incorrect.errors.length).not.toBe(0)

        hcl_incorrect =
            `
        boolsig bool1 1
        `

        result_incorrect = hcl2js.assemble(hcl_incorrect)
        expect(result_incorrect.errors.length).not.toBe(0)

        hcl_incorrect =
            `
        boolsig bool1 0
        `

        result_incorrect = hcl2js.assemble(hcl_incorrect)
        expect(result_incorrect.errors.length).not.toBe(0)
    })

    test('hcl2js - complex function', () => {
        let hcl2js = new Hcl2js()

        /*
        Correct intsig declaration
        */
        let hcl_correct =
            `
        intsig true     '1'
        intsig false    '0'

        bool f1 = true || false && false;
        bool f2 = false || true && true;
        bool f3 = (true || false) && true;
        bool f4 = true || false && false || true;
        bool f5 = true && true && true || false || false;
        `

        let result_correct = hcl2js.assemble(hcl_correct)
        expect(result_correct.errors.length).toBe(0)

        let func = eval(result_correct.output)
        expect(func.f1()).toBeTruthy()
        expect(func.f2()).toBeTruthy()
        expect(func.f3()).toBeTruthy()
        expect(func.f4()).toBeTruthy()
        expect(func.f5()).toBeTruthy()

        /*
        Correct intsig declaration
        */
        hcl_correct =
            `
        intsig true     '1'
        intsig false    '0'

        intsig ten      '10'
        intsig seven    '7'
        intsig three    '3'

        intsig val      '3'

        bool f1 = val in { ten } ||
            val in { three };
        bool f2 = val in { false, ten } &&
            val in { seven, true } ||
            val in { false, true, three };  
        `

        result_correct = hcl2js.assemble(hcl_correct)
        expect(result_correct.errors.length).toBe(0)

        func = eval(result_correct.output)
        expect(func.f1()).toBeTruthy()
        expect(func.f2()).toBeTruthy()

        let hcl_incorrect =
            `
        intsig true     '1'
        intsig false    '0'

        intsig ten      '10'
        intsig seven    '7'
        intsig three    '3'

        intsig val      '3'

        bool f1 = val in { ten, seven } &&
            val in { three };
        bool f2 = val in { false, ten } ||
            val in { seven, true } &&
            val in { false, true, three };  
        `

        let result_incorrect = hcl2js.assemble(hcl_incorrect)
        expect(result_incorrect.errors.length).toBe(0)

        func = eval(result_incorrect.output)
        expect(func.f1()).toBeFalsy()
        expect(func.f2()).toBeFalsy()
    })

    /*------------------ New Tests -------------------*/
    test('hcl2js - var declaration', () => {
        let hcl2js = new Hcl2js()

        /*
        Correct intsig declaration
        */
        let hcl_correct =
            `
        intsig int1 'VAL'
        intsig int2 '1'
        intsig int3 '-1'
        `

        let result_correct = hcl2js.assemble(hcl_correct)
        expect(result_correct.errors.length).toBe(0)

        /*
        Correct boolsig declaration
        */
        hcl_correct =
            `
        boolsig bool1 '1'
        boolsig bool2 '0'
        `

        result_correct = hcl2js.assemble(hcl_correct)
        expect(result_correct.errors.length).toBe(0)


        /*
        Incorrect intsig declaration
        */
        let hcl_incorrect =
            `
        intsig int1 'VAL
        `

        let result_incorrect = hcl2js.assemble(hcl_incorrect)
        expect(result_incorrect.errors.length).not.toBe(0)

        hcl_incorrect =
            `
        intsig int1 1
        `

        result_incorrect = hcl2js.assemble(hcl_incorrect)
        expect(result_incorrect.errors.length).not.toBe(0)

        hcl_incorrect =
            `
        intsig int1 -1
        `

        result_incorrect = hcl2js.assemble(hcl_incorrect)
        expect(result_incorrect.errors.length).not.toBe(0)

        /*
        Incorrect intsig declaration
        */
        hcl_incorrect =
            `
        boolsig bool1 'VAL
        `

        result_incorrect = hcl2js.assemble(hcl_incorrect)
        expect(result_incorrect.errors.length).not.toBe(0)

        hcl_incorrect =
            `
        boolsig bool1 1
        `

        result_incorrect = hcl2js.assemble(hcl_incorrect)
        expect(result_incorrect.errors.length).not.toBe(0)

        hcl_incorrect =
            `
        boolsig bool1 0
        `

        result_incorrect = hcl2js.assemble(hcl_incorrect)
        expect(result_incorrect.errors.length).not.toBe(0)
    })
})