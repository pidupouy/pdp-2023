import { Sim } from "../../../model/seq/sim"
import { simStatus } from "../../../model/interfaces/ISimulator"
import { registers_enum } from "../../../model/interfaces/IRegisters"
import { KernelController } from "../../../controllers/kernelController";
import { loadDefaultFile } from "../../testUtils";
import { describe, expect, test } from '@jest/globals';
import { toUint32 } from "../../../model/numberUtils";
import { alufct } from "../../../model/interfaces/IAlu";

let defaultInstructions = JSON.parse(loadDefaultFile("", "instructionSet.json"))
let defaultHcl = loadDefaultFile("seq", "hcl32bits.txt")

const programTest = `
    0x0000:                |  |  Init:  
    0x0000: 30f02a000000   |  |      irmovl 42,%eax 
    0x0006: 400f13000000   |  |      rmmovl %eax,0x13 
    0x000c: 503f13000000   |  |      mrmovl 0x13,%ebx 
    0x0012: 10             |  |      halt 
`

const loadFile = {
    "kernel" : "seq32bits",
    "cpuState" : {"pc":"18","valP":"19","simStatus":5,"icode":1,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"19"},
    "registers" : { "current" : {"eax":"42","ecx":"0","edx":"0","ebx":"42","esp":"0","ebp":"0","esi":"0","edi":"0"}, "next" : {"eax":"42","ecx":"0","edx":"0","ebx":"42","esp":"0","ebp":"0","esi":"0","edi":"0"}},
    "flags" : { "current" : {"ZF":false,"OF":false,"SF":false}, "next" : {"ZF":false,"OF":false,"SF":false}},
    "status" : "HALT",
    "error" : "",
    "stepNum" : 4,
    "memory" : { "current" : [{"address":"0010","value":"0000102a"}], "next" : {"address":"0013","isWrite":false,"bytes":"42"}},
    "compilationResult" : {"output":"  0x0000:                       |  |  Init:  \n  0x0000: 30f02a000000          |  |      irmovl 42,%eax \n  0x0006: 400f13000000          |  |      rmmovl %eax,0x13 \n  0x000c: 503f13000000          |  |      mrmovl 0x13,%ebx \n  0x0012: 10                    |  |      halt  \n","errors":[],"data":{"_labelToPC":{},"_lineNumberToPc":{},"_pcToLineNumber":{}}}
    }

const saveFile =
    `"cpuState" : {"pc":"18","valP":"19","simStatus":5,"icode":1,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"19"},
"registers" : { "current" : {"eax":"42","ecx":"0","edx":"0","ebx":"42","esp":"0","ebp":"0","esi":"0","edi":"0"}, "next" : {"eax":"42","ecx":"0","edx":"0","ebx":"42","esp":"0","ebp":"0","esi":"0","edi":"0"}},
"flags" : { "current" : {"ZF":false,"OF":false,"SF":false}, "next" : {"ZF":false,"OF":false,"SF":false}},
"status" : "HALT",
"error" : "",
"stepNum" : 4,
"memory" : { "current" : [{"address":"0010","value":"0000102a"}], "next" : {"address":"0013","isWrite":false,"bytes":"42"}},
`

function nextMemoryViewTest(result: any, wordSize: number, address: bigint, isWrite: boolean, bytes: number[]) {
    expect(result.wordSize === wordSize).toBeTruthy();
    expect(result.address === address).toBeTruthy();
    expect(result.isWrite === isWrite).toBeTruthy();
    for (let i = 0; i < wordSize; i++) {
        expect(result.bytes[i] === bytes[i]).toBeTruthy();
    }
}

function getMemoryViewTest(result: any, wordSize: number, startAddress: bigint, maxAddress: bigint, modifiedMem: Map<bigint, bigint>, bytes: Map<bigint, bigint>) {
    expect(result.wordSize === wordSize).toBeTruthy();
    expect(BigInt(result.startAddress) === startAddress).toBeTruthy();
    expect(BigInt(result.maxAddress) === maxAddress).toBeTruthy();
    expect(modifiedMem.size === result.modifiedMem.size).toBeTruthy();
    expect(bytes.size === result.bytes.size).toBeTruthy();
    modifiedMem.forEach((value, key) => {
        expect(result.modifiedMem.get(key) == modifiedMem.get(key)).toBeTruthy();
    });
    bytes.forEach((value, key) => {
        expect(result.bytes.get(key) == bytes.get(key)).toBeTruthy();
    });
}

function stage_verification(stage: any, fetch: (bigint | string | number)[], decode: (string | bigint)[], execute: (bigint | boolean)[], memory: (bigint | boolean)[], writeBack: string[], updatePC: bigint[]): void {
    stage.Fetch.lines.forEach((value: any, key: number) => {
        expect(value.name === "").toBeTruthy();
        verifyValues(value.labels, ["icode", "ifun", "ra", "rb", "valC", "valP"])
        verifyValues(value.values, fetch)
    })

    stage.Decode.lines.forEach((value: any, key: number) => {
        expect(value.name === "").toBeTruthy();
        verifyValues(value.labels, ["srcA", "srcB", "valA", "valB"])
        verifyValues(value.values, decode)
    })

    stage.Execute.lines.forEach((value: any, key: number) => {
        expect(value.name === "").toBeTruthy();
        verifyValues(value.labels, ["aluA", "aluB", "valE", "Bch"])
        verifyValues(value.values, execute)
    })

    stage.Memory.lines.forEach((value: any, key: number) => {
        expect(value.name === "").toBeTruthy();
        verifyValues(value.labels, ["addr", "data", "r", "w", "valM"])
        verifyValues(value.values, memory)
    })

    stage.WriteBack.lines.forEach((value: any, key: number) => {
        expect(value.name === "").toBeTruthy();
        verifyValues(value.labels, ["dstE", "dstM"])
        verifyValues(value.values, writeBack)
    })

    stage.UpdatePC.lines.forEach((value: any, key: number) => {
        expect(value.name === "").toBeTruthy();
        verifyValues(value.labels, ["newPC"])
        verifyValues(value.values, updatePC)
    })
}

function register_verification(registers: { [key: string]: bigint }, eax: bigint, ebx: bigint, ecx: bigint, edx: bigint, edi: bigint, esi: bigint, ebp: bigint, esp: bigint): void {
    expect(registers.eax === eax).toBeTruthy();
    expect(registers.ebx === ebx).toBeTruthy();
    expect(registers.ecx === ecx).toBeTruthy()
    expect(registers.edx === edx).toBeTruthy()
    expect(registers.esi === esi).toBeTruthy()
    expect(registers.edi === edi).toBeTruthy()
    expect(registers.ebp === ebp).toBeTruthy()
    expect(registers.esp === esp).toBeTruthy()
}

function verifyValues(result: any, expectedResult: any): void {
    let index = 0;
    for (let name of result) {
        expect(name === expectedResult[index]).toBeTruthy();
        index++;
    }
}

describe("Simulator - seq32bits - tests", () => {
    test("Simulator - seq32bits - step", () => {
        let sim = new KernelController("seq32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.loadProgram(programTest);
        sim.step();

        let stageResult = sim.getStageView();
        stage_verification(stageResult,
            [3, 0, "none", "eax", BigInt(0x2a), BigInt(6)],
            ["none", "none", BigInt(0), BigInt(0)],
            [BigInt(0x2a), BigInt(0), BigInt(0x2a), false],
            [BigInt(0), BigInt(0), false, false, BigInt(0)],
            ["eax", "none"],
            [BigInt(6)]);

        register_verification(sim.getRegistersView(), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        register_verification(sim.getNextRegistersView(), BigInt(42), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));

        getMemoryViewTest(sim.getMemoryView(), 4, BigInt(0), BigInt(0x2000), new Map<bigint, bigint>(), new Map<bigint, bigint>([
            [BigInt(0), BigInt(48)], [BigInt(1), BigInt(240)], [BigInt(2), BigInt(42)], [BigInt(3), BigInt(0)], [BigInt(4), BigInt(0)], [BigInt(5), BigInt(0)]
            , [BigInt(6), BigInt(64)], [BigInt(7), BigInt(15)], [BigInt(8), BigInt(19)], [BigInt(9), BigInt(0)], [BigInt(10), BigInt(0)], [BigInt(11), BigInt(0)]
            , [BigInt(12), BigInt(80)], [BigInt(13), BigInt(63)], [BigInt(14), BigInt(19)], [BigInt(15), BigInt(0)], [BigInt(16), BigInt(0)], [BigInt(17), BigInt(0)]
            , [BigInt(18), BigInt(16)]]));
    });

    test("Simulator - seq32bits - continue", () => {
        let sim = new KernelController("seq32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.loadProgram(programTest);
        sim.continue();

        let stageResult = sim.getStageView();
        stage_verification(stageResult,
            [1, 0, "none", "none", BigInt(0), BigInt(0x13)],
            ["none", "none", BigInt(0), BigInt(0)],
            [BigInt(0), BigInt(0), BigInt(0), false],
            [BigInt(0), BigInt(0), false, false, BigInt(0)],
            ["none", "none"],
            [BigInt(0x13)]);

        register_verification(sim.getRegistersView(), BigInt(42), BigInt(42), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        register_verification(sim.getNextRegistersView(), BigInt(42), BigInt(42), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));

        getMemoryViewTest(sim.getMemoryView(), 4, BigInt(0), BigInt(0x2000), new Map<bigint, bigint>([[BigInt(0x10), BigInt(0x2a100000)]]), new Map<bigint, bigint>([
            [BigInt(0), BigInt(48)], [BigInt(1), BigInt(240)], [BigInt(2), BigInt(42)], [BigInt(3), BigInt(0)], [BigInt(4), BigInt(0)], [BigInt(5), BigInt(0)]
            , [BigInt(6), BigInt(64)], [BigInt(7), BigInt(15)], [BigInt(8), BigInt(19)], [BigInt(9), BigInt(0)], [BigInt(10), BigInt(0)], [BigInt(11), BigInt(0)]
            , [BigInt(12), BigInt(80)], [BigInt(13), BigInt(63)], [BigInt(14), BigInt(19)], [BigInt(15), BigInt(0)], [BigInt(16), BigInt(0)], [BigInt(17), BigInt(0)]
            , [BigInt(18), BigInt(16)], [BigInt(19), BigInt(42)], [BigInt(20), BigInt(0)], [BigInt(21), BigInt(0)], [BigInt(22), BigInt(0)]]));
    });

    test("Simulator - seq32bits - reset", () => {
        let sim = new KernelController("seq32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.loadProgram(programTest);
        sim.continue();
        sim.reset();

        let stageResult = sim.getStageView();
        stage_verification(stageResult,
            [0, 0, "none", "none", BigInt(0), BigInt(0)],
            ["none", "none", BigInt(0), BigInt(0)],
            [BigInt(0), BigInt(0), BigInt(0), false],
            [BigInt(0), BigInt(0), false, false, BigInt(0)],
            ["none", "none"],
            [BigInt(0)]);

        register_verification(sim.getRegistersView(), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        register_verification(sim.getNextRegistersView(), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));

        getMemoryViewTest(sim.getMemoryView(), 4, BigInt(0), BigInt(0x2000), new Map<bigint, bigint>(), new Map<bigint, bigint>([
            [BigInt(0), BigInt(48)], [BigInt(1), BigInt(240)], [BigInt(2), BigInt(42)], [BigInt(3), BigInt(0)], [BigInt(4), BigInt(0)], [BigInt(5), BigInt(0)]
            , [BigInt(6), BigInt(64)], [BigInt(7), BigInt(15)], [BigInt(8), BigInt(19)], [BigInt(9), BigInt(0)], [BigInt(10), BigInt(0)], [BigInt(11), BigInt(0)]
            , [BigInt(12), BigInt(80)], [BigInt(13), BigInt(63)], [BigInt(14), BigInt(19)], [BigInt(15), BigInt(0)], [BigInt(16), BigInt(0)], [BigInt(17), BigInt(0)]
            , [BigInt(18), BigInt(16)]]));
    });

    test("Simulator - seq32bits - undo", () => {
        let sim = new KernelController("seq32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.loadProgram(programTest);
        sim.step();
        sim.step();
        sim.undo();

        let stageResult = sim.getStageView();
        stage_verification(stageResult,
            [3, 0, "none", "eax", BigInt(0x2a), BigInt(6)],
            ["none", "none", BigInt(0), BigInt(0)],
            [BigInt(0x2a), BigInt(0), BigInt(0x2a), false],
            [BigInt(0), BigInt(0), false, false, BigInt(0)],
            ["eax", "none"],
            [BigInt(6)]);

        register_verification(sim.getRegistersView(), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        register_verification(sim.getNextRegistersView(), BigInt(42), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));

        getMemoryViewTest(sim.getMemoryView(), 4, BigInt(0), BigInt(0x2000), new Map<bigint, bigint>(), new Map<bigint, bigint>([
            [BigInt(0), BigInt(48)], [BigInt(1), BigInt(240)], [BigInt(2), BigInt(42)], [BigInt(3), BigInt(0)], [BigInt(4), BigInt(0)], [BigInt(5), BigInt(0)]
            , [BigInt(6), BigInt(64)], [BigInt(7), BigInt(15)], [BigInt(8), BigInt(19)], [BigInt(9), BigInt(0)], [BigInt(10), BigInt(0)], [BigInt(11), BigInt(0)]
            , [BigInt(12), BigInt(80)], [BigInt(13), BigInt(63)], [BigInt(14), BigInt(19)], [BigInt(15), BigInt(0)], [BigInt(16), BigInt(0)], [BigInt(17), BigInt(0)]
            , [BigInt(18), BigInt(16)]]));
    });

    test("Simulator - seq32bits - load", () => {
        let sim = new KernelController("seq32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.loadProgram(programTest);
        sim.load(loadFile);

        let stageResult = sim.getStageView();
        stage_verification(stageResult,
            [1, 0, "none", "none", BigInt(0), BigInt(0x13)],
            ["none", "none", BigInt(0), BigInt(0)],
            [BigInt(0), BigInt(0), BigInt(0), false],
            [BigInt(0), BigInt(0), false, false, BigInt(0)],
            ["none", "none"],
            [BigInt(0x13)]);

        register_verification(sim.getRegistersView(), BigInt(42), BigInt(42), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        register_verification(sim.getNextRegistersView(), BigInt(42), BigInt(42), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));

        getMemoryViewTest(sim.getMemoryView(), 4, BigInt(0), BigInt(0x2000), new Map<bigint, bigint>([[BigInt(0x10), BigInt(0x2a100000)]]), new Map<bigint, bigint>([
            [BigInt(0), BigInt(48)], [BigInt(1), BigInt(240)], [BigInt(2), BigInt(42)], [BigInt(3), BigInt(0)], [BigInt(4), BigInt(0)], [BigInt(5), BigInt(0)]
            , [BigInt(6), BigInt(64)], [BigInt(7), BigInt(15)], [BigInt(8), BigInt(19)], [BigInt(9), BigInt(0)], [BigInt(10), BigInt(0)], [BigInt(11), BigInt(0)]
            , [BigInt(12), BigInt(80)], [BigInt(13), BigInt(63)], [BigInt(14), BigInt(19)], [BigInt(15), BigInt(0)], [BigInt(16), BigInt(0)], [BigInt(17), BigInt(0)]
            , [BigInt(18), BigInt(16)], [BigInt(19), BigInt(42)]]));
    });

    test("Simulator - seq32bits - save", () => {
        let sim = new KernelController("seq32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.loadProgram(programTest);
        sim.continue();

        let saveResult = sim.save();
        expect(saveResult).toBe(saveFile);
    });

    /**
     * Get functions
     */
    test("Simulator - seq32bits - getStageView", () => {
        let sim = new KernelController("seq32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.loadProgram(programTest);
        let result: {
            "Fetch": {
                "columns": never[],
                "lines":
                {
                    "name": string,
                    "labels": string[],
                    "values": (number | bigint | string)[]
                }[],
            },
            "Decode": {
                "columns": never[],
                "lines":
                {
                    "name": string,
                    "labels": string[],
                    "values": (bigint | string)[]
                }[],

            },
            "Execute": {
                "columns": never[],
                "lines":
                {
                    "name": string,
                    "labels": string[],
                    "values": (boolean | bigint)[]
                }[],

            },
            "Memory": {
                "columns": never[],
                "lines":
                {
                    "name": string,
                    "labels": string[],
                    "values": (boolean | bigint)[]
                }[],
            },
            "WriteBack": {
                "columns": never[],
                "lines":
                {
                    "name": string,
                    "labels": string[],
                    "values": string[]
                }[],
            },
            "UpdatePC": {
                "columns": never[],
                "lines":
                {
                    "name": string,
                    "labels": string[],
                    "values": bigint[]
                }[],
            }
        };

        result = sim.getStageView();
        stage_verification(result,
            [0, 0, "none", "none", BigInt(0), BigInt(0)],
            ["none", "none", BigInt(0), BigInt(0)],
            [BigInt(0), BigInt(0), BigInt(0), false],
            [BigInt(0), BigInt(0), false, false, BigInt(0)],
            ["none", "none"],
            [BigInt(0)]);
    });

    test("Simulator - seq32bits - getRegistersView", () => {
        let sim = new KernelController("seq32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        let result: { [key : string] : bigint };

        sim.registers.write(registers_enum.eax, BigInt(10));
        sim.registers.write(registers_enum.ebx, BigInt(-10));
        sim.registers.write(registers_enum.ecx, BigInt(0x100));
        sim.registers.write(registers_enum.edx, BigInt(0xffffffac));
        sim.registers.write(registers_enum.edi, BigInt(-512));
        sim.registers.write(registers_enum.esi, BigInt(512));
        sim.registers.write(registers_enum.ebp, BigInt(0xffffffff));
        sim.registers.write(registers_enum.esp, BigInt(0x80000000));

        result = sim.getRegistersView();
        register_verification(result, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));

        sim.updateSim();
        result = sim.getRegistersView();
        register_verification(result, BigInt(10), toUint32(BigInt(-10)), BigInt(0x100), BigInt(0xffffffac), toUint32(BigInt(-512)), BigInt(512), BigInt(0xffffffff), BigInt(0x80000000));
    });

    test("Simulator - seq32bits - getNextRegisterView", () => {
        let sim = new KernelController("seq32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        let result: { [key : string] : bigint };

        sim.registers.write(registers_enum.eax, BigInt(10));
        sim.registers.write(registers_enum.ebx, BigInt(-10));
        sim.registers.write(registers_enum.ecx, BigInt(0x100));
        sim.registers.write(registers_enum.edx, BigInt(0xffffffac));
        sim.registers.write(registers_enum.edi, BigInt(-512));
        sim.registers.write(registers_enum.esi, BigInt(512));
        sim.registers.write(registers_enum.ebp, BigInt(0xffffffff));
        sim.registers.write(registers_enum.esp, BigInt(0x80000000));

        result = sim.getNextRegistersView();
        register_verification(result, BigInt(10), toUint32(BigInt(-10)), BigInt(0x100), BigInt(0xffffffac), toUint32(BigInt(-512)), BigInt(512), BigInt(0xffffffff), BigInt(0x80000000));

        sim.updateSim();
        result = sim.getNextRegistersView();
        register_verification(result, BigInt(10), toUint32(BigInt(-10)), BigInt(0x100), BigInt(0xffffffac), toUint32(BigInt(-512)), BigInt(512), BigInt(0xffffffff), BigInt(0x80000000));
    });

    test("Simulator - seq32bits - getMemoryView", () => {
        let sim = new KernelController("seq32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        let result: any

        result = sim.getMemoryView();
        getMemoryViewTest(result, 4, BigInt(0), BigInt(0x2000), new Map<bigint, bigint>, new Map<bigint, bigint>);

        sim.loadProgram(programTest);
        sim.continue();

        result = sim.getMemoryView();
        getMemoryViewTest(result, 4, BigInt(0), BigInt(0x2000), new Map<bigint, bigint>([[BigInt(0x10), BigInt(0x2a100000)]]), new Map<bigint, bigint>([
            [BigInt(0), BigInt(48)], [BigInt(1), BigInt(240)], [BigInt(2), BigInt(42)], [BigInt(3), BigInt(0)], [BigInt(4), BigInt(0)], [BigInt(5), BigInt(0)]
            , [BigInt(6), BigInt(64)], [BigInt(7), BigInt(15)], [BigInt(8), BigInt(19)], [BigInt(9), BigInt(0)], [BigInt(10), BigInt(0)], [BigInt(11), BigInt(0)]
            , [BigInt(12), BigInt(80)], [BigInt(13), BigInt(63)], [BigInt(14), BigInt(19)], [BigInt(15), BigInt(0)], [BigInt(16), BigInt(0)], [BigInt(17), BigInt(0)]
            , [BigInt(18), BigInt(16)], [BigInt(19), BigInt(42)], [BigInt(20), BigInt(0)], [BigInt(21), BigInt(0)], [BigInt(22), BigInt(0)]]));
    });

    test("Simulator - seq32bits - getNextMemoryView", () => {
        let sim = new KernelController("seq32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.loadProgram(programTest);
        let result: any

        result = sim.getNextMemoryView();
        nextMemoryViewTest(result, 4, BigInt(0), false, [0, 0, 0, 0])

        sim.step();
        result = sim.getNextMemoryView();
        nextMemoryViewTest(result, 4, BigInt(0), false, [0, 0, 0, 0])

        sim.step();
        result = sim.getNextMemoryView();
        nextMemoryViewTest(result, 4, BigInt(0x13), true, [42, 0, 0, 0])
    });

    test("Simulator - seq32bits - getFlagsView", () => {
        let sim = new KernelController("seq32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        let result: { [key : string] : boolean };

        sim.alu.computeCC(BigInt(10), BigInt(-20), alufct.A_ADD);

        result = sim.getFlagsView();
        expect(result.ZF).toBeFalsy();
        expect(result.OF).toBeFalsy();
        expect(result.SF).toBeFalsy();

        sim.updateSim();
        result = sim.getFlagsView();

        expect(result.ZF).toBeFalsy();
        expect(result.OF).toBeFalsy();
        expect(result.SF).toBeTruthy();
    });

    test("Simulator - seq32bits - getNextFlagsView", () => {
        let sim = new KernelController("seq32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        let result: { [key : string] : boolean };

        sim.alu.computeCC(BigInt(10), BigInt(-20), alufct.A_ADD);

        result = sim.getNextFlagsView();
        expect(result.ZF).toBeFalsy();
        expect(result.OF).toBeFalsy();
        expect(result.SF).toBeTruthy();

        sim.updateSim();
        result = sim.getNextFlagsView();

        expect(result.ZF).toBeFalsy();
        expect(result.OF).toBeFalsy();
        expect(result.SF).toBeTruthy();
    });

    test("Simulator - seq32bits - getStatusView", () => {
        let sim = new KernelController("seq32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        let result: {
            "STAT": simStatus,
            "ERR": string,
            "PC": bigint
        };

        result = sim.getStatusView();
        expect(result.STAT === simStatus.AOK).toBeTruthy();
        expect(result.ERR === "").toBeTruthy();
        expect(result.PC === BigInt(0)).toBeTruthy();
    });

    test("Simulator - seq32bits - getPerformancesView", () => {
        let sim = new KernelController("seq32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        let result;

        result = sim.getPerformancesView();
        expect(result).toStrictEqual(undefined);
    });

    test("Simulator - seq32bits - getHighlightRules", () => {
        let sim = new KernelController("seq32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.loadProgram(programTest);
        let result:
            {
                "address": bigint,
                "tag": string
            }[];

        result = sim.getHighlightRules();
        result.forEach((value, key) => {
            expect(value.address === BigInt(-1)).toBeTruthy();
            expect(value.tag === "*").toBeTruthy();
        })

        sim.step();

        result = sim.getHighlightRules();
        result.forEach((value, key) => {
            expect(value.address === BigInt(0)).toBeTruthy();
            expect(value.tag === "*").toBeTruthy();
        })
    });

    test("Simulator - seq32bits - getStepNum", () => {
        let sim = new KernelController("seq32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.loadProgram(programTest);
        let result: number;

        result = sim.getStepNum();
        expect(result === 0).toBeTruthy();

        sim.step();
        result = sim.getStepNum();
        expect(result === 1).toBeTruthy();

        sim.continue();
        result = sim.getStepNum();
        expect(result === 4).toBeTruthy();
    });

    test("Simulator - seq32bits - isInterfaceTightened", () => {
        let sim = new KernelController("seq32bits", defaultInstructions, defaultHcl).getSim() as Sim;

        expect(sim.isInterfaceTightened()).toBeFalsy();
    });
});