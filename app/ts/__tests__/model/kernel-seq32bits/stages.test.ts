import { describe, expect, test } from '@jest/globals';
import { loadDefaultFile, registerVerification32Bits, flagsVerification, memoryWordVerification } from "../../testUtils";
import { registers_enum } from "../../../model/interfaces/IRegisters"
import { KernelController } from "../../../controllers/kernelController";
import { Sim } from "../../../model/seq/sim";
import { decode, execute, fetch, memory, updatePC, writeBack } from "../../../model/seq/stages";

let defaultInstructions = JSON.parse(loadDefaultFile("", "instructionSet.json"))
let defaultHcl = loadDefaultFile("seq", "hcl32bits.txt")

/**
 * Instructions to test
 */
let nop_prog: string =
    "  0x0000: 00           | nop\n" +
    "                       | \n";

let halt_prog: string =
    "  0x0000: 00           | halt\n" +
    "                       | \n";

let rrmovl_prog: string =
    "  0x0000: 2001         | rrmovl %eax, %ecx\n" +
    "                       | \n";

let irmovl_prog: string =
    "  0x0000: 30f510000000 | irmovl 0x10, %ebp\n" +
    "                       | \n";

let rmmovl_prog: string =
    "  0x0000: 402f00010000 | rmmovl %edx, 0x100\n" +
    "                       | \n";

let rmmovl2_prog: string =
    "  0x0000: 402000000000 | rmmovl %edx,(%eax)\n" +
    "                       | \n";

let mrmovl_prog: string =
    "  0x0000: 500f00010000 | mrmovl 0x100,%eax\n" +
    "                       | \n";

let mrmovl2_prog: string =
    "  0x0000: 500200000000 | mrmovl (%edx),%eax\n" +
    "                       | \n";

let addl_prog: string =
    "  0x0000: 6001         | addl %eax,%ecx\n" +
    "                       | \n";

let subl_prog: string =
    "  0x0000: 6101         | subl %eax,%ecx\n" +
    "                       | \n";

let andl_prog: string =
    "  0x0000: 6201         | andl %eax,%ecx\n" +
    "                       | \n";

let xorl_prog: string =
    "  0x0000: 6301         | xorl %eax,%ecx\n" +
    "                       | \n";

let sall_prog: string =
    "  0x0000: 6401         | sall %eax,%ecx\n" +
    "                       | \n";

let sarl_prog: string =
    "  0x0000: 6501         | sarl %eax,%ecx\n" +
    "                       | \n";

let jmp_prog: string =
    "  0x0000:              | init:\n" +
    "  0x0000: 00           | nop\n" +
    "  0x0000: 7000000000   | jmp init\n" +
    "                       | \n";

let jle_prog: string =
    "  0x0000:              | init:\n" +
    "  0x0000: 6101         | subl %eax,%ecx\n" +
    "  0x0002: 7100000000   | jle init\n" +
    "                       | \n";

let jl_prog: string =
    "  0x0000:              | init:\n" +
    "  0x0000: 6101         | subl %eax,%ecx\n" +
    "  0x0002: 7200000000   | jl init\n" +
    "                       | \n";

let je_prog: string =
    "  0x0000:              | init:\n" +
    "  0x0000: 6101         | subl %eax,%ecx\n" +
    "  0x0002: 7300000000   | je init\n" +
    "                       | \n";

let jne_prog: string =
    "  0x0000:              | init:\n" +
    "  0x0000: 6101         | subl %eax,%ecx\n" +
    "  0x0002: 7400000000   | jne init\n" +
    "                       | \n";

let jge_prog: string =
    "  0x0000:              | init:\n" +
    "  0x0000: 6101         | subl %eax,%ecx\n" +
    "  0x0002: 7500000000   | jge init\n" +
    "                       | \n";

let jg_prog: string =
    "  0x0000:              | init:\n" +
    "  0x0000: 6101         | subl %eax,%ecx\n" +
    "  0x0002: 7600000000   | jg init\n" +
    "                       | \n";

let call_prog: string =
    "  0x0000: 8005000000   | call fun\n" +
    "  0x0005:              | fun:\n" +
    "  0x0005: 00           | nop\n" +
    "                       | \n";

let ret_prog: string =
    "  0x0000: 30f400020000 | irmovl 512,%esp\n" +
    "  0x0006: 800c000000   | call fun\n" +
    "  0x000b: 00           | nop\n" +
    "  0x000c:              | fun:\n" +
    "  0x000c: 90           | ret\n" +
    "                       | \n";

let pushl_prog: string =
    "  0x0000: 30f400020000 | irmovl 512,%esp\n" +
    "  0x0006: a00f         | pushl %eax\n" +
    "                       | \n";

let popl_prog: string =
    "  0x0000: 30f400020000 | irmovl 512,%esp\n" +
    "  0x0006: a00f         | pushl %eax\n" +
    "  0x0008: b03f         | popl %ebx\n" +
    "                       | \n";

let iaddl_prog: string =
    "  0x0000: c0f100010000 | iaddl 0x100,%ecx\n" +
    "                       | \n";

let isubl_prog: string =
    "  0x0000: c1f100010000 | isubl 0x100,%ecx\n" +
    "                       | \n";

let iandl_prog: string =
    "  0x0000: c2f131010000 | iandl 0x131,%ecx\n" +
    "                       | \n";

let ixorl_prog: string =
    "  0x0000: c3f131010000 | ixorl 0x131,%ecx\n" +
    "                       | \n";

let isall_prog: string =
    "  0x0000: c4f101000000 | isall 0x1,%ecx\n" +
    "                       | \n";

let isarl_prog: string =
    "  0x0000: c5f101000000 | isarl 0x1,%ecx\n" +
    "                       | \n";

/**
 * Some macro
 */
function load_sim(program: string): Sim {
    let sim = new KernelController("seq32bits", defaultInstructions, defaultHcl).getSim() as Sim;
    sim.memory.loadProgram(program);
    return sim;
}

function fetch_verification(sim: Sim, icode: number, ifun: number, ra: registers_enum, rb: registers_enum, valC: bigint, valP: bigint): void {
    fetch(sim);
    expect(sim.context.icode).toBe(icode);
    expect(sim.context.ifun).toBe(ifun);
    expect(sim.context.ra).toBe(ra);
    expect(sim.context.rb).toBe(rb);
    expect(sim.context.valC === valC).toBeTruthy();
    expect(sim.context.valP === valP).toBeTruthy();
}

function decode_verification(sim: Sim, srcA: registers_enum, srcB: registers_enum, valA: bigint, valB: bigint, dstE: registers_enum, dstM: registers_enum): void {
    decode(sim);
    expect(sim.context.srcA).toBe(srcA);
    expect(sim.context.srcB).toBe(srcB);
    expect(sim.context.valA === valA).toBeTruthy();
    expect(sim.context.valB === valB).toBeTruthy();
    expect(sim.context.dstE).toBe(dstE);
    expect(sim.context.dstM).toBe(dstM);
}

function execute_verification(sim: Sim, aluA: bigint, aluB: bigint, valE: bigint, bcond: boolean): void {
    execute(sim);
    expect(sim.context.aluA === aluA).toBeTruthy();
    expect(sim.context.aluB === aluB).toBeTruthy();
    expect(sim.context.valE === valE).toBeTruthy();
    expect(sim.context.bcond).toBe(bcond);
}

function memory_verification(sim: Sim, mem_addr: bigint, mem_data: bigint, mem_read: boolean, mem_write: boolean, valM: bigint): void {
    memory(sim);
    expect(sim.context.mem_addr === mem_addr).toBeTruthy();
    expect(sim.context.mem_data === mem_data).toBeTruthy();
    expect(sim.context.mem_read).toBe(mem_read);
    expect(sim.context.mem_write).toBe(mem_write);
    expect(sim.context.valM === valM).toBeTruthy();
}

function writeBack_verification(sim: Sim) {
    writeBack(sim);
}

function updatePC_verification(sim: Sim, newPC: bigint) {
    updatePC(sim);
    expect(sim.context.newPC === newPC).toBeTruthy();
}

/**
 * Test suite
 */
describe("Stages - seq32bits - tests", () => {
    test("Stages - seq32bits - nop", () => {
        let sim = load_sim(nop_prog);

        //pre-context verification
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 0, 0, registers_enum.none, registers_enum.none, BigInt(0), BigInt(1))
        decode_verification(sim, registers_enum.none, registers_enum.none, BigInt(0), BigInt(0), registers_enum.none, registers_enum.none);
        execute_verification(sim, BigInt(0), BigInt(0), BigInt(0), false);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(1));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - halt", () => {
        let sim = load_sim(halt_prog);

        //pre-context verification
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 0, 0, registers_enum.none, registers_enum.none, BigInt(0), BigInt(1))
        decode_verification(sim, registers_enum.none, registers_enum.none, BigInt(0), BigInt(0), registers_enum.none, registers_enum.none);
        execute_verification(sim, BigInt(0), BigInt(0), BigInt(0), false);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(1));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - rrmovl", () => {
        let sim = load_sim(rrmovl_prog);

        //pre-context verification
        sim.registers.write(registers_enum.eax, BigInt(0x10));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0x10), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 2, 0, registers_enum.eax, registers_enum.ecx, BigInt(0), BigInt(2))
        decode_verification(sim, registers_enum.eax, registers_enum.none, BigInt(0x10), BigInt(0), registers_enum.ecx, registers_enum.none);
        execute_verification(sim, BigInt(0x10), BigInt(0), BigInt(0x10), false);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(2));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0x10), BigInt(0), BigInt(0x10), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - irmovl", () => {
        let sim = load_sim(irmovl_prog);

        //pre-context verification
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 3, 0, registers_enum.none, registers_enum.ebp, BigInt(0x10), BigInt(6))
        decode_verification(sim, registers_enum.none, registers_enum.none, BigInt(0), BigInt(0), registers_enum.ebp, registers_enum.none);
        execute_verification(sim, BigInt(0x10), BigInt(0), BigInt(0x10), false);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(6));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0x10), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - rmmovl", () => {
        let sim = load_sim(rmmovl_prog);

        //pre-context verification
        sim.registers.write(registers_enum.edx, BigInt(0x200));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0x200), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 4, 0, registers_enum.edx, registers_enum.none, BigInt(0x100), BigInt(6))
        decode_verification(sim, registers_enum.edx, registers_enum.none, BigInt(0x200), BigInt(0), registers_enum.none, registers_enum.none);
        execute_verification(sim, BigInt(0x100), BigInt(0), BigInt(0x100), false);
        memory_verification(sim, BigInt(0x100), BigInt(0x200), false, true, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(6));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0x200), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        memoryWordVerification(sim.memory, BigInt(0x100), BigInt(0x200));
    });

    test("Stages - seq32bits - rmmovl2", () => {
        let sim = load_sim(rmmovl2_prog);

        //pre-context verification
        sim.registers.write(registers_enum.edx, BigInt(0x200));
        sim.updateSim();
        sim.registers.write(registers_enum.eax, BigInt(0x100));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0x100), BigInt(0), BigInt(0), BigInt(0x200), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 4, 0, registers_enum.edx, registers_enum.eax, BigInt(0), BigInt(6))
        decode_verification(sim, registers_enum.edx, registers_enum.eax, BigInt(0x200), BigInt(0x100), registers_enum.none, registers_enum.none);
        execute_verification(sim, BigInt(0), BigInt(0x100), BigInt(0x100), false);
        memory_verification(sim, BigInt(0x100), BigInt(0x200), false, true, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(6));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0x100), BigInt(0), BigInt(0), BigInt(0x200), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        memoryWordVerification(sim.memory, BigInt(0x100), BigInt(0x200));
    });

    test("Stages - seq32bits - mrmovl", () => {
        let sim = load_sim(mrmovl_prog);

        //pre-context verification
        sim.memory.writeWord(BigInt(0x100), BigInt(0x200));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        memoryWordVerification(sim.memory, BigInt(0x100), BigInt(0x200));

        //Cpu state verification
        fetch_verification(sim, 5, 0, registers_enum.eax, registers_enum.none, BigInt(0x100), BigInt(6))
        decode_verification(sim, registers_enum.none, registers_enum.none, BigInt(0), BigInt(0), registers_enum.none, registers_enum.eax);
        execute_verification(sim, BigInt(0x100), BigInt(0), BigInt(0x100), false);
        memory_verification(sim, BigInt(0x100), BigInt(0), true, false, BigInt(0x200));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(6));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0x200), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        memoryWordVerification(sim.memory, BigInt(0x100), BigInt(0x200));
    });

    test("Stages - seq32bits - mrmovl2", () => {
        let sim = load_sim(mrmovl2_prog);

        //pre-context verification
        sim.memory.writeWord(BigInt(0x100), BigInt(0x200));
        sim.registers.write(registers_enum.edx, BigInt(0x100));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0x100), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        memoryWordVerification(sim.memory, BigInt(0x100), BigInt(0x200));

        //Cpu state verification
        fetch_verification(sim, 5, 0, registers_enum.eax, registers_enum.edx, BigInt(0), BigInt(6))
        decode_verification(sim, registers_enum.none, registers_enum.edx, BigInt(0), BigInt(0x100), registers_enum.none, registers_enum.eax);
        execute_verification(sim, BigInt(0), BigInt(0x100), BigInt(0x100), false);
        memory_verification(sim, BigInt(0x100), BigInt(0), true, false, BigInt(0x200));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(6));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0x200), BigInt(0), BigInt(0), BigInt(0x100), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        memoryWordVerification(sim.memory, BigInt(0x100), BigInt(0x200));
    });

    test("Stages - seq32bits - addl", () => {
        let sim = load_sim(addl_prog);

        //pre-context verification
        sim.registers.write(registers_enum.ecx, BigInt(0x200));
        sim.updateSim();
        sim.registers.write(registers_enum.eax, BigInt(0x100));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0x100), BigInt(0), BigInt(0x200), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 6, 0, registers_enum.eax, registers_enum.ecx, BigInt(0), BigInt(2))
        decode_verification(sim, registers_enum.eax, registers_enum.ecx, BigInt(0x100), BigInt(0x200), registers_enum.ecx, registers_enum.none);
        execute_verification(sim, BigInt(0x100), BigInt(0x200), BigInt(0x300), false);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(2));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0x100), BigInt(0), BigInt(0x300), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - subl", () => {
        let sim = load_sim(subl_prog);

        //pre-context verification
        sim.registers.write(registers_enum.ecx, BigInt(0x200));
        sim.updateSim();
        sim.registers.write(registers_enum.eax, BigInt(0x100));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0x100), BigInt(0), BigInt(0x200), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 6, 1, registers_enum.eax, registers_enum.ecx, BigInt(0), BigInt(2))
        decode_verification(sim, registers_enum.eax, registers_enum.ecx, BigInt(0x100), BigInt(0x200), registers_enum.ecx, registers_enum.none);
        execute_verification(sim, BigInt(0x100), BigInt(0x200), BigInt(0x100), false);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(2));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0x100), BigInt(0), BigInt(0x100), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - andl", () => {
        let sim = load_sim(andl_prog);

        //pre-context verification
        sim.registers.write(registers_enum.ecx, BigInt(0x231));
        sim.updateSim();
        sim.registers.write(registers_enum.eax, BigInt(0x131));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0x131), BigInt(0), BigInt(0x231), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 6, 2, registers_enum.eax, registers_enum.ecx, BigInt(0), BigInt(2))
        decode_verification(sim, registers_enum.eax, registers_enum.ecx, BigInt(0x131), BigInt(0x231), registers_enum.ecx, registers_enum.none);
        execute_verification(sim, BigInt(0x131), BigInt(0x231), BigInt(0x031), false);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(2));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0x131), BigInt(0), BigInt(0x031), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - xorl", () => {
        let sim = load_sim(xorl_prog);

        //pre-context verification
        sim.registers.write(registers_enum.ecx, BigInt(0x231));
        sim.updateSim();
        sim.registers.write(registers_enum.eax, BigInt(0x131));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0x131), BigInt(0), BigInt(0x231), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 6, 3, registers_enum.eax, registers_enum.ecx, BigInt(0), BigInt(2))
        decode_verification(sim, registers_enum.eax, registers_enum.ecx, BigInt(0x131), BigInt(0x231), registers_enum.ecx, registers_enum.none);
        execute_verification(sim, BigInt(0x131), BigInt(0x231), BigInt(0x300), false);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(2));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0x131), BigInt(0), BigInt(0x300), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - sall", () => {
        let sim = load_sim(sall_prog);

        //pre-context verification
        sim.registers.write(registers_enum.ecx, BigInt(0x131));
        sim.updateSim();
        sim.registers.write(registers_enum.eax, BigInt(0x1));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0x1), BigInt(0), BigInt(0x131), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 6, 4, registers_enum.eax, registers_enum.ecx, BigInt(0), BigInt(2))
        decode_verification(sim, registers_enum.eax, registers_enum.ecx, BigInt(0x1), BigInt(0x131), registers_enum.ecx, registers_enum.none);
        execute_verification(sim, BigInt(0x1), BigInt(0x131), BigInt(0x262), false);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(2));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0x1), BigInt(0), BigInt(0x262), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - sarl", () => {
        let sim = load_sim(sarl_prog);

        //pre-context verification
        sim.registers.write(registers_enum.ecx, BigInt(0x262));
        sim.updateSim();
        sim.registers.write(registers_enum.eax, BigInt(0x1));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0x1), BigInt(0), BigInt(0x262), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 6, 5, registers_enum.eax, registers_enum.ecx, BigInt(0), BigInt(2))
        decode_verification(sim, registers_enum.eax, registers_enum.ecx, BigInt(0x1), BigInt(0x262), registers_enum.ecx, registers_enum.none);
        execute_verification(sim, BigInt(0x1), BigInt(0x262), BigInt(0x131), false);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(2));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0x1), BigInt(0), BigInt(0x131), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - jmp", () => {
        let sim = load_sim(jmp_prog);

        //pre-context verification
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        sim.step();
        sim.updateSim();

        //Cpu state verification
        fetch_verification(sim, 7, 0, registers_enum.none, registers_enum.none, BigInt(0), BigInt(5))
        decode_verification(sim, registers_enum.none, registers_enum.none, BigInt(0), BigInt(0), registers_enum.none, registers_enum.none);
        execute_verification(sim, BigInt(0), BigInt(0), BigInt(0), true);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(0));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - jle", () => {
        let sim = load_sim(jle_prog);

        //pre-context verification
        sim.registers.write(registers_enum.ecx, BigInt(0x200));
        sim.updateSim();
        sim.registers.write(registers_enum.eax, BigInt(0x300));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0x300), BigInt(0), BigInt(0x200), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        sim.step();
        writeBack(sim);
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0x300), BigInt(0), BigInt(0xffffff00), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, true);

        //Cpu state verification
        fetch_verification(sim, 7, 1, registers_enum.none, registers_enum.none, BigInt(0), BigInt(7))
        decode_verification(sim, registers_enum.none, registers_enum.none, BigInt(0), BigInt(0), registers_enum.none, registers_enum.none);
        execute_verification(sim, BigInt(0), BigInt(0), BigInt(0), true);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(0));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0x300), BigInt(0), BigInt(0xffffff00), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, true);
    });

    test("Stages - seq32bits - jl", () => {
        let sim = load_sim(jl_prog);

        //pre-context verification
        sim.registers.write(registers_enum.ecx, BigInt(0x200));
        sim.updateSim();
        sim.registers.write(registers_enum.eax, BigInt(0x300));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0x300), BigInt(0), BigInt(0x200), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        sim.step();
        writeBack(sim);
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0x300), BigInt(0), BigInt(0xffffff00), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, true);

        //Cpu state verification
        fetch_verification(sim, 7, 2, registers_enum.none, registers_enum.none, BigInt(0), BigInt(7))
        decode_verification(sim, registers_enum.none, registers_enum.none, BigInt(0), BigInt(0), registers_enum.none, registers_enum.none);
        execute_verification(sim, BigInt(0), BigInt(0), BigInt(0), true);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(0));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0x300), BigInt(0), BigInt(0xffffff00), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, true);
    });

    test("Stages - seq32bits - je", () => {
        let sim = load_sim(je_prog);

        //pre-context verification
        sim.registers.write(registers_enum.ecx, BigInt(0x300));
        sim.updateSim();
        sim.registers.write(registers_enum.eax, BigInt(0x300));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0x300), BigInt(0), BigInt(0x300), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        sim.step();
        writeBack(sim);
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0x300), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, true, false, false);

        //Cpu state verification
        fetch_verification(sim, 7, 3, registers_enum.none, registers_enum.none, BigInt(0), BigInt(7))
        decode_verification(sim, registers_enum.none, registers_enum.none, BigInt(0), BigInt(0), registers_enum.none, registers_enum.none);
        execute_verification(sim, BigInt(0), BigInt(0), BigInt(0), true);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(0));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0x300), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, true, false, false);
    });

    test("Stages - seq32bits - jne", () => {
        let sim = load_sim(jne_prog);

        //pre-context verification
        sim.registers.write(registers_enum.ecx, BigInt(0x300));
        sim.updateSim();
        sim.registers.write(registers_enum.eax, BigInt(0x200));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0x200), BigInt(0), BigInt(0x300), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        sim.step();
        writeBack(sim);
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0x200), BigInt(0), BigInt(0x100), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 7, 4, registers_enum.none, registers_enum.none, BigInt(0), BigInt(7))
        decode_verification(sim, registers_enum.none, registers_enum.none, BigInt(0), BigInt(0), registers_enum.none, registers_enum.none);
        execute_verification(sim, BigInt(0), BigInt(0), BigInt(0), true);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(0));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0x200), BigInt(0), BigInt(0x100), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - jge", () => {
        let sim = load_sim(jge_prog);

        //pre-context verification
        sim.registers.write(registers_enum.ecx, BigInt(0x300));
        sim.updateSim();
        sim.registers.write(registers_enum.eax, BigInt(0x200));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0x200), BigInt(0), BigInt(0x300), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        sim.step();
        writeBack(sim);
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0x200), BigInt(0), BigInt(0x100), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 7, 5, registers_enum.none, registers_enum.none, BigInt(0), BigInt(7))
        decode_verification(sim, registers_enum.none, registers_enum.none, BigInt(0), BigInt(0), registers_enum.none, registers_enum.none);
        execute_verification(sim, BigInt(0), BigInt(0), BigInt(0), true);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(0));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0x200), BigInt(0), BigInt(0x100), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - jg", () => {
        let sim = load_sim(jg_prog);

        //pre-context verification
        sim.registers.write(registers_enum.ecx, BigInt(0x300));
        sim.updateSim();
        sim.registers.write(registers_enum.eax, BigInt(0x200));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0x200), BigInt(0), BigInt(0x300), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        sim.step();
        writeBack(sim);
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0x200), BigInt(0), BigInt(0x100), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 7, 6, registers_enum.none, registers_enum.none, BigInt(0), BigInt(7))
        decode_verification(sim, registers_enum.none, registers_enum.none, BigInt(0), BigInt(0), registers_enum.none, registers_enum.none);
        execute_verification(sim, BigInt(0), BigInt(0), BigInt(0), true);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(0));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0x200), BigInt(0), BigInt(0x100), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - call", () => {
        let sim = load_sim(call_prog);

        //pre-context verification
        sim.registers.write(registers_enum.esp, BigInt(0x200));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0x200));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 8, 0, registers_enum.none, registers_enum.none, BigInt(0x5), BigInt(5))
        decode_verification(sim, registers_enum.none, registers_enum.esp, BigInt(0), BigInt(0x200), registers_enum.esp, registers_enum.none);
        execute_verification(sim, BigInt(-4), BigInt(0x200), BigInt(0x1fc), false);
        memory_verification(sim, BigInt(0x1fc), BigInt(0x5), false, true, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(5));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0x1fc));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - ret", () => {
        let sim = load_sim(ret_prog);

        //pre-context verification
        sim.step();
        sim.step();
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0x200));
        flagsVerification(sim.alu, false, false, false);
        sim.updateSim();
        writeBack(sim);
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0x1fc));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 9, 0, registers_enum.none, registers_enum.none, BigInt(0), BigInt(0xd))
        decode_verification(sim, registers_enum.esp, registers_enum.esp, BigInt(0x1fc), BigInt(0x1fc), registers_enum.esp, registers_enum.none);
        execute_verification(sim, BigInt(4), BigInt(0x1fc), BigInt(0x200), false);
        memory_verification(sim, BigInt(0x1fc), BigInt(0), true, false, BigInt(0xb));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(0xb));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0x200));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - pushl", () => {
        let sim = load_sim(pushl_prog);

        //pre-context verification
        sim.step();
        writeBack(sim)
        sim.updateSim();
        sim.registers.write(registers_enum.eax, BigInt(0x100));
        sim.updateSim()
        registerVerification32Bits(sim.registers, BigInt(0x100), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0x200));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 10, 0, registers_enum.eax, registers_enum.none, BigInt(0), BigInt(8))
        decode_verification(sim, registers_enum.eax, registers_enum.esp, BigInt(0x100), BigInt(0x200), registers_enum.esp, registers_enum.none);
        execute_verification(sim, BigInt(-4), BigInt(0x200), BigInt(0x1fc), false);
        memory_verification(sim, BigInt(0x1fc), BigInt(0x100), false, true, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(8));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0x100), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0x1fc));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - popl", () => {
        let sim = load_sim(popl_prog);

        //pre-context verification
        sim.registers.write(registers_enum.eax, BigInt(0x100));
        sim.updateSim()
        sim.step();
        sim.step();
        sim.updateSim()
        writeBack(sim);
        registerVerification32Bits(sim.registers, BigInt(0x100), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0x1fc));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 11, 0, registers_enum.ebx, registers_enum.none, BigInt(0), BigInt(10))
        decode_verification(sim, registers_enum.esp, registers_enum.esp, BigInt(0x1fc), BigInt(0x1fc), registers_enum.esp, registers_enum.ebx);
        execute_verification(sim, BigInt(4), BigInt(0x1fc), BigInt(0x200), false);
        memory_verification(sim, BigInt(0x1fc), BigInt(0), true, false, BigInt(0x100));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(10));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0x100), BigInt(0x100), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0x200));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - iaddl", () => {
        let sim = load_sim(iaddl_prog);

        //pre-context verification
        sim.registers.write(registers_enum.ecx, BigInt(0x200));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0x200), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 12, 0, registers_enum.none, registers_enum.ecx, BigInt(0x100), BigInt(6))
        decode_verification(sim, registers_enum.none, registers_enum.ecx, BigInt(0), BigInt(0x200), registers_enum.ecx, registers_enum.none);
        execute_verification(sim, BigInt(0x100), BigInt(0x200), BigInt(0x300), false);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(6));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0x300), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - isubl", () => {
        let sim = load_sim(isubl_prog);

        //pre-context verification
        sim.registers.write(registers_enum.ecx, BigInt(0x200));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0x200), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 12, 1, registers_enum.none, registers_enum.ecx, BigInt(0x100), BigInt(6))
        decode_verification(sim, registers_enum.none, registers_enum.ecx, BigInt(0), BigInt(0x200), registers_enum.ecx, registers_enum.none);
        execute_verification(sim, BigInt(0x100), BigInt(0x200), BigInt(0x100), false);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(6));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0x100), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - iandl", () => {
        let sim = load_sim(iandl_prog);

        //pre-context verification
        sim.registers.write(registers_enum.ecx, BigInt(0x231));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0x231), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 12, 2, registers_enum.none, registers_enum.ecx, BigInt(0x131), BigInt(6))
        decode_verification(sim, registers_enum.none, registers_enum.ecx, BigInt(0), BigInt(0x231), registers_enum.ecx, registers_enum.none);
        execute_verification(sim, BigInt(0x131), BigInt(0x231), BigInt(0x031), false);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(6));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0x031), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - ixorl", () => {
        let sim = load_sim(ixorl_prog);

        //pre-context verification
        sim.registers.write(registers_enum.ecx, BigInt(0x231));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0x231), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 12, 3, registers_enum.none, registers_enum.ecx, BigInt(0x131), BigInt(6))
        decode_verification(sim, registers_enum.none, registers_enum.ecx, BigInt(0), BigInt(0x231), registers_enum.ecx, registers_enum.none);
        execute_verification(sim, BigInt(0x131), BigInt(0x231), BigInt(0x300), false);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(6));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0x300), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - isall", () => {
        let sim = load_sim(isall_prog);

        //pre-context verification
        sim.registers.write(registers_enum.ecx, BigInt(0x131));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0x131), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 12, 4, registers_enum.none, registers_enum.ecx, BigInt(0x1), BigInt(6))
        decode_verification(sim, registers_enum.none, registers_enum.ecx, BigInt(0), BigInt(0x131), registers_enum.ecx, registers_enum.none);
        execute_verification(sim, BigInt(0x1), BigInt(0x131), BigInt(0x262), false);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(6));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0x262), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
    });

    test("Stages - seq32bits - isarl", () => {
        let sim = load_sim(isarl_prog);

        //pre-context verification
        sim.registers.write(registers_enum.ecx, BigInt(0x262));
        sim.updateSim();
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0x262), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        //Cpu state verification
        fetch_verification(sim, 12, 5, registers_enum.none, registers_enum.ecx, BigInt(0x1), BigInt(6))
        decode_verification(sim, registers_enum.none, registers_enum.ecx, BigInt(0), BigInt(0x262), registers_enum.ecx, registers_enum.none);
        execute_verification(sim, BigInt(0x1), BigInt(0x262), BigInt(0x131), false);
        memory_verification(sim, BigInt(0), BigInt(0), false, false, BigInt(0));
        writeBack_verification(sim);
        updatePC_verification(sim, BigInt(6));

        //apply last cycle modifications
        sim.updateSim();

        //post-context verification
        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0x131), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
    });
});