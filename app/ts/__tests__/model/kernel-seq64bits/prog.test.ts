import { describe, expect, test } from '@jest/globals';
import { loadDefaultFile, registerVerification64Bits, flagsVerification, memoryWordVerification  } from "../../testUtils";
import { Sim } from "../../../model/seq/sim";
import { KernelController } from "../../../controllers/kernelController"

let defaultInstructions = JSON.parse(loadDefaultFile("", "instructionSet.json"))
let defaultHcl = loadDefaultFile("seq", "hcl64bits.txt")

let prog1: string = `
                              |  |  # prog1: Pad with 1 nop's
0x0000: 30f20a00000000000000  |  |      irmovl 10,%edx 
0x000a: 30f00300000000000000  |  |      irmovl 3,%eax 
0x0014: 00                    |  |      nop  
0x0015: 00                    |  |      nop  
0x0016: 00                    |  |      nop  
0x0017: 6020                  |  |      addl %edx,%eax 
0x0019: 10                    |  |      halt  
`

let prog2: string = `
                              |  |  # prog2: Pad with 2 nop's
0x0000: 30f20a00000000000000  |  |      irmovl 10,%edx 
0x000a: 30f00300000000000000  |  |      irmovl 3,%eax 
0x0014: 00                    |  |      nop  
0x0015: 00                    |  |      nop  
0x0016: 6020                  |  |      addl %edx,%eax 
0x0018: 10                    |  |      halt
`

let prog3: string = `
                              |  |  # prog3: Pad with 1 nop
0x0000: 30f20a00000000000000  |  |      irmovl 10,%edx 
0x000a: 30f00300000000000000  |  |      irmovl 3,%eax 
0x0014: 00                    |  |      nop  
0x0015: 6020                  |  |      addl %edx,%eax 
0x0017: 10                    |  |      halt 
`

let prog4: string = `
                              |  |  # prog4: No padding
0x0000: 30f20a00000000000000  |  |      irmovl 10,%edx 
0x000a: 30f00300000000000000  |  |      irmovl 3,%eax 
0x0014: 6020                  |  |      addl %edx,%eax 
0x0016: 10                    |  |      halt
`

let prog5: string = `
                              |  |  # prog5: Load/use hazard
0x0000: 30f28000000000000000  |  |      irmovl 128,%edx 
0x000a: 30f10300000000000000  |  |      irmovl 3,%ecx 
0x0014: 40120000000000000000  |  |      rmmovl %ecx,0(%edx) 
0x001e: 30f30a00000000000000  |  |      irmovl 10,%ebx 
0x0028: 50020000000000000000  |  |      mrmovl 0(%edx),%eax # Load %eax
0x0032: 6030                  |  |      addl %ebx,%eax # Use %eax
0x0034: 10                    |  |      halt
`

let prog6: string = `
                              |  |  # prog6: Forwarding Priority
0x0000: 30f20a00000000000000  |  |      irmovl 10,%edx 
0x000a: 30f20300000000000000  |  |      irmovl 3,%edx 
0x0014: 2020                  |  |      rrmovl %edx,%eax 
0x0016: 10                    |  |      halt 
`

let prog7: string = `
                              |  |  # prog7: Demonstration of return
0x0000: 30f44000000000000000  |  |      irmovl Stack,%esp # Intialize stack pointer
0x000a: 803000000000000000    |  |      call proc # Procedure call
0x0013: 30f20a00000000000000  |  |      irmovl 10,%edx # Return point
0x001d: 10                    |  |      halt  
0x001e:                       |  |  .pos 0x30 
0x0030:                       |  |  proc:  
0x0030: 90                    |  |      ret  # proc:
0x0031: 2023                  |  |      rrmovl %edx,%ebx # Not executed
0x0033:                       |  |  .pos 0x40 
0x0040:                       |  |  Stack:  # Stack: Stack pointer
`

let prog8: string = `
                              |  |  # prog8: Branch Cancellation
0x0000: 6300                  |  |      xorl %eax,%eax 
0x0002: 741600000000000000    |  |      jne target # Not taken
0x000b: 30f00100000000000000  |  |      irmovl 1,%eax # Fall through
0x0015: 10                    |  |      halt  
0x0016:                       |  |  target:  
0x0016: 30f20200000000000000  |  |      irmovl 2,%edx # Target
0x0020: 30f30300000000000000  |  |      irmovl 3,%ebx # Target+1
0x002a: 10                    |  |      halt
`

let prog9 = `
                              |  |  # Execution begins at address 0
0x0000:                       |  |  .pos 0 
0x0000: 30f40002000000000000  |  |  init:      irmovl Stack,%esp # Set up Stack pointer
0x000a: 703400000000000000    |  |      jmp Main # Execute main program
                              |  |  
                              |  |  # Array of 4 elements
0x0013:                       |  |  .align 4 
0x0014: 0d00000000000000      |  |  array:  .long 0xd 
0x001c: c000000000000000      |  |  .long 0xc0 
0x0024: 000b000000000000      |  |  .long 0xb00 
0x002c: 00a0000000000000      |  |  .long 0xa000 
                              |  |  
0x0034: 30f00400000000000000  |  |  Main:      irmovl 4,%eax 
0x003e: a00f                  |  |      pushl %eax # Push 4
0x0040: 30f21400000000000000  |  |      irmovl array,%edx 
0x004a: a02f                  |  |      pushl %edx # Push array
0x004c: 805600000000000000    |  |      call Sum # Sum(array, 4)
0x0055: 10                    |  |      halt  
                              |  |  
                              |  |  # int Sum(int *Start, int Count)
0x0056: 50140800000000000000  |  |  Sum:      mrmovl 8(%esp),%ecx # ecx = Start
0x0060: 50241000000000000000  |  |      mrmovl 16(%esp),%edx # edx = Count
0x006a: 30f00000000000000000  |  |      irmovl 0,%eax # sum = 0
0x0074: 6222                  |  |      andl %edx,%edx 
0x0076: 73ac00000000000000    |  |      je End 
0x007f: 50610000000000000000  |  |  Loop:      mrmovl (%ecx),%esi # get *Start
0x0089: 6060                  |  |      addl %esi,%eax # add to sum
0x008b: 30f30800000000000000  |  |      irmovl 8,%ebx # 
0x0095: 6031                  |  |      addl %ebx,%ecx # Start++
0x0097: 30f3ffffffffffffffff  |  |      irmovl -1,%ebx # 
0x00a1: 6032                  |  |      addl %ebx,%edx # Count--
0x00a3: 747f00000000000000    |  |      jne Loop # Stop when 0
0x00ac: 90                    |  |  End:      ret  
0x00ad:                       |  |  .pos 0x200 
0x0200:                       |  |  Stack:  # The stack goes here
`

let prog10: string = `
                              |  |  # Execution begins at address 0
0x0000:                       |  |  .pos 0 
0x0000: 30f40006000000000000  |  |  init:      irmovl Stack,%esp # Set up Stack pointer
0x000a: 30f50006000000000000  |  |      irmovl Stack,%ebp # Set up base pointer
0x0014: 704000000000000000    |  |      jmp Main # Execute main program
                              |  |  
                              |  |  # Array of 4 elements
0x001d:                       |  |  .align 4 
0x0020: 0d00000000000000      |  |  array:  .long 0xd 
0x0028: c000000000000000      |  |  .long 0xc0 
0x0030: 000b000000000000      |  |  .long 0xb00 
0x0038: 00a0000000000000      |  |  .long 0xa000 
                              |  |  
0x0040: 30f00400000000000000  |  |  Main:      irmovl 4,%eax 
0x004a: a00f                  |  |      pushl %eax # Push 4
0x004c: 30f22000000000000000  |  |      irmovl array,%edx 
0x0056: a02f                  |  |      pushl %edx # Push array
0x0058: 806200000000000000    |  |      call rSum # Sum(array, 4)
0x0061: 10                    |  |      halt  
                              |  |  
                              |  |  # int Sum(int *Start, int Count)
0x0062: a05f                  |  |  rSum:      pushl %ebp 
0x0064: 2045                  |  |      rrmovl %esp,%ebp 
0x0066: a03f                  |  |      pushl %ebx # Save value of %ebx
0x0068: 50351000000000000000  |  |      mrmovl 16(%ebp),%ebx # Get Start
0x0072: 50051800000000000000  |  |      mrmovl 24(%ebp),%eax # Get Count
0x007c: 6200                  |  |      andl %eax,%eax # Test value of Count
0x007e: 71c300000000000000    |  |      jle L38 # If <= 0, goto zreturn
0x0087: 30f2ffffffffffffffff  |  |      irmovl -1,%edx 
0x0091: 6020                  |  |      addl %edx,%eax # Count--
0x0093: a00f                  |  |      pushl %eax # Push Count
0x0095: 30f20800000000000000  |  |      irmovl 8,%edx 
0x009f: 2030                  |  |      rrmovl %ebx,%eax 
0x00a1: 6020                  |  |      addl %edx,%eax 
0x00a3: a00f                  |  |      pushl %eax # Push Start+1
0x00a5: 806200000000000000    |  |      call rSum # Sum(Start+1, Count-1)
0x00ae: 50230000000000000000  |  |      mrmovl (%ebx),%edx 
0x00b8: 6020                  |  |      addl %edx,%eax # Add *Start
0x00ba: 70c500000000000000    |  |      jmp L39 # goto done
0x00c3: 6300                  |  |  L38:      xorl %eax,%eax # zreturn:
0x00c5: 5035f8ffffffffffffff  |  |  L39:      mrmovl -8(%ebp),%ebx # done: Restore %ebx
0x00cf: 2054                  |  |      rrmovl %ebp,%esp # Deallocate stack frame
0x00d1: b05f                  |  |      popl %ebp # Restore %ebp
0x00d3: 90                    |  |      ret  
                              |  |  
0x00d4:                       |  |  .pos 0x600 
0x0600:                       |  |  Stack:  # The stack goes here
`

let prog11: string = `
                              |  |  # /* $begin cjr-ys */
                              |  |  # Code to generate a combination of not-taken branch and ret
0x0000: 30f40001000000000000  |  |      irmovl Stack,%esp 
0x000a: 30f03800000000000000  |  |      irmovl rtnp,%eax 
0x0014: a00f                  |  |      pushl %eax # Set up return pointer
0x0016: 6300                  |  |      xorl %eax,%eax # Set Z condition code
0x0018: 742c00000000000000    |  |      jne target # Not taken (First part of combination)
0x0021: 30f00100000000000000  |  |      irmovl 1,%eax # Should execute this
0x002b: 10                    |  |      halt  
0x002c: 90                    |  |  target:      ret  # Second part of combination
0x002d: 30f30200000000000000  |  |      irmovl 2,%ebx # Should not execute this
0x0037: 10                    |  |      halt  
0x0038: 30f20300000000000000  |  |  rtnp:      irmovl 3,%edx # Should not execute this
0x0042: 10                    |  |      halt  
0x0043:                       |  |  .pos 0x100 
0x0100:                       |  |  Stack:
`

let prog12: string = `
0x0000: 30f60100000000000000  |  |      irmovl 1,%esi 
0x000a: 30f70200000000000000  |  |      irmovl 2,%edi 
0x0014: 30f50400000000000000  |  |      irmovl 4,%ebp 
0x001e: 30f0e0ffffffffffffff  |  |      irmovl -32,%eax 
0x0028: 30f24000000000000000  |  |      irmovl 64,%edx 
0x0032: 6120                  |  |      subl %edx,%eax 
0x0034: 733f00000000000000    |  |      je target 
0x003d: 00                    |  |      nop  
0x003e: 10                    |  |      halt  
0x003f:                       |  |  target:  
0x003f: 6062                  |  |      addl %esi,%edx 
0x0041: 00                    |  |      nop  
0x0042: 00                    |  |      nop  
0x0043: 00                    |  |      nop  
0x0044: 10                    |  |      halt
`
let prog13: string = `
                              |  |  # Test of Pop semantics for Y86
0x0000: 30f40001000000000000  |  |      irmovl 0x100,%esp # Initialize stack pointer
0x000a: 30f0cdab000000000000  |  |      irmovl 0xABCD,%eax 
0x0014: a00f                  |  |      pushl %eax # Put known value on stack
0x0016: b04f                  |  |      popl %esp # Either get 0xABCD, or 0xfc
0x0018: 10                    |  |      halt
`

let prog14: string = `
                              |  |  # Assembly Code to test semantics of pushl
0x0000: 30f40001000000000000  |  |      irmovl 0x100,%esp 
0x000a: a04f                  |  |      pushl %esp # Ambiguous
0x000c: b00f                  |  |      popl %eax 
0x000e: 10                    |  |      halt  
`

let prog15: string = `
                              |  |  # Test of Push semantics for Y86
0x0000: 30f40001000000000000  |  |      irmovl 0x100,%esp # Initialize stack pointer
0x000a: 2040                  |  |      rrmovl %esp,%eax # Save stack pointer
0x000c: a04f                  |  |      pushl %esp # Push the stack pointer (old or new?)
0x000e: b02f                  |  |      popl %edx # Get it back
0x0010: 6120                  |  |      subl %edx,%eax # Compute difference.  Either 0 (old) or 4 (new).
0x0012: 10                    |  |      halt
`

let prog16: string = `
                              |  |  # Test instruction that modifies %esp followed by ret
0x0000: 30f34000000000000000  |  |      irmovl mem,%ebx 
0x000a: 50430000000000000000  |  |      mrmovl 0(%ebx),%esp # Sets %esp to point to return point
0x0014: 90                    |  |      ret  # Returns to return point 
0x0015: 10                    |  |      halt  # 
0x0016: 30f60500000000000000  |  |  rtnpt:      irmovl 5,%esi # Return point
0x0020: 10                    |  |      halt  
0x0050:                       |  |  .pos 0x40 
0x0040: 5000000000000000      |  |  mem:  .long stack # Holds desired stack pointer
0x0048:                       |  |  .pos 0x50 
0x0050: 1600000000000000      |  |  stack:  .long rtnpt # Top of stack: Holds return point
`

describe("Programs - seq64bits - tests", () => {
    test("Programs - seq64bits - prog1", () => {
        let sim = new KernelController("seq64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog1);

        sim.continue();

        registerVerification64Bits(sim.registers, BigInt('0xd'), BigInt(0), BigInt(0), BigInt('0xa'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        expect(sim.context.pc === BigInt('0x19')).toBeTruthy();
    });

    test("Programs - seq64bits - prog2", () => {
        let sim = new KernelController("seq64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog2);

        sim.continue();

        registerVerification64Bits(sim.registers, BigInt('0xd'), BigInt(0), BigInt(0), BigInt('0xa'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        expect(sim.context.pc === BigInt('0x18')).toBeTruthy();
    });

    test("Programs - seq64bits - prog3", () => {
        let sim = new KernelController("seq64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog3);

        sim.continue();

        registerVerification64Bits(sim.registers, BigInt('0xd'), BigInt(0), BigInt(0), BigInt('0xa'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        expect(sim.context.pc === BigInt('0x17')).toBeTruthy();
    });

    test("Programs - seq64bits - prog4", () => {
        let sim = new KernelController("seq64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog4);

        sim.continue();

        registerVerification64Bits(sim.registers, BigInt('0xd'), BigInt(0), BigInt(0), BigInt('0xa'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        expect(sim.context.pc === BigInt('0x16')).toBeTruthy();
    });

    test("Programs - seq64bits - prog5", () => {
        let sim = new KernelController("seq64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog5);

        sim.continue();

        registerVerification64Bits(sim.registers, BigInt('0xd'), BigInt('0xa'), BigInt('0x3'), BigInt('0x80'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        memoryWordVerification(sim.memory, BigInt('0x80'), BigInt('0x3'));
        expect(sim.context.pc === BigInt('0x34')).toBeTruthy();
    });

    test("Programs - seq64bits - prog6", () => {
        let sim = new KernelController("seq64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog6);

        sim.continue();

        registerVerification64Bits(sim.registers, BigInt('0x3'), BigInt(0), BigInt(0), BigInt('0x3'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        expect(sim.context.pc === BigInt('0x16')).toBeTruthy();
    });

    test("Programs - seq64bits - prog7", () => {
        let sim = new KernelController("seq64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog7);

        sim.continue();

        registerVerification64Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0), BigInt('0xa'), BigInt(0), BigInt(0), BigInt(0), BigInt('0x40'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        memoryWordVerification(sim.memory, BigInt('0x38'), BigInt('0x13'));
        expect(sim.context.pc === BigInt('0x1d')).toBeTruthy();
    });

    test("Programs - seq64bits - prog8", () => {
        let sim = new KernelController("seq64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog8);

        sim.continue();

        registerVerification64Bits(sim.registers, BigInt('0x1'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, true, false, false);
        expect(sim.context.pc === BigInt('0x15')).toBeTruthy();
    });
    
    test("Programs - seq64bits - prog9", () => {
        let sim = new KernelController("seq64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog9);

        sim.continue();

        registerVerification64Bits(sim.registers, BigInt('0xabcd'), BigInt('0xffffffffffffffff'), BigInt('0x34'), BigInt(0), BigInt('0xa000'), BigInt(0), BigInt(0), BigInt('0x1f0'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, true, false, false);
        memoryWordVerification(sim.memory, BigInt('0x1e8'), BigInt('0x55'));
        memoryWordVerification(sim.memory, BigInt('0x1f0'), BigInt('0x14'));
        memoryWordVerification(sim.memory, BigInt('0x1f8'), BigInt('0x4'));
        expect(sim.context.pc === BigInt('0x55')).toBeTruthy();
    });

    test("Programs - seq64bits - prog10", () => {
        let sim = new KernelController("seq64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog10);

        sim.continue();

        registerVerification64Bits(sim.registers, BigInt('0xabcd'), BigInt(0), BigInt(0), BigInt('0xd'), BigInt(0), BigInt(0), BigInt('0x600'), BigInt('0x5f0'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);

        memoryWordVerification(sim.memory, BigInt('0x538'), BigInt('0x38'));
        memoryWordVerification(sim.memory, BigInt('0x540'), BigInt('0x568'));
        memoryWordVerification(sim.memory, BigInt('0x548'), BigInt('0xae'));
        memoryWordVerification(sim.memory, BigInt('0x550'), BigInt('0x40'));
        memoryWordVerification(sim.memory, BigInt('0x558'), BigInt(0));

        memoryWordVerification(sim.memory, BigInt('0x560'), BigInt('0x30'));
        memoryWordVerification(sim.memory, BigInt('0x568'), BigInt('0x590'));
        memoryWordVerification(sim.memory, BigInt('0x570'), BigInt('0xae'));
        memoryWordVerification(sim.memory, BigInt('0x578'), BigInt('0x38'));
        memoryWordVerification(sim.memory, BigInt('0x580'), BigInt('0x1'));

        memoryWordVerification(sim.memory, BigInt('0x588'), BigInt('0x28'));
        memoryWordVerification(sim.memory, BigInt('0x590'), BigInt('0x5b8'));
        memoryWordVerification(sim.memory, BigInt('0x598'), BigInt('0xae'));
        memoryWordVerification(sim.memory, BigInt('0x5a0'), BigInt('0x30'));
        memoryWordVerification(sim.memory, BigInt('0x5a8'), BigInt('0x2'));

        memoryWordVerification(sim.memory, BigInt('0x5b0'), BigInt('0x20'));
        memoryWordVerification(sim.memory, BigInt('0x5b8'), BigInt('0x5e0'));
        memoryWordVerification(sim.memory, BigInt('0x5c0'), BigInt('0xae'));
        memoryWordVerification(sim.memory, BigInt('0x5c8'), BigInt('0x28'));
        memoryWordVerification(sim.memory, BigInt('0x5d0'), BigInt('0x3'));

        memoryWordVerification(sim.memory, BigInt('0x5d8'), BigInt('0x0'));
        memoryWordVerification(sim.memory, BigInt('0x5e0'), BigInt('0x600'));
        memoryWordVerification(sim.memory, BigInt('0x5e8'), BigInt('0x61'));
        memoryWordVerification(sim.memory, BigInt('0x5f0'), BigInt('0x20'));
        memoryWordVerification(sim.memory, BigInt('0x5f8'), BigInt('0x4'));
        expect(sim.context.pc === BigInt('0x61')).toBeTruthy();
    });

    test("Programs - seq64bits - prog11", () => {
        let sim = new KernelController("seq64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog11);

        sim.continue();

        registerVerification64Bits(sim.registers, BigInt('0x1'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt('0xf8'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, true, false, false);
        memoryWordVerification(sim.memory, BigInt('0xf8'), BigInt('0x38'));
        expect(sim.context.pc === BigInt('0x2b')).toBeTruthy();
    });

    test("Programs - seq64bits - prog12", () => {
        let sim = new KernelController("seq64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog12);

        sim.continue();

        registerVerification64Bits(sim.registers, BigInt('0xffffffffffffffa0'), BigInt(0), BigInt(0), BigInt('0x40'), BigInt('0x1'), BigInt('0x2'), BigInt('0x4'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, true);
        expect(sim.context.pc === BigInt('0x3e')).toBeTruthy();
    });

    test("Programs - seq64bits - prog13", () => {
        let sim = new KernelController("seq64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog13);

        sim.continue();

        registerVerification64Bits(sim.registers, BigInt('0xabcd'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt('0xabcd'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        memoryWordVerification(sim.memory, BigInt('0xf8'), BigInt('0xabcd'));
        expect(sim.context.pc === BigInt('0x18')).toBeTruthy();
    });

    test("Programs - seq64bits - prog14", () => {
        let sim = new KernelController("seq64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog14);

        sim.continue();

        registerVerification64Bits(sim.registers, BigInt('0x100'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt('0x100'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        memoryWordVerification(sim.memory, BigInt('0xf8'), BigInt('0x100'));
        expect(sim.context.pc === BigInt('0xe')).toBeTruthy();
    });

    test("Programs - seq64bits - prog15", () => {
        let sim = new KernelController("seq64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog15);

        sim.continue();

        registerVerification64Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0), BigInt('0x100'), BigInt(0), BigInt(0), BigInt(0), BigInt('0x100'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, true, false, false);
        memoryWordVerification(sim.memory, BigInt('0xf8'), BigInt('0x100'));
        expect(sim.context.pc === BigInt('0x12')).toBeTruthy();
    });

    test("Programs - seq64bits - prog16", () => {
        let sim = new KernelController("seq64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog16);

        sim.continue();

        registerVerification64Bits(sim.registers, BigInt(0), BigInt('0x40'), BigInt(0), BigInt(0), BigInt('0x5'), BigInt(0), BigInt(0), BigInt('0x58'), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        expect(sim.context.pc === BigInt('0x20')).toBeTruthy();
    });
});