import { Sim } from "../../../model/pipe/sim"
import { simStatus } from "../../../model/interfaces/ISimulator"
import { registers_enum } from "../../../model/interfaces/IRegisters"
import { KernelController } from "../../../controllers/kernelController";
import { loadDefaultFile } from "../../testUtils";
import { describe, expect, test } from '@jest/globals';
import { toUint64 } from "../../../model/numberUtils";
import { alufct } from "../../../model/interfaces/IAlu";

let defaultInstructions = JSON.parse(loadDefaultFile("", "instructionSet.json"))
let defaultHcl = loadDefaultFile("pipe", "hcl64bits.txt")

const programTest = `
0x0000:                       |  |  Init:  
0x0000: 30f02a00000000000000  |  |      irmovl 42,%eax 
0x000a: 400f1f00000000000000  |  |      rmmovl %eax,0x1f 
0x0014: 503f1f00000000000000  |  |      mrmovl 0x1f,%ebx 
0x001e: 10                    |  |      halt  
`

const loadFile = {
    "kernel" : "pipe64bits",
    "cpuState" : {
    "fetch" : {
    "current" : {"pc":"34","valP":"0","simStatus":1,"icode":0,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"34"},
    "next" : {"pc":"-1","valP":"0","simStatus":1,"icode":0,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"35"},
    "op" : "LOAD"
    },
    "decode" : {
    "current" : {"pc":"33","valP":"34","simStatus":1,"icode":0,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"0"},
    "next" : {"pc":"34","valP":"35","simStatus":1,"icode":0,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"0"},
    "op" : "LOAD"
    },
    "execute" : {
    "current" : {"pc":"-1","valP":"0","simStatus":5,"icode":0,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"0"},
    "next" : {"pc":"33","valP":"0","simStatus":1,"icode":0,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"0"},
    "op" : "LOAD"
    },
    "memory" : {
    "current" : {"pc":"-1","valP":"0","simStatus":5,"icode":0,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"0"},
    "next" : {"pc":"-1","valP":"0","simStatus":5,"icode":0,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"0"},
    "op" : "LOAD"
    },
    "writeBack" : {
    "current" : {"pc":"30","valP":"0","simStatus":2,"icode":1,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"0"},
    "next" : {"pc":"-1","valP":"0","simStatus":5,"icode":0,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"0"},
    "op" : "LOAD"
    }
    },
    "registers" : { "current" : {"eax":"42","ecx":"0","edx":"0","ebx":"0","esp":"0","ebp":"0","esi":"0","edi":"0","r8":"0","r9":"0","r10":"0","r11":"0","r12":"0","r13":"0","r14":"0"}, "next" : {"eax":"42","ecx":"0","edx":"0","ebx":"0","esp":"0","ebp":"0","esi":"0","edi":"0","r8":"0","r9":"0","r10":"0","r11":"0","r12":"0","r13":"0","r14":"0"}},
    "flags" : { "current" : {"ZF":false,"OF":false,"SF":false}, "next" : {"ZF":false,"OF":false,"SF":false}},
    "cycles" : 4,
    "instructions" : 4,
    "status" : "HALT",
    "error" : "",
    "stepNum" : 8,
    "memory" : { "current" : [{"address":"00000018","value":"000000000000102a"}], "next" : {"address":"0000001f","isWrite":false,"bytes":"42"}},
    "compilationResult" : {"output":"  0x0000:                       |  |  Init:  \n  0x0000: 30f02a00000000000000  |  |      irmovl 42,%eax \n  0x000a: 400f1f00000000000000  |  |      rmmovl %eax,0x1f \n  0x0014: 503f1f00000000000000  |  |      mrmovl 0x1f,%ebx \n  0x001e: 10                    |  |      halt  \n","errors":[],"data":{"_labelToPC":{},"_lineNumberToPc":{},"_pcToLineNumber":{}}}
    }

const saveFile =
    `"cpuState" : {
"fetch" : {
"current" : {"pc":"34","valP":"0","simStatus":1,"icode":0,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"34"},
"next" : {"pc":"-1","valP":"0","simStatus":1,"icode":0,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"35"},
"op" : "LOAD"
},
"decode" : {
"current" : {"pc":"33","valP":"34","simStatus":1,"icode":0,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"0"},
"next" : {"pc":"34","valP":"35","simStatus":1,"icode":0,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"0"},
"op" : "LOAD"
},
"execute" : {
"current" : {"pc":"-1","valP":"0","simStatus":5,"icode":0,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"0"},
"next" : {"pc":"33","valP":"0","simStatus":1,"icode":0,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"0"},
"op" : "LOAD"
},
"memory" : {
"current" : {"pc":"-1","valP":"0","simStatus":5,"icode":0,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"0"},
"next" : {"pc":"-1","valP":"0","simStatus":5,"icode":0,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"0"},
"op" : "LOAD"
},
"writeBack" : {
"current" : {"pc":"30","valP":"0","simStatus":2,"icode":1,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"0"},
"next" : {"pc":"-1","valP":"0","simStatus":5,"icode":0,"ifun":0,"ra":15,"rb":15,"valC":"0","valA":"0","valB":"0","srcA":15,"srcB":15,"dstE":15,"dstM":15,"aluA":"0","aluB":"0","valE":"0","cc":0,"bcond":false,"mem_addr":"0","mem_data":"0","mem_read":false,"mem_write":false,"valM":"0","newPC":"0"},
"op" : "LOAD"
}
},
"registers" : { "current" : {"eax":"42","ecx":"0","edx":"0","ebx":"0","esp":"0","ebp":"0","esi":"0","edi":"0","r8":"0","r9":"0","r10":"0","r11":"0","r12":"0","r13":"0","r14":"0"}, "next" : {"eax":"42","ecx":"0","edx":"0","ebx":"0","esp":"0","ebp":"0","esi":"0","edi":"0","r8":"0","r9":"0","r10":"0","r11":"0","r12":"0","r13":"0","r14":"0"}},
"flags" : { "current" : {"ZF":false,"OF":false,"SF":false}, "next" : {"ZF":false,"OF":false,"SF":false}},
"cycles" : 4,
"instructions" : 4,
"status" : "HALT",
"error" : "",
"stepNum" : 8,
"memory" : { "current" : [{"address":"00000018","value":"000000000000102a"}], "next" : {"address":"0000001f","isWrite":false,"bytes":"42"}},
`

function nextMemoryViewTest(result: any, wordSize: number, address: bigint, isWrite: boolean, bytes: number[]) {
    expect(result.wordSize === wordSize).toBeTruthy();
    expect(result.address === address).toBeTruthy();
    expect(result.isWrite === isWrite).toBeTruthy();
    for (let i = 0; i < wordSize; i++) {
        expect(result.bytes[i] === bytes[i]).toBeTruthy();
    }
}

function getMemoryViewTest(result: any, wordSize: number, startAddress: bigint, maxAddress: bigint, modifiedMem: Map<bigint, bigint>, bytes: Map<bigint, bigint>) {
    expect(result.wordSize === wordSize).toBeTruthy();
    expect(BigInt(result.startAddress) === startAddress).toBeTruthy();
    expect(BigInt(result.maxAddress) === maxAddress).toBeTruthy();
    expect(modifiedMem.size === result.modifiedMem.size).toBeTruthy();
    expect(bytes.size === result.bytes.size).toBeTruthy();
    modifiedMem.forEach((value, key) => {
        expect(result.modifiedMem.get(key) == modifiedMem.get(key)).toBeTruthy();
    });
    bytes.forEach((value, key) => {
        expect(result.bytes.get(key) == bytes.get(key)).toBeTruthy();
    });
}

function stage_verification(stage: any, writeBack: (string | bigint)[][], memory: (string | boolean | bigint)[][], execute: (string | bigint)[][], decode: (string | bigint)[][], fetch: (string | bigint)[][]): void {
    verifyValues(stage.WriteBack.columns, ["", "stat", "instr", "valE", "valM", "dstE", "dstM"])
    stage.WriteBack.lines.forEach((value: any, key: number) => {
        expect(value.name === "State" || value.name === "Input").toBeTruthy();
        verifyValues(value.labels, ["", "", "", "", "", ""])
        verifyValues(value.values, writeBack[key])
    })

    verifyValues(stage.Memory.columns, ["", "stat", "instr", "Bch", "valE", "valA", "dstE", "dstM"])
    stage.Memory.lines.forEach((value: any, key: number) => {
        expect(value.name === "State" || value.name === "Input").toBeTruthy();
        verifyValues(value.labels, ["", "", "", "", "", "", ""])
        verifyValues(value.values, memory[key])
    })

    verifyValues(stage.Execute.columns, ["", "stat", "instr", "srcA", "srcB", "valA", "valB", "valC", "dstE", "dstM"])
    stage.Execute.lines.forEach((value: any, key: number) => {
        expect(value.name === "State" || value.name === "Input").toBeTruthy();
        verifyValues(value.labels, ["", "", "", "", "", "", "", "", ""])
        verifyValues(value.values, execute[key])
    })

    verifyValues(stage.Decode.columns, ["", "stat", "instr", "ra", "rb", "valC", "valP"])
    stage.Decode.lines.forEach((value: any, key: number) => {
        expect(value.name === "State" || value.name === "Input").toBeTruthy();
        verifyValues(value.labels, ["", "", "", "", "", ""])
        verifyValues(value.values, decode[key])
    })

    verifyValues(stage.Fetch.columns, ["", "stat", "newPC"])
    stage.Fetch.lines.forEach((value: any, key: number) => {
        expect(value.name === "State" || value.name === "Input").toBeTruthy();
        verifyValues(value.labels, ["", ""])
        verifyValues(value.values, fetch[key])
    })
}

function register_verification(registers: { [key: string]: bigint }, eax: bigint, ebx: bigint, ecx: bigint, edx: bigint, edi: bigint, esi: bigint, ebp: bigint, esp: bigint): void {
    expect(registers.eax === eax).toBeTruthy();
    expect(registers.ebx === ebx).toBeTruthy();
    expect(registers.ecx === ecx).toBeTruthy()
    expect(registers.edx === edx).toBeTruthy()
    expect(registers.esi === esi).toBeTruthy()
    expect(registers.edi === edi).toBeTruthy()
    expect(registers.ebp === ebp).toBeTruthy()
    expect(registers.esp === esp).toBeTruthy()
}

function verifyValues(result: any, expectedResult: any): void {
    let index = 0;
    for (let name of result) {
        expect(name === expectedResult[index]).toBeTruthy();
        index++;
    }
}

describe("Simulator - pipe64bits - tests", () => {
    test("Simulator - pipe64bits - step", () => {
        let sim = new KernelController("pipe64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.loadProgram(programTest);
        sim.step();

        let stageResult = sim.getStageView();
        stage_verification(stageResult,
            [["BUBBLE", "nop", BigInt(0), BigInt(0), "none", "none"], ["BUBBLE", "nop", BigInt(0), BigInt(0), "none", "none"]],
            [["BUBBLE", "nop", false, BigInt(0), BigInt(0), "none", "none"], ["BUBBLE", "nop", false, BigInt(0), BigInt(0), "none", "none"]],
            [["BUBBLE", "nop", "none", "none", BigInt(0), BigInt(0), BigInt(0), "none", "none"], ["BUBBLE", "nop", "none", "none", BigInt(0), BigInt(0), BigInt(0), "none", "none"]],
            [["BUBBLE", "nop", "none", "none", BigInt(0), BigInt(0)], ["AOK", "irmovl", "none", "eax", BigInt(0x2a), BigInt(0xa)]],
            [["BUBBLE", BigInt(0)], ["AOK", BigInt(0xa)]]);

        register_verification(sim.getRegistersView(), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        register_verification(sim.getNextRegistersView(), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));

        getMemoryViewTest(sim.getMemoryView(), 8, BigInt(0), BigInt(0x2000), new Map<bigint, bigint>(), new Map<bigint, bigint>([
            [BigInt(0), BigInt(48)], [BigInt(1), BigInt(240)], [BigInt(2), BigInt(42)], [BigInt(3), BigInt(0)], [BigInt(4), BigInt(0)], [BigInt(5), BigInt(0)]
            , [BigInt(6), BigInt(0)], [BigInt(7), BigInt(0)], [BigInt(8), BigInt(0)], [BigInt(9), BigInt(0)], [BigInt(10), BigInt(64)], [BigInt(11), BigInt(15)]
            , [BigInt(12), BigInt(31)], [BigInt(13), BigInt(0)], [BigInt(14), BigInt(0)], [BigInt(15), BigInt(0)], [BigInt(16), BigInt(0)], [BigInt(17), BigInt(0)]
            , [BigInt(18), BigInt(0)], [BigInt(19), BigInt(0)], [BigInt(20), BigInt(80)], [BigInt(21), BigInt(63)], [BigInt(22), BigInt(31)], [BigInt(23), BigInt(0)]
            , [BigInt(24), BigInt(0)], [BigInt(25), BigInt(0)], [BigInt(26), BigInt(0)], [BigInt(27), BigInt(0)], [BigInt(28), BigInt(0)], [BigInt(29), BigInt(0)]
            , [BigInt(30), BigInt(16)]]));
    });

    test("Simulator - pipe64bits - continue", () => {
        let sim = new KernelController("pipe64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.loadProgram(programTest);
        sim.continue();

        let stageResult = sim.getStageView();
        stage_verification(stageResult,
            [["HALT", "halt", BigInt(0), BigInt(0), "none", "none"], ["BUBBLE", "nop", BigInt(0), BigInt(0), "none", "none"]],
            [["BUBBLE", "nop", false, BigInt(0), BigInt(0), "none", "none"], ["BUBBLE", "nop", false, BigInt(0), BigInt(0), "none", "none"]],
            [["BUBBLE", "nop", "none", "none", BigInt(0), BigInt(0), BigInt(0), "none", "none"], ["AOK", "nop", "none", "none", BigInt(0), BigInt(0), BigInt(0), "none", "none"]],
            [["AOK", "nop", "none", "none", BigInt(0), BigInt(0x22)], ["AOK", "nop", "none", "none", BigInt(0), BigInt(0x23)]],
            [["AOK", BigInt(0x22)], ["AOK", BigInt(0x23)]]);

        register_verification(sim.getRegistersView(), BigInt(42), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        register_verification(sim.getNextRegistersView(), BigInt(42), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));

        getMemoryViewTest(sim.getMemoryView(), 8, BigInt(0), BigInt(0x2000), new Map<bigint, bigint>([[BigInt(24), BigInt(0x2a10000000000000)]]), new Map<bigint, bigint>([
            [BigInt(0), BigInt(48)], [BigInt(1), BigInt(240)], [BigInt(2), BigInt(42)], [BigInt(3), BigInt(0)], [BigInt(4), BigInt(0)], [BigInt(5), BigInt(0)]
            , [BigInt(6), BigInt(0)], [BigInt(7), BigInt(0)], [BigInt(8), BigInt(0)], [BigInt(9), BigInt(0)], [BigInt(10), BigInt(64)], [BigInt(11), BigInt(15)]
            , [BigInt(12), BigInt(31)], [BigInt(13), BigInt(0)], [BigInt(14), BigInt(0)], [BigInt(15), BigInt(0)], [BigInt(16), BigInt(0)], [BigInt(17), BigInt(0)]
            , [BigInt(18), BigInt(0)], [BigInt(19), BigInt(0)], [BigInt(20), BigInt(80)], [BigInt(21), BigInt(63)], [BigInt(22), BigInt(31)], [BigInt(23), BigInt(0)]
            , [BigInt(24), BigInt(0)], [BigInt(25), BigInt(0)], [BigInt(26), BigInt(0)], [BigInt(27), BigInt(0)], [BigInt(28), BigInt(0)], [BigInt(29), BigInt(0)]
            , [BigInt(30), BigInt(16)], [BigInt(31), BigInt(42)], [BigInt(32), BigInt(0)], [BigInt(33), BigInt(0)], [BigInt(34), BigInt(0)], [BigInt(35), BigInt(0)]
            , [BigInt(36), BigInt(0)], [BigInt(37), BigInt(0)], [BigInt(38), BigInt(0)]]));
    });

    test("Simulator - pipe64bits - reset", () => {
        let sim = new KernelController("pipe64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.loadProgram(programTest);
        sim.continue();
        sim.reset();

        let stageResult = sim.getStageView();
        stage_verification(stageResult,
            [["BUBBLE", "nop", BigInt(0), BigInt(0), "none", "none"], ["BUBBLE", "nop", BigInt(0), BigInt(0), "none", "none"]],
            [["BUBBLE", "nop", false, BigInt(0), BigInt(0), "none", "none"], ["BUBBLE", "nop", false, BigInt(0), BigInt(0), "none", "none"]],
            [["BUBBLE", "nop", "none", "none", BigInt(0), BigInt(0), BigInt(0), "none", "none"], ["BUBBLE", "nop", "none", "none", BigInt(0), BigInt(0), BigInt(0), "none", "none"]],
            [["BUBBLE", "nop", "none", "none", BigInt(0), BigInt(0)], ["BUBBLE", "nop", "none", "none", BigInt(0), BigInt(0)]],
            [["BUBBLE", BigInt(0)], ["BUBBLE", BigInt(0)]]);

        register_verification(sim.getRegistersView(), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        register_verification(sim.getNextRegistersView(), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));

        getMemoryViewTest(sim.getMemoryView(), 8, BigInt(0), BigInt(0x2000), new Map<bigint, bigint>(), new Map<bigint, bigint>([
            [BigInt(0), BigInt(48)], [BigInt(1), BigInt(240)], [BigInt(2), BigInt(42)], [BigInt(3), BigInt(0)], [BigInt(4), BigInt(0)], [BigInt(5), BigInt(0)]
            , [BigInt(6), BigInt(0)], [BigInt(7), BigInt(0)], [BigInt(8), BigInt(0)], [BigInt(9), BigInt(0)], [BigInt(10), BigInt(64)], [BigInt(11), BigInt(15)]
            , [BigInt(12), BigInt(31)], [BigInt(13), BigInt(0)], [BigInt(14), BigInt(0)], [BigInt(15), BigInt(0)], [BigInt(16), BigInt(0)], [BigInt(17), BigInt(0)]
            , [BigInt(18), BigInt(0)], [BigInt(19), BigInt(0)], [BigInt(20), BigInt(80)], [BigInt(21), BigInt(63)], [BigInt(22), BigInt(31)], [BigInt(23), BigInt(0)]
            , [BigInt(24), BigInt(0)], [BigInt(25), BigInt(0)], [BigInt(26), BigInt(0)], [BigInt(27), BigInt(0)], [BigInt(28), BigInt(0)], [BigInt(29), BigInt(0)]
            , [BigInt(30), BigInt(16)]]));
    });

    test("Simulator - pipe64bits - undo", () => {
        let sim = new KernelController("pipe64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.loadProgram(programTest);
        sim.step();
        sim.step();
        sim.undo();

        let stageResult = sim.getStageView();
        stage_verification(stageResult,
            [["BUBBLE", "nop", BigInt(0), BigInt(0), "none", "none"], ["BUBBLE", "nop", BigInt(0), BigInt(0), "none", "none"]],
            [["BUBBLE", "nop", false, BigInt(0), BigInt(0), "none", "none"], ["BUBBLE", "nop", false, BigInt(0), BigInt(0), "none", "none"]],
            [["BUBBLE", "nop", "none", "none", BigInt(0), BigInt(0), BigInt(0), "none", "none"], ["BUBBLE", "nop", "none", "none", BigInt(0), BigInt(0), BigInt(0), "none", "none"]],
            [["BUBBLE", "nop", "none", "none", BigInt(0), BigInt(0)], ["AOK", "irmovl", "none", "eax", BigInt(0x2a), BigInt(0xa)]],
            [["BUBBLE", BigInt(0)], ["AOK", BigInt(0xa)]]);

        register_verification(sim.getRegistersView(), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        register_verification(sim.getNextRegistersView(), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));

        getMemoryViewTest(sim.getMemoryView(), 8, BigInt(0), BigInt(0x2000), new Map<bigint, bigint>(), new Map<bigint, bigint>([
            [BigInt(0), BigInt(48)], [BigInt(1), BigInt(240)], [BigInt(2), BigInt(42)], [BigInt(3), BigInt(0)], [BigInt(4), BigInt(0)], [BigInt(5), BigInt(0)]
            , [BigInt(6), BigInt(0)], [BigInt(7), BigInt(0)], [BigInt(8), BigInt(0)], [BigInt(9), BigInt(0)], [BigInt(10), BigInt(64)], [BigInt(11), BigInt(15)]
            , [BigInt(12), BigInt(31)], [BigInt(13), BigInt(0)], [BigInt(14), BigInt(0)], [BigInt(15), BigInt(0)], [BigInt(16), BigInt(0)], [BigInt(17), BigInt(0)]
            , [BigInt(18), BigInt(0)], [BigInt(19), BigInt(0)], [BigInt(20), BigInt(80)], [BigInt(21), BigInt(63)], [BigInt(22), BigInt(31)], [BigInt(23), BigInt(0)]
            , [BigInt(24), BigInt(0)], [BigInt(25), BigInt(0)], [BigInt(26), BigInt(0)], [BigInt(27), BigInt(0)], [BigInt(28), BigInt(0)], [BigInt(29), BigInt(0)]
            , [BigInt(30), BigInt(16)]]));
    });

    test("Simulator - pipe64bits - load", () => {
        let sim = new KernelController("pipe64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.loadProgram(programTest);
        sim.load(loadFile);

        let stageResult = sim.getStageView();
        stage_verification(stageResult,
            [["HALT", "halt", BigInt(0), BigInt(0), "none", "none"], ["BUBBLE", "nop", BigInt(0), BigInt(0), "none", "none"]],
            [["BUBBLE", "nop", false, BigInt(0), BigInt(0), "none", "none"], ["BUBBLE", "nop", false, BigInt(0), BigInt(0), "none", "none"]],
            [["BUBBLE", "nop", "none", "none", BigInt(0), BigInt(0), BigInt(0), "none", "none"], ["AOK", "nop", "none", "none", BigInt(0), BigInt(0), BigInt(0), "none", "none"]],
            [["AOK", "nop", "none", "none", BigInt(0), BigInt(0x22)], ["AOK", "nop", "none", "none", BigInt(0), BigInt(0x23)]],
            [["AOK", BigInt(0x22)], ["AOK", BigInt(0x23)]]);

        register_verification(sim.getRegistersView(), BigInt(42), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        register_verification(sim.getNextRegistersView(), BigInt(42), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));

        getMemoryViewTest(sim.getMemoryView(), 8, BigInt(0), BigInt(0x2000), new Map<bigint, bigint>([[BigInt(24), BigInt(0x2a10000000000000)]]), new Map<bigint, bigint>([
            [BigInt(0), BigInt(48)], [BigInt(1), BigInt(240)], [BigInt(2), BigInt(42)], [BigInt(3), BigInt(0)], [BigInt(4), BigInt(0)], [BigInt(5), BigInt(0)]
            , [BigInt(6), BigInt(0)], [BigInt(7), BigInt(0)], [BigInt(8), BigInt(0)], [BigInt(9), BigInt(0)], [BigInt(10), BigInt(64)], [BigInt(11), BigInt(15)]
            , [BigInt(12), BigInt(31)], [BigInt(13), BigInt(0)], [BigInt(14), BigInt(0)], [BigInt(15), BigInt(0)], [BigInt(16), BigInt(0)], [BigInt(17), BigInt(0)]
            , [BigInt(18), BigInt(0)], [BigInt(19), BigInt(0)], [BigInt(20), BigInt(80)], [BigInt(21), BigInt(63)], [BigInt(22), BigInt(31)], [BigInt(23), BigInt(0)]
            , [BigInt(24), BigInt(0)], [BigInt(25), BigInt(0)], [BigInt(26), BigInt(0)], [BigInt(27), BigInt(0)], [BigInt(28), BigInt(0)], [BigInt(29), BigInt(0)]
            , [BigInt(30), BigInt(16)], [BigInt(31), BigInt(42)]]));
    });

    test("Simulator - pipe64bits - save", () => {
        let sim = new KernelController("pipe64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.loadProgram(programTest);
        sim.continue();

        let saveResult = sim.save();
        expect(saveResult).toBe(saveFile);
    });

    /**
     * Get functions
     */
    test("Simulator - pipe64bits - getStageView", () => {
        let sim = new KernelController("pipe64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.loadProgram(programTest);
        let result: {
            "WriteBack": {
                "columns": string[],
                "lines":
                {
                    "name": string,
                    "labels": string[],
                    "values": (string | bigint)[]
                }[],
            },
            "Memory": {
                "columns": string[],
                "lines":
                {
                    "name": string,
                    "labels": string[],
                    "values": (string | bigint | boolean)[]
                }[],
            },
            "Execute": {
                "columns": string[],
                "lines":
                {
                    "name": string,
                    "labels": string[],
                    "values": (string | bigint)[]
                }[],

            },
            "Decode": {
                "columns": string[],
                "lines":
                {
                    "name": string,
                    "labels": string[],
                    "values": (string | bigint)[]
                }[],

            },
            "Fetch": {
                "columns": string[],
                "lines":
                {
                    "name": string,
                    "labels": string[],
                    "values": (string | bigint)[]
                }[],
            }
        };

        result = sim.getStageView();
        stage_verification(result,
            [["BUBBLE", "nop", BigInt(0), BigInt(0), "none", "none"], ["BUBBLE", "nop", BigInt(0), BigInt(0), "none", "none"]],
            [["BUBBLE", "nop", false, BigInt(0), BigInt(0), "none", "none"], ["BUBBLE", "nop", false, BigInt(0), BigInt(0), "none", "none"]],
            [["BUBBLE", "nop", "none", "none", BigInt(0), BigInt(0), BigInt(0), "none", "none"], ["BUBBLE", "nop", "none", "none", BigInt(0), BigInt(0), BigInt(0), "none", "none"]],
            [["BUBBLE", "nop", "none", "none", BigInt(0), BigInt(0)], ["BUBBLE", "nop", "none", "none", BigInt(0), BigInt(0)]],
            [["BUBBLE", BigInt(0)], ["BUBBLE", BigInt(0)]]);
    });

    test("Simulator - pipe64bits - getRegistersView", () => {
        let sim = new KernelController("pipe64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        let result: {
            "eax": bigint,
            "ebx": bigint,
            "ecx": bigint,
            "edx": bigint,
            "esi": bigint,
            "edi": bigint,
            "ebp": bigint,
            "esp": bigint
        };

        sim.registers.write(registers_enum.eax, BigInt(10));
        sim.registers.write(registers_enum.ebx, BigInt(-10));
        sim.registers.write(registers_enum.ecx, BigInt(0x100));
        sim.registers.write(registers_enum.edx, BigInt(0xffffffac));
        sim.registers.write(registers_enum.edi, BigInt(-512));
        sim.registers.write(registers_enum.esi, BigInt(512));
        sim.registers.write(registers_enum.ebp, BigInt(0xffffffff));
        sim.registers.write(registers_enum.esp, BigInt(0x80000000));

        result = sim.getRegistersView();
        register_verification(result, BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));

        sim.updateSim();
        result = sim.getRegistersView();
        register_verification(result, BigInt(10), toUint64(BigInt(-10)), BigInt(0x100), BigInt(0xffffffac), toUint64(BigInt(-512)), BigInt(512), BigInt(0xffffffff), BigInt(0x80000000));
    });

    test("Simulator - pipe64bits - getNextRegisterView", () => {
        let sim = new KernelController("pipe64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        let result: {
            "eax": bigint,
            "ebx": bigint,
            "ecx": bigint,
            "edx": bigint,
            "esi": bigint,
            "edi": bigint,
            "ebp": bigint,
            "esp": bigint
        };

        sim.registers.write(registers_enum.eax, BigInt(10));
        sim.registers.write(registers_enum.ebx, BigInt(-10));
        sim.registers.write(registers_enum.ecx, BigInt(0x100));
        sim.registers.write(registers_enum.edx, BigInt(0xffffffac));
        sim.registers.write(registers_enum.edi, BigInt(-512));
        sim.registers.write(registers_enum.esi, BigInt(512));
        sim.registers.write(registers_enum.ebp, BigInt(0xffffffff));
        sim.registers.write(registers_enum.esp, BigInt(0x80000000));

        result = sim.getNextRegistersView();
        register_verification(result, BigInt(10), toUint64(BigInt(-10)), BigInt(0x100), BigInt(0xffffffac), toUint64(BigInt(-512)), BigInt(512), BigInt(0xffffffff), BigInt(0x80000000));

        sim.updateSim();
        result = sim.getNextRegistersView();
        register_verification(result, BigInt(10), toUint64(BigInt(-10)), BigInt(0x100), BigInt(0xffffffac), toUint64(BigInt(-512)), BigInt(512), BigInt(0xffffffff), BigInt(0x80000000));
    });

    test("Simulator - pipe64bits - getMemoryView", () => {
        let sim = new KernelController("pipe64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        let result: any

        result = sim.getMemoryView();
        getMemoryViewTest(result, 8, BigInt(0), BigInt(0x2000), new Map<bigint, bigint>, new Map<bigint, bigint>);

        sim.loadProgram(programTest);
        sim.continue();

        result = sim.getMemoryView();
        getMemoryViewTest(sim.getMemoryView(), 8, BigInt(0), BigInt(0x2000), new Map<bigint, bigint>([[BigInt(24), BigInt(0x2a10000000000000)]]), new Map<bigint, bigint>([
            [BigInt(0), BigInt(48)], [BigInt(1), BigInt(240)], [BigInt(2), BigInt(42)], [BigInt(3), BigInt(0)], [BigInt(4), BigInt(0)], [BigInt(5), BigInt(0)]
            , [BigInt(6), BigInt(0)], [BigInt(7), BigInt(0)], [BigInt(8), BigInt(0)], [BigInt(9), BigInt(0)], [BigInt(10), BigInt(64)], [BigInt(11), BigInt(15)]
            , [BigInt(12), BigInt(31)], [BigInt(13), BigInt(0)], [BigInt(14), BigInt(0)], [BigInt(15), BigInt(0)], [BigInt(16), BigInt(0)], [BigInt(17), BigInt(0)]
            , [BigInt(18), BigInt(0)], [BigInt(19), BigInt(0)], [BigInt(20), BigInt(80)], [BigInt(21), BigInt(63)], [BigInt(22), BigInt(31)], [BigInt(23), BigInt(0)]
            , [BigInt(24), BigInt(0)], [BigInt(25), BigInt(0)], [BigInt(26), BigInt(0)], [BigInt(27), BigInt(0)], [BigInt(28), BigInt(0)], [BigInt(29), BigInt(0)]
            , [BigInt(30), BigInt(16)], [BigInt(31), BigInt(42)], [BigInt(32), BigInt(0)], [BigInt(33), BigInt(0)], [BigInt(34), BigInt(0)], [BigInt(35), BigInt(0)]
            , [BigInt(36), BigInt(0)], [BigInt(37), BigInt(0)], [BigInt(38), BigInt(0)]]));
    });

    test("Simulator - pipe64bits - getNextMemoryView", () => {
        let sim = new KernelController("pipe64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.loadProgram(programTest);
        let result: any

        result = sim.getNextMemoryView();
        nextMemoryViewTest(result, 8, BigInt(0), false, [0, 0, 0, 0, 0, 0, 0, 0])

        sim.step();
        result = sim.getNextMemoryView();
        nextMemoryViewTest(result, 8, BigInt(0), false, [0, 0, 0, 0, 0, 0, 0, 0])

        sim.step();
        sim.step();
        sim.step();
        sim.step();
        result = sim.getNextMemoryView();
        nextMemoryViewTest(result, 8, BigInt(0x1f), true, [42, 0, 0, 0, 0, 0, 0, 0])
    });

    test("Simulator - pipe64bits - getFlagsView", () => {
        let sim = new KernelController("pipe64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        let result: {
            "ZF": boolean,
            "OF": boolean,
            "SF": boolean
        };

        sim.alu.computeCC(BigInt(10), BigInt(-20), alufct.A_ADD);

        result = sim.getFlagsView();
        expect(result.ZF).toBeFalsy();
        expect(result.OF).toBeFalsy();
        expect(result.SF).toBeFalsy();

        sim.updateSim();
        result = sim.getFlagsView();

        expect(result.ZF).toBeFalsy();
        expect(result.OF).toBeFalsy();
        expect(result.SF).toBeTruthy();
    });

    test("Simulator - pipe64bits - getNextFlagsView", () => {
        let sim = new KernelController("pipe64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        let result: {
            "ZF": boolean,
            "OF": boolean,
            "SF": boolean
        };

        sim.alu.computeCC(BigInt(10), BigInt(-20), alufct.A_ADD);

        result = sim.getNextFlagsView();
        expect(result.ZF).toBeFalsy();
        expect(result.OF).toBeFalsy();
        expect(result.SF).toBeTruthy();

        sim.updateSim();
        result = sim.getNextFlagsView();

        expect(result.ZF).toBeFalsy();
        expect(result.OF).toBeFalsy();
        expect(result.SF).toBeTruthy();
    });

    test("Simulator - pipe64bits - getStatusView", () => {
        let sim = new KernelController("pipe64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        let result: {
            "STAT": simStatus,
            "ERR": string,
            "PC": bigint
        };

        result = sim.getStatusView();
        expect(result.STAT === simStatus.AOK).toBeTruthy();
        expect(result.ERR === "").toBeTruthy();
        expect(result.PC === BigInt(0)).toBeTruthy();
    });

    test("Simulator - pipe64bits - getPerformancesView", () => {
        let sim = new KernelController("pipe64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.loadProgram(programTest);
        let result: { [key : string] : number } | undefined;
        
        result = sim.getPerformancesView();
        if (result === undefined){
            expect(sim.getPerformancesView() !== undefined).toBeTruthy();
            return;
        }
        expect(result.Cycles === 0).toBeTruthy();
        expect(result.Instructions === 0).toBeTruthy();
        expect(result.CPI === 1).toBeTruthy();

        sim.continue();
        result = sim.getPerformancesView();
        if (result === undefined){
            expect(sim.getPerformancesView() !== undefined).toBeTruthy();
            return;
        }
        expect(result.Cycles === 4).toBeTruthy();
        expect(result.Instructions === 4).toBeTruthy();
        expect(result.CPI === 1).toBeTruthy();
    });

    test("Simulator - pipe64bits - getHighlightRules", () => {
        let sim = new KernelController("pipe64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.loadProgram(programTest);
        let result:
            {
                "address": bigint,
                "tag": string
            }[];
        let tags = ["F", "D", "E", "M", "W"];

        result = sim.getHighlightRules();
        result.forEach((value, key) => {
            expect(value.address === BigInt(-1)).toBeTruthy();
            expect(value.tag === tags[key]).toBeTruthy();
        })

        sim.step();
        let expectedResult = [BigInt(0), BigInt(-1), BigInt(-1), BigInt(-1), BigInt(-1)]

        result = sim.getHighlightRules();
        result.forEach((value, key) => {
            expect(value.address === expectedResult[key]).toBeTruthy();
            expect(value.tag === tags[key]).toBeTruthy();
        })
    });

    test("Simulator - pipe64bits - getStepNum", () => {
        let sim = new KernelController("pipe64bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.loadProgram(programTest);
        let result: number;

        result = sim.getStepNum();
        expect(result === 0).toBeTruthy();

        sim.step();
        result = sim.getStepNum();
        expect(result === 1).toBeTruthy();

        sim.continue();
        result = sim.getStepNum();
        expect(result === 8).toBeTruthy();
    });

    test("Simulator - pipe64bits - isInterfaceTightened", () => {
        let sim = new KernelController("pipe64bits", defaultInstructions, defaultHcl).getSim() as Sim;

        expect(sim.isInterfaceTightened()).toBeFalsy();
    });
});