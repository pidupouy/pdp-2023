import { describe, expect, test } from '@jest/globals';
import { loadDefaultFile, registerVerification32Bits, flagsVerification, memoryWordVerification  } from "../../testUtils";
import { Sim } from "../../../model/pipe/sim";
import { KernelController } from "../../../controllers/kernelController"

let defaultInstructions = JSON.parse(loadDefaultFile("", "instructionSet.json"))
let defaultHcl = loadDefaultFile("pipe", "hcl32bits.txt")

let prog1: string = `
                       |  |  #prog1: Pad with 3 nop's
0x0000: 30f20a000000   |  |      irmovl 10,%edx
0x0006: 30f003000000   |  |      irmovl 3,%eax
0x000c: 00             |  |      nop
0x000d: 00             |  |      nop
0x000e: 00             |  |      nop
0x000f: 6020           |  |      addl %edx,%eax
0x0011: 10             |  |      halt
`

let prog2: string = `
                       |  |  # prog2: Pad with 2 nop's
0x0000: 30f20a000000   |  |      irmovl 10,%edx
0x0006: 30f003000000   |  |      irmovl 3,%eax
0x000c: 00             |  |      nop
0x000d: 00             |  |      nop
0x000e: 6020           |  |      addl %edx,%eax
0x0010: 10             |  |      halt
`

let prog3: string = `
                       |  |  # prog3: Pad with 1 nop
0x0000: 30f20a000000   |  |      irmovl 10,%edx
0x0006: 30f003000000   |  |      irmovl 3,%eax
0x000c: 00             |  |      nop
0x000d: 6020           |  |      addl %edx,%eax
0x000f: 10             |  |      halt
`

let prog4: string = `
                       |  |  # prog4: No padding
0x0000: 30f20a000000   |  |      irmovl 10,%edx
0x0006: 30f003000000   |  |      irmovl 3,%eax
0x000c: 6020           |  |      addl %edx,%eax
0x000e: 10             |  |      halt
`

let prog5: string = `
                       |  |  # prog5: Load/use hazard
0x0000: 30f280000000   |  |      irmovl 128,%edx
0x0006: 30f103000000   |  |      irmovl 3,%ecx
0x000c: 401200000000   |  |      rmmovl %ecx,0(%edx)
0x0012: 30f30a000000   |  |      irmovl 10,%ebx
0x0018: 500200000000   |  |      mrmovl 0(%edx),%eax # Load %eax
0x001e: 6030           |  |      addl %ebx,%eax # Use %eax
0x0020: 10             |  |      halt
`

let prog6: string = `
                       |  |  # prog6: Forwarding Priority
0x0000: 30f20a000000   |  |      irmovl 10,%edx
0x0006: 30f203000000   |  |      irmovl 3,%edx
0x000c: 2020           |  |      rrmovl %edx,%eax
0x000e: 10             |  |      halt
`

let prog7: string = `
                       |  |  # prog7: Demonstration of return
0x0000: 30f430000000   |  |      irmovl Stack,%esp # Intialize stack pointer
0x0006: 8020000000     |  |      call proc # Procedure call
0x000b: 30f20a000000   |  |      irmovl 10,%edx # Return point
0x0011: 10             |  |      halt  
0x0012:                |  |  .pos 0x20 
0x0020:                |  |  proc:  
0x0020: 90             |  |      ret  # proc:
0x0021: 2023           |  |      rrmovl %edx,%ebx # Not executed
0x0023:                |  |  .pos 0x30 
0x0030:                |  |  Stack:  # Stack: Stack pointer
`

let prog8: string = `
                       |  |  # prog8:Branch Cancellation
0x0000: 6300           |  |      xorl %eax,%eax 
0x0002: 740e000000     |  |      jne target # Not taken
0x0007: 30f001000000   |  |      irmovl 1,%eax # Fall through
0x000d: 10             |  |      halt  
0x000e:                |  |  target:  
0x000e: 30f202000000   |  |      irmovl 2,%edx # Target
0x0014: 30f303000000   |  |      irmovl 3,%ebx # Target+1
0x001a: 10             |  |      halt  
`

let prog9 = `
                       |  |  # Execution begins at address 0
0x0000:                |  |  .pos 0
0x0000: 30f400010000   |  |  init:      irmovl Stack,%esp # Set up Stack pointer
0x0006: 701c000000     |  |      jmp Main # Execute main program
                       |  |
                       |  |  # Array of 4 elements
0x000b:                |  |  .align 4
0x000c: 0d000000       |  |  array:  .long 0xd
0x0010: c0000000       |  |  .long 0xc0
0x0014: 000b0000       |  |  .long 0xb00
0x0018: 00a00000       |  |  .long 0xa000
                       |  |
0x001c: 30f004000000   |  |  Main:      irmovl 4,%eax
0x0022: a00f           |  |      pushl %eax # Push 4
0x0024: 30f20c000000   |  |      irmovl array,%edx
0x002a: a02f           |  |      pushl %edx # Push array
0x002c: 8032000000     |  |      call Sum # Sum(array, 4)
0x0031: 10             |  |      halt
                       |  |
                       |  |  # int Sum(int *Start, int Count)
0x0032: 501404000000   |  |  Sum:      mrmovl 4(%esp),%ecx # ecx = Start
0x0038: 502408000000   |  |      mrmovl 8(%esp),%edx # edx = Count
0x003e: 30f000000000   |  |      irmovl 0,%eax # sum = 0
0x0044: 6222           |  |      andl %edx,%edx
0x0046: 7368000000     |  |      je End
                       |  |
0x004b: 506100000000   |  |  Loop:      mrmovl (%ecx),%esi # get *Start
0x0051: 6060           |  |      addl %esi,%eax # add to sum
0x0053: 30f304000000   |  |      irmovl 4,%ebx
0x0059: 6031           |  |      addl %ebx,%ecx # Start
0x005b: 30f3ffffffff   |  |      irmovl -1,%ebx
0x0061: 6032           |  |      addl %ebx,%edx # Count--
0x0063: 744b000000     |  |      jne Loop # Stop when 0
0x0068: 90             |  |  End:      ret
0x0069:                |  |  .pos 0x100
0x0100:                |  |  Stack:  # The stack goes here
`

let prog10: string = `
                       |  |  # Execution begins at address 0
0x0000:                |  |  .pos 0
0x0000: 30f400040000   |  |  init:      irmovl Stack,%esp # Set up Stack pointer
0x0006: 30f500040000   |  |      irmovl Stack,%ebp # Set up base pointer
0x000c: 7024000000     |  |      jmp Main # Execute main program
                       |  |  
                       |  |  # Array of 4 elements
0x0011:                |  |  .align 4 
0x0014: 0d000000       |  |  array:  .long 0xd 
0x0018: c0000000       |  |  .long 0xc0 
0x001c: 000b0000       |  |  .long 0xb00 
0x0020: 00a00000       |  |  .long 0xa000 
                       |  |  
0x0024: 30f004000000   |  |  Main:      irmovl 4,%eax 
0x002a: a00f           |  |      pushl %eax # Push 4
0x002c: 30f214000000   |  |      irmovl array,%edx 
0x0032: a02f           |  |      pushl %edx # Push array
0x0034: 803a000000     |  |      call rSum # Sum(array, 4)
0x0039: 10             |  |      halt  
                       |  |  
                       |  |  # int Sum(int *Start, int Count)
0x003a: a05f           |  |  rSum:      pushl %ebp 
0x003c: 2045           |  |      rrmovl %esp,%ebp 
0x003e: a03f           |  |      pushl %ebx # Save value of %ebx
0x0040: 503508000000   |  |      mrmovl 8(%ebp),%ebx # Get Start
0x0046: 50050c000000   |  |      mrmovl 12(%ebp),%eax # Get Count
0x004c: 6200           |  |      andl %eax,%eax # Test value of Count
0x004e: 717b000000     |  |      jle L38 # If <= 0, goto zreturn
0x0053: 30f2ffffffff   |  |      irmovl -1,%edx 
0x0059: 6020           |  |      addl %edx,%eax # Count--
0x005b: a00f           |  |      pushl %eax # Push Count
0x005d: 30f204000000   |  |      irmovl 4,%edx 
0x0063: 2030           |  |      rrmovl %ebx,%eax 
0x0065: 6020           |  |      addl %edx,%eax 
0x0067: a00f           |  |      pushl %eax # Push Start+1
0x0069: 803a000000     |  |      call rSum # Sum(Start+1, Count-1)
0x006e: 502300000000   |  |      mrmovl (%ebx),%edx 
0x0074: 6020           |  |      addl %edx,%eax # Add *Start
0x0076: 707d000000     |  |      jmp L39 # goto done
0x007b: 6300           |  |  L38:      xorl %eax,%eax # zreturn:
0x007d: 5035fcffffff   |  |  L39:      mrmovl -4(%ebp),%ebx # done: Restore %ebx
0x0083: 2054           |  |      rrmovl %ebp,%esp # Deallocate stack frame
0x0085: b05f           |  |      popl %ebp # Restore %ebp
0x0087: 90             |  |      ret
0x0088:                |  |  .pos 0x400
0x0400:                |  |  Stack:  # The stack goes here
`

let prog11: string = `
                       |  |  # Code to generate a combination of not-taken branch and ret
0x0000: 30f440000000   |  |      irmovl Stack,%esp 
0x0006: 30f024000000   |  |      irmovl rtnp,%eax 
0x000c: a00f           |  |      pushl %eax # Set up return pointer
0x000e: 6300           |  |      xorl %eax,%eax # Set Z condition code
0x0010: 741c000000     |  |      jne target # Not taken (First part of combination)
0x0015: 30f001000000   |  |      irmovl 1,%eax # Should execute this
0x001b: 10             |  |      halt
0x001c: 90             |  |  target:      ret  # Second part of combination
0x001d: 30f302000000   |  |      irmovl 2,%ebx # Should not execute this
0x0023: 10             |  |      halt
0x0024: 30f203000000   |  |  rtnp:      irmovl 3,%edx # Should not execute this
0x002a: 10             |  |      halt
0x002b:                |  |  .pos 0x40
0x0040:                |  |  Stack:
`

let prog12: string = `
0x0000: 30f601000000   |  |      irmovl 1,%esi
0x0006: 30f702000000   |  |      irmovl 2,%edi
0x000c: 30f504000000   |  |      irmovl 4,%ebp
0x0012: 30f0e0ffffff   |  |      irmovl -32,%eax
0x0018: 30f240000000   |  |      irmovl 64,%edx
0x001e: 6120           |  |      subl %edx,%eax
0x0020: 7327000000     |  |      je target
0x0025: 00             |  |      nop
0x0026: 10             |  |      halt
0x0027:                |  |  target:
0x0027: 6062           |  |      addl %esi,%edx
0x0029: 00             |  |      nop
0x002a: 00             |  |      nop
0x002b: 00             |  |      nop
0x002c: 10             |  |      halt
`
let prog13: string = `
                       |  |  # Test of Pop semantics for Y86
0x0000: 30f400010000   |  |      irmovl 0x100,%esp # Initialize stack pointer
0x0006: 30f0cdab0000   |  |      irmovl 0xABCD,%eax
0x000c: a00f           |  |      pushl %eax # Put known value on stack
0x000e: b04f           |  |      popl %esp # Either get 0xABCD, or 0xfc
0x0010: 10             |  |      halt
`

let prog14: string = `
                       |  |  # Assembly Code to test semantics of pushl
0x0000: 30f400010000   |  |      irmovl 0x100,%esp
0x0006: a04f           |  |      pushl %esp # Ambiguous
0x0008: b00f           |  |      popl %eax
0x000a: 10             |  |      halt
`

let prog15: string = `
                       |  |  # Test of Push semantics for Y86
0x0000: 30f400010000   |  |      irmovl 0x100,%esp # Initialize stack pointer
0x0006: 2040           |  |      rrmovl %esp,%eax # Save stack pointer
0x0008: a04f           |  |      pushl %esp # Push the stack pointer (old or new?)
0x000a: b02f           |  |      popl %edx # Get it back
0x000c: 6120           |  |      subl %edx,%eax # Compute difference.  Either 0 (old) or 4 (new).
0x000e: 10             |  |      halt
`

let prog16: string = `
                       |  | # Test instruction that modifies %esp followed by ret
0x000: 308340000000    |  | 	irmovl mem,%ebx
0x006: 504300000000    |  | 	mrmovl  0(%ebx),%esp # Sets %esp to point to return point
0x00c: 90              |  | 	ret		     # Returns to return point 
0x00d: 10              |  | 	halt                 # 
0x00e: 308605000000    |  | rtnpt:  irmovl 5,%esi       # Return point
0x014: 10              |  | 	halt
0x040:                 |  | .pos 0x40
0x040: 50000000        |  | mem:	.long stack	     # Holds desired stack pointer
0x050:                 |  | .pos 0x50
0x050: 0e000000        |  | stack:	.long rtnpt          # Top of stack: Holds return point
`

describe("Programs - pipe32bits - tests", () => {
    test("Programs - pipe32bits - prog1", () => {
        let sim = new KernelController("pipe32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog1);

        sim.continue();

        registerVerification32Bits(sim.registers, BigInt(0xd), BigInt(0), BigInt(0), BigInt(0xa), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        expect(sim.context.getCurrentFetch().pc === BigInt(0x15)).toBeTruthy();
    });

    test("Programs - pipe32bits - prog2", () => {
        let sim = new KernelController("pipe32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog2);

        sim.continue();

        registerVerification32Bits(sim.registers, BigInt(0xd), BigInt(0), BigInt(0), BigInt(0xa), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        expect(sim.context.getCurrentFetch().pc === BigInt(0x14)).toBeTruthy();
    });

    test("Programs - pipe32bits - prog3", () => {
        let sim = new KernelController("pipe32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog3);

        sim.continue();

        registerVerification32Bits(sim.registers, BigInt(0xd), BigInt(0), BigInt(0), BigInt(0xa), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        expect(sim.context.getCurrentFetch().pc === BigInt(0x13)).toBeTruthy();
    });

    test("Programs - pipe32bits - prog4", () => {
        let sim = new KernelController("pipe32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog4);

        sim.continue();

        registerVerification32Bits(sim.registers, BigInt(0xd), BigInt(0), BigInt(0), BigInt(0xa), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        expect(sim.context.getCurrentFetch().pc === BigInt(0x12)).toBeTruthy();
    });

    test("Programs - pipe32bits - prog5", () => {
        let sim = new KernelController("pipe32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog5);

        sim.continue();

        registerVerification32Bits(sim.registers, BigInt(0xd), BigInt(0xa), BigInt(0x3), BigInt(0x80), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        memoryWordVerification(sim.memory, BigInt(0x80), BigInt(0x3));
        expect(sim.context.getCurrentFetch().pc === BigInt(0x24)).toBeTruthy();
    });

    test("Programs - pipe32bits - prog6", () => {
        let sim = new KernelController("pipe32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog6);

        sim.continue();

        registerVerification32Bits(sim.registers, BigInt(0x3), BigInt(0), BigInt(0), BigInt(0x3), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, false, false, false);
        expect(sim.context.getCurrentFetch().pc === BigInt(0x12)).toBeTruthy();
    });

    test("Programs - pipe32bits - prog7", () => {
        let sim = new KernelController("pipe32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog7);

        sim.continue();

        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0xa), BigInt(0), BigInt(0), BigInt(0), BigInt(0x30));
        flagsVerification(sim.alu, false, false, false);
        memoryWordVerification(sim.memory, BigInt(0x2c), BigInt(0xb));
        expect(sim.context.getCurrentFetch().pc === BigInt(0x15)).toBeTruthy();
    });

    test("Programs - pipe32bits - prog8", () => {
        let sim = new KernelController("pipe32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog8);

        sim.continue();

        registerVerification32Bits(sim.registers, BigInt(0x1), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0));
        flagsVerification(sim.alu, true, false, false);
        expect(sim.context.getCurrentFetch().pc === BigInt(0x1b)).toBeTruthy();
    });

    test("Programs - pipe32bits - prog9", () => {
        let sim = new KernelController("pipe32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog9);

        sim.continue();

        registerVerification32Bits(sim.registers, BigInt(0xabcd), BigInt(0xffffffff), BigInt(0x1c), BigInt(0), BigInt(0xa000), BigInt(0), BigInt(0), BigInt(0xf8));
        flagsVerification(sim.alu, true, false, false);
        memoryWordVerification(sim.memory, BigInt(0xf4), BigInt(0x31));
        memoryWordVerification(sim.memory, BigInt(0xf8), BigInt(0xc));
        memoryWordVerification(sim.memory, BigInt(0xfc), BigInt(0x4));
        expect(sim.context.getCurrentFetch().pc === BigInt(0x44)).toBeTruthy();
    });

    test("Programs - pipe32bits - prog10", () => {
        let sim = new KernelController("pipe32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog10);

        sim.continue();

        registerVerification32Bits(sim.registers, BigInt(0xabcd), BigInt(0), BigInt(0), BigInt(0xd), BigInt(0), BigInt(0), BigInt(0x400), BigInt(0x3f8));
        flagsVerification(sim.alu, false, false, false);

        memoryWordVerification(sim.memory, BigInt(0x39c), BigInt(0x20));
        memoryWordVerification(sim.memory, BigInt(0x3a0), BigInt(0x3b4));
        memoryWordVerification(sim.memory, BigInt(0x3a4), BigInt(0x6e));
        memoryWordVerification(sim.memory, BigInt(0x3a8), BigInt(0x24));
        memoryWordVerification(sim.memory, BigInt(0x3ac), BigInt(0));

        memoryWordVerification(sim.memory, BigInt(0x3b0), BigInt(0x1c));
        memoryWordVerification(sim.memory, BigInt(0x3b4), BigInt(0x3c8));
        memoryWordVerification(sim.memory, BigInt(0x3b8), BigInt(0x6e));
        memoryWordVerification(sim.memory, BigInt(0x3bc), BigInt(0x20));
        memoryWordVerification(sim.memory, BigInt(0x3c0), BigInt(0x1));

        memoryWordVerification(sim.memory, BigInt(0x3c4), BigInt(0x18));
        memoryWordVerification(sim.memory, BigInt(0x3c8), BigInt(0x3dc));
        memoryWordVerification(sim.memory, BigInt(0x3cc), BigInt(0x6e));
        memoryWordVerification(sim.memory, BigInt(0x3d0), BigInt(0x1c));
        memoryWordVerification(sim.memory, BigInt(0x3d4), BigInt(0x2));

        memoryWordVerification(sim.memory, BigInt(0x3d8), BigInt(0x14));
        memoryWordVerification(sim.memory, BigInt(0x3dc), BigInt(0x3f0));
        memoryWordVerification(sim.memory, BigInt(0x3e0), BigInt(0x6e));
        memoryWordVerification(sim.memory, BigInt(0x3e4), BigInt(0x18));
        memoryWordVerification(sim.memory, BigInt(0x3e8), BigInt(0x3));

        memoryWordVerification(sim.memory, BigInt(0x3ec), BigInt(0x0));
        memoryWordVerification(sim.memory, BigInt(0x3f0), BigInt(0x400));
        memoryWordVerification(sim.memory, BigInt(0x3f4), BigInt(0x39));
        memoryWordVerification(sim.memory, BigInt(0x3f8), BigInt(0x14));
        memoryWordVerification(sim.memory, BigInt(0x3fc), BigInt(0x4));
        expect(sim.context.getCurrentFetch().pc === BigInt(0x40)).toBeTruthy();
    });

    test("Programs - pipe32bits - prog11", () => {
        let sim = new KernelController("pipe32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog11);

        sim.continue();

        registerVerification32Bits(sim.registers, BigInt(0x1), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0x3c));
        flagsVerification(sim.alu, true, false, false);
        memoryWordVerification(sim.memory, BigInt(0x3c), BigInt(0x24));
        expect(sim.context.getCurrentFetch().pc === BigInt(0x23)).toBeTruthy();
    });

    test("Programs - pipe32bits - prog12", () => {
        let sim = new KernelController("pipe32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog12);

        sim.continue();

        registerVerification32Bits(sim.registers, BigInt(0xffffffa0), BigInt(0), BigInt(0), BigInt(0x40), BigInt(0x1), BigInt(0x2), BigInt(0x4), BigInt(0));
        flagsVerification(sim.alu, false, false, true);
        expect(sim.context.getCurrentFetch().pc === BigInt(0x2b)).toBeTruthy();
    });

    test("Programs - pipe32bits - prog13", () => {
        let sim = new KernelController("pipe32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog13);

        sim.continue();

        registerVerification32Bits(sim.registers, BigInt(0xabcd), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0xabcd));
        flagsVerification(sim.alu, false, false, false);
        memoryWordVerification(sim.memory, BigInt(0xfc), BigInt(0xabcd));
        expect(sim.context.getCurrentFetch().pc === BigInt(0x14)).toBeTruthy();
    });

    test("Programs - pipe32bits - prog14", () => {
        let sim = new KernelController("pipe32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog14);

        sim.continue();

        registerVerification32Bits(sim.registers, BigInt(0x100), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0), BigInt(0x100));
        flagsVerification(sim.alu, false, false, false);
        memoryWordVerification(sim.memory, BigInt(0xfc), BigInt(0x100));
        expect(sim.context.getCurrentFetch().pc === BigInt(0xe)).toBeTruthy();
    });

    test("Programs - pipe32bits - prog15", () => {
        let sim = new KernelController("pipe32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog15);

        sim.continue();

        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0), BigInt(0), BigInt(0x100), BigInt(0), BigInt(0), BigInt(0), BigInt(0x100));
        flagsVerification(sim.alu, true, false, false);
        memoryWordVerification(sim.memory, BigInt(0xfc), BigInt(0x100));
        expect(sim.context.getCurrentFetch().pc === BigInt(0x12)).toBeTruthy();
    });

    test("Programs - pipe32bits - prog16", () => {
        let sim = new KernelController("pipe32bits", defaultInstructions, defaultHcl).getSim() as Sim;
        sim.memory.loadProgram(prog16);

        sim.continue();

        registerVerification32Bits(sim.registers, BigInt(0), BigInt(0x40), BigInt(0), BigInt(0), BigInt(0x5), BigInt(0), BigInt(0), BigInt(0x54));
        flagsVerification(sim.alu, false, false, false);
        expect(sim.context.getCurrentFetch().pc === BigInt(0x18)).toBeTruthy();
    });
});