import fs from "fs"
import { expect } from '@jest/globals';
import { IRegisters, registers_enum } from "../model/interfaces/IRegisters";
import { IAlu, alufct } from "../model/interfaces/IAlu";
import { IMemory } from "../model/interfaces/IMemory";
import { ISimulator } from "../model/interfaces/ISimulator";

const DEFAULT_FILES_PATH = './ts/model/'

export function loadDefaultFile(kernelName : string, fileName : string) {
    let path = "";

    switch(kernelName){
        case "":
            path = DEFAULT_FILES_PATH + fileName;
            break;
        case "seq" :
        case "pipe":
            path = DEFAULT_FILES_PATH + kernelName + '/' + fileName;
            break;
        default :
            throw new Error('Failed to read default file "' + fileName + '". Reason : Invalid path "' + DEFAULT_FILES_PATH + kernelName + '"')
    }
    
    try {
        return fs.readFileSync(path, 'utf8')
    } catch (err) {
        throw new Error('Failed to read default file "' + fileName + '". Reason : ' + err.message)
    }
}

export function registerVerification32Bits(registers: IRegisters, eax: bigint, ebx: bigint, ecx: bigint, edx: bigint, esi: bigint, edi: bigint, ebp: bigint, esp: bigint): void {
    expect(registers.read(registers_enum.eax) === eax).toBeTruthy();
    expect(registers.read(registers_enum.ebx) === ebx).toBeTruthy();
    expect(registers.read(registers_enum.ecx) === ecx).toBeTruthy()
    expect(registers.read(registers_enum.edx) === edx).toBeTruthy()
    expect(registers.read(registers_enum.esi) === esi).toBeTruthy()
    expect(registers.read(registers_enum.edi) === edi).toBeTruthy()
    expect(registers.read(registers_enum.ebp) === ebp).toBeTruthy()
    expect(registers.read(registers_enum.esp) === esp).toBeTruthy()
}

export function registerNextVerification32Bits(registers: IRegisters, eax: bigint, ebx: bigint, ecx: bigint, edx: bigint, esi: bigint, edi: bigint, ebp: bigint, esp: bigint): void {
    expect(registers.readNext(registers_enum.eax) === eax).toBeTruthy();
    expect(registers.readNext(registers_enum.ebx) === ebx).toBeTruthy();
    expect(registers.readNext(registers_enum.ecx) === ecx).toBeTruthy()
    expect(registers.readNext(registers_enum.edx) === edx).toBeTruthy()
    expect(registers.readNext(registers_enum.esi) === esi).toBeTruthy()
    expect(registers.readNext(registers_enum.edi) === edi).toBeTruthy()
    expect(registers.readNext(registers_enum.ebp) === ebp).toBeTruthy()
    expect(registers.readNext(registers_enum.esp) === esp).toBeTruthy()
}

export function registerVerification64Bits(registers: IRegisters, eax: bigint, ebx: bigint, ecx: bigint, edx: bigint, esi: bigint, edi: bigint, ebp: bigint, esp: bigint, r8: bigint, r9: bigint, r10: bigint, r11: bigint, r12: bigint, r13: bigint, r14: bigint): void {
    expect(registers.read(registers_enum.eax) === eax).toBeTruthy();
    expect(registers.read(registers_enum.ebx) === ebx).toBeTruthy();
    expect(registers.read(registers_enum.ecx) === ecx).toBeTruthy()
    expect(registers.read(registers_enum.edx) === edx).toBeTruthy()
    expect(registers.read(registers_enum.esi) === esi).toBeTruthy()
    expect(registers.read(registers_enum.edi) === edi).toBeTruthy()
    expect(registers.read(registers_enum.ebp) === ebp).toBeTruthy()
    expect(registers.read(registers_enum.esp) === esp).toBeTruthy()
    expect(registers.read(registers_enum.r8) === r8).toBeTruthy();
    expect(registers.read(registers_enum.r9) === r9).toBeTruthy();
    expect(registers.read(registers_enum.r10) === r10).toBeTruthy()
    expect(registers.read(registers_enum.r11) === r11).toBeTruthy()
    expect(registers.read(registers_enum.r12) === r12).toBeTruthy()
    expect(registers.read(registers_enum.r13) === r13).toBeTruthy()
    expect(registers.read(registers_enum.r14) === r14).toBeTruthy()
}

export function registerNextVerification64Bits(registers: IRegisters, eax: bigint, ebx: bigint, ecx: bigint, edx: bigint, esi: bigint, edi: bigint, ebp: bigint, esp: bigint, r8: bigint, r9: bigint, r10: bigint, r11: bigint, r12: bigint, r13: bigint, r14: bigint): void {
    expect(registers.readNext(registers_enum.eax) === eax).toBeTruthy();
    expect(registers.readNext(registers_enum.ebx) === ebx).toBeTruthy();
    expect(registers.readNext(registers_enum.ecx) === ecx).toBeTruthy()
    expect(registers.readNext(registers_enum.edx) === edx).toBeTruthy()
    expect(registers.readNext(registers_enum.esi) === esi).toBeTruthy()
    expect(registers.readNext(registers_enum.edi) === edi).toBeTruthy()
    expect(registers.readNext(registers_enum.ebp) === ebp).toBeTruthy()
    expect(registers.readNext(registers_enum.esp) === esp).toBeTruthy()
    expect(registers.readNext(registers_enum.r8) === r8).toBeTruthy();
    expect(registers.readNext(registers_enum.r9) === r9).toBeTruthy();
    expect(registers.readNext(registers_enum.r10) === r10).toBeTruthy()
    expect(registers.readNext(registers_enum.r11) === r11).toBeTruthy()
    expect(registers.readNext(registers_enum.r12) === r12).toBeTruthy()
    expect(registers.readNext(registers_enum.r13) === r13).toBeTruthy()
    expect(registers.readNext(registers_enum.r14) === r14).toBeTruthy()
}

/**
 * Macro for computeAlu tests.
 * @param alu
 * @param val1
 * @param val2
 * @param alufct
 * @param expected_result
 */
export function computeAluTest(alu: IAlu, val1: bigint, val2: bigint, alufct: alufct, expected_result: bigint) : void {
    let value = alu.computeAlu(val1, val2, alufct);
    expect(value === expected_result).toBeTruthy();
}

export function flagsVerification(alu: IAlu, expected_ZF: boolean, expected_OF: boolean, expected_SF: boolean) {
    expect(alu.getZF()).toBe(expected_ZF);
    expect(alu.getOF()).toBe(expected_OF);
    expect(alu.getSF()).toBe(expected_SF);
}

export function flagsNextVerification(alu: IAlu, expected_ZF: boolean, expected_OF: boolean, expected_SF: boolean) {
    expect(alu.getNextZF()).toBe(expected_ZF);
    expect(alu.getNextOF()).toBe(expected_OF);
    expect(alu.getNextSF()).toBe(expected_SF);
}

/**
 * Macro for checking the flags after val1 and val2 operation alufct.
 * It currently check ZF, SF and OF.
 * @param val1
 * @param val2
 * @param alufct
 * @param expected_ZF
 * @param expected_SF
 * @param expected_OF
 */
export function computeCCTest(alu: IAlu, val1: bigint, val2: bigint, alufct: alufct, expected_ZF: boolean, expected_SF: boolean, expected_OF: boolean) : void {
    alu.computeCC(val1, val2, alufct);
    alu.updateFlags();

    flagsVerification(alu, expected_ZF, expected_OF, expected_SF);
}

/**
 * Macro for checking the flags are not immediately update after val1 and val2 operation alufct.
 * It should update at the next cycle, after the update_flags function.
 * It currently check ZF, SF and OF.
 * @param val1
 * @param val2
 * @param alufct
 * @param expected_ZF
 * @param expected_SF
 * @param expected_OF
 */
export function updateFlagsTest(alu: IAlu, val1: bigint, val2: bigint, fct: alufct, expected_ZF: boolean, expected_SF: boolean, expected_OF: boolean) : void {
    // reset flags to false
    alu.computeCC(BigInt(0), BigInt(10), alufct.A_ADD);
    alu.updateFlags();

    alu.computeCC(val1, val2, fct);
    flagsVerification(alu, false, false, false);
    flagsNextVerification(alu, expected_ZF, expected_OF, expected_SF);

    alu.updateFlags();
    flagsVerification(alu, expected_ZF, expected_OF, expected_SF);
    flagsNextVerification(alu, expected_ZF, expected_OF, expected_SF);
}

export function memoryWordVerification(memory: IMemory, address: bigint, word: bigint): void {
    expect(memory.readWord(address) === word).toBeTruthy();
}

export function wordTest(memory: IMemory, address: bigint, word: bigint) {
    memory.writeWord(address, word);
    memory.updateMemory();
    expect(memory.readWord(address) === word).toBeTruthy();
}

export function byteTest(memory: IMemory, address: bigint, byte: number) {
    memory.writeByte(address, byte);
    expect(memory.readByte(address)).toBe(byte);
}

export function nextMemoryViewTest(memory: IMemory, wordSize: number, address: bigint, isWrite: boolean, bytes: number[]) {
    let result = memory.getNextMemoryView();
    expect(result.wordSize === wordSize).toBeTruthy();
    expect(result.address === address).toBeTruthy();
    expect(result.isWrite === isWrite).toBeTruthy();
    for (let i = 0; i < wordSize; i++) {
        expect(result.bytes[i] === bytes[i]).toBeTruthy();
    }
    expect(result.bytes.length === wordSize).toBeTruthy();
}

export function getMemoryViewTest(memory: IMemory, wordSize: number, startAddress: bigint, maxAddress: bigint, modifiedMem: Map<bigint, bigint>, bytes: Map<bigint, bigint>) {
    let result = memory.getMemoryView();
    expect(result.wordSize === wordSize).toBeTruthy();
    expect(BigInt(result.startAddress) === startAddress).toBeTruthy();
    expect(BigInt(result.maxAddress) === maxAddress).toBeTruthy();
    expect(modifiedMem.size === result.modifiedMem.size).toBeTruthy();
    expect(bytes.size === result.bytes.size).toBeTruthy();
    modifiedMem.forEach((value, key) => {
        expect(result.modifiedMem.get(key) == modifiedMem.get(key)).toBeTruthy();
    });
    bytes.forEach((value, key) => {
        expect(result.bytes.get(key) == bytes.get(key)).toBeTruthy();
    });
}