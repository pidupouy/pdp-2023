import { KernelController } from '../../controllers/kernelController'
import { describe, expect, test } from '@jest/globals';

describe("KernelController - tests", () => {
    test("KernelController - default kernel", () => {
        let controller = new KernelController()

        expect(() => {
            controller.useKernel("test", [], "")
        }).toThrow()
    })
})