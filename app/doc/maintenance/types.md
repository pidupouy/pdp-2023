# Types usage

## Number

The default type in javascript to handle numbers.
Even if a Number is on 64 bits, it represent integer only up to 53 bits because the remaining bits are represented like float.
So it's unsafe to use Number to store values that we wan to represent as a 64 bits integer.

In the simulator, we use Number to store value that aren't meant to represent 32bits or 64bits values, like the opcode/ifun (0 to 15), the number of steps, cycles or instructions done.

## BigInt

A type that can represent numbers with a virtually infinite number of bits.

BigInt and Number can't work together, so BigInt + Number is impossible.  
A BigInt number will never overflow.  
Math library doesn't work with BigInt either.  
Json stringify can't handle BigInt on it's own, you must use the following trick :  
`JSON.stringify(content, (key, value) => typeof value === 'bigint' || typeof value === 'number' ? value.toString() : value // return everything else unchanged)`

To write a BigInt value you must put `n` at the end of the number, like `315498722187n` or use the constructor `BigInt('315498722187')` with a string as a parameter.
It is important to note that `BigInt(315498722187)` is wrong, because the number you gave in parameter is a Number not a BigInt, so if the number is bigger than 53bits, you will obtain a unexpected result. You can try `BigInt(0xfffffffffffffffe) === BigInt(0xffffffffffffffff)` and `BigInt(0xfffffffffffffffen) === BigInt(0xffffffffffffffffn)` on a javascript console.

## Uint32Array et BigUint64Array

To ensure that the alu compute and the registers store only 32 bits or 64 bits numbers, we use the types `Uint32Array` and `BigUint64Array`.

We use them in the registers and alu classes.