# Potentials Improvements

## Ability to set breakpoints in the editors

The module used for the `Editor` component is [brace](https://github.com/thlorenz/brace) which is a browserify compatible version of [ace](https://github.com/ajaxorg/ace/).
It has support for **breakpoints**, so adding this feature should not be too troublesome, the simulator's `continue()` function also allows breakpoints to be passed.
Here is an [article](https://ourcodeworld.com/articles/read/1052/how-to-add-toggle-breakpoints-on-the-ace-editor-gutter) explaining the steps to follow.

The ability to use breakpoints must also be added to the accessibility mode and the request through CLI.

## Expanding tests suites

The testing of the model files are mostly done but we need to redo and expand the testing of the compilers HLC and Yas.
There is also a lack of testing for the controllers, the vue and the cli files.

## Step Limit 

In order to prevent infinite loop, an arbitrary MAX_STEPS constant has been declared (see [SimulatorController](../ts/controllers/simulatorController.ts)). This constant is used as the default value for the `continue()` method. Neither the CLI or the view provide a way to change this argument. Therefore, every programs running more than MAX_STEPS instructions will be stopped. An option to change this parameter would be nice.

## Fix Vue bugs

For example, in the register vue, if the number in any register is different than 0, then this shifts the flags vue a little to the right.
This is surely due to a css parameter that don't adapt to the register panel.

## Redo the load file function

The `loadDefaultFile` function was hastily modified to work with a refactoring of the model folders, and, even if it works fine, the code can largely be improved.

## More try/catch covered code

Thanks to the `PromptDialog` component, we can easily display success, warning or errors messages.
It has been used a lot in try/catch code blocks to ensure no error is silently ignored.
But not every bit of code has these safeties, so adding more try/catch blocks would help.

## Factoring CLI and server code

Currently, there is four functions exported by the file `./app/cli.js` that allow to ask the simulator to assemble, execute a number of step, continue or return a save file.
It is possible to reduce them by adding a `command` element to the query, just like in the `get("./cli")` method of `./app/server.js`.
It could be great to add the possibility to choose the returning format, like the current dump or like a save file.
