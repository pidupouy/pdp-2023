# Web Server

The server part is composed of two files, the [server](../../server.js) itself which handle GET and POST request, and the [cli](../../cli.js) file, which handle simulator request.

## [Server](../server.js)

Server files that listen on the port `8080` for GET and POST request

To launch it, use:

```bash
npm run release
```

or

```bash
# We increased the allowed header size in order to handle GET request using heavy files.
node --max-http-header-size 80000 ./server.js
```

### ./cli route

Used to send request to the simulator and return the dump of the simulator's state.

Supported arguments are:

* `ys`: The source code, *encoded in base64*
* `kernelName` (optional): The name of the kernel to use
* `hcl` (optional): The hcl code, *encoded in base64*
* `instructionSet` (optional): The instruction set *json* representation, *encoded in base64*
* `command` (optional): The command "assemble", "step" or "continue" to execute, *encoded in base64* .
* `nbSteps`(optional) : The number of steps to do in a command step request, *encoded in base64*.

### ./accessibility-mode route

Used to make a javascript-free version of the simulator that can be seen in text-based browsers like lynx.

It works by sending an html page in which we replace elements by the result of the simulator.  
This implies that in accessibility-mode, each time you click on a button, you send a GET or POST request that contains all the needed data to create a new SimulatorController and run the desired command. For example, if you do multiple step in a row, you have recreate each time the same simulator, with the same hcl, instructionSet and y86 code, compiled and run it with a number a step increasing each time.  
This isn't efficient but there is no other mean to keep the simulator between each request.  

## [CLI](../cli.js)

It serves to extract simulator request parameters from the query and execute it.  

Each exported function create a new Simulator with the desire hcl, instructionSet and y86code, assemble it, and execute it like requested.  
Then it return a dump of the simulator state.  
If an error occurred, it return the errors list.  

## [Utility script](../cli.py)

An utility script has been created in order to simplify the creation of requests.