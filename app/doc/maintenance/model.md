# Model organization

Here is a breakdown of the folders and files present in the model.

## Seq folder

This folder holds all the files specific to sequential kernels, like seq32bits and seq64bits

### sim.ts

This file implements the interface ISimulator

### stages.ts

This file implements all the functions that represent each stage of an instruction execution.
Those functions like `fetch` are used in the `step` function of `sim.ts`.

The code is based on the tcl/tk simulator.

### hcl32bits and hcl64bits

Default HCL files for the sequential kernels.
The 32bits and 64bits files are separated because because when we push or ret something on the pile there is a gap of 4 bytes in 32bits and 8 bytes in 64bits, and to ensure that the hcl code is not dependant on our implementation, we have made two separated files.

## Pipe folder

This folder holds all the files specific to pipelined kernels, like pipe32bits and pipe64bits

### sim.ts

This file implements the interface ISimulator

### pcontext.ts and pipeline.ts

The pipelined kernels are composed of a `PContext` that holds five `Pipeline`, one for each stage of the pipeline like `fetch`.
Each of the `Pipeline` holds two `Context`, on for the current state of the stage, and the other for the next input state of the stage.

### stages.ts

This file implements all the functions that represent each stage of an instruction execution.
Those functions like `fetch` are used in the `step` function of `sim.ts`.

The code is based on the tcl/tk simulator.

At each step, the simulator update the `PContext` by copying all the input state value of each stage to the current state value.
Then, when the stage functions are called, like `decode`, the simulator will compute 

### hcl32bits and hcl64bits

Default HCL files for the pipelined kernels.
The 32bits and 64bits files are separated because because when we push or ret something on the pile there is a gap of 4 bytes in 32bits and 8 bytes in 64bits, and to ensure that the hcl code is not dependant on our implementation, we have made two separated files.