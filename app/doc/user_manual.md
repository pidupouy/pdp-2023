# User Manual

## Top Buttons

### Assemble

Assembles the y86 code present in the editor tab.

### Reset

Resets the CPU without reassembling the y86 code.

### Undo

Comes back to the last step

### Step

Executes the next line.

### Continue

Continues execution from the current line to the end of the program.

### Load

Loads a previously saved y86 code, machine state, instruction set or HCL code. it can take the following formats: `.ys`, `.json` and `.hcl`.

### Save

Opens the save menu where the information to be saved to a file can be selected, the current tab will be preselected, saving only the editor or HCL tab will create a `.ys` or `.hcl` file. The generated file will contain a timestamp in its name.

## Tabs

### Editor Tab

The editor tab allows edition of the y86 source code and provides a view of the state of the CPU and memory. It is possible to change the size of the font using the buttons above the editor.

### Execution Tab

The execution tab shows the assembled y86 code and provides a view of the state of the CPU and memory.
When running the code, in sequential, a green line will highlight the instruction that has been executed, in pipelined, many instructions will be highlighted in different colors to show which instruction is in each stage of the pipeline.
If you hover the registers, flags or memory values with the mouse, then you will see the values at the next cycles. If the value is meant to be changed, then the register name, flag name, or address in memory will be highlighted in green.

### Instruction Set Tab

The instruction set tab shows the instruction set of the current kernel and allows its edition. To create a new instruction, you need to define its name, icode (number from 0 to 15), ifun (number from 0 to 15) and its arguments. The definition of the arguments is done using a string that can contain the following keywords:
    `valC`  represents a numerical value or label, exclusive with `valC?`,
    `valC?` represents an optional numerical value or label that defaults to 0, exclusive with `valC`,
    `rA` and `rB` represent registers.
The arguments string can also contain the following characters: `(`, `)`, `|`, `:`, `[`, `]`, `*`, `+`, `-`.

It is possible to create multiple instructions with the same name and icode but different arguments.

The instruction set tab allows to remove or edit all the default instructions present at the start. But this isn't without consequences, if you remove the instruction named "halt", then the simulator won't be able to enter the HLT state that mark the end of the program, also, if there isn't an instruction with the icode 0 and ifun 0 (nop by default), then the pipelined version won't work because it need this to implement it's BUBBLE instruction.
In the case you can't get your modifications working, there is still a button "reload default" to load the default instruction set.
Each time you are in the instruction set tab and leave it, the modifications will be automatically applied and a popup will tell you if they are valid or not. 

### HCL Tab

The HCL tab allows edition of the HCL source code used to simulate the CPU that runs the y86 code. It is possible to change the size of the font using the buttons above the editor.

## Keyboard shortcuts

The buttons of the simulator are mapped on alt + key shortcuts, here is the list :
- "Step" : Alt + S
- "Continue" : Alt + C
- "Reset" : Alt + R
- "Assemble" : Alt + A
- "Undo" : Alt + U
- "Load" : Alt + L
- "Save" : Alt + M
- "Settings" : Alt + P
- "About" : Alt + I

## Cli version

### Accessibility mode

To access the version for visually impaired, you need to use the route `/accessibility-mode` in the url.

This version has all the functionalities of the standard version, except for some changes :
- No key shortcut implemented
- The instruction set is represented as a json file.
- The next values of registers, flags and memory are write beside current values.
- The memory only display address with non-zero value.

### Request through CLI

To send a request to the simulator, you need to use the route `/cli` in the url and use the get method.

The query must contains some elements to work :
- `ys` : the data of the y86 code you want to execute.

There is also some optional elements :
- `kernelName` : the name of the kernel to use ("seq32bits" by default).
- `hcl` : the data of the hcl code you want to use (default file present in the simulator by default).
- `instructionSet` : the data of the instructionSet you want tu use (default file present in the simulator by default).
- `command` : choose if you want to "assemble", "step" or "continue" ("continue" by default).
- `nbSteps` : choose the number of steps to execute if you command step (0 by default).

If the execution end without errors and the request parameters were valid, you will receive data in json format with the following elements :
- `CpuState`: representation of the cpu state, different between seq and pipe kernels
- `CurrentRegisters`: [{"name": "registerName", "value_hex": "hexadecimal value", "value_dec": "decimal_value"}, ...]
- `NextRegisters`: [{"name": "registerName", "value_hex": "hexadecimal value", "value_dec": "decimal_value", "isModified": boolean}, ...]
- `CurrentFlags`: [{"name": "flagName", "value": "boolean value"}, ...]
- `NextFlags`: [{"name": "flagName", "value": "boolean value", "isModified": boolean}, ...]
- `Status`: [{"name":"STAT","value":"status value"}, {"name":"ERR","value":"error message"}, {"name":"PC","value":"pc value"}]
- `Performances`: undefined or [{"name": "Cycles","value": cycles value}, {"name": "Instructions","value": instructions value}, {"name": "CPI","value": CPI value}]
- `CurrentMemory`: [{"address": "address value","value": "hexadecimal value"}, ...]
- `NextMemory`: {"wordSize": value,"address": "address value","value": "hexadecimal value","isWrite": boolean}
