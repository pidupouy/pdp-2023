# Grammars used in the simulator

To handle grammars and compile them, the simulator use a Javascript module named Chevrotain (docs here : https://chevrotain.io/docs/).
This module help us to write a compiler by giving us classes and functions.
Each compiler is separated in three parts :
- Lexer : return a list of recognized tokens from the text we gave in input.
- Parser : return a tree representing the construction of the token's list. A node is represented by a rule and a leaf by a token.
- Visitor : return the compiled code by browsing through the tree.

## HCL 

The simulator use an HCL compiler to configure the cpu's behavior.
We describe the multiplexers that will choose which value the cpu will use in the different stage of the instruction execution.

### Lexer

Here is the list of all the tokens that are recognized by the hcl Lexer, in the order of recognition (- Regex : Token Name)

- /quote/ : QUOTE
- /boolsig/ : BOOLARG
- /bool/ : BOOL
- /intsig/ : INTARG
- /int/ : INT
- /in/ : IN

- /;/ : SEMI
- /:/ : COLON
- /,/ : COMMA

- /\(/ : LPAREN
- /\)/ : RPAREN
- /{/ : LBRACE
- /}/ : RBRACE
- /\[/ : LBRACK
- /\]/ : RBRACK

- /&&/ : AND
- /\|\|/ : OR
- /&/ : BITWISE_AND
- /\|/ : BITWISE_OR
- /\^/ : XOR

- /'.+?(?=')'/ : QSTRING
- /[a-zA-Z][a-zA-Z0-9_]*/ : VAR
- /[0-9][0-9]*/ : P_NUM
- /-[0-9][0-9]*/ : N_NUM

- /!=/ : DIFF
- /==/ : EQUAL
- />=/ : GREATER_EQUAL_THAN
- /<=/ : LESS_EQUAL_THAN
- />/ : GREATER_THAN
- /</ : LESS_THAN
- /!/ : NOT
- /=/ : ASSIGN

- /[^\S\n]+/ : WHITE_SPACE
- /\n/ : NEW_LINE
- /[ \r\t\f]/ : OTHER_SPACE
- /\#[^\n]+/ : COMMENTS

### Grammar

Here is a representation of the chevrotain grammar, write in jison.

hcl  
    : statements  
    ;  

statements  
    : MANY(statement)  
    ;  

statement  
    : QUOTE QSTRING               
    | BOOLARG VAR QSTRING  
    | INTARG VAR QSTRING      
    | BOOL VAR ASSIGN expr SEMI       
    | INT VAR ASSIGN expr SEMI        
    ;

expr  
    : expr_or IN LBRACE exprlist RBRACE  
    | expr_or  
    ;  

expr_or   
    : expr_and OR expr_or  
    | expr_and  
    ;  

expr_and   
    : expr_bitwise_or AND expr_and  
    | expr_bitwise_or  
    ;  

expr_bitwise_or  
    : expr_xor BITWISE_OR expr_bitwise_or  
    | expr_xor  
    ;  
 
expr_xor  
    : expr_bitwise_and XOR expr_xor  
    | expr_bitwise_and  
    ;  

expr_bitwise_and  
    : expr_equal XOR expr_bitwise_and  
    | expr_equal  
    ;  

expr_equal  
    : expr_comp DIFF expr_equal  
    | expr_comp EQUAL expr_equal  
    | expr_comp  
    ;  

expr_comp  
    : term GREATER_OR_EQUAL expr_comp  
    | term LESS_OR_EQUAL expr_comp  
    | term GREATER expr_comp  
    | term LESS expr_comp  
    | term   
    ;  

term   
    : LPAREN expr RPAREN  
    | LBRACK caselist RBRACK  
    | NOT expr  
    | factor  
    ;  

factor   
    : VAR      
    | P_NUM        
    | N_NUM
    ;  

exprlist  
    : expr MANY(COMMA expr)  
    ;  

caselist  
    : MANY(expr COLON expr SEMI)   
    ;  

## YAS grammar

The simulator use an YAS compiler to transform the assembly code into binary code.

### Lexer

Here is the list of all the tokens that are recognized by the hcl Lexer, in the order of recognition (- Regex : Token Name)

- /[^\S\n]+/ : WHITE_SPACE
- /[ \r\t\f]/ : OTHER_SPACE
- /\n/ : NEW_LINE
- /\#[^\n]+/ : COMMENT

- /:/ : COLON
- /[a-zA-Z][0-9a-zA-Z_]*/ : IDENTIFIER
- /\.[a-zA-Z][0-9a-zA-Z_]*/ : DIRECTIVE_IDENTIFIER
- /(0x[0-9a-fA-F]+)|(\-?\b[0-9]+\b)/ : NUMBER
- /[^\n#]+/ : ARGS
- /./ : INVALID

### Grammar

Here is a representation of the chevrotain grammar, write in jison.

document  
    : line_list  
    ;  

line_list  
    : AT_LEAST_ONE(line)  
    ;  

line  
    : label statement line_comment NEW_LINE  
    | label line_comment NEW_LINE  
    | statement line_comment NEW_LINE  
    | line_comment NEW_LINE  
    ;  

statement  
    : directive  
    | instruction  
    ;  

label  
    : IDENTIFIER COLON  
    ;  

directive  
    : DIRECTIVE_IDENTIFIER NUMBER  
    | DIRECTIVE_IDENTIFIER IDENTIFIER  
    ;  

instruction  
    : IDENTIFIER arg_list  
    | IDENTIFIER  
    ;  

arg_list  
    : IDENTIFIER arg_list  
    | IDENTIFIER  
    | NUMBER arg_list  
    | NUMBER  
    | ARGS  
    ;  

line_comment  
    : COMMENT  
    |   
    ;  

## Help

- MANY(...) : Allow 0 or more times the objects in parenthesis.
- AT_LEAST_ONE(...) : Allow 1 or more times the objects in parenthesis.