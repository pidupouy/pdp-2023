I - Améliorations des slides :
    -> simplifier diagramme UML page 19
        -> interface "IInstructionSet" coincée entre un grand nombre de blocs
        -> élaguer les listes de methodes (notamment les non-essentielles)

    -> cas d'utilisations
        -> présenter comme un diagramme de séquence
        -> format à standardiser

II - Améliorations du projet en général
    -> expliciter les méthodes de non-régression (tests)
        -> s'assurer que les fonctionnalités du projet existant fonctionnent toujours

    -> préciser la façon dont les instructions sont rajoutées (où on se sert de la regex ?)
    -> rendre la vue plus indépendante en ne mettant pas le mécanisme de sauvegarde dans la vue