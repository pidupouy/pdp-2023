\subsection{Pipeline 32 bits}
    Actuellement le simulateur web ne peut simuler qu'un processeur séquentiel, c'est à dire qu'il exécute qu'une instruction complète par cycle. 
    
    Or, depuis 1989, les processeurs peuvent être ``pipe-linés''~: le processus d'exécution d'une instruction est découpé matériellement en plusieurs étapes où les instructions vont progresser étape par étape.
    Ainsi, lorsque l'instruction 1 sera à l'étape 2, alors l'instruction 2 sera à l'étape 1.
    De plus, le top d'horloge qui définit la fréquence du processeur sera basée sur le temps d'exécution de l'étape la plus longue.
    Cela donne une latence plus élevée pour la première instruction, car elle prendra autant de cycles qu'il y a d'étapes, mais un débit d'instructions effectuées plus élevé car les instructions vont s'enchaîner à chaque cycle après la première instruction. De plus, la durée d'un cycle sera réduite car on prend moins de temps à faire une étape que pour l'exécution complète de toutes les étapes.

    Dans le premier processeur pipe-liné d'Intel, le Pentium, il y avait 5 étapes.
    Cette architecture à été conservée pour le simulateur y86, qui est une version simplifié du x86 d'Intel.
    Les étapes sont séparées par des ``verrous'' (``\textit{latch}'') qui ne laissent passer l'instruction qu'au prochain top d'horloge.
    À chaque étage, on retrouve des registres différents qui reçoivent les informations nécessaires au fonctionnement de l'étage.
    
    Les 5 étages sont~:
    \begin{enumerate}
        \item \textit{Fetch}~: On récupère l'instruction à exécuter~;
        \item \textit{Decode}~: On décode l'instruction et on place les bons registres en entrée de l'unité arithmétique et logique (\Gls{UAL})~;
        \item \textit{Execute}~: L'\Gls{UAL} exécute l'instruction qu'on lui lui a fournie~;
        \item \textit{Memory}~: On écrit/lit en mémoire si l'instruction le nécessite~;
        \item \textit{WriteBack}~: On met à jour les registres avec les nouvelles valeurs.
    \end{enumerate}

    Dans la figure ci-dessous, on peut observer l'architecture du pipe-line, avec les 5 \textit{latches} représentés par de longs rectangles horizontaux, avec à l'intérieur les registres de l'étage.
    Les unités fonctionnelles du processeur sont représentées par les formes bleues, telles que l'\Gls{UAL} ou la mémoire.
    Les \glspl{Multiplexeur} sont représentés par les formes grises, qui seront codées en HCL. 
    Les flèches représentent les bus de données sur un ou plusieurs bits.
    
    \begin{figure}[H]
        \centering
        \includegraphics[scale=0.7]{Images/archi.png}
        \caption{Visualisation des blocs fonctionnels de l'architecture pipe-linée}
        \label{fig:archi}
    \end{figure}

    L'architecture ci-dessus sera représentée sur le simulateur grâce au langage \gls{HCL}.
    L'utilisation de cette nouvelle architecture n'entraîne pas que des avantages, il existe aussi des contraintes, liées aux dépendances entre instructions successives présentes dans le pipe-line.
    Par exemple, des instructions successives qui modifient puis lisent la valeur d'un même registre, une instruction de saut conditionnel qui attend le résultat de la comparaison pour savoir quelle branche prendre, ou encore le résultat d'accès à la mémoire qui n'est plus disponible au cycle suivant.
    Pour gérer ces dépendances, les étages doivent pouvoir se mettre en pause (``\textit{stall}'') ou bien propager une instruction qui ne fait rien~: ```\texttt{nop}'' (``\textit{bubble}''), selon les cas.

    \subsubsection{Data Forwarding}

    Un des problèmes induits par une architecture pipe-linée est la dépendance des registres.
    Si une instruction modifie la valeur d'un registre et que la suivante utilise la valeur de ce même registre, alors on a un problème car la valeur du registre ne sera mise à jour que lorsque l'instruction aura atteint l'étage \textit{Write Back}.

    Une solution simple serait de faire patienter l'instruction suivante jusqu'à la mise à jour du registre, mais cela ralentirait considérablement le processeur.
    Au lieu de cela, comme on peut le voir sur la figure de l'architecture \ref{fig:archi}, il y a plusieurs flèches qui redescendent des étages supérieurs vers l'étage \textit{Decode} qui est chargé de fournir la valeur des registres qui seront placés dans l'\Gls{UAL}.
    C'est la technique du ``\textit{Data Forwarding}'', qui consiste à aller chercher la valeur du registre dans le pipe-line avant qu'elle soit mise dans le registre lui-même.

    Il faudra donc que le \gls{Noyau} pipe-liné puisse s'affranchir de la dépendance des registres en allant chercher la valeur des registres aux endroits suivants~:
    \begin{itemize}
        \item la banque de registres (comme dans le séquentiel)~;
        \item à la sortie de l'étage \textit{Execute} (sortie de l'\Gls{UAL})~;
        \item à la sortie de l'étage \textit{Memory}~;
        \item à la sortie de l'étage \textit{Write Back}.
    \end{itemize}

    \subsubsection{Load/Use hazard}

    Un des problèmes induits par une architecture pipe-linée est l'accès à la mémoire.
    Si une instruction veut accéder à la valeur d'un registre et que ce même registre s'apprête à recevoir une valeur en mémoire, alors, même avec le \textit{Data Forwarding}, le processeur doit attendre que la valeur soit sortie de la mémoire pour y accéder.

    Pour régler cela, le processeur doit être capable de détecter le problème et de déclencher l'apparition d'une bulle à l'étage \textit{Decode}, c'est à dire qu'il va dynamiquement ajouter des instructions \texttt{nop}, qui ne feront rien, pour forcer le pipe-line à attendre à partir de l'étage \textit{Decode}.
    Parallèlement à cela, l'étage précédent, \textit{Fetch}, devra se mettre en attente (\textit{stall}) pour ne pas perdre d'instructions.
    Ce faisant, l'instruction qui accédera à la mémoire continuera de parcourir le pipe-line et, lorsqu'elle sortira de l'étage \textit{Memory}, l'instruction qui attend à l'étage \textit{Decode} pourra continuer à avancer.

    \subsubsection{Instruction \texttt{ret}}

    Un des problèmes induits par une architecture pipe-linée est le retour de fonction.
    En effet, si une instruction \texttt{ret} apparaît dans le code, alors le processeur ne doit pas continuer à exécuter les instructions suivantes.

    Pour régler cela, comme pour le ``\textit{Load/Use Hazard}'', le processeur doit mettre l'étage \textit{Fetch} en attente si l'instruction \texttt{ret} se trouve dans l'étage \textit{Decode}.
    Ensuite, l'instruction doit se propager et l'étage \textit{Decode} doit faire apparaître continuellement des bulles.
    Une fois que \texttt{ret} est parvenu au bout du pipeline, et que le compteur ordinal à été mis à jour, on peut recommencer à accepter les instructions à l'étage \textit{Fetch} et continuer l'exécution.

    \subsubsection{Prédiction de branchement}

    Un des problèmes induits par une architecture pipe-linée est la gestion des branchements conditionnels.
    Comme le résultat d'une comparaison n'arrive qu'après l'étage \textit{Execute}, l'étage \textit{Fetch} ne peut pas savoir si il doit prendre le branchement ou non au moment où il reçoit l'instruction pour mettre à jour le compteur ordinal. 

    Pour régler cela, une solution simple aurait été à nouveau d'attendre, comme pour l'instruction \texttt{ret}, mais comme les sauts sont nombreux dans un programme et que forcément l'un des deux choix sera le bon, le processeur va choisir une branche et s'y tenir jusqu'à ce qu'il reçoive la confirmation ou non.
    Concrètement, une fois que le résultat de la comparaison sera arrivé, si le processeur a fait le bon choix, alors il continuera sur sa lancée, mais s'il a fait le mauvais, alors il purgera le pipe-line avec des bulles pour repartir de la prochaine instruction correcte.
    Pour simplifier la prédiction de branchement, le simulateur devra appliquer la stratégie de toujours prendre le branchement.

\subsection{Grammaires Yas et HCL}

    \subsubsection{Changement de module Javascript}
    
    Suite à la sortie de Typescript~4, il est apparu que plusieurs modules utilisés par le simulateur n'étaient plus maintenus ou ne disposent pas de version compatible avec Typescript~4.
    C'est notamment le cas du module Jison qui permet de gérer les grammaires Yas et HCL du simulateur.
    Il faut donc trouver un nouveau module pour le remplacer, et ensuite adapter et éventuellement corriger la grammaire pour le nouveau module.

    Après quelques recherches du groupe et des anciens développeurs du simulateur, le module Chevrotain a été retenu.
    En plus d'être disponible sous Typescript~4, il est régulièrement mis à jour et ne devrait donc pas être abandonné dans un futur proche, notamment en vue de la sortie de Typescript~5.

    \subsubsection{Correction de la grammaire}

    D'après les différentes \textit{issues} présentes sur le dépôt Git du projet, l'ancienne grammaire comporte des erreurs qu'il faudra corriger lorsque l'on refera le module de grammaire.
    Il s'agit surtout de la grammaire HCL, qui ne respecte pas la précédence des opérateurs OR, AND, XOR, NOT, COMP et IN, alors que la grammaire semble identique à celle de la version Tcl/Tk. 

    En effet, dans l'ancienne grammaire, si l'on se retrouve avec une formule de la forme ``a AND b OR c AND d'', cette expression sera traduite comme étant (``((a AND b) OR c) AND d)'' au lieu de ``((a AND b) OR (c AND d))''.
    Il est donc pour l'instant impossible d'écrire un code HCL sans parenthéser explicitement les formules contenant ces opérateurs.
    Cela tient du fait que, lors du passage de la version Tcl/Tk (qui utilise Bison) à la version web basée sur Jison, les marquages de précédence (\texttt{\%left}, \texttt{\%right}, etc.) n'ont pas été traduits dans la nouvelle grammaire en Jison.

    Pour remédier à cela, il faudra donc trouver un moyen de déclarer la précédence des opérateurs avec Chevrotain, ou bien modifier la grammaire pour qu'elle soit sous la forme ETF, et donc qu'elle respecte naturellement la précédence des opérateurs.
    Pour construire une telle grammaire, on se base sur la précédence des opérateurs en C, qui respecte une table bien définie~\cite{COperatorPrecedence}.
    
    \subsubsection{Extension de la grammaire}

    Comme l'on va refaire la grammaire HCL, M.~Pellegrini a souhaité que soient ajoutés de nouveaux opérateurs afin d'étendre les capacités de la grammaire.
    Les opérateurs concernés sont les opérateurs binaires suivant~:
    \begin{itemize}
        \item \textbf{AND} (`\&')~: compare chaque bit du premier opérande avec le bit du seconde opérande, retournant 1 si les deux bits sont à 1 et 0 sinon.
        Exemple : 11010101 \& 01101110 donne 01000100 (0xD5 \& 0x6E donne 0x44 en hexadécimal).

        \item \textbf{OR} (`$|$')~: compare chaque bit du premier opérande avec le bit du seconde opérande, retournant 1 si un des deux bits est 1 et 0 sinon.
        Exemple : 11010101 $|$ 01101110 donne 11111111 (0xD5 $|$ 0x6E donne 0xFF en hexadécimal).

        \item \textbf{XOR} (`$\oplus$')~: compare chaque bit du premier opérande avec le bit du seconde opérande, retournant 1 si les deux bits ont des valeurs différentes et 0 sinon.
        Exemple : 11010101 $\oplus$ 01101110 donne 10111011 (0xD5 $\oplus$ 0x6E donne 0xBB en hexadécimal).
    \end{itemize}
    
    Ici aussi, il faudra les incorporer à la grammaire ETF pour que leur précédence d'opérateur soit pris en compte.

\subsection{Simulateur version ligne de commande}

Que ce soit pour l'exécution des modules d'évaluation VPL ou pour l'accessibilité à un simulateur texte, il sera nécessaire de faire fonctionner le simulateur en ligne de commande, et pas seulement avec l'interface graphique offerte par Vue.JS et JavaScript.
Cette version devra être tout aussi fonctionnelle que la version web, que ce soit pour l'exécution de code assembleur, de modification du code HCL ou encore d'affichage de l'état de la mémoire et des registres.

    \subsubsection{Requêtes pour le simulateur}

    Pour pouvoir utiliser les mêmes fonctionnalités que le simulateur web, on devra pouvoir envoyer un certains nombre de requêtes différentes qui renverront des résultats différents~:
    
    \begin{enumerate}
        \item \textit{save}~: permet de sauvegarder l'état du simulateur (code HCL, état machine, liste d'instructions, code y86) $\longrightarrow$ retourne le fichier contenant l'état du simulateur~;
        \item \textit{load}~: permet de charger l'état du simulateur à partir d'un fichier $\longrightarrow$ retourne l'état du simulateur une fois chargé~;
        \item \textit{assemble}~: permet de compiler le code y86 pour qu'il soit prêt $\longrightarrow$ retourne le nouvel état du simulateur avec le code compilé~;
        \item \textit{step}~: permet, lorsque du code compilé est chargé, d'exécuter un cycle du processeur $\longrightarrow$ retourne le nouvel état du simulateur~;
        \item \textit{reset}~: permet, lorsque du code compilé est chargé, de réinitialiser l'état du processeur et de la mémoire comme au chargement du code $\longrightarrow$ retourne le nouvel état du simulateur~;
        \item \textit{continue}~: permet, lorsque du code compilé est chargé, d'exécuter le code jusqu'à sa terminaison $\longrightarrow$ retourne l'état du simulateur une fois le code exécuté~;
        \item \textit{test}~: permet, lorsque du code compilé est chargé, d'exécuter le code jusqu'à sa terminaison $\longrightarrow$ retourne les états successifs du simulateur lors de l'exécution du code sous la forme d'un fichier.
    \end{enumerate}

    \subsubsection{Réception de nouveaux paramètres}

    Pour pouvoir pleinement utiliser le simulateur, on doit pouvoir modifier le code HCL qui paramètre le simulateur, ainsi que l'ensemble des instructions disponibles. Pour cela, le simulateur doit pouvoir lire les fichiers correspondants envoyés à travers une requête. Le format de ces fichiers sera le même que celui actuellement utilisés pour l'initialisation du simulateur ou la sauvegarde de son état.

    En cas d'erreur dans le fichier envoyé, le simulateur devra conserver son ensemble d'instructions ou son code HCL précédent.
 
    \subsubsection{Envoi de l'état de la mémoire}

    Pour pouvoir fonctionner grâce à l'envoi de requêtes, il sera nécessaire que le simulateur puisse fournir toutes les informations concernant son état actuel.
    Il faudra donc mettre en place ou améliorer les fonctions déjà existantes permettant d'effectuer un \textit{dump} de la mémoire et de l'état du processeur sous la forme d'un fichier.
    D'après les modifications apportées lors du stage de 2022, le format retenu sera JSON.

    Comme la mémoire disponible pour le simulateur représente plus de 8188 octets, et que la majeure partie sera composée uniquement de bits à 0, on pourra ne garder que les adresses ayant des valeurs autres que 0, pour faire de l'économie en mémoire.
    Ensuite, il suffira de compléter les adresses manquantes par des valeurs à 0.

    Cet envoi de l'état de la mémoire devra être possible entre chaque cycle du processeur émulé.

    \subsubsection{Envoi des états successifs de la mémoire}

    Pour pouvoir analyser qu'un code assembleur s'exécute correctement et modifie la mémoire et l'état du processeur comme attendu, dans le cadre des évaluations VPL, il faut pouvoir exécuter le programme sur le simulateur et retourner l'ensemble des \textit{dumps} produits à chaque cycle d'exécution. L'analyse de ces données par le module d'évaluation permettra ensuite de déterminer si le code assembleur fourni par l'étudiant via Moodle est correct par rapport à l'exercice.

    Il faudra tester que le simulateur renvoie bien l'ensemble correct des informations nécessaires concernant un code qu'il aura exécuté.
    Il faudra aussi étudier si la taille de ses données ne devient pas rapidement excessive pour un programme qui s'exécute pendant beaucoup de cycles.

\subsection{Accessibilité (déficients visuels)}
 
Le client a indiqué qu'il fallait apporter des modifications au simulateur web pour qu'il soit accessible aux non-voyants.
L'un des élèves des années passées, qui était non-voyant, n'avait pas pu suivre le cours dans de bonnes conditions car le simulateur web n'était pas adapté pour lui.

Pour nous renseigner sur les bonnes pratiques, nous nous sommes basées sur des guides de bonnes pratiques d'accessibilité pour le web~:
\begin{itemize}
    \item Modèle VPTCS (Visibilité, Perception, Technique, Contenus et Services)~\cite{VPTCS}.
    
    Ces 5 points capitaux peuvent être perçus comme étant les 5 commandements à la création d'un site web~:
    \begin{itemize}
        \item Visibilité~: Trouver le site~;
        \item Perception~: Utiliser le site~;
        \item Technique~: Que le site fonctionne~;
        \item Contenus~: Que les contenus soient de bonne qualité~;
        \item Services~: Que ce qui se passe après la visite soit de bonne qualité.
    \end{itemize}

    \item WCAG~: \textit{Web Content Accessibility Guidelines}~\cite{WCAG}
\end{itemize}

    \subsubsection{Compatibilité avec les navigateurs texte}

    Il faut tester la version web du simulateur sur des navigateurs comme Lynx ou Links pour avoir une vue de ce qu'il donne actuellement en version texte et de ce qu'il faudra améliorer.
     
    Malheureusement, la plupart des navigateurs textes ne supportent pas le JavaScript et, comme le site web repose entièrement sur Vue.js, rien ne s'affichera sur le navigateur texte.
    Pour contrer cela, il faudrait faire une version PHP du site, ce qui permettrait d'envoyer au navigateur du client des données HTML et CSS qu'il pourra interpréter.
    Cela demanderait de refaire le site de presque zéro, ce qui est hors de portée pour ce projet.
    De plus, une telle version chargerait le serveur pour chaque instance, alors que la version actuelle, les utilisateurs à l'Université de Bordeaux qui vont sur la page web du cours travaillent ensuite uniquement en mode \textit{client web}

    La solution envisagée est donc de faire une version du simulateur accessible en ligne de commande, en complétant les besoins précédents.
    
    \subsubsection{Navigation au clavier}

    Plutôt que d'utiliser la souris pour se déplacer sur un site web, les personnes à déficience visuelle utilisent le clavier, ou un clavier braille.
    Il faut donc s'assurer que le simulateur web est correctement navigable en utilisant seulement le clavier.
    Des raccourcis pour naviguer entre les menus seront à mettre en place, ainsi qu'une page listant tout les raccourcis, accessible depuis n'importe quel endroit.

    Actuellement, il se trouve que la navigation par clavier est impossible~: la tabulation ou les flèches directionnelles ne permettent pas de changer de champ sur le site.

    \subsubsection{Synthèse vocale}

    Il faut tester que la synthèse vocale fonctionne sur le simulateur web, et qu'il est possible de naviguer grâce à elle.
    Il faudra faire attention à ce qu'elle comprenne tout les mots employés, notamment pour ce qui est réservé à l'architecture comme les registres, états de la machine, de la mémoire, du code y86, HCL, etc.
    Cela devra être fait pour toutes les langues disponibles dans le simulateur web.

    Il faudra possiblement rechercher des modules de synthèse vocale pour Vue.js.
    Par exemple, il existe vue-announcer qui permet notamment d'énoncer les changements de contextes soudains dû a des scripts JavaScript.

    \subsubsection{Présentation visuelle}

    Pour s'adapter aux autres défaillances visuelles, il faudra examiner et améliorer si nécessaire la présentation du simulateur.
    Il faudra prendre en compte les divers types de daltonismes et s'assurer que la coloration syntaxique ainsi que les couleurs du site s'y adaptent, avec possiblement un moyen de configurer les palettes de couleurs utilisées.

    Il faut aussi examiner la possibilité de changer la taille et la forme des éléments du simulateur pour qu'ils soient plus clairement visible. 
    
\subsection{Noyaux 64~bits} 

La mise en place d'une architecture 64~bits introduit plusieurs changements à prendre en compte. L'utilisation d'adresses 64~bits au lieu d'adresses 32~bits étend l'espace de mémoire théoriquement disponible et force des changements, par exemple au niveau des registres qui devront eux aussi être plus grands, notamment pour permettre de stocker les adresses mémoire.

    \subsubsection{Paramétrage du nombre de bits utilisés}

    La structure permettant de changer de \gls{Noyau} pour le simulateur est déjà en place, ainsi qu'une variable nommé WORD\_SIZE à l'intérieur du \gls{Noyau} qui permet de définir la taille d'un mot machine. Cependant, celle-ci n'est pas identique  à chaque endroit dans le code. Il faudra donc vérifier que dans le code on se serve de cette variable pour déterminer la longueur des mots.

    \subsubsection{Gestion des débordements}

    Comme les adresses auront un nombre de bits doublé, il faudra s'assurer que lors des opérations arithmétiques, la limite et la retenue s'appliquent bien au 64ème bit et non au 32ème.

    \subsubsection{Affichage}

    Comme les adresses et les registres de la machine doubleront de taille, il faudra adapter l'affichage du simulateur web pour prendre en compte ces changements de taille. Le simulateur devra pouvoir passer d'un mode 32~bits à un mode 64~bits sans action supplémentaire requise par l'utilisateur que le choix du \gls{Noyau} d'exécution du simulateur.

    Cela passera donc par des éléments CSS qui devront s'adapter en fonction de la taille de ce qu'ils ont a afficher. La disposition des éléments graphique devra rester la même dans le meilleur des cas.

\subsection{Affichage des performances}

Le principal atout d'un \gls{Noyau} pipe-liné est de pouvoir augmenter le débit d'instructions traitées, une fois passé la latence pour parvenir au bout du pipeline.
Plus précisément, même si la première instruction va prendre 5~cycles, la prochaine instruction ne prendra qu'un cycle car elle aura déjà parcouru le reste du pipe-line.
Cela n'est plus vrai évidemment lorsqu'une instruction nécessite de faire apparaître une bulle dans le pipe-line, comme après un retour de fonction.

Ainsi, en reprenant la méthode de comptage du simulateur Tcl/Tk, on va compter le nombre d'instructions traités, le nombre de cycles écoulés, et calculer la moyenne du nombre d'instructions par cycle effectuées.
Il faudra spécifiquement ne pas compter les bulles qui apparaissent dans le pipeline comme des instructions effectuées.

\subsection{Affichage du pipe-line} 

Le \gls{Noyau} pipe-liné va induire des changements dans l'affichage graphique du simulateur, comme il y aura plus de variables à afficher dans ce contexte.
En effet, comme on peut le voir sur cette figure, chaque étage doit afficher différents registres en deux exemplaires, un pour l'état à l'entrée de l'étage suivant et un autre à la sortie de l'étage précédent~: ce sont l'état courant et l'état futur des \textit{latches} qui isolent les différents étages du pipe-line.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.3]{Images/affichagepipe.PNG}
    \caption{Interface graphique du simulateur web V2 avec le noyau pipe-liné}
    \label{fig:affichagepipe}
\end{figure}

Les étages STATE et INPUT présents sur l'image correspondent à ce que nous décrivons.
Heureusement, une partie du travail a déjà été réalisée dans les développements précédents, et le composant Vue.js qui affiche l'état du processeur peut déjà afficher l'état d'un processeur pipe-liné.
Il faudra cependant modifier correctement le nouveau \gls{Noyau} pour qu'il envoie les informations nécessaires.

\subsection{Surlignage des lignes de code}

Comme pour la version séquentielle et le simulateur Tcl/Tk, on doit afficher, à côté de chaque ligne de code, à quel étage elle se trouve actuellement~: si la ligne 0x0000 se trouve à l'étage \textit{Fetch}, un ``F'' doit apparaître à côté de la ligne, et ainsi de suite pour le reste des étages.

Au début, aucune ligne ne doit être soulignée car aucune ne se trouvera dans un étage du pipe-line.
À la fin de l'exécution, seule la dernière ligne, contenant un \texttt{halt} si l'exécution s'est bien passée, sera affichée à l'étage \textit{Write Back}.
   
\subsection{Modules d'évaluations VPL} 

Le client à indiqué qu'il souhaitait pouvoir utiliser le simulateur avec des modules d'évaluation VPL, afin de proposer et corriger automatiquement des exercices sur la plateforme Moodle de l'université.
Ces exercices seront évalués automatiquement feront partie du contrôle continu de L'UE d'architecture des ordinateurs.