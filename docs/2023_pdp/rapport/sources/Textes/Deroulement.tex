\subsection{Réécriture des grammaires HCL et YAS}

    \subsubsection{Chevrotain}
    
    Comme expliqué précédemment, nous avons remplacé l'ancien module qui traitait les grammaires, Jison, par un nouveau module qui est toujours maintenu, Chevrotain. Contrairement à son prédécesseur, Chevrotain ne permet pas de générer automatiquement un analyseur syntaxique (\textit{lexer}) et un analyseur grammatical (\textit{parser}) à partir d'une grammaire définie dans un fichier ``.jison''~; il aide au contraire à écrire ``à la main'' les éléments du compilateur.
    
    Cela rend donc la prise en main plus difficile car, quand bien même nous avions l'ancienne grammaire pour nous guider, il fallait la retranscrire dans un autre langage, JavaScript.
    
    Voici donc les primitives et fonctions fournies par Chevrotain qui nous ont permis de refaire les compilateurs HCL et YAS~:
    
    \begin{itemize}
        \item \textit{Lexer}
    
        Le premier élément du compilateur est le \textit{Lexer}. Son rôle est d'analyser le code source pour y repérer les mots-clés définis, appelés ``jetons'' (\textit{tokens}), et ainsi renvoyer la liste des \textit{tokens} à la suite du code source.
        Si un mot ne correspond à aucun \textit{token} défini, alors une erreur de compilation est levée pour signaler une erreur lexicale dans le code source.

        On peut donc définir des \textit{tokens} avec la fonction \textit{createToken}, qui demande un nom pour le \textit{token} ainsi que sa définition avec une expression régulière (``\textit{regular expression}'' ou ``\textit{regex}'').
        \begin{minted}{javascript}
const Var = createToken({ name: "VAR", pattern: /[a-zA-Z][a-zA-Z0-9_]*/ })
const Diff = createToken({ name: "DIFF", pattern: /!=/ })
const Not = createToken({ name: "NOT", pattern: /!/ })
const Assign = createToken({ name: "ASSIGN", pattern: /=/ })
        \end{minted}

        Ensuite on regroupe les \textit{tokens} dans une liste.
        L'ordre d'insertion dans la liste est très important car il définit l'ordre dans lequel le \textit{Lexer} va vérifier si un mot est reconnu par un \textit{token} ou non.
        Par exemple, le \textit{token} ``DIFF'' doit se trouver avant le \textit{token} ``NOT'' car ils commencent tout les deux par les mêmes caractères et ``DIFF'' est plus long que ``NOT''.
        Si on inversait l'ordre, le mot ``!='' serait alors reconnu comme deux tokens~: ``NOT ASSIGN'', au lieu d'un seul~: ``DIFF''

        \begin{minted}{javascript}
const allTokens = [
  Diff,
  Not,
  Assign,
  Var,
]
        \end{minted}

        Enfin il nous suffit de créer une nouvelle instance de la classe fournie \textit{Lexer} avec la liste des \textit{tokens} à reconnaître, puis d'appeler la méthode \textit{tokenize} sur le code source.

        \begin{minted}{javascript}
const SelectLexer = new Lexer(allTokens)
const lexingResult = SelectLexer.tokenize(inputText)
        \end{minted}

        \item \textit{Parser}

        Le deuxième élément du compilateur est le \textit{Parser}. Il va se charger d'analyser la liste des \textit{tokens} renvoyés par le \textit{Lexer} et construire un arbre à partir des règles décrites dans le \textit{Parser}.

        Pour cela, on commence par créer une nouvelle classe qui étend la classe \textit{CstParser} fournie.
        Dans le constructeur, on commence par appeler le constructeur du parent en lui passant le vocabulaire des \textit{tokens} (tout les \textit{tokens} reconnaissables par le \textit{Lexer}), puis on définit les règles qui composent la grammaire à l'aide des méthodes \textit{this.RULE}, suivi du nom de la règle et de sa définition.
        Le \textit{Parser} va alors se déplacer dans la liste des \textit{tokens} reconnus (résultat du \textit{Lexer}), et essayer faire correspondre ces données d'entrée aux règles (\textit{this.SUBRULE}), en consommant les \textit{tokens} (\textit{this.CONSUME}).
        Si, lors du parcours, le \textit{Parser} se trouve dans une situation qui n'est pas permise par la grammaire, une erreur de compilation syntaxique est levée.

        Pour définir une règle de la grammaire, on dispose de méthodes fournies dans la classe \textit{CstParser}, dont certaines permettent de simplifier la définition de la grammaire~:

        \begin{itemize}
            \item \textbf{RULE}~: Définit une règle~;
            \item \textbf{SUBRULE}~: le contexte courant doit correspondre à la règle indiquée~;
            \item \textbf{CONSUME}~: le contexte courant doit correspondre au \textit{token} indiqué~;
            \item \textbf{OR}~: le contexte courant doit correspondre à une des alternatives~;
            \item \textbf{OPTION}~: le contexte courant doit correspondre, zéro ou une seule fois, à la définition donnée~;
            \item \textbf{MANY}~: le contexte courant doit correspondre, zéro ou plusieurs fois à la suite, à la définition donnée.
        \end{itemize}
        
        Enfin, à la fin du constructeur, on appelle \textit{this.performSelfAnalysis()} qui va analyser la grammaire obtenue pour vérifier si elle est correcte. 
        Grâce à cela, on a pu rapidement identifier les problèmes de récursions à gauche et de précédence d'opérateur qui caractérisaient l'ancienne grammaire HCL.
        
        \begin{minted}{javascript}
class HclParser extends CstParser {
  constructor() {
    super(tokenVocabulary)
    
    const $ = this
    
    $.RULE("hcl", () => {
      $.SUBRULE($.statements)
    })
    
    $.RULE("statements", () => {
      $.MANY(() => {
        $.SUBRULE($.statement)
      });
    })
    
    $.RULE("statement", () => {
      $.OR([
        { ALT: () => $.SUBRULE($.quote_statement) },
        { ALT: () => $.SUBRULE($.boolarg_statement) },
        { ALT: () => $.SUBRULE($.intarg_statement) },
        { ALT: () => $.SUBRULE($.bool_statement) },
        { ALT: () => $.SUBRULE($.int_statement) }
      ])
    })
    
    $.RULE("bool_statement", () => {
      $.CONSUME(Bool)
      $.CONSUME(Var)
      $.CONSUME(Assign)
      $.SUBRULE($.expr)
      $.CONSUME(Semi)
    })

    $.RULE("expr", () => {
      $.SUBRULE($.expr_and)
      $.OPTION(() => {
        $.CONSUME(Or)
        $.SUBRULE($.expr) 
      })
    })
    
    this.performSelfAnalysis()
  }
}
        \end{minted}

        Une fois la définition du \textit{Parser} faite, il ne restait plus qu'à instancier la classe, lui passer en entrée le résultat du \textit{Lexer} et appeler la première règle de la grammaire pour obtenir l'arbre de parcours du code source.

        \begin{minted}{javascript}
const parserInstance = new HclParser()
parserInstance.input = lexingResult
const cst = parserInstance.hcl()
        \end{minted}

        \item \textit{Visitor}

        Le troisième et dernier élément du compilateur est le visiteur. Il va parcourir l'arbre fourni par le \textit{Parser} afin de construire le code compilé en sortie.

        Pour cela, on commence par à nouveau étendre une classe fournie par Chevrotain, \textit{BaseCstVisitor}.
        On définit ensuite le constructeur en appelant le constructeur parent avec \textit{super}, ainsi que la méthode \textit{this.validateVisitor()} qui va vérifier que le \textit{Visitor} est correctement défini. Ceci permet de vérifier que le \textit{Visitor} contient une méthode de parcours pour chaque règle présente dans le \textit{Parser}.

        Ensuite, on définit donc les méthodes qui porteront le même nom que les règles du \textit{Parser}.

        La variable \textit{ctx} qui est passé en paramètre de chaque méthode correspond au noeud courant de l'arbre qu'on parcourt. Ainsi, quand on appelle la méthode \textit{hcl} pour la première fois, \textit{ctx} est le nœud racine de l'arbre. 
        
        Traduit en JavaScript, \textit{ctx} est un objet qui possède d'autres objets auxquels on peut accéder grâce au nom de la règle (\textit{ctx.statements}) si le fils est une règle ou grâce au nom du \textit{token} (\textit{ctx.OR}) si le fils est un \textit{token}.

        L'appel \textit{this.visit} va chercher la fonction de \textit{HclVisitor} qui correspond au nœud qu'on passe en paramètre. Ici, on l'appelle avec un nœud \textit{statements}, ce qui va donc appeler la fonction du même nom \textit{statements(ctx)}.

        Comme pour un objet classique, on peut ajouter des attributs à la classe pour nous aider à compiler le code.
        Ici par exemple, la \textit{HclVisitor} possède une liste nommé \textit{errors}.

        \begin{minted}{javascript}
class HclVisitor extends BaseCstVisitor {
    errors = []

    constructor() {
        super()
        this.validateVisitor()
    }

    hcl(ctx) {
        this.visit(ctx.statements)
        
        let jsOutput = "code compilé"
        
        return {
          type: "HCL",
          jsOutput: jsOutput,
          errors: this.errors
        }
    }
}
        \end{minted}

        Comme on peut avoir plusieurs fois le même \textit{token} ou la même sous-règle dans une règle, notamment grâce à \textit{MANY}, on peut parcourir les mêmes \textit{tokens}/sous-règles comme une liste.

        \begin{minted}{javascript}
statements(ctx) {
    if(ctx.statement) {
        for (const statement of ctx.statement) {
            this.visit(statement)
        }
    }
}
        \end{minted}

        Pareillement, pour les règles utilisant la méthode \textit{OR} de Chevrotain, si un \textit{token}/sous-règle n'est pas présent, alors à la place de la liste on aura un élément \textit{null}.

        \begin{minted}{javascript}
statement(ctx) {
    if (ctx.quote_statement) {
        this.visit(ctx.quote_statement)
    } else if (ctx.boolarg_statement) {
        this.visit(ctx.boolarg_statement)
    } else if (ctx.intarg_statement) {
        this.visit(ctx.intarg_statement)
    } else if (ctx.bool_statement) {
        this.visit(ctx.bool_statement)
    } else if (ctx.int_statement) {
        this.visit(ctx.int_statement)
    }
}
        \end{minted}

        Ensuite, il ne reste plus qu'à construire le code compilé durant le parcours de l'arbre grâce au retour des méthodes ou à des attributs ajoutés à la classe (Comme la liste \textit{errors} vue précédemment).
        L'attribut \textit{token.image} permet d'accéder à la valeur du \textit{token} sous la forme d'une chaîne, c'est-à-dire le mot qui a été reconnu par le \textit{Lexer}.

        \begin{minted}{javascript}
expr(ctx) {
    if (ctx.expr) {
        const expr_value_left = this.visit(ctx.expr_and).value
        const expr_value_right = this.visit(ctx.expr).value
        const operator_value = ctx.OR[0].image
        return {
            type: "EXPR",
            value: expr_value_left + operator_value + expr_value_right
        }
    } else {
        return {
            type: "EXPR",
            value: this.visit(ctx.expr_and).value
        }
    }
}
        \end{minted}
    \end{itemize}


    \subsubsection{Nouvelle grammaire HCL}

    Comme dit précédemment, plusieurs problèmes de précédence d'opérateurs ont été repérés sur la grammaire HCL, qu'il fallait donc corriger avant de l'implémenter avec Chevrotain.

    Malheureusement, il n'est pas possible avec Chevrotain de définir la précédence des règles, comme on pouvait le faire en Jison avec \textit{\%left} et \textit{\%right}.
    Il fallait donc que la nouvelle grammaire respecte naturellement la précédence des opérateurs, c'est-à-dire qu'il nous fallait une grammaire ETF.

    Pour respecter la précédence des opérateurs en C, nous avons donc créé une règle pour chaque opérateur, avec une priorité différente, et nous avons déroulé les différentes règles à partir de l'opérateur avec la plus haute précédence (OR), jusqu'à celui avec la précédence la plus basse (COMP).

    La nouvelle grammaire HCL qui a été transposée en Chevrotain se trouve en annexe du rapport~(voir annexe~\ref{New_HCL_Grammar}).

\subsection{Mise à jour des dépendances}

    \subsubsection{Nombre de vulnérabilités après la mise à jour}
        \begin{figure}[H]
           \centering
           \includegraphics[scale=1]{Images/update/after_audit.png}
           \caption{Nombre de vulnérabilités après la mise à jour}
        \end{figure}

        Nous pouvons constater avec l'image ci-dessus que le nombre de vulnérabilités à diminué. Nous sommes passés de~40 à~7.
        Cependant, il reste encore des vulnérabilités. Il faut donc vérifier manuellement celles-ci pour évaluer leur gravité et tenter de les corriger.
    
    \subsubsection{Mise à jour de Vue.js~2.7 vers Vue.js~3}
    
    Pour la mise à jour du code de Vue.js~2.7 vers Vue.js~3, nous avons du mettre à jour les fonctions de Vue.js~2.7 vers celle de Vue.js~3.
    Le changement majeur induit par ce changement de version fut la méthode de lancement de l'application~: 

    \begin{minted}{JavaScript}
    import Vue from 'vue'
    import App from './App.vue'
    
    Vue.config.productionTip = false

    // Allows calling app's methods in a convenient way: app.getY86Code()
    window.app = new Vue({
      render: h => h(App)
    }).$mount('#app').$children[0]
    \end{minted}

    qui est devenue~:
    
    \begin{minted}{JavaScript}
    import { createApp } from 'vue'
    import App from './App.vue'

    // Allows calling app's methods in a convenient way: app.getY86Code()
    window.app = createApp(App);
    window.app.config.productionTip = false
    window.app.mount('#app');
    \end{minted}
    
    \subsubsection{Mise à jour de TypeScript~3 vers TypeScript~4}

    Une grosse partie de la mise à jour ayant été réalisé par Corentin Mercier durant le stage de 2021, nous avons juste mis à jour Typescript~4 dans sa dernière version.

    \subsubsection{Problèmes rencontrés}

    Après la mise à jour des bibliothèques, un problème est survenu~: quand on cliquait sur le bouton ``Step'' le simulateur avançait de deux étapes au lieu d'une.
    Le problème n'a pas été clairement identifié. Il nous a semblé que cela était dû à l'ancien comportement de Vue.js, qui nécessitait d'envoyer un évènement de mise à jour au bouton quand on cliquait dessus, alors que sur Vue.js version~3 le @click déclenche l'évènement tout seul.

\subsection{Création du noyau pipe-liné en 32 bits}

    \subsubsection{Changement de noyau}

    La première étape pour ajouter le noyau pipe-liné a été d'ajouter un nouveau noyau à la liste des noyaux supportés par le simulateur web.
    
    Pour cela, nous avons créé un nouveau dossier \textit{./app/ts/model/kernel-pipe32bits} et avons copié tous les éléments du noyau séquentiel (\textit{./app/ts/model/kernel-seq32bits}) pour nous en servir comme base de départ.

    Une fois ajouté à la liste des noyaux disponibles, nous avons pu échanger entre les deux noyaux dans les paramètres du simulateur. Malheureusement, après plusieurs essais, il est apparu que l'implémentation initiale du changement de noyau ne fonctionnait pas correctement~: les instructions ainsi que le code HCL ne changeaient pas au changement de noyau et, lorsqu'il y avait deux noyaux, les paramètres ne fonctionnaient plus (langue, thème).

    Le comportement statique provenait des fonctions \textbf{currentThemeFile()}, \textbf{currentLanguage()} et \textbf{currentKernel()} dans \textit{App.vue}, déclarées comme \textbf{computed}, et qui ne changeaient donc jamais de valeur malgré les inputs sur la page web.

    Pour simplifier le code, et car nous ne comprenions pas pourquoi ces méthodes ne se mettaient pas à jour, nous avons déclaré ces fonctions comme des \textit{methods} classiques, en modifiant le \textit{watch}, résolvant ainsi le bogue qui empêchait de changer de noyau.

    \subsubsection{Nouveau contexte d'exécution}

    La deuxième étape fut de créer le nouveau contexte d'exécution. Comme dans le noyau séquentiel, le noyau pipe-liné conserve des valeurs dans des registres à chaque étage~; c'est le contexte d'exécution. En plus d'avoir plus de registres à afficher pour chaque étage, le noyau pipe-liné affiche l'état sortant et l'état entrant à chaque étage (ou \textit{latch}). Il fallait donc aussi revoir l'affichage de l'état du processeur pour qu'il ressemble à celui de la version Tcl/Tk.

    Dans le cas du noyau séquentiel, l'état des registres des étages est stocké dans une unique classe \textit{Context} qui est possédée par la classe \textit{Sim}. De plus, pour que les appels aux fonctions HCL fonctionnent, le contexte est passé à l'attribut \textit{ctx} de la classe \textit{HCL}, qui gère les appels aux fonctions définies dans le code HCL. C'est donc à cet attribut \textit{ctx} que les variables définies en HCL font référence, telles que PC~:

    \begin{minted}{javascript}
intsig pc                       'ctx.pc' 
    \end{minted}

    Dans le simulateur Tcl/Tk, chaque étage possède un équivalent de la classe \textit{Context}, et en possède en fait deux exemplaires~: un pour l'état entrant et l'autre pour l'état sortant.

    \vspace{5 pt}

    Pour conserver le fonctionnement de la classe \textit{HCL}, nous avons créé une classe \textit{PContext}, que nous avons mise dans l'attribut \textit{ctx}. Cette classe possède les mêmes structures de données que la version Tcl/Tk~: 5 objets de type \textit{Pipeline} pour chaque étage, qui possède chacun 2 objets de type \textit{Context} qui conserveront les registres de l'étage à l'état sortant et entrant.

    \vspace{5 pt}

    Concernant l'affichage, côté \textit{front end}, le fichier de vue qui gère l'état du CPU avait déjà été prévu pour l'affichage du noyau pipe-liné. Il ne restait plus qu'à envoyer les données correctement formatées côté contrôleur.
    Cela donne l'affichage suivant~:

    \begin{figure}[H]
        \centering
        \includegraphics[scale=0.5]{Images/Cpu_state_pipeline.png}
        \caption{Panneau de contrôle de l'état de la machine pipe-linée}
        \label{fig:état_machine}
    \end{figure}

    \subsubsection{Nouveau fichier HCL}

    Le fichier HCL, qui modélise le câblage du chemin de données du processeur, comme montré sur la figure~\ref{fig:archi}, est différent entre le noyau séquentiel et pipe-liné.

    Pour l'écrire, nous avons commencé à prendre comme base le fichier HCL présent dans le simulateur Tcl/Tk, qui décrit correctement l'architecture pipe-linée, en adaptant les variables à la structure du simulateur web.
    
    Puis, en avançant dans le développement du nouveau noyau, nous avons modifié le code HCL pour corriger certains comportements qui étaient différents de celui du simulateur Tcl/Tk. Cependant, le client nous a rapidement repris, en nous expliquant que modifier le code HCL revenait à modifier ou changer de processeur, ce qui serait une facilité bien trop grande et non attendue dans le développement d'un simulateur. Nous avons donc annulé nos changements, pour nous cantonner au code HCL du Tcl/Tk. Cependant, des extensions ont été permises sur le simulateur web, sur le noyau séquentiel, comme la fonction \textit{is\_bch} qui n'est pas présente dans le simulateur Tcl/Tk et qui permet de rajouter de nouvelles instructions qui causent un branchement (par exemple~: \textit{loop}).

    \subsubsection{Nouveaux étages}

    La partie la plus importante fut de modifier le fichier \textit{stages} contenant les fonctions qui représentent l'action de chacun des étages du processeur, à savoir~: \textit{fetch}, \textit{decode}, \textit{execute}, \textit{memory} et \textit{writeBack}.

    Comme cela avait été le cas lors du développement du noyau séquentiel, nous avons essentiellement retranscrit le fonctionnement des fonctions du simulateur Tcl/Tk dans nos propres fonctions.

    Le fonctionnement de chaque étage suit globalement ce schéma~:
    \begin{enumerate}
        \item On remplit le contexte \textit{next} de l'étage suivant en appelant les fonctions HCL correspondantes~;
        \item Ces fonctions HCL vont baser leur calcul sur les informations du contexte \textit{current} de l'étage courant~;
        \item On transmet, si nécessaire, d'autres informations, comme les exceptions et erreurs d'exécution, du contexte \textit{current} de l'étage courant vers le contexte \textit{next} de l'étage suivant~;
        \item On vérifie si un étage doit attendre (\textbf{stall}) ou propager une bulle (\textbf{bubble}), en appelant les fonctions HCL correspondantes~;
        \item On recopie le contexte \textit{next} de l'étage courant dans le contexte \textit{current} de l'étage courant. Si l'étage courant doit propager une bulle, alors on place un \textit{nop} à la place dans l'étage courant et, si il doit attendre, on ne fait rien.
    \end{enumerate}
    Il y a deux étages qui font légèrement exception à cela~: l'étage \textbf{fetch} doit lui-même calculer son contexte \textit{next}, car il n'y a personne avant lui~;
    l'étage \textbf{writeBack} doit simplement mettre à jour les registres, car il n'a pas d'étage qui le suit.

    \subsubsection{Performances du noyau pipe-liné}

    Pour calculer les performances, nous sommes partis de la méthode de calcul du simulateur Tcl/Tk, qui suit la logique suivante~: 
    \begin{enumerate}
        \item On attend que la première instruction parviennent jusqu'à l'étage \textit{Write Back}~;
        \item On incrémente le nombre d'instructions et de cycles effectués tant que l'étage \textit{Write Back} ne contient pas de bulle~;
        \item On calcule le nombre d'instructions par cycle (CPI).
    \end{enumerate}

    Cela donne l'implémentation suivante en JavaScript~:

    \begin{minted}{javascript}
/* Performance monitoring */
if (this.getContext().getCurrentWriteBack().simStatus != simStatus.BUBBLE) {
    this.starting_up = false;
    this.instructions++;
    this.cycles++;
} else {
    if (!this.starting_up)
        this.cycles++;
}

this.cpi = this.instructions > 0 ? this.cycles/this.instructions : 1.0
    \end{minted}

\subsection{Version serveur du simulateur (CLI)}

Comme indiqué dans l'analyse de l'existant, une version serveur du simulateur a été développée par les groupes précédents, qui permettait de retourner l'état de la machine à la fin d'une simulation. Le \textit{dump} retournait une liste des registres et des zones mémoire qui ont été modifiées durant l'exécution, l'état des drapeaux, le message résultant de l'exécution ainsi que le statut du processeur.

Nous nous sommes mis d'accord avec Mme Zanon-Boito, en charge de cette partie du projet côté client, afin de rajouter des fonctionnalités qui seront utilisées pour l'évaluation des codes étudiants via le module VPL de Moodle et pour l'accessibilité aux malvoyants.

Nous avons rajouté un argument \textit{nbStep} définissant le nombre de pas/instructions que l'on souhaite exécuter lors de la simulation. Il est initialisé par défaut à 10000, afin de pouvoir exécuter quasiment n'importe quel programme de grande taille, puis redéfini ensuite à sa valeur transmise dans la requête.

Le code compilé de l'exécution a également été rajouté. Il a été récupéré à partir du résultat de la compilation à travers la variable \textit{yasCompilationResult} puis traité par la fonction \textit{computeHighlightsAndTags} qui permet de le formater.

Pour introduire l'accessibilité pour malvoyants, il fallait tout d'abord rajouter l'état complet des registres et de la mémoire, qui seront utiles pour l'affichage de l'état de la machine. En raison du fait qu'une liste de registres était déjà accessible dans le fichier SimulatorController.ts, l'état complet des registres a pu simplement être implémenté en rajoutant la variable \texttt{All\_Registers} dans le contenu du \textit{dump}.

Le reste n'ayant pas pu être implémenté à temps, ce point sera développé dans la partie ``Problèmes non résolus''.

\subsection{Accessibilité pour malvoyants}

    \subsubsection{Problème principal}
    
    Le principal problème pour l'accessibilité des malvoyants est que les navigateurs adaptés n'utilisent pas la technologie JavaScript. Nous devons donc générer une page statique et l'envoyer au client.
    
    La solution proposée au client a été de faire une page statique en langage HTML, qui contiendrait un formulaire ainsi que le code compilé et le résultat de l'exécution du code.

    \subsubsection{Premier concept proposé}

        Dans notre premier concept, nous n'avons pas inclus le code Object (ou code compilé) mais le principe est le même. Notre client nous a demandé d'ajouter le code compilé pour le concept final.
        Nous pouvons voir à quoi ressemble le premier concept lorsqu'aucun code n'est compilé~:

        \begin{figure}[H]
           \centering
           \includegraphics[scale=0.23]{Images/accecibility/v1_no_code.png}
           \caption{Première version du mode malvoyant, sans code exécuté}
        \end{figure}

        Pour finir la démonstration, nous présentons le premier concept après que du code a été compilé~:

        \begin{figure}[H]
           \centering
           \includegraphics[scale=0.23]{Images/accecibility/v1_code_exec.png}
           \caption{Première version avec du code exécuté du mode malvoyant}
        \end{figure}
    
    \subsubsection{Dernier concept proposé}
        Pour la version définitive du mode malvoyant, nous avons ajouté une section code compilé comme demandé par le client. Cependant l'affichage de celui-ci ne se fait pas correctement sur la navigateur Lynx. Mais il se fait correctement sur le navigateur Firefox. 
        Cette proposition étant une preuve de concept et par manque de temps, nous avons préféré le laisser en l'état.
        Sur la première version, étaient présentes des chaînes "JSON" qui rendaient l'aspect assez moche, nous les avons donc transformés en jolis tableaux.
        
        Voici à quoi ressemble la version finale du mode malvoyant sans code exécuté~:
        
        \begin{figure}[H]
           \centering
           \includegraphics[scale=0.25]{Images/accecibility/v2_no_code.png}
           \caption{Première version du mode malvoyant sans code exécuté}
        \end{figure}

        Pour finir la démonstration, nous présentons le dernier concept après que du code a été compilé~:
        
        \begin{figure}[H]
           \centering
           \includegraphics[scale=0.25]{Images/accecibility/v2_with_code.png}
           \caption{Dernière version du mode malvoyant avec code exécuté}
        \end{figure}