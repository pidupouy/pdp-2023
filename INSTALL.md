# Building the project

To see how to build the project, please refer to the **README.md** file in the *app/* subdirectory.

# Installing the project

## Quick install at CREMI

Example using the account 'aesnard'.

```bash
git clone https://gitub.u-bordeaux.fr/fpellegr/y86js_v2.git y86
cd y86/app
npm install
npm run build
cp -rf dist /net/cremi/auesnard/espaces/www/y86
```

Then visit: https://aurelien-esnard.emi.u-bordeaux.fr/y86/

## Export on https://y86.emi.u-bordeaux.fr/

It requires root access to the VM `v0-y86` at CREMI.

```bash
cd app
scp -r dist/* y86:/var/www/y86/
```

with .ssh/config :

```
host y86
HostName v0-y86     
user root
ProxyJump cremi
```
